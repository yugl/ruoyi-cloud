package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysFile;
import com.ruoyi.system.api.factory.RemoteFileFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 文件服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteFileService", value = ServiceNameConstants.FILE_SERVICE, fallbackFactory = RemoteFileFallbackFactory.class)
public interface RemoteFileService {
    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<SysFile> upload(@RequestPart(value = "file") MultipartFile file);

    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/uploadFiles", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<List<SysFile>> uploadFiles(@RequestPart(value = "files") MultipartFile[] file);

    /**
     * 上传文件
     *
     * @param content 内容
     * @return 结果
     */
    @PostMapping(value = "/qrCodeUrl")
    public R<String> qrCodeUrl(@RequestParam(value = "content") String content);

    /**
     * 上传文件base64编码
     *
     * @param content
     * @return
     */
    @PostMapping(value = "/upFileBase64")
    public R<SysFile> upFileBase64(@RequestParam(value = "content") String content);


    /**
     * 删除文件
     * @param filePath
     * @return
     */
    @PostMapping(value = "/deleteFile")
    public R<String> deleteFile(@RequestParam(value = "filePath") String filePath);
}
