package com.ruoyi.system.api.domain;

import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

/**
 * 文件信息
 *
 * @author ruoyi
 */
@Data
public class SysFile {
    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件地址
     */
    private String url;

    /**
     * 文件存储ID
     */
    private String fastdfsId;

    /**
     * 文件名称
     */
    @Excel(name = "文件名称")
    private String fileName;

    /**
     * 文件大小
     */
    @Excel(name = "文件大小")
    private Long fileSize;

    /**
     * 文件格式
     */
    @Excel(name = "文件格式")
    private String fileExt;

}
