package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.factory.RemoteInterfaceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 日志服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteInterfaceService", value = ServiceNameConstants.INTERFACE_SERVICE, fallbackFactory = RemoteInterfaceFallbackFactory.class)
public interface RemoteInterfaceService {

    /**
     * 只允许rest form调用外部接口
     *
     * @param action String 接口标识
     * @param header json 格式的参数
     * @param map
     * @return JSONString 返回结果
     */
    @RequestMapping(value = "/api/invoke/invokeRestForm", method = RequestMethod.POST)
    String invokeRest(@RequestParam("code") String code,
                      @RequestParam(value = "header", required = false) String header,
                      @RequestBody MultiValueMap<String, Object> map) throws RuntimeException;

    /**
     * 只允许rest调用外部接口
     *
     * @param action String 接口标识
     * @param header json 格式的参数 调用外部 restful接口使用
     * @param body   json 格式的参数 调用外部 restful接口使用
     * @return JSONString 返回结果
     */
    @RequestMapping(value = "/api/invoke/invokeRest", method = RequestMethod.POST, produces = "application/json")
    String invokeRest(@RequestParam("code") String code,
                      @RequestParam(value = "header", required = false) String header,
                      @RequestBody String body) throws Exception;

    /**
     * 调用外部 webservice接口
     *
     * @param code
     * @param queryXml xml 格式的参数
     * @return
     */
    @RequestMapping(value = "/api/invoke/invokeWs", method = RequestMethod.POST, produces = "application/json")
    String invokeWs(@RequestParam("code") String code,
                    @RequestBody String queryXml) throws Exception;


    /**
     * 只允许rest form调用外部接口
     *
     * @param action String 接口标识
     * @param header json 格式的参数
     * @param map
     * @return JSONString 返回结果
     */
    @RequestMapping(value = "/api/identity/auth", method = RequestMethod.POST)
    AjaxResult authIdentity(@RequestBody Map<String, String> map) throws RuntimeException;
}
