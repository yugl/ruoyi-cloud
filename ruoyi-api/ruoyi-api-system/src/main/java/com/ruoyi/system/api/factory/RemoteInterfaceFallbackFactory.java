package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.RemoteInterfaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.util.Map;

/**
 * 日志服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteInterfaceFallbackFactory implements FallbackFactory<RemoteInterfaceService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteInterfaceFallbackFactory.class);

    @Override
    public RemoteInterfaceService create(Throwable throwable) {
        log.error("接口服务调用失败:{}", throwable.getMessage());
        return new RemoteInterfaceService() {
            @Override
            public String invokeRest(String code, String header, MultiValueMap<String, Object> map) throws RuntimeException {
                throw new RuntimeException("接口服务失败");
            }

            @Override
            public String invokeRest(String code, String header, String body) throws Exception {
                throw new RuntimeException("接口服务失败");
            }

            @Override
            public String invokeWs(String code, String queryXml) throws Exception {
                throw new RuntimeException("接口服务失败");
            }

            @Override
            public AjaxResult authIdentity(Map<String, String> map) throws RuntimeException {
                throw new RuntimeException("身份认证接口失败");
            }
        };
    }
}
