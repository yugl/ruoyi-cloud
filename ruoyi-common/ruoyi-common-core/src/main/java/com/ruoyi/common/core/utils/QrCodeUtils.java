package com.ruoyi.common.core.utils;

import cn.hutool.extra.qrcode.QrCodeUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * 二维码
 */
public class QrCodeUtils {

    /**
     * 解析二维码
     *
     * @param url
     * @return
     */
    public static String decodeQrCode(String url) {
        String content = null;
        try {
            BufferedImage image = ImageIO.read(new URL(url));
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "png", os);
            InputStream input = new ByteArrayInputStream(os.toByteArray());
            content = QrCodeUtil.decode(input);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("解析二维码失败！");
        }
        return content;
    }
}
