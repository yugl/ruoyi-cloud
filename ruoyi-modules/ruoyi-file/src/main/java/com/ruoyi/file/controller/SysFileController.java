package com.ruoyi.file.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.file.service.impl.FastDfsSysFileServiceImpl;
import com.ruoyi.system.api.domain.SysFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件请求处理
 *
 * @author ruoyi
 */
@RestController
public class SysFileController {
    private static final Logger log = LoggerFactory.getLogger(SysFileController.class);

    private final FastDfsSysFileServiceImpl sysFileService;

    public SysFileController(FastDfsSysFileServiceImpl sysFileService) {
        this.sysFileService = sysFileService;
    }

    /**
     * 单个文件上传请求
     */
    @PostMapping("upFileBase64")
    public R<SysFile> upload(@RequestParam("content") String content) {
        try {
            // 上传并返回访问地址
            SysFile sysFile = sysFileService.upFileBase64(content);
            return R.ok(sysFile);
        } catch (Exception e) {
            log.error("上传文件失败", e);
            return R.fail(e.getMessage());
        }
    }

    /**
     * 单个文件上传请求
     */
    @PostMapping("upload")
    public R<SysFile> upload(MultipartFile file) {
        try {
            // 上传并返回访问地址
            SysFile sysFile = sysFileService.uploadFile(file);
            return R.ok(sysFile);
        } catch (Exception e) {
            log.error("上传文件失败", e);
            return R.fail(e.getMessage());
        }
    }

    /**
     * 多个文件上传请求
     */
    @PostMapping("uploadFiles")
    public R<List<SysFile>> uploadFiles(MultipartFile[] files) {
        try {
            // 上传并返回访问地址
            List<SysFile> list = sysFileService.uploadFiles(files);
            return R.ok(list);
        } catch (Exception e) {
            log.error("上传文件失败", e);
            return R.fail(e.getMessage());
        }
    }

    /**
     * 二维码
     */
    @PostMapping("qrCodeUrl")
    public R<String> qrCodeUrl(@RequestParam("content") String content) {
        try {
            String url = sysFileService.qrCodeUrl(content);
            return R.ok(url);
        } catch (Exception e) {
            log.error("上传文件失败", e);
            return R.fail(e.getMessage());
        }
    }


    /**
     * 删除文件
     */
    @PostMapping("deleteFile")
    public R<String> deleteFile(@RequestParam("filePath") String filePath) {
        try {
            sysFileService.deleteFile(filePath);
            return R.ok(filePath);
        } catch (Exception e) {
            log.error("删除文件失败", e);
            return R.fail(e.getMessage());
        }
    }


}