package com.ruoyi.file.service;

import com.ruoyi.system.api.domain.SysFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件上传接口
 *
 * @author ruoyi
 */
public interface ISysFileService {
    /**
     * 文件上传接口
     *
     * @param file 上传的文件
     * @return 访问地址
     * @throws Exception
     */
    public SysFile uploadFile(MultipartFile file) throws Exception;

    /**
     * 多个文件上传接口
     *
     * @param files 上传的文件
     * @return 访问地址
     * @throws Exception
     */
    public List<SysFile> uploadFiles(MultipartFile[] files) throws Exception;

    /**
     * 上传byte
     *
     * @param bytes
     * @return
     * @throws Exception
     */
    String qrCodeUrl(String content) throws Exception;
}
