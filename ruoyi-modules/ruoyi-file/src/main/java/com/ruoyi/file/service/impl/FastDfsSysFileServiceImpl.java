package com.ruoyi.file.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.ruoyi.common.core.utils.file.FileTypeUtils;
import com.ruoyi.common.core.utils.file.FileUtils;
import com.ruoyi.file.service.ISysFileService;
import com.ruoyi.file.utils.QRCodeUtils;
import com.ruoyi.system.api.domain.SysFile;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * FastDFS 文件存储
 *
 * @author ruoyi
 */
@Service
public class FastDfsSysFileServiceImpl implements ISysFileService {
    /**
     * 域名或本机访问地址
     */
    @Value("${fdfs.domain}")
    public String domain;

    @Value("${qrcode.height}")
    private Integer height;

    @Value("${qrcode.width}")
    private Integer width;

    @Autowired
    private FastFileStorageClient storageClient;

    /**
     * FastDfs文件上传接口
     *
     * @param file 上传的文件
     * @return 访问地址
     * @throws Exception
     */
    @Override
    public SysFile uploadFile(MultipartFile file) throws Exception {
        StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
        String url = domain + "/" + storePath.getFullPath();
        SysFile sysFile = new SysFile();
        sysFile.setName(FileUtils.getName(url));
        sysFile.setUrl(url);
        sysFile.setFastdfsId(FileUtils.getName(url).substring(0, FileUtils.getName(url).indexOf(".")));
        sysFile.setFileExt(FileTypeUtils.getFileType(file.getOriginalFilename()));
        sysFile.setFileSize(file.getSize());
        sysFile.setFileName(file.getOriginalFilename());
        return sysFile;
    }

    /**
     * FastDfs文件上传接口
     *
     * @param files 上传的文件
     * @return 访问地址
     * @throws Exception
     */
    @Override
    public List<SysFile> uploadFiles(MultipartFile[] files) throws Exception {
        List<SysFile> list = new ArrayList<>();
        for (MultipartFile file : files) {
            StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
            String url = domain + "/" + storePath.getFullPath();
            SysFile sysFile = new SysFile();
            sysFile.setName(FileUtils.getName(url));
            sysFile.setUrl(url);
            sysFile.setFastdfsId(FileUtils.getName(url).substring(0, FileUtils.getName(url).indexOf(".")));
            sysFile.setFileExt(FileTypeUtils.getFileType(file.getOriginalFilename()));
            sysFile.setFileSize(file.getSize());
            sysFile.setFileName(file.getOriginalFilename());
            list.add(sysFile);
        }
        return list;
    }


    /**
     * 生成二维码
     *
     * @param content
     * @return
     * @throws Exception
     */
    @Override
    public String qrCodeUrl(String content) throws Exception {
        QRCodeUtils qrCode = new QRCodeUtils(width, height);
        qrCode.setMargin(3);
        BufferedImage image = qrCode.getBufferedImage(content);
        //以流的方式将图片上传到fastdfs上：
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "png", outputStream);
        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        StorePath storePath = storageClient.uploadImageAndCrtThumbImage(inputStream, inputStream.available(), "png", null);
        return domain + "/" + storePath.getFullPath();
    }


    /**
     * 文件base64
     *
     * @param file
     * @return
     */
    public SysFile upFileBase64(String file) {
        //去除掉换行符 去掉空格
        file = file.split(",")[1].replaceAll("\n", "").trim();
        byte[] decode = Base64.decode(file);
        // 处理数据
        for (int i = 0; i < decode.length; ++i) {
            if (decode[i] < 0) {
                decode[i] += 256;
            }
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(decode);
        StorePath storePath = storageClient.uploadFile(stream, decode.length, "jpg", null);
        String url = domain + "/" + storePath.getFullPath();
        SysFile sysFile = new SysFile();
        sysFile.setName(FileUtils.getName(url));
        sysFile.setUrl(url);
        sysFile.setFastdfsId(FileUtils.getName(url).substring(0, FileUtils.getName(url).indexOf(".")));

        File tempFile = FileUtil.createTempFile();
        tempFile = Base64.decodeToFile(file, tempFile);

        return sysFile;
    }


    public void deleteFile(String filePath) throws Exception {
        storageClient.deleteFile(filePath);
    }
}
