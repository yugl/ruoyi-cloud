package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingSmsPhone;
import com.ruoyi.manage.dto.MeetingSmsPhoneDto;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.mapper.MeetingSmsPhoneMapper;
import com.ruoyi.manage.service.IMeetingSmsPhoneService;
import com.ruoyi.manage.utils.SmsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * 人工发送短信Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Service
public class MeetingSmsPhoneServiceImpl extends ServiceImpl<MeetingSmsPhoneMapper, MeetingSmsPhone> implements IMeetingSmsPhoneService {
    @Autowired
    private MeetingSmsPhoneMapper meetingSmsPhoneMapper;

    @Autowired
    SmsUtils smsUtils;

    /**
     * 查询人工发送短信列表
     *
     * @param meetingSmsPhone 人工发送短信
     * @return 人工发送短信
     */
    @Override
    public AjaxResult selectdList(MeetingSmsPhoneDto meetingSmsPhone) {
        QueryWrapper<MeetingSmsPhone> q = new QueryWrapper<>();
        if (meetingSmsPhone.getMeetingId() != null) {
            q.eq("meeting_id", meetingSmsPhone.getMeetingId());
        }
        q.orderByDesc("created_time");
        List<MeetingSmsPhone> list = this.list(q);
        return AjaxResult.success(list);
    }

    @Transactional
    @Override
    public AjaxResult importData(MultipartFile file, Long meetingId) {
        ExcelUtil<MeetingSmsPhone> util = new ExcelUtil<MeetingSmsPhone>(MeetingSmsPhone.class);
        List<MeetingSmsPhone> list = null;
        try {
            list = util.importExcel(file.getInputStream());
            if (list.size() > 0) {
                list.forEach(obj -> {
                    obj.setMeetingId(meetingId);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean result = this.remove(new QueryWrapper<MeetingSmsPhone>().eq("meeting_id", meetingId));
        this.saveBatch(list);
        return AjaxResult.success();
    }

    /**
     * 人工发送短信
     *
     * @param meetingId
     * @return
     */
    @Override
    public AjaxResult smsSend(Long meetingId, Long templateId) {
        List<MeetingSmsPhone> list = this.meetingSmsPhoneMapper.selectList(new QueryWrapper<MeetingSmsPhone>()
                .eq("meeting_id", meetingId)
                .eq("status", "0")
        );
        //发送短信
        ArrayList<String> result = new ArrayList<>();
        for (MeetingSmsPhone meetingSmsPhone : list) {
            SmsSendVo smsSendVo = new SmsSendVo();
            smsSendVo.setTemplateId(templateId);
            smsSendVo.setMeetingId(meetingSmsPhone.getMeetingId());
            smsSendVo.setName(meetingSmsPhone.getName());
            smsSendVo.setPhone(meetingSmsPhone.getPhone());
            smsSendVo.setType("2");
            AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
            if ((int) ajaxResult.get("code") != 200) {
                result.add(meetingSmsPhone.getPhone());
            }
        }
        if (result.size() > 0) {
            return AjaxResult.error(500, "以下手机号发送短信失败：" + String.join(",", result));
        }
        return AjaxResult.success();
    }

    /**
     * 自定义短信内容发送短信
     *
     * @param meetingId  会议ID
     * @param smsContent 自定义短信内容
     * @return
     */
    @Override
    public AjaxResult customSmsSend(Long meetingId, String smsContent) {
        List<MeetingSmsPhone> list = this.meetingSmsPhoneMapper.selectList(new QueryWrapper<MeetingSmsPhone>()
                .eq("meeting_id", meetingId)
                .eq("status", "0")
        );
        if (list.size() == 0) {
            return AjaxResult.error(500, "请导入发送短信手机号！");
        }
        //发送短信
        ArrayList<String> result = new ArrayList<>();
        for (MeetingSmsPhone meetingSmsPhone : list) {
            SmsSendVo smsSendVo = new SmsSendVo();
            smsSendVo.setMeetingId(meetingSmsPhone.getMeetingId());
            smsSendVo.setName(meetingSmsPhone.getName());
            smsSendVo.setPhone(meetingSmsPhone.getPhone());
            smsSendVo.setType("3");
            AjaxResult ajaxResult = smsUtils.smsSend(meetingSmsPhone.getPhone(), smsContent, smsSendVo);
            if ((int) ajaxResult.get("code") != 200) {
                result.add(meetingSmsPhone.getPhone());
            }
        }
        if (result.size() > 0) {
            return AjaxResult.error(500, "以下手机号发送短信失败：" + String.join(",", result));
        }
        return AjaxResult.success();

    }


}
