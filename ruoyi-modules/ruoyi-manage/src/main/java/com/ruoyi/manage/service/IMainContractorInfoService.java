package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MainContractorInfo;
import com.ruoyi.manage.dto.MainContractorInfoDto;

import java.util.List;

/**
 * 主承办基本信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMainContractorInfoService extends IService<MainContractorInfo> {


    /**
     * 查询主承办基本信息列表
     *
     * @param mainContractorInfo 主承办基本信息
     * @return 主承办基本信息集合
     */
    public List<MainContractorInfo> selectdList(MainContractorInfoDto mainContractorInfo);

    public String checkNameUnique(String companyName);

    /**
     * 通过公司名称精确查找会议信息
     * @param companyName
     * @return
     */
    List<MainContractorInfo> selectdListByCompanyName(String companyName);

    /**
     * 获取所有主承办公司名称
     * @return
     */
    List<String> getCompanyNames();

}
