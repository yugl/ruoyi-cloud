package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MainContractorInfo;
import com.ruoyi.manage.dto.MainContractorInfoDto;
import com.ruoyi.manage.mapper.MainContractorInfoMapper;
import com.ruoyi.manage.service.IMainContractorInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 主承办基本信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MainContractorInfoServiceImpl extends ServiceImpl<MainContractorInfoMapper, MainContractorInfo> implements IMainContractorInfoService {
    //@Autowired
    //private MainContractorInfoMapper mainContractorInfoMapper;

    /**
     * 查询主承办基本信息列表
     *
     * @param mainContractorInfo 主承办基本信息
     * @return 主承办基本信息
     */
    @Override
    public List<MainContractorInfo> selectdList(MainContractorInfoDto mainContractorInfo) {
        QueryWrapper<MainContractorInfo> q = new QueryWrapper<>();
        if (mainContractorInfo.getCompanyName() != null && !mainContractorInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", mainContractorInfo.getCompanyName());
        }
        if (mainContractorInfo.getTaxpayerIdentifierNum() != null && !mainContractorInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", mainContractorInfo.getTaxpayerIdentifierNum());
        }
        if (mainContractorInfo.getIndustry() != null && !mainContractorInfo.getIndustry().trim().equals("")) {
            q.eq("industry", mainContractorInfo.getIndustry());
        }
        if (mainContractorInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", mainContractorInfo.getIncorporationDate());
        }
        if (mainContractorInfo.getRegisteredCapital() != null && !mainContractorInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", mainContractorInfo.getRegisteredCapital());
        }
        if (mainContractorInfo.getChargePerson() != null && !mainContractorInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", mainContractorInfo.getChargePerson());
        }
        if (mainContractorInfo.getChargePersonPhone() != null && !mainContractorInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", mainContractorInfo.getChargePersonPhone());
        }
        if (mainContractorInfo.getPreventionPerson() != null && !mainContractorInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", mainContractorInfo.getPreventionPerson());
        }
        if (mainContractorInfo.getPreventionPersonPhone() != null && !mainContractorInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", mainContractorInfo.getPreventionPersonPhone());
        }
        if (mainContractorInfo.getAccountManager() != null && !mainContractorInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", mainContractorInfo.getAccountManager());
        }
        if (mainContractorInfo.getAccountManagerPhone() != null && !mainContractorInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", mainContractorInfo.getAccountManagerPhone());
        }
        if (mainContractorInfo.getAccountManagerUsername() != null && !mainContractorInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", mainContractorInfo.getAccountManagerUsername());
        }
        if (mainContractorInfo.getBusinessLicenseUrl() != null && !mainContractorInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", mainContractorInfo.getBusinessLicenseUrl());
        }
        if (mainContractorInfo.getBusinessNature() != null && !mainContractorInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", mainContractorInfo.getBusinessNature());
        }
        List<MainContractorInfo> list = this.list(q);
        return list;
    }


    /**
     * 校验主/承办公司名称是否唯一
     *
     * @param companyName
     * @return 结果
     */
    @Override
    public String checkNameUnique(String companyName) {
        QueryWrapper<MainContractorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MainContractorInfo::getCompanyName, companyName);
        List<MainContractorInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    @Override
    public List<MainContractorInfo> selectdListByCompanyName(String companyName) {
        QueryWrapper<MainContractorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MainContractorInfo::getCompanyName, companyName).eq(MainContractorInfo::getStatus, "0");
        List<MainContractorInfo> list = this.baseMapper.selectList(q);
        return list;
    }

    @Override
    public List<String> getCompanyNames() {
        QueryWrapper<MainContractorInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(MainContractorInfo::getCompanyName);
        List<MainContractorInfo> list = this.baseMapper.selectList(q);
        return list.stream().map(f -> f.getCompanyName()).collect(Collectors.toList());
    }


}
