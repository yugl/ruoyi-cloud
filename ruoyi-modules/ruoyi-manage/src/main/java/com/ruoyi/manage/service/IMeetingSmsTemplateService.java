package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingSmsTemplate;
import com.ruoyi.manage.dto.MeetingSmsTemplateDto;

import java.util.List;

/**
 * 会议短信模板Service接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface IMeetingSmsTemplateService extends IService<MeetingSmsTemplate> {


    /**
     * 查询会议短信模板列表
     *
     * @param meetingSmsTemplate 会议短信模板
     * @return 会议短信模板集合
     */
    public List<MeetingSmsTemplate> selectdList(Long meetingId);

    AjaxResult add(MeetingSmsTemplateDto dto);

    AjaxResult getInfo(Long id);

    AjaxResult findMeetingSmsSelect(Long meetingId);

    public String getSmsContent(Long meetingId, Long templateId);

    AjaxResult changeEnableStatus(MeetingSmsTemplateDto dto);
}
