package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AntiepidemicConfig;
import com.ruoyi.manage.dto.AntiepidemicConfigDto;

/**
 * 防疫填报模板Service接口
 *
 * @author ruoyi
 * @date 2022-04-12
 */
public interface IAntiepidemicConfigService extends IService<AntiepidemicConfig> {


    /**
     * 查询防疫填报模板列表
     *
     * @param antiepidemicConfig 防疫填报模板
     * @return 防疫填报模板集合
     */
    public List<AntiepidemicConfig> selectdList(AntiepidemicConfigDto antiepidemicConfig);

    AjaxResult findSelect();

    AjaxResult changeEnableStatus(AntiepidemicConfigDto dto);

    AjaxResult addOrUpdate(AntiepidemicConfig antiepidemicConfig);

    AjaxResult getInfo(Long id);

    AjaxResult remove(List<Long> ids);
}
