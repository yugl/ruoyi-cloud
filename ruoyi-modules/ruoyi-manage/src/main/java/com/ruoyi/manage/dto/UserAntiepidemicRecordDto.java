package com.ruoyi.manage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 员工防疫记录对象 user_antiepidemic_record
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class UserAntiepidemicRecordDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    @ApiModelProperty(name = "phone", value = "手机号")
    private String phone;

    private String meetingName;

    private Long meetingId;

    private Long id;

    private List<Map<String, String>> list;

    private List<String> phoneList;
}
