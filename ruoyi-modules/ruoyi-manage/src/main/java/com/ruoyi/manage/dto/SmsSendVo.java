package com.ruoyi.manage.dto;

import lombok.Data;

import java.util.Map;

/**
 * 短信发送接收参数封装
 */
@Data
public class SmsSendVo {

    //1：平台短信 2：会议使用 必填, 默认平台短信 3：自定义短信
    private String type = "1";
    //会议ID 如果是会议短信，会议id必填
    private Long meetingId;
    //模板ID  必填
    private Long templateId;
    //公司名称 根据自己功能可填可不填
    private String companyName;

    //封装的参数，可以使用map自己封装参数，也可以为null
    Map<String, Object> map;
    //姓名
    String name;
    //手机号
    String phone;
}
