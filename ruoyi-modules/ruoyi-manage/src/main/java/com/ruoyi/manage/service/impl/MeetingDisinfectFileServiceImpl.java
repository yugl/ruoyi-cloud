package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.manage.domain.MeetingDisinfectFile;
import com.ruoyi.manage.dto.MeetingDisinfectFileDto;
import com.ruoyi.manage.mapper.MeetingDisinfectFileMapper;
import com.ruoyi.manage.service.IMeetingDisinfectFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会议消毒记录文件Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-31
 */
@Service
public class MeetingDisinfectFileServiceImpl extends ServiceImpl<MeetingDisinfectFileMapper, MeetingDisinfectFile> implements IMeetingDisinfectFileService {
    @Autowired
    private MeetingDisinfectFileMapper meetingDisinfectFileMapper;

    /**
     * 查询会议消毒记录文件列表
     *
     * @param meetingDisinfectFile 会议消毒记录文件
     * @return 会议消毒记录文件
     */
    @Override
    public List<MeetingDisinfectFile> selectdList(MeetingDisinfectFileDto meetingDisinfectFile) {
        QueryWrapper<MeetingDisinfectFile> q = new QueryWrapper<>();
        if (meetingDisinfectFile.getDisinfectId() != null) {
            q.eq("disinfect_id", meetingDisinfectFile.getDisinfectId());
        }
        List<MeetingDisinfectFile> list = this.list(q);
        return list;
    }


}
