package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信发送记录 sms_send_record
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsSendRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议id
     */
    private Long meetingId;
    /**
     * 会议名称
     */
    @TableField(exist = false)
    private String meetingName;

    /**
     * 短信模板id
     */
    private Long smsTemplateId;
    /**
     * 短信模板名称
     */
    private String smsTemplateName;
    /**
     * 发送内容
     */
    private String content;

    private String phone;

    private String name;

    @TableField(fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    private String status;


}
