package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingTimeRuleConfig;
import com.ruoyi.manage.dto.MeetingTimeRuleConfigDto;
import com.ruoyi.manage.mapper.MeetingTimeRuleConfigMapper;
import com.ruoyi.manage.service.IMeetingTimeRuleConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 会议入场时间规则Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingTimeRuleConfigServiceImpl extends ServiceImpl<MeetingTimeRuleConfigMapper, MeetingTimeRuleConfig> implements IMeetingTimeRuleConfigService {
    @Autowired
    private MeetingTimeRuleConfigMapper meetingTimeRuleConfigMapper;

    /**
     * 查询会议入场时间规则列表
     *
     * @param meetingTimeRuleConfig 会议入场时间规则
     * @return 会议入场时间规则
     */
    @Override
    public List<MeetingTimeRuleConfig> selectdList(MeetingTimeRuleConfigDto meetingTimeRuleConfig) {
        QueryWrapper<MeetingTimeRuleConfig> q = new QueryWrapper<>();
        if (meetingTimeRuleConfig.getMeetingId() != null) {
            q.eq("meeting_id", meetingTimeRuleConfig.getMeetingId());
        }
        if (meetingTimeRuleConfig.getUserRole() != null && !meetingTimeRuleConfig.getUserRole().trim().equals("")) {
            q.eq("user_role", meetingTimeRuleConfig.getUserRole());
        }
        if (meetingTimeRuleConfig.getStartDate() != null) {
            q.eq("start_date", meetingTimeRuleConfig.getStartDate());
        }
        if (meetingTimeRuleConfig.getEndDate() != null) {
            q.eq("end_date", meetingTimeRuleConfig.getEndDate());
        }
        if (meetingTimeRuleConfig.getStartTime() != null) {
            q.eq("start_time", meetingTimeRuleConfig.getStartTime());
        }
        if (meetingTimeRuleConfig.getEndTime() != null) {
            q.eq("end_time", meetingTimeRuleConfig.getEndTime());
        }
        if (meetingTimeRuleConfig.getStatus() != null && !meetingTimeRuleConfig.getStatus().trim().equals("")) {
            q.eq("status", meetingTimeRuleConfig.getStatus());
        }
        if (meetingTimeRuleConfig.getCreatedBy() != null && !meetingTimeRuleConfig.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingTimeRuleConfig.getCreatedBy());
        }
        if (meetingTimeRuleConfig.getCreatedTime() != null) {
            q.eq("created_time", meetingTimeRuleConfig.getCreatedTime());
        }
        if (meetingTimeRuleConfig.getUpdatedBy() != null && !meetingTimeRuleConfig.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingTimeRuleConfig.getUpdatedBy());
        }
        if (meetingTimeRuleConfig.getUpdatedTime() != null) {
            q.eq("updated_time", meetingTimeRuleConfig.getUpdatedTime());
        }
        List<MeetingTimeRuleConfig> list = this.list(q);
        return list;
    }

    /**
     * 新增
     *
     * @param meetingTimeRuleConfig
     * @return
     */
    @Transactional
    @Override
    public AjaxResult add(MeetingTimeRuleConfig meetingTimeRuleConfig) {
        Long count = this.getBaseMapper().selectCount(new QueryWrapper<MeetingTimeRuleConfig>()
                .eq("meeting_id", meetingTimeRuleConfig.getMeetingId())
                .eq("status", "0")
                .eq("user_role", meetingTimeRuleConfig.getUserRole())
        );
        if (count != null && count > 0) {
            return AjaxResult.error(500, "数据已存在");
        }
        this.save(meetingTimeRuleConfig);
        return AjaxResult.success();
    }


}
