package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppQuestionnaireConfig;
import com.ruoyi.manage.domain.AppUserQuestionnaireInfo;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.AppUserQuestionnaireInfoDto;
import com.ruoyi.manage.mapper.AppQuestionnaireConfigMapper;
import com.ruoyi.manage.mapper.AppUserQuestionnaireInfoMapper;
import com.ruoyi.manage.service.IAppUserQuestionnaireInfoService;
import com.ruoyi.manage.service.IMeetingInfoService;
import com.ruoyi.manage.service.IMeetingUserHealthManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户问卷调查信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class AppUserQuestionnaireInfoServiceImpl extends ServiceImpl<AppUserQuestionnaireInfoMapper, AppUserQuestionnaireInfo> implements IAppUserQuestionnaireInfoService {
    @Autowired
    private AppUserQuestionnaireInfoMapper appUserQuestionnaireInfoMapper;
    @Autowired
    private IMeetingInfoService meetingInfoService;
    @Autowired
    private IMeetingUserHealthManageService meetingUserHealthManageService;
    @Autowired
    AppQuestionnaireConfigMapper appQuestionnaireConfigMapper;

    /**
     * 查询用户问卷调查信息列表
     *
     * @param appUserQuestionnaireInfo 用户问卷调查信息
     * @return 用户问卷调查信息
     */
    @Override
    public List<AppUserQuestionnaireInfo> selectdList(AppUserQuestionnaireInfoDto dto) {
        return appUserQuestionnaireInfoMapper.selectdList(dto);
    }

    /**
     * app填写调查问卷
     *
     * @param dto
     * @return
     */
    @Transactional
    @Override
    public AjaxResult addQuestionnaire(AppUserQuestionnaireInfoDto dto) {
        //校验用户该会议只能填写一次问卷
        /*Long count = appUserQuestionnaireInfoMapper.selectCount(new QueryWrapper<AppUserQuestionnaireInfo>()
                .eq("phone", dto.getPhone())
                .eq("meeting_id", dto.getMeetingId())
                .eq("questionnaire_id", dto.getQuestionnaireId())
        );
        if (count != null && count > 0) {
            return AjaxResult.error(500, "您已填写过问卷");
        }*/


        MeetingInfo meetingInfo = meetingInfoService.getBaseMapper().selectById(dto.getMeetingId());
        MeetingUserHealthManage userHealthManage = meetingUserHealthManageService.getBaseMapper().selectOne(new QueryWrapper<MeetingUserHealthManage>()
                .eq("phone", dto.getPhone())
                .eq("meeting_id", dto.getMeetingId())
                .eq("status", "0")
        );
        List<AppUserQuestionnaireInfo> objs = new ArrayList<>();
        List<AppUserQuestionnaireInfoDto.SubjectVo> list = dto.getList();
        for (AppUserQuestionnaireInfoDto.SubjectVo obj : list) {
            AppUserQuestionnaireInfo info = new AppUserQuestionnaireInfo();
            info.setMeetingId(dto.getMeetingId());
            info.setMeetingName(dto.getMeetingName());
            info.setName(userHealthManage.getName());
            info.setPhone(dto.getPhone());
            info.setSubject(obj.getSubject());
            info.setAnswer(String.join(",", obj.getAnswer()));
            info.setQuestionnaireId(dto.getQuestionnaireId());
            objs.add(info);
        }
        this.saveBatch(objs);
        return AjaxResult.success();
    }

    @Override
    public AjaxResult getInfo(Long id) {
        AppUserQuestionnaireInfo questionnaireInfo = this.getById(id);
        AppQuestionnaireConfig appQuestionnaireConfig = appQuestionnaireConfigMapper.selectById(questionnaireInfo.getQuestionnaireId());
        questionnaireInfo.setQuestionnaireName(appQuestionnaireConfig.getQuestionnaireName());
        return AjaxResult.success(questionnaireInfo);
    }


}
