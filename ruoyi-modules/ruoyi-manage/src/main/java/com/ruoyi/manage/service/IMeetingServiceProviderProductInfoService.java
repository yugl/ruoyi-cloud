package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingServiceProviderProductInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderProductInfoDto;

/**
 * 会议服务商产品信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingServiceProviderProductInfoService extends IService<MeetingServiceProviderProductInfo> {


    /**
     * 查询会议服务商产品信息列表
     *
     * @param meetingServiceProviderProductInfo 会议服务商产品信息
     * @return 会议服务商产品信息集合
     */
    public List<MeetingServiceProviderProductInfo> selectdList(MeetingServiceProviderProductInfoDto meetingServiceProviderProductInfo);

}
