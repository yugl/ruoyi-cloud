package com.ruoyi.manage.enumeration;

/**
 * 部门信息
 */
public enum DeptEnum {

    MAIN(201, "hostOrganizer"),
    UNDERTAKER(202, "undertakeOrganizer"),
    SERVICE_PROVIDER(200, "serviceProvider"),
    EXHIBITOR(203, "exhibitor"),
    VENUE(204, "venue");

    private Long name;
    private String index;

    // 构造方法
    private DeptEnum(long name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static Long getName(String index) {
        for (DeptEnum c : DeptEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public long getName() {
        return name;
    }

    public void setName(long name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
