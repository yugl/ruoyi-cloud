package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.MeetingExhibitorInfo;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IMeetingExhibitorInfoService;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingMainContractorInfo;
import com.ruoyi.manage.dto.MeetingMainContractorInfoDto;
import com.ruoyi.manage.service.IMeetingMainContractorInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 会议承办信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议承办信息控制器", tags = {"会议承办信息管理"})
@RestController
@RequestMapping("/meetingMainContractor")
public class MeetingMainContractorInfoController extends BaseController {
    @Autowired
    private IMeetingMainContractorInfoService meetingMainContractorInfoService;
    @Autowired
    private IMeetingExhibitorInfoService meetingExhibitorInfoService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    IRegisterUserService registerUserService;

    /**
     * 查询会议承办信息列表
     */
    @ApiOperation("查询会议承办信息列表")
    @RequiresPermissions("manage:meetingMainContractor:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingMainContractorInfoDto meetingMainContractorInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            meetingMainContractorInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }

        startPage();
        List<MeetingMainContractorInfo> list = meetingMainContractorInfoService.selectdList(meetingMainContractorInfo);
        return getDataTable(list);
    }

    /**
     * 导出会议承办信息列表
     */
    @ApiOperation("导出会议承办信息列表")
    @RequiresPermissions("manage:meetingMainContractor:export")
    @Log(title = "会议承办信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingMainContractorInfoDto meetingMainContractorInfo) throws IOException {
        startPage();
        List<MeetingMainContractorInfo> list = meetingMainContractorInfoService.selectdList(meetingMainContractorInfo);
        ExcelUtil<MeetingMainContractorInfo> util = new ExcelUtil<MeetingMainContractorInfo>(MeetingMainContractorInfo.class);
        util.exportExcel(response, list, "会议承办信息数据");
    }

    /**
     * 导入会议承办信息
     */
    @ApiOperation("导出会议承办信息列表")
    @RequiresPermissions("manage:meetingMainContractor:importData")
    @Log(title = "会议承办信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingMainContractorInfo> util = new ExcelUtil<MeetingMainContractorInfo>(MeetingMainContractorInfo.class);
        List<MeetingMainContractorInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingMainContractorInfoService.saveOrUpdateBatch(list);
        } else {
            message = meetingMainContractorInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议承办信息详细信息
     */
    @ApiOperation("获取会议承办信息详细信息")
    @RequiresPermissions("manage:meetingMainContractor:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingMainContractorInfoService.getById(id));
    }


    /**
     * 获取主办公司的所有展商
     */
    @ApiOperation("获取会议承办信息详细信息")
    @RequiresPermissions("manage:meetingMainContractor:query")
    @GetMapping(value = "/companyName/{companyName}/exhibitorInfo")
    public AjaxResult getExhibitorInfoByCompanyName(@ApiParam("主办公司名")
                              @NotNull(message = "主办公司名不能为空")
                              @PathVariable("companyName") String companyName) {
        List<Long> meetingIds = meetingMainContractorInfoService.selectMeetingIdsByCompanyName(companyName);
        List<MeetingExhibitorInfo> meetingExhibitorInfos = meetingExhibitorInfoService.selectListByMeetingIds(meetingIds);
        return AjaxResult.success(meetingExhibitorInfos);
    }

    /**
     * 新增会议承办信息
     */
    @ApiOperation("新增会议承办信息")
    @RequiresPermissions("manage:meetingMainContractor:add")
    @Log(title = "会议承办信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingMainContractorInfo meetingMainContractorInfo) {
        if (UserConstants.NOT_UNIQUE.equals(meetingMainContractorInfoService.checkNameUnique(meetingMainContractorInfo.getMeetingId(),meetingMainContractorInfo.getCompanyName()))) {
            return AjaxResult.error("新增会议主承办信息'" + meetingMainContractorInfo.getCompanyName() + "'失败，公司名称已存在!");
        }
        meetingMainContractorInfo.setCreatedTime(DateUtils.getNowDate());
        meetingMainContractorInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(meetingMainContractorInfo.getChargePersonPhone(), meetingMainContractorInfo.getChargePerson(), meetingMainContractorInfo.getCompanyName(), PersonRole.MAIN.getIndex());
        return toAjax(meetingMainContractorInfoService.save(meetingMainContractorInfo));
    }

    /**
     * 修改会议承办信息
     */
    @ApiOperation("修改会议承办信息")
    @RequiresPermissions("manage:meetingMainContractor:edit")
    @Log(title = "会议承办信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingMainContractorInfo meetingMainContractorInfo) {
        return toAjax(meetingMainContractorInfoService.updateById(meetingMainContractorInfo));
    }

    /**
     * 删除会议承办信息
     */
    @ApiOperation("删除会议承办信息")
    @RequiresPermissions("manage:meetingMainContractor:remove")
    @Log(title = "会议承办信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingMainContractorInfoService.removeByIds(ids));
    }
}
