package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.VenueBoothInfo;
import com.ruoyi.manage.dto.VenueBoothInfoDto;
import com.ruoyi.manage.mapper.VenueBoothInfoMapper;
import com.ruoyi.manage.service.IVenueBoothInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 场馆展位信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class VenueBoothInfoServiceImpl extends ServiceImpl<VenueBoothInfoMapper, VenueBoothInfo> implements IVenueBoothInfoService {
    //@Autowired
    //private VenueBoothInfoMapper venueBoothInfoMapper;

    /**
     * 查询场馆展位信息列表
     *
     * @param venueBoothInfo 场馆展位信息
     * @return 场馆展位信息
     */
    @Override
    public List<VenueBoothInfo> selectdList(VenueBoothInfoDto venueBoothInfo) {
        QueryWrapper<VenueBoothInfo> q = new QueryWrapper<>();
        if (venueBoothInfo.getVenueId() != null) {
            q.eq("venue_id", venueBoothInfo.getVenueId());
        }
        if (venueBoothInfo.getBoothName() != null && !venueBoothInfo.getBoothName().trim().equals("")) {
            q.like("booth_name", venueBoothInfo.getBoothName());
        }
        if (venueBoothInfo.getBoothOrderNum() != null) {
            q.eq("booth_order_num", venueBoothInfo.getBoothOrderNum());
        }
        if (venueBoothInfo.getBoothArea() != null) {
            q.eq("booth_area", venueBoothInfo.getBoothArea());
        }
        if (venueBoothInfo.getExhibitorName() != null && !venueBoothInfo.getExhibitorName().trim().equals("")) {
            q.like("exhibitor_name", venueBoothInfo.getExhibitorName());
        }
        if (venueBoothInfo.getChargePerson() != null && !venueBoothInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", venueBoothInfo.getChargePerson());
        }
        if (venueBoothInfo.getChargePersonPhone() != null && !venueBoothInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", venueBoothInfo.getChargePersonPhone());
        }
        if (venueBoothInfo.getPreventionPerson() != null && !venueBoothInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", venueBoothInfo.getPreventionPerson());
        }
        if (venueBoothInfo.getPreventionPersonPhone() != null && !venueBoothInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", venueBoothInfo.getPreventionPersonPhone());
        }
        List<VenueBoothInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(Long venueId, String venueBoothName) {
        QueryWrapper<VenueBoothInfo> q = new QueryWrapper<>();
        q.lambda().eq(VenueBoothInfo::getBoothName, venueBoothName);
        q.lambda().eq(VenueBoothInfo::getVenueId, venueId);
        List<VenueBoothInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 如果 venueBoothName为null，查询指定场馆的所有展馆
     *
     * @param venueId
     * @param venueBoothName
     * @return
     */
    @Override
    public List<VenueBoothInfo> selectdListByVenueIdAndVenueBoothName(Long venueId, String venueBoothName) {
        QueryWrapper<VenueBoothInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(VenueBoothInfo::getBoothName);
        if (venueBoothName != null)
            q.lambda().eq(VenueBoothInfo::getBoothName, venueBoothName);

        if (venueId != null)
            q.lambda().eq(VenueBoothInfo::getVenueId, venueId);

        List<VenueBoothInfo> list = this.baseMapper.selectList(q);
        return list;
    }


}
