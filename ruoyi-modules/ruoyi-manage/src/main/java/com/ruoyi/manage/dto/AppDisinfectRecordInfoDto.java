package com.ruoyi.manage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 会议消毒记录对象 meeting_disinfect_record_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class AppDisinfectRecordInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    @ApiModelProperty(name = "meetingId", value = "会议ID")
    private Long meetingId;
    /**
     * 会议名称
     */
    @NotNull(message = "会议名称不能为空")
    @ApiModelProperty(name = "meetingName", value = "会议名称")
    private String meetingName;
    /**
     * 电话
     */
    @NotNull(message = "手机号不能为空")
    @ApiModelProperty(name = "phone", value = "手机号")
    private String phone;
    /**
     * 消毒位置
     */
    @ApiModelProperty(name = "disinfectAddress", value = "消毒位置")
    private String disinfectAddress;

    /**
     *
     */
    @NotNull(message = "消毒文件不能为空")
    @ApiModelProperty(name = "file", value = "消毒文件")
    private List<String> fileList;
}
