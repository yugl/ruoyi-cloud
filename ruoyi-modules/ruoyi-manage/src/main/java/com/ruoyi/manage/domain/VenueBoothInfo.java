package com.ruoyi.manage.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 场馆展位信息对象 venue_booth_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class VenueBoothInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 场馆ID
     */
    @Excel(name = "场馆ID")
    private Long venueId;

    /**
     * 展馆名称
     */
    @Excel(name = "展馆名称")
    private String boothName;

    /**
     * 展厅/展位号
     */
    @Excel(name = "展位号")
    private String boothOrderNum;

    /**
     * 面积
     */
    @Excel(name = "面积")
    private Long boothArea;

    /**
     * 参展商
     */
    @Excel(name = "参展商")
    private String exhibitorName;

    /**
     * 责任人
     */
    @Excel(name = "责任人")
    private String chargePerson;

    /**
     * 责任人联系电话
     */
    @Excel(name = "责任人联系电话")
    private String chargePersonPhone;

    /**
     * 防疫负责人
     */
    @Excel(name = "防疫负责人")
    private String preventionPerson;

    /**
     * 防疫负责人联系电话
     */
    @Excel(name = "防疫负责人联系电话")
    private String preventionPersonPhone;

    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


}
