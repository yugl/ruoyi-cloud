package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppQuestionnaireConfig;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.dto.AppQuestionnaireConfigDto;
import com.ruoyi.manage.mapper.AppQuestionnaireConfigMapper;
import com.ruoyi.manage.service.IAppQuestionnaireConfigService;
import com.ruoyi.manage.service.IAppSubjectInfoService;
import com.ruoyi.manage.service.IMeetingRuleConfigService;
import com.ruoyi.manage.vo.AppSubjectInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 问卷调查配置Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Service
public class AppQuestionnaireConfigServiceImpl extends ServiceImpl<AppQuestionnaireConfigMapper, AppQuestionnaireConfig> implements IAppQuestionnaireConfigService {
    @Autowired
    private AppQuestionnaireConfigMapper appQuestionnaireConfigMapper;

    @Autowired
    IMeetingRuleConfigService meetingRuleConfigService;

    @Autowired
    IAppSubjectInfoService subjectInfoService;

    /**
     * 查询问卷调查配置列表
     *
     * @param appQuestionnaireConfig 问卷调查配置
     * @return 问卷调查配置
     */
    @Override
    public List<AppQuestionnaireConfig> selectdList(AppQuestionnaireConfigDto appQuestionnaireConfig) {
        QueryWrapper<AppQuestionnaireConfig> q = new QueryWrapper<>();
        if (appQuestionnaireConfig.getQuestionnaireName() != null && !appQuestionnaireConfig.getQuestionnaireName().trim().equals("")) {
            q.like("questionnaire_name", appQuestionnaireConfig.getQuestionnaireName());
        }
        List<AppQuestionnaireConfig> list = this.list(q);
        return list;
    }

    /**
     * 移动端获取调查问卷
     *
     * @param meetingId
     * @return
     */
    @Override
    public AjaxResult getAppQuestionnaire(String meetingId) {
        MeetingRuleConfig meetingRuleConfig = meetingRuleConfigService.getBaseMapper().selectOne(
                new QueryWrapper<MeetingRuleConfig>()
                        .eq("meeting_id", meetingId)
        );
        if (meetingRuleConfig == null) {
            return AjaxResult.success();
        }
        AppQuestionnaireConfig appQuestionnaireConfig = this.getById(meetingRuleConfig.getQuestionnaireId());
        List<AppSubjectInfoVo> list = subjectInfoService.selectSubjectList(meetingRuleConfig.getQuestionnaireId());
        HashMap<String, Object> map = new HashMap<>();
        map.put("subjectList", list);
        map.put("id", appQuestionnaireConfig.getId());
        map.put("questionnaireName", appQuestionnaireConfig.getQuestionnaireName());
        map.put("isAudienceCommitment", meetingRuleConfig.getIsAudienceCommitment());
        map.put("audienceCommitment", meetingRuleConfig.getAudienceCommitment());
        return AjaxResult.success(map);
    }

    /**
     * 问卷下拉
     *
     * @return
     */
    @Override
    public AjaxResult selectList() {
        QueryWrapper<AppQuestionnaireConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id as value", "questionnaire_name as label");
        queryWrapper.eq("status", "0");
        List<Map<String, Object>> map = this.getBaseMapper().selectMaps(queryWrapper);
        return AjaxResult.success(map);
    }

    @Override
    public AjaxResult changeEnableStatus(AppQuestionnaireConfigDto dto) {
        AppQuestionnaireConfig config = this.getById(dto.getId());
        config.setEnableStatus(dto.getEnableStatus());
        boolean result = this.updateById(config);
        if (result) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error(500, "操作失败");
        }

    }


}
