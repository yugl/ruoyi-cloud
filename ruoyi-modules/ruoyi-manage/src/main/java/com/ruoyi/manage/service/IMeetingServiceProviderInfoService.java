package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingServiceProviderInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderInfoDto;

/**
 * 会议服务商信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingServiceProviderInfoService extends IService<MeetingServiceProviderInfo> {


    /**
     * 查询会议服务商信息列表
     *
     * @param meetingServiceProviderInfo 会议服务商信息
     * @return 会议服务商信息集合
     */
    public List<MeetingServiceProviderInfo> selectdList(MeetingServiceProviderInfoDto meetingServiceProviderInfo);

    /**
     * 通过公司名称精确查找会议ids
     * @param companyName
     * @return
     */
    List<Long> selectMeetingIdsByCompanyName(String companyName);

    public String checkNameUnique(Long meetingId,String companyName);

}
