package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.VenueInfo;
import com.ruoyi.manage.dto.VenueInfoDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.service.IVenueInfoService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * 场馆基本信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "场馆基本信息控制器", tags = {"场馆基本信息管理"})
@RestController
@RequestMapping("/venue")
public class VenueInfoController extends BaseController {
    @Autowired
    private IVenueInfoService venueInfoService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询场馆基本信息列表
     */
    @ApiOperation("查询场馆基本信息列表")
    @RequiresPermissions("manage:venue:list")
    @GetMapping("/list")
    public TableDataInfo list(VenueInfoDto venueInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            venueInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }
        startPage();
        List<VenueInfo> list = venueInfoService.selectdList(venueInfo);
        return getDataTable(list);
    }

    /**
     * 导出场馆基本信息列表
     */
    @ApiOperation("导出场馆基本信息列表")
    @RequiresPermissions("manage:venue:export")
    @Log(title = "场馆基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VenueInfoDto venueInfo) throws IOException {

        startPage();
        List<VenueInfo> list = venueInfoService.selectdList(venueInfo);
        ExcelUtil<VenueInfo> util = new ExcelUtil<VenueInfo>(VenueInfo.class);
        util.exportExcel(response, list, "场馆基本信息数据");
    }

    /**
     * 导入场馆基本信息
     */
    @ApiOperation("导出场馆基本信息列表")
    @RequiresPermissions("manage:venue:importData")
    @Log(title = "场馆基本信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<VenueInfo> util = new ExcelUtil<VenueInfo>(VenueInfo.class);
        List<VenueInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = venueInfoService.saveOrUpdateBatch(list);
        } else {
            message = venueInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取场馆基本信息详细信息
     */
    @ApiOperation("获取场馆基本信息详细信息")
    @RequiresPermissions("manage:venue:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("id") Long id) {
        return AjaxResult.success(venueInfoService.getById(id));
    }

    /**
     * 通过公司名称获取场馆信息
     */
    @ApiOperation("通过公司名称获取场馆信息")
    @RequiresPermissions("manage:venue:query")
    @GetMapping(value = "/companyName/{companyName}")
    public AjaxResult getInfoByCompanyName(@ApiParam("公司名称") @NotNull(message = "公司名称不能为空且精确匹配")
                                           @PathVariable("companyName") String companyName) {
        return AjaxResult.success(venueInfoService.selectdListByCompanyName(companyName));
    }

    /**
     * 通过场馆名称获取场馆基本信息
     */
    @ApiOperation("通过场馆名称获取场馆基本信息")
    @RequiresPermissions("manage:venue:query")
    @GetMapping(value = "/venueName/{venueName}")
    public AjaxResult getInfoByVenueName(@ApiParam("场馆名称") @NotNull(message = "场馆名称精确匹配") @PathVariable("venueName") String venueName) {
        return AjaxResult.success(venueInfoService.selectdListByVenueName(venueName));
    }

    /**
     * 获取所有场馆信息
     */
    @ApiOperation("获取所有场馆信息")
    @RequiresPermissions("manage:venue:query")
    @GetMapping(value = "/venues")
    public AjaxResult getAll() {
        return AjaxResult.success(venueInfoService.selectdListByVenueName(null));
    }

    /**
     * 新增场馆基本信息
     */
    @ApiOperation("新增场馆基本信息")
    @RequiresPermissions("manage:venue:add")
    @Log(title = "场馆基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VenueInfo venueInfo) {
        if (UserConstants.NOT_UNIQUE.equals(venueInfoService.checkNameUnique(venueInfo.getVenueName()))) {
            return AjaxResult.error("新增场馆信息'" + venueInfo.getVenueName() + "'失败，场馆名称已存在");
        }
        venueInfo.setCreatedTime(DateUtils.getNowDate());
        venueInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(venueInfo.getChargePersonPhone(), venueInfo.getChargePerson(), venueInfo.getCompanyName(), PersonRole.VENUE.getIndex());
        return toAjax(venueInfoService.save(venueInfo));
    }

    /**
     * 修改场馆基本信息
     */
    @ApiOperation("修改场馆基本信息")
    @RequiresPermissions("manage:venue:edit")
    @Log(title = "场馆基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VenueInfo venueInfo) {
        return toAjax(venueInfoService.updateById(venueInfo));
    }

    /**
     * 删除场馆基本信息
     */
    @ApiOperation("删除场馆基本信息")
    @RequiresPermissions("manage:venue:remove")
    @Log(title = "场馆基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(venueInfoService.removeByIds(ids));
    }
}
