package com.ruoyi.manage.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * app报名信息对象 app_user_enroll_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class AppUserEnrollInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phone;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 报名时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "报名时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date enrollTime;

    /**
     * 二维码URL
     */
    @Excel(name = "二维码URL")
    private String qrCodeUrl;

    /**
     * 二维码使用状态 0：未使用 1：已使用
     */
    @Excel(name = "二维码使用状态 0：未使用 1：已使用")
    private String qrCodeStatus;

    /**
     * 报名来源 1：微信小程序
     * 2：支付宝小程序
     * 3：H5
     */
    @Excel(name = "报名来源 1：微信小程序 2：支付宝小程序 3：H5")
    private String enrollSource;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @Excel(name = "状态 0：有效1：无效")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


}
