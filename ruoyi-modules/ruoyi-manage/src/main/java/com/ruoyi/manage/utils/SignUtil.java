package com.ruoyi.manage.utils;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.web.domain.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RefreshScope
@Slf4j
@Component
public class SignUtil {

    @Value("${app.gzh.appid}")
    private String appid;

    @Value("${app.gzh.secret}")
    private String secret;


    public AjaxResult getResult(String url) {
        Map<String, String> signMap = null;
        try {
            signMap = sign(getTicket(), url);
            signMap.put("appId", appid);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(500, e.getMessage());
        }
        return AjaxResult.success(signMap);
    }

    private String getToken() {
        String accessToken = "";
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
        String resultString = HttpUtil.get(url);
        if (null != resultString && !"".equals(resultString)) {
            JSONObject jsonObject = JSONObject.parseObject(resultString);
            accessToken = jsonObject.getString("access_token");
        } else {
            throw new RuntimeException("获取Token失败");
        }
        return accessToken;
    }

    private String getTicket() {
        String ticket = "";
        String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + getToken() + "&type=jsapi";
        String resultString = HttpUtil.get(url);
        if (null != resultString && !"".equals(resultString)) {
            JSONObject jsonObject = JSONObject.parseObject(resultString);
            ticket = jsonObject.getString("ticket");
        } else {
            throw new RuntimeException("获取Ticket失败");
        }
        return ticket;
    }

    public static Map<String, String> sign(String jsapi_ticket, String url) {
        Map<String, String> ret = new HashMap<String, String>();
        String nonce_str = create_nonce_str();
        String timestamp = create_timestamp();
        String signature = "";
        //注意这里参数名必须全部小写，且必须有序
        String str = "jsapi_ticket=" + jsapi_ticket +
                "&noncestr=" + nonce_str +
                "&timestamp=" + timestamp +
                "&url=" + url;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(str.getBytes("UTF-8"));
            signature = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ret.put("url", url);
        ret.put("jsapi_ticket", jsapi_ticket);
        ret.put("nonceStr", nonce_str);
        ret.put("timestamp", timestamp);
        ret.put("signature", signature);
        return ret;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String create_nonce_str() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    private static String create_timestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }
}