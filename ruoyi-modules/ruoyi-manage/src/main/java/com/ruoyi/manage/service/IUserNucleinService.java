package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.UserNuclein;
import com.ruoyi.manage.dto.UserNucleinDto;

/**
 * 核酸检测Service接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface IUserNucleinService extends IService<UserNuclein> {


    /**
     * 查询核酸检测列表
     *
     * @param userNuclein 核酸检测
     * @return 核酸检测集合
     */
    public List<UserNuclein> selectdList(UserNucleinDto userNuclein);

}
