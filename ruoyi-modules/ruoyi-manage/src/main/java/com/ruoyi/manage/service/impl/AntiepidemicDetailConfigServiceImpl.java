package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;
import com.ruoyi.manage.dto.AntiepidemicDetailConfigDto;
import com.ruoyi.manage.mapper.AntiepidemicDetailConfigMapper;
import com.ruoyi.manage.service.IAntiepidemicDetailConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 防疫填报详情Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Service
public class AntiepidemicDetailConfigServiceImpl extends ServiceImpl<AntiepidemicDetailConfigMapper, AntiepidemicDetailConfig> implements IAntiepidemicDetailConfigService {
    @Autowired
    private AntiepidemicDetailConfigMapper antiepidemicDetailConfigMapper;

    /**
     * 查询防疫填报详情列表
     *
     * @param antiepidemicDetailConfig 防疫填报详情
     * @return 防疫填报详情
     */
    @Override
    public List<AntiepidemicDetailConfig> selectdList(AntiepidemicDetailConfigDto antiepidemicDetailConfig) {
        QueryWrapper<AntiepidemicDetailConfig> q = new QueryWrapper<>();
        if (antiepidemicDetailConfig.getAntiepidemicId() != null) {
            q.eq("antiepidemic_id", antiepidemicDetailConfig.getAntiepidemicId());
        }
        if (antiepidemicDetailConfig.getName() != null && !antiepidemicDetailConfig.getName().trim().equals("")) {
            q.like("name", antiepidemicDetailConfig.getName());
        }
        q.orderByAsc("sort_no");
        List<AntiepidemicDetailConfig> list = this.list(q);
        return list;
    }


}
