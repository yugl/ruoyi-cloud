package com.ruoyi.manage.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.UserVaccinesMapper;
import com.ruoyi.manage.domain.UserVaccines;
import com.ruoyi.manage.dto.UserVaccinesDto;
import com.ruoyi.manage.service.IUserVaccinesService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 用户疫苗记录Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Service
public class UserVaccinesServiceImpl extends ServiceImpl<UserVaccinesMapper, UserVaccines> implements IUserVaccinesService {
    @Autowired
    private UserVaccinesMapper userVaccinesMapper;

    /**
     * 查询用户疫苗记录列表
     *
     * @param userVaccines 用户疫苗记录
     * @return 用户疫苗记录
     */
    @Override
    public List<UserVaccines> selectdList(UserVaccinesDto userVaccines) {
        QueryWrapper<UserVaccines> q = new QueryWrapper<>();
                    if (userVaccines.getPhone() != null   && !userVaccines.getPhone().trim().equals("")){
                        q.eq("phone",userVaccines.getPhone());
                }
                    if (userVaccines.getInoculateOrg() != null   && !userVaccines.getInoculateOrg().trim().equals("")){
                        q.eq("inoculate_org",userVaccines.getInoculateOrg());
                }
                    if (userVaccines.getInoculateTime() != null  ){
                        q.eq("inoculate_time",userVaccines.getInoculateTime());
                }
                    if (userVaccines.getCreatedTime() != null  ){
                        q.eq("created_time",userVaccines.getCreatedTime());
                }
        List<UserVaccines> list = this.list(q);
        return list;
    }


}
