package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingVenueInfo;
import com.ruoyi.manage.dto.MeetingVenueInfoDto;
import com.ruoyi.manage.service.IMeetingVenueInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 会议场馆信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议场馆信息控制器", tags = {"会议场馆信息管理"})
@RestController
@RequestMapping("/meetingVenue")
public class MeetingVenueInfoController extends BaseController {
    @Autowired
    private IMeetingVenueInfoService meetingVenueInfoService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    IRegisterUserService registerUserService;

    /**
     * 查询会议场馆信息列表
     */
    @ApiOperation("查询会议场馆信息列表")
    @RequiresPermissions("manage:meetingVenue:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingVenueInfoDto meetingVenueInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            meetingVenueInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }

        startPage();
        List<MeetingVenueInfo> list = meetingVenueInfoService.selectdList(meetingVenueInfo);
        return getDataTable(list);
    }

    /**
     * 导出会议场馆信息列表
     */
    @ApiOperation("导出会议场馆信息列表")
    @RequiresPermissions("manage:meetingVenue:export")
    @Log(title = "会议场馆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingVenueInfoDto meetingVenueInfo) throws IOException {

        startPage();
        List<MeetingVenueInfo> list = meetingVenueInfoService.selectdList(meetingVenueInfo);
        ExcelUtil<MeetingVenueInfo> util = new ExcelUtil<MeetingVenueInfo>(MeetingVenueInfo.class);
        util.exportExcel(response, list, "会议场馆信息数据");
    }

    /**
     * 导入会议场馆信息
     */
    @ApiOperation("导出会议场馆信息列表")
    @RequiresPermissions("manage:meetingVenue:importData")
    @Log(title = "会议场馆信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingVenueInfo> util = new ExcelUtil<MeetingVenueInfo>(MeetingVenueInfo.class);
        List<MeetingVenueInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingVenueInfoService.saveOrUpdateBatch(list);
        } else {
            message = meetingVenueInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议场馆信息详细信息
     */
    @ApiOperation("获取会议场馆信息详细信息")
    @RequiresPermissions("manage:meetingVenue:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingVenueInfoService.getById(id));
    }

    /**
     * 新增会议场馆信息
     */
    @ApiOperation("新增会议场馆信息")
    @RequiresPermissions("manage:meetingVenue:add")
    @Log(title = "会议场馆信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingVenueInfo meetingVenueInfo) {
        if (UserConstants.NOT_UNIQUE.equals(meetingVenueInfoService.checkNameUnique(meetingVenueInfo.getMeetingId(),meetingVenueInfo.getCompanyName()))) {
            return AjaxResult.error("新增会议场馆信息'" + meetingVenueInfo.getCompanyName() + "'失败，公司名称已存在!");
        }
        meetingVenueInfo.setCreatedTime(DateUtils.getNowDate());
        meetingVenueInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(meetingVenueInfo.getChargePersonPhone(), meetingVenueInfo.getChargePerson(), meetingVenueInfo.getCompanyName(), PersonRole.VENUE.getIndex());
        return toAjax(meetingVenueInfoService.save(meetingVenueInfo));
    }

    /**
     * 修改会议场馆信息
     */
    @ApiOperation("修改会议场馆信息")
    @RequiresPermissions("manage:meetingVenue:edit")
    @Log(title = "会议场馆信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingVenueInfo meetingVenueInfo) {
        return toAjax(meetingVenueInfoService.updateById(meetingVenueInfo));
    }

    /**
     * 删除会议场馆信息
     */
    @ApiOperation("删除会议场馆信息")
    @RequiresPermissions("manage:meetingVenue:remove")
    @Log(title = "会议场馆信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingVenueInfoService.removeByIds(ids));
    }
}
