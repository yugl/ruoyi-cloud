package com.ruoyi.manage.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AntiepidemicConfig;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.domain.UserAntiepidemicRecord;
import com.ruoyi.manage.dto.UserAntiepidemicRecordDto;
import com.ruoyi.manage.mapper.AntiepidemicConfigMapper;
import com.ruoyi.manage.mapper.AntiepidemicDetailConfigMapper;
import com.ruoyi.manage.mapper.MeetingRuleConfigMapper;
import com.ruoyi.manage.mapper.UserAntiepidemicRecordMapper;
import com.ruoyi.manage.service.IUserAntiepidemicRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 员工防疫记录Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class UserAntiepidemicRecordServiceImpl extends ServiceImpl<UserAntiepidemicRecordMapper, UserAntiepidemicRecord> implements IUserAntiepidemicRecordService {
    @Autowired
    private UserAntiepidemicRecordMapper userAntiepidemicRecordMapper;
    @Autowired
    AntiepidemicConfigMapper antiepidemicConfigMapper;
    @Autowired
    AntiepidemicDetailConfigMapper antiepidemicDetailConfigMapper;
    @Autowired
    MeetingRuleConfigMapper meetingRuleConfigMapper;


    /**
     * 查询员工防疫记录列表
     *
     * @param userAntiepidemicRecord 员工防疫记录
     * @return 员工防疫记录
     */
    @Override
    public List<UserAntiepidemicRecord> selectdList(UserAntiepidemicRecordDto dto) {
        if (StringUtils.isNotEmpty(dto.getPhone())) {
            dto.setPhoneList(Arrays.asList(dto.getPhone().split("/")));
        }
        return userAntiepidemicRecordMapper.selectdList(dto);
    }

    /**
     * 防疫填报
     *
     * @param userAntiepidemicRecordDto
     * @return
     */
    @Override
    @Transactional
    public AjaxResult add(UserAntiepidemicRecordDto dto) {
        UserAntiepidemicRecord userAntiepidemicRecord = new UserAntiepidemicRecord();
        List<Map<String, String>> list = dto.getList();
        ArrayList<UserAntiepidemicRecord> saveList = new ArrayList<>();
        String uuid = IdUtil.fastSimpleUUID();
        for (Map<String, String> map : list) {
            UserAntiepidemicRecord bean = new UserAntiepidemicRecord();
            bean.setPhone(dto.getPhone());
            bean.setAntiepidemicId(dto.getId());
            bean.setMeetingId(dto.getMeetingId());
            bean.setAnswer(map.get("answer"));
            bean.setTitle(map.get("title"));
            bean.setIdentify(uuid);
            saveList.add(bean);
        }
        this.saveBatch(saveList);
        return AjaxResult.success();
    }

    @Override
    public UserAntiepidemicRecord getInfoByIdCard(String idCard) {
        QueryWrapper<UserAntiepidemicRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(UserAntiepidemicRecord::getIdCard, idCard);
        queryWrapper.lambda().orderByDesc(UserAntiepidemicRecord::getCreatedTime);
        List<UserAntiepidemicRecord> list = this.baseMapper.selectList(queryWrapper);
        if (list != null && list.size() > 0)
            return list.get(0);
        return null;
    }

    /**
     * 防疫填报列表
     *
     * @param meetingId
     * @return
     */
    @Override
    public AjaxResult findAntiepidemic(String meetingId) {
        MeetingRuleConfig meetingRuleConfig = meetingRuleConfigMapper.selectOne(new QueryWrapper<MeetingRuleConfig>().eq("meeting_id", meetingId));

        AntiepidemicConfig antiepidemicConfig = antiepidemicConfigMapper.selectById("1");
        if (antiepidemicConfig == null) {
            return AjaxResult.error(500, "会议没有绑定填报信息");
        }

        List<AntiepidemicDetailConfig> detailConfigs = antiepidemicDetailConfigMapper.selectList(new QueryWrapper<AntiepidemicDetailConfig>()
                .eq("status", "0")
                .eq("antiepidemic_id", antiepidemicConfig.getId())
                .orderByAsc("sort_no")
        );
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", antiepidemicConfig.getAntiepidemicName());
        map.put("id", antiepidemicConfig.getId());
        List<HashMap<String, Object>> collect = detailConfigs.stream().map(obj -> {
            HashMap<String, Object> map1 = new HashMap<>();
            map1.put("title", obj.getName());
            map1.put("type", obj.getType());
            return map1;
        }).collect(Collectors.toList());

        map.put("list", collect);
        return AjaxResult.success(map);
    }

    /**
     * 获取防疫
     *
     * @param phone
     * @return
     */
    @Override
    public List<Map<String, Object>> findAntiepidemicList(UserAntiepidemicRecordDto dto) {
        return userAntiepidemicRecordMapper.findAntiepidemicList(dto);
    }

    @Override
    public AjaxResult findAntiepidemicById(String id) {
        List<UserAntiepidemicRecord> list = this.getBaseMapper().selectList(new QueryWrapper<UserAntiepidemicRecord>()
                .eq("identify", id)
                .orderByAsc("id")
        );
        return AjaxResult.success(list);
    }

}
