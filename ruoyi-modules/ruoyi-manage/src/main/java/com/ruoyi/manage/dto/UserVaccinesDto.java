package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户疫苗记录对象 user_vaccines
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Data
public class UserVaccinesDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 接种机构
     */
    private String inoculateOrg;
    /**
     * 接种时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inoculateTime;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

}
