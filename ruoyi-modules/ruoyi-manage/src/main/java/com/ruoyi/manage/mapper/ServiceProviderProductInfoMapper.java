package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.ServiceProviderProductInfo;

/**
 * 服务商产品信息Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface ServiceProviderProductInfoMapper extends BaseMapper<ServiceProviderProductInfo> {


}
