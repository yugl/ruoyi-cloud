package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议消毒记录对象 meeting_disinfect_record_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MeetingDisinfectRecordInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 会议名称
     */
    @Excel(name = "会议名称")
    private String meetingName;
    /**
     * 电话
     */
    @Excel(name = "电话")
    private String phone;

    /**
     * 消毒位置
     */
    private String disinfectAddress;

    /**
     * 消毒时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "消毒时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date disinfectTime;

    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    @TableField(exist = false)
    private String meetingLogo;


}
