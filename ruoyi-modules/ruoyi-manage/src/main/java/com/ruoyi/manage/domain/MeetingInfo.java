package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议基本信息对象 meeting_info
 *
 * @author 于观礼
 * @date 2022-03-23
 */
@Data
public class MeetingInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议名称")
    private String meetingName;

    /**
     * 会议类别 1：展会 2：会议
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议类别 1：展会 2：会议")
    private String meetingCategory;

    /**
     * 会议类别名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议类别名称")
    private String meetingCategoryName;

    /**
     * 会议状态 1：待申请 2：审批中 3：审批完成 4：进行中 5：已完成
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议状态 1：待申请 2：审批中 3：审批完成 4：进行中 5：已完成")
    private String meetingStatus;

    /**
     * 会议状态名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议状态名称")
    private String meetingStatusName;

    /**
     * 防疫申报状态 1：未申报 2：申报中 3：申报通过 4：申报未通过
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "防疫申报状态 1：未申报 2：申报中 3：申报通过 4：申报未通过")
    private String preventionDeclareStatus;

    /**
     * 防疫申报状态名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "防疫申报状态名称")
    private String preventionDeclareStatusName;

    /**
     * 会议开始时间
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "会议开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date meetingStartTime;

    /**
     * 会议结束时间
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "会议结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date meetingEndTime;

    /**
     * 场馆ID
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "场馆ID")
    private Long venueId;

    /**
     * 场馆名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "场馆名称")
    private String venueName;

    /**
     * 主办单位
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "主办单位")
    private String mainOrg;

    /**
     * 会议地点
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议地点")
    private String meetingAddress;

    /**
     * 预计展商数量
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "预计展商数量")
    private Long expectExhibitorNum;

    /**
     * 预计观众数量
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "预计观众数量")
    private Long expectAudienceNum;

    /**
     * 会议logo
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议logo")
    private String meetingLogo;

    /**
     * 会议banner
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议banner")
    private String meetingBanner;

    /**
     * 会议介绍
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议介绍")
    private String meetingIntroduce;

    /**
     * 会议流程
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议流程")
    private String meetingFlow;

    /**
     * 审批状态 1：待审批 2：已审批 3：审批通过 4：审批不通过
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "审批状态 1：待审批 2：已审批 3：审批通过 4：审批不通过")
    private String approvalStatus;

    /**
     * 审批意见
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "审批意见")
    private String approvalOpinion;

    /**
     * 行政审批人
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "行政审批人")
    private String administrativeApprover;

    /**
     * 微信小程序二维码url
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "微信小程序二维码url")
    private String wxQrCodeUrl;

    /**
     * 支付宝小程序二维码url
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "支付宝小程序二维码url")
    private String zfbQrCodeUrl;

    /**
     * H5网页地址
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "H5网页地址")
    private String h5Url;

    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 会议防疫要求
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议防疫要求")
    private String meetingPreventionRequirements;

    /**
     * 客服联系人
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "客服联系人")
    private String customerServiceContact;

    /**
     * 客服联系人电话
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "客服联系人电话")
    private String customerServiceContactTel;

    /**
     * 客户经理
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "客户经理")
    private String customerManager;

    /**
     * 客户经理电话
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "客户经理电话")
    private String customerManagerTel;

    /**
     * 防疫负责人
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "防疫负责人")
    private String preventionOfficer;

    /**
     * 防疫负责人电话
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "防疫负责人电话")
    private String preventionOfficerTel;

    /**
     * 会议行业编码
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议行业编码")
    private String meetingIndustryCode;

    /**
     * 会议行业名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "会议行业名称")
    private String meetingIndustryName;

    /**
     * 观众承诺书 0：打开 1：关闭
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @TableField(exist = false)
    private String isAudienceCommitment;
    /**
     * 观众承诺书
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @TableField(exist = false)
    private String audienceCommitment;

    /**
     * 客户经理运营平台帐号
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Excel(name = "客户经理运营平台帐号")
    private String customerManagerUsername;

    @TableField(exist = false)
    private String isAlreadyEnroll;

}
