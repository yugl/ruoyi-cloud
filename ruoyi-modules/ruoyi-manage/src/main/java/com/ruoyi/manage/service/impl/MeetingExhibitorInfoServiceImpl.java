package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MeetingExhibitorInfo;
import com.ruoyi.manage.dto.MeetingExhibitorInfoDto;
import com.ruoyi.manage.mapper.MeetingExhibitorInfoMapper;
import com.ruoyi.manage.service.IMeetingExhibitorInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 会议展商信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingExhibitorInfoServiceImpl extends ServiceImpl<MeetingExhibitorInfoMapper, MeetingExhibitorInfo> implements IMeetingExhibitorInfoService {
    @Autowired
    private MeetingExhibitorInfoMapper meetingExhibitorInfoMapper;

    /**
     * 查询会议展商信息列表
     *
     * @param meetingExhibitorInfo 会议展商信息
     * @return 会议展商信息
     */
    @Override
    public List<MeetingExhibitorInfo> selectdList(MeetingExhibitorInfoDto meetingExhibitorInfo) {
        QueryWrapper<MeetingExhibitorInfo> q = new QueryWrapper<>();
        if (meetingExhibitorInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingExhibitorInfo.getMeetingId());
        }
        if (meetingExhibitorInfo.getCompanyName() != null && !meetingExhibitorInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", meetingExhibitorInfo.getCompanyName());
        }
        if (meetingExhibitorInfo.getTaxpayerIdentifierNum() != null && !meetingExhibitorInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", meetingExhibitorInfo.getTaxpayerIdentifierNum());
        }
        if (meetingExhibitorInfo.getIndustry() != null && !meetingExhibitorInfo.getIndustry().trim().equals("")) {
            q.eq("industry", meetingExhibitorInfo.getIndustry());
        }
        if (meetingExhibitorInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", meetingExhibitorInfo.getIncorporationDate());
        }
        if (meetingExhibitorInfo.getRegisteredCapital() != null && !meetingExhibitorInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", meetingExhibitorInfo.getRegisteredCapital());
        }
        if (meetingExhibitorInfo.getChargePerson() != null && !meetingExhibitorInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", meetingExhibitorInfo.getChargePerson());
        }
        if (meetingExhibitorInfo.getChargePersonPhone() != null && !meetingExhibitorInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", meetingExhibitorInfo.getChargePersonPhone());
        }
        if (meetingExhibitorInfo.getPreventionPerson() != null && !meetingExhibitorInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", meetingExhibitorInfo.getPreventionPerson());
        }
        if (meetingExhibitorInfo.getPreventionPersonPhone() != null && !meetingExhibitorInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", meetingExhibitorInfo.getPreventionPersonPhone());
        }
        if (meetingExhibitorInfo.getAccountManager() != null && !meetingExhibitorInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", meetingExhibitorInfo.getAccountManager());
        }
        if (meetingExhibitorInfo.getAccountManagerPhone() != null && !meetingExhibitorInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", meetingExhibitorInfo.getAccountManagerPhone());
        }
        if (meetingExhibitorInfo.getAccountManagerUsername() != null && !meetingExhibitorInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", meetingExhibitorInfo.getAccountManagerUsername());
        }
        if (meetingExhibitorInfo.getBusinessLicenseUrl() != null && !meetingExhibitorInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", meetingExhibitorInfo.getBusinessLicenseUrl());
        }
        if (meetingExhibitorInfo.getBusinessNature() != null && !meetingExhibitorInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", meetingExhibitorInfo.getBusinessNature());
        }
        if (meetingExhibitorInfo.getStatus() != null && !meetingExhibitorInfo.getStatus().trim().equals("")) {
            q.eq("status", meetingExhibitorInfo.getStatus());
        }
        if (meetingExhibitorInfo.getCreatedBy() != null && !meetingExhibitorInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingExhibitorInfo.getCreatedBy());
        }
        if (meetingExhibitorInfo.getCreatedTime() != null) {
            q.eq("created_time", meetingExhibitorInfo.getCreatedTime());
        }
        if (meetingExhibitorInfo.getUpdatedBy() != null && !meetingExhibitorInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingExhibitorInfo.getUpdatedBy());
        }
        if (meetingExhibitorInfo.getUpdatedTime() != null) {
            q.eq("updated_time", meetingExhibitorInfo.getUpdatedTime());
        }
        List<MeetingExhibitorInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<Long> selectMeetingIdsByCompanyName(String companyName) {
        QueryWrapper<MeetingExhibitorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingExhibitorInfo::getCompanyName, companyName);
        List<MeetingExhibitorInfo> list = this.baseMapper.selectList(q);
        return list.stream().map(item -> item.getMeetingId()).collect(Collectors.toList());
    }

    @Override
    public List<MeetingExhibitorInfo> selectListByMeetingIds(List<Long> meetingIds) {
        QueryWrapper<MeetingExhibitorInfo> q = new QueryWrapper<>();
        if(meetingIds != null && meetingIds.size()>0){
            q.lambda().in(MeetingExhibitorInfo::getMeetingId, meetingIds);
            return this.list(q);
        }else{
            return null;
        }
    }

    @Override
    public String checkNameUnique(Long meetingId, String companyName) {
        QueryWrapper<MeetingExhibitorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingExhibitorInfo::getCompanyName, companyName);
        q.lambda().eq(MeetingExhibitorInfo::getMeetingId, meetingId);
        List<MeetingExhibitorInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


}
