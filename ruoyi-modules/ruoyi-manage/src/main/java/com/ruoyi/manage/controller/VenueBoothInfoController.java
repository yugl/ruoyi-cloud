package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.VenueBoothInfo;
import com.ruoyi.manage.dto.VenueBoothInfoDto;
import com.ruoyi.manage.service.IVenueBoothInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 场馆展位信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "场馆展位信息控制器", tags = {"场馆展位信息管理"})
@RestController
@RequestMapping("/venueBooth")
public class VenueBoothInfoController extends BaseController {
    @Autowired
    private IVenueBoothInfoService venueBoothInfoService;

    /**
     * 查询场馆展位信息列表
     */
    @ApiOperation("查询场馆展位信息列表")
    @RequiresPermissions("manage:venueBooth:list")
    @GetMapping("/list")
    public TableDataInfo list(VenueBoothInfoDto venueBoothInfo) {
        startPage();
        List<VenueBoothInfo> list = venueBoothInfoService.selectdList(venueBoothInfo);
        return getDataTable(list);
    }

    /**
     * 导出场馆展位信息列表
     */
    @ApiOperation("导出场馆展位信息列表")
    @RequiresPermissions("manage:venueBooth:export")
    @Log(title = "场馆展位信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VenueBoothInfoDto venueBoothInfo) throws IOException {

        startPage();
        List<VenueBoothInfo> list = venueBoothInfoService.selectdList(venueBoothInfo);
        ExcelUtil<VenueBoothInfo> util = new ExcelUtil<VenueBoothInfo>(VenueBoothInfo.class);
        util.exportExcel(response, list, "场馆展位信息数据");
    }

    /**
     * 导入场馆展位信息
     */
    @ApiOperation("导入场馆展位信息列表")
    @RequiresPermissions("manage:venueBooth:importData")
    @Log(title = "场馆展位信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,Long venueId) throws Exception {
        ExcelUtil<VenueBoothInfo> util = new ExcelUtil<VenueBoothInfo>(VenueBoothInfo.class);
        List<VenueBoothInfo> list = util.importExcel(file.getInputStream());
        for(VenueBoothInfo vbi:list){
            vbi.setCreatedTime(DateUtils.getNowDate());
            vbi.setCreatedBy(SecurityUtils.getUsername());
            vbi.setVenueId(venueId);

            UpdateWrapper<VenueBoothInfo> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda().eq(VenueBoothInfo::getVenueId,venueId);
            updateWrapper.lambda().eq(VenueBoothInfo::getBoothName,vbi.getBoothName());
            updateWrapper.lambda().eq(VenueBoothInfo::getBoothOrderNum,vbi.getBoothOrderNum());
            venueBoothInfoService.saveOrUpdate(vbi,updateWrapper);
        }
        boolean message = false;
//        if (updateSupport) {
//            //message = venueBoothInfoService.saveOrUpdateBatch(list);
//            // 逐条保存或更新 VenueBoothInfo
//        } else {
//            message = venueBoothInfoService.saveBatch(list);
//        }
        return AjaxResult.success(message);
    }

    /**
     * 获取场馆展位信息详细信息
     */
    @ApiOperation("获取场馆展位信息详细信息")
    @RequiresPermissions("manage:venueBooth:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(venueBoothInfoService.getById(id));
    }


    /**
     * 通过场馆id和展馆名称获取展馆馆基本信息
     */
    @ApiOperation("通过场馆id和展馆名称获取展馆馆基本信息")
    @RequiresPermissions("manage:venueBooth:query")
    @GetMapping(value = "/venueId/{venueId}/venueBoothName/{venueBoothName}")
    public AjaxResult getInfoByVenueIdAndVenueBoothName(
            @ApiParam("场馆id") @NotNull(message = "场馆id") @PathVariable("venueId") Long venueId,
            @ApiParam("展馆名称") @NotNull(message = "展馆名称精确匹配") @PathVariable("venueBoothName") String venueBoothName) {
        return AjaxResult.success(venueBoothInfoService.selectdListByVenueIdAndVenueBoothName(venueId,venueBoothName));
    }


    /**
     * 获取指定场馆的所有展馆信息
     */
    @ApiOperation("获取指定场馆的所有展馆信息")
    @RequiresPermissions("manage:venueBooth:query")
    @GetMapping(value = "/venueId/{venueId}")
    public AjaxResult getAll(
            @ApiParam("场馆id") @NotNull(message = "场馆id") @PathVariable("venueId") Long venueId
    ) {
        return AjaxResult.success(venueBoothInfoService.selectdListByVenueIdAndVenueBoothName(venueId,null));
    }

    /**
     * 新增场馆展位信息
     */
    @ApiOperation("新增场馆展位信息")
    @RequiresPermissions("manage:venueBooth:add")
    @Log(title = "场馆展位信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VenueBoothInfo venueBoothInfo) {
        if (UserConstants.NOT_UNIQUE.equals(venueBoothInfoService.checkNameUnique(venueBoothInfo.getVenueId(),venueBoothInfo.getBoothName()))){
            return AjaxResult.error("新增展馆信息'" + venueBoothInfo.getBoothName() + "'失败，展馆名称已存在");
        }
        venueBoothInfo.setCreatedTime(DateUtils.getNowDate());
        venueBoothInfo.setCreatedBy(SecurityUtils.getUsername());
        return toAjax(venueBoothInfoService.save(venueBoothInfo));
    }

    /**
     * 修改场馆展位信息
     */
    @ApiOperation("修改场馆展位信息")
    @RequiresPermissions("manage:venueBooth:edit")
    @Log(title = "场馆展位信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VenueBoothInfo venueBoothInfo) {
        return toAjax(venueBoothInfoService.updateById(venueBoothInfo));
    }

    /**
     * 删除场馆展位信息
     */
    @ApiOperation("删除场馆展位信息")
    @RequiresPermissions("manage:venueBooth:remove")
    @Log(title = "场馆展位信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(venueBoothInfoService.removeByIds(ids));
    }
}
