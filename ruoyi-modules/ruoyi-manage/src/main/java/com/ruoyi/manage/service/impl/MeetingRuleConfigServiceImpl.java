package com.ruoyi.manage.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.*;
import com.ruoyi.manage.dto.MeetingRuleConfigDto;
import com.ruoyi.manage.mapper.*;
import com.ruoyi.manage.service.IMeetingRuleConfigService;
import com.ruoyi.manage.service.IMeetingTimeRuleConfigService;
import com.ruoyi.manage.service.ISmsSendRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 会议入场规则配置Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingRuleConfigServiceImpl extends ServiceImpl<MeetingRuleConfigMapper, MeetingRuleConfig> implements IMeetingRuleConfigService {
    @Autowired
    private MeetingRuleConfigMapper meetingRuleConfigMapper;
    @Autowired
    IMeetingTimeRuleConfigService meetingTimeRuleConfigService;
    @Autowired
    MeetingUserHealthManageMapper meetingUserHealthManageMapper;
    @Autowired
    ISmsSendRecordService smsSendRecordService;
    @Autowired
    UserNucleinMapper userNucleinMapper;
    @Autowired
    UserTravelCodeMapper userTravelCodeMapper;
    @Autowired
    UserVaccinesMapper userVaccinesMapper;

    /**
     * 短信管理列表
     *
     * @param meetingRuleConfig 会议入场规则配置
     * @return 短信管理列表
     */
    @Override
    public List<Map<String, Object>> selectdList(MeetingRuleConfigDto meetingRuleConfig) {
        List<Map<String, Object>> list = meetingRuleConfigMapper.selectSmsList(meetingRuleConfig);
        //计算已发送数量、剩余发送数量、发送比例
        for (Map<String, Object> map : list) {
            Long meetingId = Long.valueOf(map.get("meetingId").toString());
            Long smsTotalNum = Long.valueOf(map.get("smsTotalNum").toString());
            Long sendCount = smsSendRecordService.getBaseMapper().selectCount(new QueryWrapper<SmsSendRecord>().eq("meeting_id", meetingId));
            Long surplusSmsNum = smsTotalNum - (sendCount == null ? 0 : sendCount);
            String sendScale = sendCount == null ? "0%" : NumberUtil.round(NumberUtil.mul(NumberUtil.div(sendCount, smsTotalNum), 100), 0) + "%";
            map.put("sendSmsNum", sendCount);
            map.put("surplusSmsNum", surplusSmsNum);
            map.put("sendScale", sendScale);
        }
        return list;
    }

    /**
     * 用户入场校验规则
     *
     * @param idCard
     * @return
     */
    @Override
    public AjaxResult checkUserRule(Long meetingId, String idCard) {
        MeetingUserHealthManage meetingUserHealthManage = meetingUserHealthManageMapper.selectOne(new QueryWrapper<MeetingUserHealthManage>()
                .eq("id_card", idCard)
                .eq("status", "0")
                .eq("meeting_id", meetingId)
        );
        if (meetingUserHealthManage == null) {
            return AjaxResult.error(500, "您未报名该会议");
        }
        //校验时间规则
        MeetingTimeRuleConfig meetingTimeRuleConfig = meetingTimeRuleConfigService.getBaseMapper().selectOne(new QueryWrapper<MeetingTimeRuleConfig>()
                .eq("meeting_id", meetingId)
                .eq("user_role", meetingUserHealthManage.getUserRole())
                .eq("status", "0")
        );
        if (meetingTimeRuleConfig != null) {
            //判断进入时间范围
            DateTime startDateTime = DateUtil.parseDateTime(DateUtil.formatDate(meetingTimeRuleConfig.getStartDate()) + " " + DateUtil.formatTime(meetingTimeRuleConfig.getStartTime()));
            DateTime endDateTime = DateUtil.parseDateTime(DateUtil.formatDate(meetingTimeRuleConfig.getEndDate()) + " " + DateUtil.formatTime(meetingTimeRuleConfig.getEndTime()));
            boolean in = DateUtil.isIn(new Date(), startDateTime, endDateTime);
            if (!in) {
                return AjaxResult.error(500, "您不符合进入会场时间规则");
            }
        }
        MeetingRuleConfig meetingRuleConfig = getBaseMapper().selectOne(new QueryWrapper<MeetingRuleConfig>().eq("meeting_id", meetingId).eq("status", "0"));

        //行程码禁入规则 1：高风险地区 2：中风险地区 3：低风险地区
        String travelCodeRule = meetingRuleConfig.getTravelCodeRule();
        if (StringUtils.isNotEmpty(travelCodeRule)) {
            //计算距离最近14天的日期
            DateTime dateTime = DateUtil.offsetDay(new Date(), -14);
            Long count = userTravelCodeMapper.selectCount(new QueryWrapper<UserTravelCode>()
                    .eq("id_card", idCard)
                    .in("risk_level", Arrays.asList(travelCodeRule.split(",")))
                    .ge("travel_date", dateTime)
            );
            if (count > 0) {
                return AjaxResult.error(500, "您行程码不符合进入规则");
            }
        }
        //校验核酸规则（几小时以内）0代表不需要核酸
        Long nucleicAcidRule = meetingRuleConfig.getNucleicAcidRule();
        if (nucleicAcidRule != null) {
            DateTime dateTime = DateUtil.offsetHour(new Date(), -nucleicAcidRule.intValue());
            Long count = userNucleinMapper.selectCount(new QueryWrapper<UserNuclein>()
                    .eq("id_card", idCard)
                    .ge("test_time", dateTime)
            );
            if (count == 0) {
                return AjaxResult.error(500, "您未有" + nucleicAcidRule + "小时内的核酸");
            }
        }
        //校验疫苗规则（几次疫苗）0代表不需要疫苗
        Long vaccinesRule = meetingRuleConfig.getVaccinesRule();
        if (vaccinesRule != null) {
            Long count = userVaccinesMapper.selectCount(new QueryWrapper<UserVaccines>()
                    .eq("id_card", idCard)
            );
            if (count < vaccinesRule) {
                return AjaxResult.error(500, "您疫苗次数不符合进入会场");
            }
        }
        return AjaxResult.success();
    }

    @Override
    public MeetingRuleConfig getInfo(Long meetingId) {
        return getBaseMapper().selectOne(new QueryWrapper<MeetingRuleConfig>()
                .eq("meeting_id", meetingId)
                .eq("status", "0"));
    }

    @Override
    public AjaxResult add(MeetingRuleConfig meetingRuleConfig) {
        MeetingRuleConfig old = this.getBaseMapper().selectOne(new QueryWrapper<MeetingRuleConfig>()
                .eq("meeting_id", meetingRuleConfig.getMeetingId())
                .eq("status", "0")
        );
        if (old == null) {
            this.save(meetingRuleConfig);
        } else {
            BeanUtils.copyProperties(meetingRuleConfig, old, "id");
            this.saveOrUpdate(old);
        }
        return AjaxResult.success();
    }

}
