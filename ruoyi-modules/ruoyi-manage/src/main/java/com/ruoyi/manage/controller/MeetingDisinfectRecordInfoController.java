package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingDisinfectRecordInfo;
import com.ruoyi.manage.dto.MeetingDisinfectRecordInfoDto;
import com.ruoyi.manage.service.IMeetingDisinfectRecordInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 会议消毒记录Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议消毒记录控制器", tags = {"会议消毒记录管理"})
@RestController
@RequestMapping("/meetingDisinfectRecord")
public class MeetingDisinfectRecordInfoController extends BaseController {
    @Autowired
    private IMeetingDisinfectRecordInfoService meetingDisinfectRecordInfoService;

    /**
     * 查询会议消毒记录列表
     */
    @ApiOperation("查询会议消毒记录列表")
    @PostMapping("/list")
    public TableDataInfo list(@RequestBody MeetingDisinfectRecordInfoDto meetingDisinfectRecordInfo) {
        startPage();
        List<Map<String, Object>> list = meetingDisinfectRecordInfoService.selectdList(meetingDisinfectRecordInfo);
        return getDataTable(list);
    }

    /**
     * 导入会议消毒记录
     */
    @ApiOperation("导出会议消毒记录列表")
    @RequiresPermissions("manage:meetingDisinfectRecord:importData")
    @Log(title = "会议消毒记录导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingDisinfectRecordInfo> util = new ExcelUtil<MeetingDisinfectRecordInfo>(MeetingDisinfectRecordInfo.class);
        List<MeetingDisinfectRecordInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingDisinfectRecordInfoService.saveOrUpdateBatch(list);
        } else {
            message = meetingDisinfectRecordInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议消毒记录详细信息
     */
    @ApiOperation("获取会议消毒记录详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("id") Long id) {
        return AjaxResult.success(meetingDisinfectRecordInfoService.getById(id));
    }

    /**
     * 新增会议消毒记录
     */
    @ApiOperation("新增会议消毒记录")
    @Log(title = "会议消毒记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingDisinfectRecordInfo meetingDisinfectRecordInfo) {
        return toAjax(meetingDisinfectRecordInfoService.save(meetingDisinfectRecordInfo));
    }

    /**
     * 修改会议消毒记录
     */
    @ApiOperation("修改会议消毒记录")
    @Log(title = "会议消毒记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingDisinfectRecordInfo meetingDisinfectRecordInfo) {
        return toAjax(meetingDisinfectRecordInfoService.updateById(meetingDisinfectRecordInfo));
    }

    /**
     * 删除会议消毒记录
     */
    @ApiOperation("删除会议消毒记录")
    @Log(title = "会议消毒记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingDisinfectRecordInfoService.removeByIds(ids));
    }
}
