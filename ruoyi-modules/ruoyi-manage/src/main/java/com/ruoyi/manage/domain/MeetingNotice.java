package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 会议通知公告对象 meeting_notice
 *
 * @author ruoyi
 * @date 2022-04-01
 */
@Data
public class MeetingNotice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公告ID
     */
    private Long id;

    /**
     * 会议id
     */
    @Excel(name = "会议id")
    private Long meetingId;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 公告标题
     */
    @NotNull(message = "公告标题不能为空")
    @Excel(name = "公告标题")
    private String noticeTitle;

    /**
     * 公告内容
     */
    @NotNull(message = "公告内容不能为空")
    @Excel(name = "公告内容")
    private String noticeContent;

    /**
     * 有效开始时间
     */
    @NotNull(message = "有效开始时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 有效结束时间
     */
    @NotNull(message = "有效结束时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 公告状态（0待发布 1已发布 2已撤回 3删除）
     */
    @Excel(name = "公告状态", readConverterExp = "0=待发布,1=已发布,2=已撤回,3=删除")
    private String status;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建者")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新者")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

}
