package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppPrivacyAggreement;
import com.ruoyi.manage.dto.AppPrivacyAggreementDto;

import java.util.List;

/**
 * 隐私协议设置Service接口
 *
 * @author 于观礼
 * @date 2022-03-14
 */
public interface IAppPrivacyAggreementService extends IService<AppPrivacyAggreement> {


    /**
     * 查询隐私协议设置列表
     *
     * @param appPrivacyAggreement 隐私协议设置
     * @return 隐私协议设置集合
     */
    public List<AppPrivacyAggreement> selectdList(AppPrivacyAggreementDto appPrivacyAggreement);

    AjaxResult getInfo();
}
