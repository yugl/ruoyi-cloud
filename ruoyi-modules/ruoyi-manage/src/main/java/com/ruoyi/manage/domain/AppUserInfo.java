package com.ruoyi.manage.domain;

import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实名认证信息对象 app_user_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
@ApiModel("app用户实名认证信息")
public class AppUserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(name = "id", value = "主键")
    private Long id;

    /**
     * 姓名
     */
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;

    /**
     * openid
     */
    @ApiModelProperty(name = "openid", value = "openid")
    private String openid;

    /**
     * 手机号
     */
    @ApiModelProperty(name = "phone", value = "手机号")
    private String phone;

    /**
     * 身份证号
     */
    @ApiModelProperty(name = "idCard", value = "身份证号")
    private String idCard;

    /**
     * 公司名称
     */
    @ApiModelProperty(name = "companyName", value = "公司名称")
    private String companyName;

    /**
     * 采集头像url
     */
    @ApiModelProperty(name = "headPortraitUrl", value = "采集头像url")
    private String headPortraitUrl;

    /**
     * 数据来源
     * 1：微信小程序
     * 2：支付宝小程序
     * 3：H5
     */
    @ApiModelProperty(name = "dataSource", value = "1：微信小程序 2：支付宝小程序 3：H5")
    private String dataSource;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @TableLogic
    @ApiModelProperty(name = "status", value = "状态 0：有效 1：无效")
    private String status;

    /**
     * 创建人
     */
    @ApiModelProperty(name = "createdBy", value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(name = "createdTime", value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(name = "updatedBy", value = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(name = "updatedTime", value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 角色
     */
    @TableField(exist = false)
    private String roles;

    @TableField(exist = false)
    AlipaySystemOauthTokenResponse aliUserInfo;


}
