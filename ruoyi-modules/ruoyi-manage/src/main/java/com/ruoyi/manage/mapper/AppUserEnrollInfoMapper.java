package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppUserEnrollInfo;
import com.ruoyi.system.api.domain.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * app报名信息Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface AppUserEnrollInfoMapper extends BaseMapper<AppUserEnrollInfo> {


    @Select("select * from `ry-cloud`.sys_user where phonenumber=#{phone} and status='0' and del_flag='0' limit 1 ")
    List<SysUser> selectSysuser(@Param("phone") String phone);
}
