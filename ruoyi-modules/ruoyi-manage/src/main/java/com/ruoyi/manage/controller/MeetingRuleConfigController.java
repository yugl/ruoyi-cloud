package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.dto.MeetingRuleConfigDto;
import com.ruoyi.manage.service.IMeetingRuleConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 会议入场规则配置Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议入场规则配置控制器", tags = {"会议入场规则配置管理"})
@RestController
@RequestMapping("/meetingRuleConfig")
public class MeetingRuleConfigController extends BaseController {
    @Autowired
    private IMeetingRuleConfigService meetingRuleConfigService;

    /**
     * 短信管理列表
     */
    @ApiOperation("短信管理列表")
    @PostMapping("/list")
    public TableDataInfo list(MeetingRuleConfigDto meetingRuleConfig) {
        startPage();
        List<Map<String, Object>> list = meetingRuleConfigService.selectdList(meetingRuleConfig);
        return getDataTable(list);
    }

    /**
     * 导入会议入场规则配置
     */
    @ApiOperation("导出会议入场规则配置列表")
    @Log(title = "会议入场规则配置导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingRuleConfig> util = new ExcelUtil<MeetingRuleConfig>(MeetingRuleConfig.class);
        List<MeetingRuleConfig> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingRuleConfigService.saveOrUpdateBatch(list);
        } else {
            message = meetingRuleConfigService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议入场规则配置详细信息
     */
    @ApiOperation("获取会议入场规则配置详细信息")
    @GetMapping(value = "/{meetingId}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("meetingId") Long meetingId) {
        MeetingRuleConfig info = meetingRuleConfigService.getInfo(meetingId);
        if (info == null) {
            return AjaxResult.success(info);
        }
        String travelCodeRule = info.getTravelCodeRule();
        String entranceGuardRule = info.getEntranceGuardRule();
        List<Integer> collect = new ArrayList<Integer>();
        if (StringUtils.isNotEmpty(travelCodeRule)) {
            travelCodeRule = travelCodeRule.replaceAll("\\[", "").replaceAll("]", "");
            List<String> list = Arrays.asList(travelCodeRule.split(","));
            collect = list.stream().map(obj -> {
                return Integer.parseInt(obj);
            }).collect(Collectors.toList());
        }

        List<Integer> collect1 = new ArrayList<Integer>();
        if (StringUtils.isNotEmpty(entranceGuardRule)) {
            entranceGuardRule = entranceGuardRule.replaceAll("\\[", "").replaceAll("]", "");
            List<String> list = Arrays.asList(entranceGuardRule.split(","));
            collect1 = list.stream().map(obj -> {
                return Integer.parseInt(obj);
            }).collect(Collectors.toList());
        }
        info.setTravelCodeRuleArr(collect);
        info.setEntranceGuardRuleArr(collect1);
        return AjaxResult.success(info);
    }

    /**
     * 新增会议入场规则配置
     */
    @ApiOperation("新增会议入场规则配置")
    @Log(title = "会议入场规则配置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody MeetingRuleConfig meetingRuleConfig) {
        String travelCodeRule = meetingRuleConfig.getTravelCodeRule();
        String entranceGuardRule = meetingRuleConfig.getEntranceGuardRule();
        if (StringUtils.isNotEmpty(travelCodeRule)) {
            meetingRuleConfig.setTravelCodeRule(travelCodeRule.replaceAll("\"", ""));
        }
        if (StringUtils.isNotEmpty(entranceGuardRule)) {
            meetingRuleConfig.setEntranceGuardRule(entranceGuardRule.replaceAll("\"", ""));
        }
        return meetingRuleConfigService.add(meetingRuleConfig);
    }

    /**
     * 修改会议入场规则配置
     */
    @ApiOperation("修改会议入场规则配置")
    @Log(title = "会议入场规则配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingRuleConfig meetingRuleConfig) {
        String travelCodeRule = meetingRuleConfig.getTravelCodeRule();
        String entranceGuardRule = meetingRuleConfig.getEntranceGuardRule();
        if (StringUtils.isNotEmpty(travelCodeRule)) {
            meetingRuleConfig.setTravelCodeRule(travelCodeRule.replaceAll("\"", ""));
        }
        if (StringUtils.isNotEmpty(entranceGuardRule)) {
            meetingRuleConfig.setEntranceGuardRule(entranceGuardRule.replaceAll("\"", ""));
        }
        return toAjax(meetingRuleConfigService.updateById(meetingRuleConfig));
    }

    /**
     * 删除会议入场规则配置
     */
    @ApiOperation("删除会议入场规则配置")
    @Log(title = "会议入场规则配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingRuleConfigService.removeByIds(ids));
    }

    /**
     * 用户入场校验是否符合规则
     *
     * @return
     */
    @PostMapping("/checkUserRule")
    public AjaxResult checkUserRule(@RequestParam("meetingId") Long meetingId,
                                    @RequestParam("idCard") String idCard) {
        return meetingRuleConfigService.checkUserRule(meetingId, idCard);
    }
}
