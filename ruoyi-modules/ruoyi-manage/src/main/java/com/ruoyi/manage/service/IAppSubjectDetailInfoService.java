package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.AppSubjectDetailInfo;
import com.ruoyi.manage.dto.AppSubjectDetailInfoDto;

/**
 * 问卷题目详情Service接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface IAppSubjectDetailInfoService extends IService<AppSubjectDetailInfo> {


    /**
     * 查询问卷题目详情列表
     *
     * @param appSubjectDetailInfo 问卷题目详情
     * @return 问卷题目详情集合
     */
    public List<AppSubjectDetailInfo> selectdList(AppSubjectDetailInfoDto appSubjectDetailInfo);

}
