package com.ruoyi.manage.vo;

import lombok.Data;

@Data
public class AppSubjectDetailVo {

    private int sortNo;
    private String answer;
}
