package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.dto.MeetingInfoDto;
import com.ruoyi.manage.mapper.MeetingInfoMapper;
import com.ruoyi.manage.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 会议基本信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-23
 */
@Service
public class MeetingInfoServiceImpl extends ServiceImpl<MeetingInfoMapper, MeetingInfo> implements IMeetingInfoService {
    //@Autowired
    //private MeetingInfoMapper meetingInfoMapper;
    @Autowired
    IMeetingMainContractorInfoService iMeetingMainContractorInfoService;

    @Autowired
    IMeetingExhibitorInfoService iMeetingExhibitorInfoService;

    @Autowired
    IMeetingServiceProviderInfoService iMeetingServiceProviderInfoService;

    @Autowired
    IMeetingVenueInfoService iMeetingVenueInfoService;

    /**
     * 查询会议基本信息列表
     *
     * @param meetingInfo 会议基本信息
     * @return 会议基本信息
     */
    @Override
    public List<MeetingInfo> selectdList(MeetingInfoDto meetingInfo) {
        QueryWrapper<MeetingInfo> q = new QueryWrapper<>();
        if (meetingInfo.getMeetingName() != null && !meetingInfo.getMeetingName().trim().equals("")) {
            q.like("meeting_name", meetingInfo.getMeetingName());
        }
        if (meetingInfo.getMeetingCategory() != null && !meetingInfo.getMeetingCategory().trim().equals("")) {
            q.eq("meeting_category", meetingInfo.getMeetingCategory());
        }
        if (meetingInfo.getMeetingCategoryName() != null && !meetingInfo.getMeetingCategoryName().trim().equals("")) {
            q.like("meeting_category_name", meetingInfo.getMeetingCategoryName());
        }
        if (meetingInfo.getMeetingStatus() != null && !meetingInfo.getMeetingStatus().trim().equals("")) {
            q.eq("meeting_status", meetingInfo.getMeetingStatus());
        }
        if (meetingInfo.getMeetingStatusName() != null && !meetingInfo.getMeetingStatusName().trim().equals("")) {
            q.like("meeting_status_name", meetingInfo.getMeetingStatusName());
        }
        if (meetingInfo.getPreventionDeclareStatus() != null && !meetingInfo.getPreventionDeclareStatus().trim().equals("")) {
            q.eq("prevention_declare_status", meetingInfo.getPreventionDeclareStatus());
        }
        if (meetingInfo.getPreventionDeclareStatusName() != null && !meetingInfo.getPreventionDeclareStatusName().trim().equals("")) {
            q.like("prevention_declare_status_name", meetingInfo.getPreventionDeclareStatusName());
        }
        if (meetingInfo.getMeetingStartTime() != null) {
            q.eq("meeting_start_time", meetingInfo.getMeetingStartTime());
        }
        if (meetingInfo.getMeetingEndTime() != null) {
            q.eq("meeting_end_time", meetingInfo.getMeetingEndTime());
        }
        if (meetingInfo.getVenueId() != null) {
            q.eq("venue_id", meetingInfo.getVenueId());
        }
        if (meetingInfo.getVenueName() != null && !meetingInfo.getVenueName().trim().equals("")) {
            q.like("venue_name", meetingInfo.getVenueName());
        }
        if (meetingInfo.getMainOrg() != null && !meetingInfo.getMainOrg().trim().equals("")) {
            q.eq("main_org", meetingInfo.getMainOrg());
        }
        if (meetingInfo.getMeetingAddress() != null && !meetingInfo.getMeetingAddress().trim().equals("")) {
            q.eq("meeting_address", meetingInfo.getMeetingAddress());
        }
        if (meetingInfo.getExpectExhibitorNum() != null) {
            q.eq("expect_exhibitor_num", meetingInfo.getExpectExhibitorNum());
        }
        if (meetingInfo.getExpectAudienceNum() != null) {
            q.eq("expect_audience_num", meetingInfo.getExpectAudienceNum());
        }
        if (meetingInfo.getMeetingLogo() != null && !meetingInfo.getMeetingLogo().trim().equals("")) {
            q.eq("meeting_logo", meetingInfo.getMeetingLogo());
        }
        if (meetingInfo.getMeetingBanner() != null && !meetingInfo.getMeetingBanner().trim().equals("")) {
            q.eq("meeting_banner", meetingInfo.getMeetingBanner());
        }
        if (meetingInfo.getMeetingIntroduce() != null && !meetingInfo.getMeetingIntroduce().trim().equals("")) {
            q.eq("meeting_introduce", meetingInfo.getMeetingIntroduce());
        }
        if (meetingInfo.getMeetingFlow() != null && !meetingInfo.getMeetingFlow().trim().equals("")) {
            q.eq("meeting_flow", meetingInfo.getMeetingFlow());
        }
        if (meetingInfo.getApprovalStatus() != null && !meetingInfo.getApprovalStatus().trim().equals("")) {
            q.eq("approval_status", meetingInfo.getApprovalStatus());
        }
        if (meetingInfo.getApprovalOpinion() != null && !meetingInfo.getApprovalOpinion().trim().equals("")) {
            q.eq("approval_opinion", meetingInfo.getApprovalOpinion());
        }
        if (meetingInfo.getWxQrCodeUrl() != null && !meetingInfo.getWxQrCodeUrl().trim().equals("")) {
            q.eq("wx_qr_code_url", meetingInfo.getWxQrCodeUrl());
        }
        if (meetingInfo.getZfbQrCodeUrl() != null && !meetingInfo.getZfbQrCodeUrl().trim().equals("")) {
            q.eq("zfb_qr_code_url", meetingInfo.getZfbQrCodeUrl());
        }
        if (meetingInfo.getH5Url() != null && !meetingInfo.getH5Url().trim().equals("")) {
            q.eq("h5_url", meetingInfo.getH5Url());
        }
        if (meetingInfo.getMeetingPreventionRequirements() != null && !meetingInfo.getMeetingPreventionRequirements().trim().equals("")) {
            q.eq("meeting_prevention_requirements", meetingInfo.getMeetingPreventionRequirements());
        }
        if (meetingInfo.getCustomerServiceContact() != null && !meetingInfo.getCustomerServiceContact().trim().equals("")) {
            q.eq("customer_service_contact", meetingInfo.getCustomerServiceContact());
        }
        if (meetingInfo.getCustomerServiceContactTel() != null && !meetingInfo.getCustomerServiceContactTel().trim().equals("")) {
            q.eq("customer_service_contact_tel", meetingInfo.getCustomerServiceContactTel());
        }
        if (meetingInfo.getCustomerManager() != null && !meetingInfo.getCustomerManager().trim().equals("")) {
            q.eq("customer_manager", meetingInfo.getCustomerManager());
        }
        if (meetingInfo.getCustomerManagerTel() != null && !meetingInfo.getCustomerManagerTel().trim().equals("")) {
            q.eq("customer_manager_tel", meetingInfo.getCustomerManagerTel());
        }
        if (meetingInfo.getCustomerManagerUsername() != null && !meetingInfo.getCustomerManagerUsername().trim().equals("")) {
            q.eq("customer_manager_username", meetingInfo.getCustomerManagerUsername());
        }
        if (meetingInfo.getPreventionOfficer() != null && !meetingInfo.getPreventionOfficer().trim().equals("")) {
            q.eq("prevention_officer", meetingInfo.getPreventionOfficer());
        }
        if (meetingInfo.getPreventionOfficerTel() != null && !meetingInfo.getPreventionOfficerTel().trim().equals("")) {
            q.eq("prevention_officer_tel", meetingInfo.getPreventionOfficerTel());
        }
        if (meetingInfo.getAdministrativeApprover() != null && !meetingInfo.getAdministrativeApprover().trim().equals("")) {
            q.eq("administrative_approver", meetingInfo.getAdministrativeApprover());
        }
        List<MeetingInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<MeetingInfo> selectdListByCompanyName(String companyName, String userRole, MeetingInfoDto meetingInfo, List<Long> meetingIds) {
        QueryWrapper<MeetingInfo> q = new QueryWrapper<>();
        if (meetingInfo.getMeetingName() != null && !meetingInfo.getMeetingName().trim().equals("")) {
            q.like("meeting_name", meetingInfo.getMeetingName());
        }
        if (meetingInfo.getMeetingCategory() != null && !meetingInfo.getMeetingCategory().trim().equals("")) {
            q.eq("meeting_category", meetingInfo.getMeetingCategory());
        }
        if (meetingInfo.getMeetingCategoryName() != null && !meetingInfo.getMeetingCategoryName().trim().equals("")) {
            q.like("meeting_category_name", meetingInfo.getMeetingCategoryName());
        }
        if (meetingInfo.getMeetingStatus() != null && !meetingInfo.getMeetingStatus().trim().equals("")) {
            q.eq("meeting_status", meetingInfo.getMeetingStatus());
        }
        if (meetingInfo.getMeetingStatusName() != null && !meetingInfo.getMeetingStatusName().trim().equals("")) {
            q.like("meeting_status_name", meetingInfo.getMeetingStatusName());
        }
        if (meetingInfo.getPreventionDeclareStatus() != null && !meetingInfo.getPreventionDeclareStatus().trim().equals("")) {
            q.eq("prevention_declare_status", meetingInfo.getPreventionDeclareStatus());
        }
        if (meetingInfo.getPreventionDeclareStatusName() != null && !meetingInfo.getPreventionDeclareStatusName().trim().equals("")) {
            q.like("prevention_declare_status_name", meetingInfo.getPreventionDeclareStatusName());
        }
        if (meetingInfo.getMeetingStartTime() != null) {
            q.eq("meeting_start_time", meetingInfo.getMeetingStartTime());
        }
        if (meetingInfo.getMeetingEndTime() != null) {
            q.eq("meeting_end_time", meetingInfo.getMeetingEndTime());
        }
        if (meetingInfo.getVenueId() != null) {
            q.eq("venue_id", meetingInfo.getVenueId());
        }
        if (meetingInfo.getVenueName() != null && !meetingInfo.getVenueName().trim().equals("")) {
            q.like("venue_name", meetingInfo.getVenueName());
        }
        if (meetingInfo.getMainOrg() != null && !meetingInfo.getMainOrg().trim().equals("")) {
            q.eq("main_org", meetingInfo.getMainOrg());
        }
        if (meetingInfo.getMeetingAddress() != null && !meetingInfo.getMeetingAddress().trim().equals("")) {
            q.eq("meeting_address", meetingInfo.getMeetingAddress());
        }
        if (meetingInfo.getExpectExhibitorNum() != null) {
            q.eq("expect_exhibitor_num", meetingInfo.getExpectExhibitorNum());
        }
        if (meetingInfo.getExpectAudienceNum() != null) {
            q.eq("expect_audience_num", meetingInfo.getExpectAudienceNum());
        }
        if (meetingInfo.getMeetingLogo() != null && !meetingInfo.getMeetingLogo().trim().equals("")) {
            q.eq("meeting_logo", meetingInfo.getMeetingLogo());
        }
        if (meetingInfo.getMeetingBanner() != null && !meetingInfo.getMeetingBanner().trim().equals("")) {
            q.eq("meeting_banner", meetingInfo.getMeetingBanner());
        }
        if (meetingInfo.getMeetingIntroduce() != null && !meetingInfo.getMeetingIntroduce().trim().equals("")) {
            q.eq("meeting_introduce", meetingInfo.getMeetingIntroduce());
        }
        if (meetingInfo.getMeetingFlow() != null && !meetingInfo.getMeetingFlow().trim().equals("")) {
            q.eq("meeting_flow", meetingInfo.getMeetingFlow());
        }
        if (meetingInfo.getApprovalStatus() != null && !meetingInfo.getApprovalStatus().trim().equals("")) {
            q.eq("approval_status", meetingInfo.getApprovalStatus());
        }
        if (meetingInfo.getApprovalOpinion() != null && !meetingInfo.getApprovalOpinion().trim().equals("")) {
            q.eq("approval_opinion", meetingInfo.getApprovalOpinion());
        }
        if (meetingInfo.getWxQrCodeUrl() != null && !meetingInfo.getWxQrCodeUrl().trim().equals("")) {
            q.eq("wx_qr_code_url", meetingInfo.getWxQrCodeUrl());
        }
        if (meetingInfo.getZfbQrCodeUrl() != null && !meetingInfo.getZfbQrCodeUrl().trim().equals("")) {
            q.eq("zfb_qr_code_url", meetingInfo.getZfbQrCodeUrl());
        }
        if (meetingInfo.getH5Url() != null && !meetingInfo.getH5Url().trim().equals("")) {
            q.eq("h5_url", meetingInfo.getH5Url());
        }
        if (meetingInfo.getMeetingPreventionRequirements() != null && !meetingInfo.getMeetingPreventionRequirements().trim().equals("")) {
            q.eq("meeting_prevention_requirements", meetingInfo.getMeetingPreventionRequirements());
        }
        if (meetingInfo.getCustomerServiceContact() != null && !meetingInfo.getCustomerServiceContact().trim().equals("")) {
            q.eq("customer_service_contact", meetingInfo.getCustomerServiceContact());
        }
        if (meetingInfo.getCustomerServiceContactTel() != null && !meetingInfo.getCustomerServiceContactTel().trim().equals("")) {
            q.eq("customer_service_contact_tel", meetingInfo.getCustomerServiceContactTel());
        }
        if (meetingInfo.getCustomerManager() != null && !meetingInfo.getCustomerManager().trim().equals("")) {
            q.eq("customer_manager", meetingInfo.getCustomerManager());
        }
        if (meetingInfo.getCustomerManagerTel() != null && !meetingInfo.getCustomerManagerTel().trim().equals("")) {
            q.eq("customer_manager_tel", meetingInfo.getCustomerManagerTel());
        }
        if (meetingInfo.getCustomerManagerUsername() != null && !meetingInfo.getCustomerManagerUsername().trim().equals("")) {
            q.eq("customer_manager_username", meetingInfo.getCustomerManagerUsername());
        }
        if (meetingInfo.getPreventionOfficer() != null && !meetingInfo.getPreventionOfficer().trim().equals("")) {
            q.eq("prevention_officer", meetingInfo.getPreventionOfficer());
        }
        if (meetingInfo.getPreventionOfficerTel() != null && !meetingInfo.getPreventionOfficerTel().trim().equals("")) {
            q.eq("prevention_officer_tel", meetingInfo.getPreventionOfficerTel());
        }
        if (meetingInfo.getAdministrativeApprover() != null && !meetingInfo.getAdministrativeApprover().trim().equals("")) {
            q.eq("administrative_approver", meetingInfo.getAdministrativeApprover());
        }

        //-----------------------

        if (meetingIds != null && meetingIds.size() > 0) {
            q.in("id", meetingIds);
            //q.lambda().in(MeetingInfo::getId, meetingIds);
        } else {
            return new ArrayList<MeetingInfo>();
        }
        q.orderByDesc("created_time");
        return this.list(q);
    }


}
