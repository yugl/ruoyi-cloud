package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingInfoApprovalOpinion;
import com.ruoyi.manage.dto.MeetingInfoApprovalOpinionDto;

import java.util.List;

/**
 * 政务审核意见Service接口
 *
 * @author wangguiyu
 * @date 2022-05-04
 */
public interface IMeetingInfoApprovalOpinionService extends IService<MeetingInfoApprovalOpinion> {


    /**
     * 查询政务审核意见列表
     *
     * @param meetingInfoApprovalOpinion 政务审核意见
     * @return 政务审核意见集合
     */
    public List<MeetingInfoApprovalOpinion> selectdList(MeetingInfoApprovalOpinionDto meetingInfoApprovalOpinion);

}
