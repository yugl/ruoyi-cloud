package com.ruoyi.manage.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.SmsSendRecord;
import com.ruoyi.manage.dto.SmsSendRecordDto;
import com.ruoyi.manage.service.ISmsSendRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 短信管理Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "短信发送记录控制器", tags = {"短信发送记录管理"})
@RestController
@RequestMapping("/smsSendRecord")
public class SmsSendRecordController extends BaseController {
    @Autowired
    private ISmsSendRecordService smsSendRecordService;

    /**
     * 查询短信管理列表
     */
    @ApiOperation("查询短信发送记录列表")
    @RequiresPermissions("manage:serviceProviderProduct:list")
    @PostMapping("/list")
    public TableDataInfo list(SmsSendRecordDto dto) {
        startPage();
        List<SmsSendRecord> list = smsSendRecordService.selectdList(dto);
        return getDataTable(list);
    }

    /**
     * 获取短信发送数量
     */
    @PostMapping("/countSmsSend")
    public AjaxResult countSmsSend(@RequestParam Long meetingId) {
        return smsSendRecordService.countSmsSend(meetingId);
    }
}
