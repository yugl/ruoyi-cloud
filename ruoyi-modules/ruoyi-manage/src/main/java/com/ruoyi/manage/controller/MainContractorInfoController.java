package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MainContractorInfo;
import com.ruoyi.manage.dto.MainContractorInfoDto;
import com.ruoyi.manage.service.IMainContractorInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 主承办基本信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "主承办基本信息控制器", tags = {"主承办基本信息管理"})
@RestController
@RequestMapping("/mainContractor")
public class MainContractorInfoController extends BaseController {
    @Autowired
    private IMainContractorInfoService mainContractorInfoService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询主承办基本信息列表
     */
    @ApiOperation("查询主承办基本信息列表")
    @RequiresPermissions("manage:mainContractor:list")
    @GetMapping("/list")
    public TableDataInfo list(MainContractorInfoDto mainContractorInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            mainContractorInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }
        startPage();
        List<MainContractorInfo> list = mainContractorInfoService.selectdList(mainContractorInfo);
        return getDataTable(list);
    }

    /**
     * 导出主承办基本信息列表
     */
    @ApiOperation("导出主承办基本信息列表")
    @RequiresPermissions("manage:mainContractor:export")
    @Log(title = "主承办基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MainContractorInfoDto mainContractorInfo) throws IOException {

        startPage();
        List<MainContractorInfo> list = mainContractorInfoService.selectdList(mainContractorInfo);
        ExcelUtil<MainContractorInfo> util = new ExcelUtil<MainContractorInfo>(MainContractorInfo.class);
        util.exportExcel(response, list, "主承办基本信息数据");
    }

    /**
     * 导入主承办基本信息
     */
    @ApiOperation("导出主承办基本信息列表")
    @RequiresPermissions("manage:mainContractor:importData")
    @Log(title = "主承办基本信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MainContractorInfo> util = new ExcelUtil<MainContractorInfo>(MainContractorInfo.class);
        List<MainContractorInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = mainContractorInfoService.saveOrUpdateBatch(list);
        } else {
            message = mainContractorInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取主承办基本信息详细信息
     */
    @ApiOperation("获取主承办基本信息详细信息")
    @RequiresPermissions("manage:mainContractor:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(mainContractorInfoService.getById(id));
    }

    /**
     * 通过公司名称获取会议承办信息
     */
    @ApiOperation("通过公司名称获取会议承办信息")
    @RequiresPermissions("manage:mainContractor:query")
    @GetMapping(value = "/companyName/{companyName}")
    public AjaxResult getInfoByCompanyName(@ApiParam("公司名称") @NotNull(message = "公司名称不能为空且精确匹配")
                                           @PathVariable("companyName") String companyName) {
        return AjaxResult.success(mainContractorInfoService.selectdListByCompanyName(companyName));
    }

    /**
     * 获取所有主承办公司名称
     */
    @ApiOperation("获取所有主承办公司名称")
    @RequiresPermissions("manage:mainContractor:query")
    @GetMapping(value = "/companyNames")
    public AjaxResult getCompanyNames() {
        return AjaxResult.success(mainContractorInfoService.getCompanyNames());
    }


    /**
     * 新增主承办基本信息
     */
    @ApiOperation("新增主承办基本信息")
    @RequiresPermissions("manage:mainContractor:add")
    @Log(title = "主承办基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MainContractorInfo mainContractorInfo) {
        if (UserConstants.NOT_UNIQUE.equals(mainContractorInfoService.checkNameUnique(mainContractorInfo.getCompanyName()))) {
            return AjaxResult.error("新增主承办信息'" + mainContractorInfo.getCompanyName() + "'失败，公司名称已存在");
        }
        mainContractorInfo.setCreatedTime(DateUtils.getNowDate());
        mainContractorInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(mainContractorInfo.getChargePersonPhone(), mainContractorInfo.getChargePerson(), mainContractorInfo.getCompanyName(), PersonRole.MAIN.getIndex());
        return toAjax(mainContractorInfoService.save(mainContractorInfo));
    }

    /**
     * 修改主承办基本信息
     */
    @ApiOperation("修改主承办基本信息")
    @RequiresPermissions("manage:mainContractor:edit")
    @Log(title = "主承办基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MainContractorInfo mainContractorInfo) {
        return toAjax(mainContractorInfoService.updateById(mainContractorInfo));
    }

    /**
     * 删除主承办基本信息
     */
    @ApiOperation("删除主承办基本信息")
    @RequiresPermissions("manage:mainContractor:remove")
    @Log(title = "主承办基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(mainContractorInfoService.removeByIds(ids));
    }
}
