package com.ruoyi.manage.utils;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;
import com.ruoyi.manage.properties.AlipayConfig;
import com.ruoyi.manage.properties.WxConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AppUtils {

    @Autowired
    WxConfig wxConfig;


    @Autowired
    AlipayConfig alipayConfig;


    /**
     * 微信根据code获取openid
     *
     * @param code
     * @return
     */
    public String getWxOpenid(String code) {
        System.out.println("wxConfig-url" + wxConfig.getUrl());
        try {
            String response = HttpUtil.get(String.format(wxConfig.getUrl(), wxConfig.getAppid(), wxConfig.getSecret(), code));
            JSONObject res = JSONUtil.parseObj(response);
            return res.getStr("openid");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException("微信获取openid失败！");
        }
    }

    /**
     * 支付宝根据code获取openid
     *
     * @param code
     * @return
     */
    public String getAlipayOpenid(String code) {
        String openid = null;
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    alipayConfig.getUrl(),
                    alipayConfig.getAppid(),
                    alipayConfig.getAppPrivateKey(),
                    "json",
                    "GBK",
                    alipayConfig.getPublicKey(),
                    alipayConfig.getSignType());
            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            request.setGrantType("authorization_code");
            request.setCode(code);
            AlipaySystemOauthTokenResponse response = alipayClient.execute(request);
            if (response.isSuccess()) {
                openid = response.getUserId();
            } else {
                throw new RuntimeException("支付宝获取用户信息失败！");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException("支付宝获取用户ID失败！");
        }
        return openid;
    }

    private AlipayUserInfoShareResponse getAliUserInfo(String accessToken) throws Exception {
        AlipayClient alipayClient = new DefaultAlipayClient(
                alipayConfig.getUrl(),
                alipayConfig.getAppid(),                    // 1. 填入appid
                alipayConfig.getAppPrivateKey(),            // 2. 填入私钥
                "json",
                "GBK",
                alipayConfig.getPublicKey(),         // 3. 填入公钥
                alipayConfig.getSignType());
        AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();
        AlipayUserInfoShareResponse response = alipayClient.execute(request, accessToken);
        if (response.isSuccess()) {
            return response;
        }
        return null;
    }

}
