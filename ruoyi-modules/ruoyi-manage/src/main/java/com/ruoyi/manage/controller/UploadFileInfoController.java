package com.ruoyi.manage.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.file.FileTypeUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.UploadFileInfo;
import com.ruoyi.manage.dto.UploadFileInfoDto;
import com.ruoyi.manage.service.IUploadFileInfoService;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.domain.SysFile;
import io.seata.common.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 上传文件信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "上传文件信息控制器", tags = {"上传文件信息管理"})
@RestController
@RequestMapping("/uploadFile")
public class UploadFileInfoController extends BaseController {
    @Autowired
    private IUploadFileInfoService uploadFileInfoService;

    @Autowired
    RemoteFileService remoteFileService;

    /**
     * 查询上传文件信息列表
     */
    @ApiOperation("查询上传文件信息列表")
    @RequiresPermissions("manage:uploadFile:list")
    @GetMapping("/list")
    public TableDataInfo list(UploadFileInfoDto uploadFileInfo) {
        startPage();
        List<UploadFileInfo> list = uploadFileInfoService.selectdList(uploadFileInfo);
        return getDataTable(list);
    }

    /**
     * 导出上传文件信息列表
     */
    @ApiOperation("导出上传文件信息列表")
    @RequiresPermissions("manage:uploadFile:export")
    @Log(title = "上传文件信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UploadFileInfoDto uploadFileInfo) throws IOException {

        startPage();
        List<UploadFileInfo> list = uploadFileInfoService.selectdList(uploadFileInfo);
        ExcelUtil<UploadFileInfo> util = new ExcelUtil<UploadFileInfo>(UploadFileInfo.class);
        util.exportExcel(response, list, "上传文件信息数据");
    }

    /**
     * 导入上传文件信息
     */
    @ApiOperation("导出上传文件信息列表")
    @RequiresPermissions("manage:uploadFile:importData")
    @Log(title = "上传文件信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<UploadFileInfo> util = new ExcelUtil<UploadFileInfo>(UploadFileInfo.class);
        List<UploadFileInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = uploadFileInfoService.saveOrUpdateBatch(list);
        } else {
            message = uploadFileInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取上传文件信息详细信息
     */
    @ApiOperation("获取上传文件信息详细信息")
    @RequiresPermissions("manage:uploadFile:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(uploadFileInfoService.getById(id));
    }

    /**
     * 批量上传文件并记录文件详情到表
     */
    @ApiOperation("批量上传文件并记录文件详情到表")
    @RequiresPermissions("manage:uploadFile:add")
    @Log(title = "上传文件信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestParam(value = "uploadfiles", required = false) MultipartFile[] files,UploadFileInfo uploadFileInfo) {
        uploadFileInfo.setCreatedTime(DateUtils.getNowDate());
        uploadFileInfo.setCreatedBy(SecurityUtils.getUsername());
        List<UploadFileInfo> fileInfos = new ArrayList<>();
        if(files!=null && files.length > 0){
            for(MultipartFile file:files){
                UploadFileInfo fileInfo = new UploadFileInfo();
                fileInfo.setFileName(file.getOriginalFilename());
                fileInfo.setFileExt(FileTypeUtils.getFileType(file.getOriginalFilename()));
                fileInfo.setFileSize(file.getSize());
                fileInfo.setFileType(uploadFileInfo.getFileType());//文件类型:1 防疫材料
                fileInfo.setMeetingId(uploadFileInfo.getMeetingId());
                fileInfo.setCompanyName(uploadFileInfo.getCompanyName());
                fileInfo.setCreatedBy(uploadFileInfo.getCreatedBy());
                fileInfo.setCreatedTime(uploadFileInfo.getCreatedTime());
                //Feign调用文件上传接口
                R<SysFile> sysFile =  remoteFileService.upload(file);
                fileInfo.setFileUrl(sysFile.getData().getUrl());
                fileInfo.setFastdfsId(sysFile.getData().getFastdfsId());
                fileInfos.add(fileInfo);
            }
        }
        boolean b = uploadFileInfoService.saveBatch(fileInfos);
        if(b){
            return AjaxResult.success(fileInfos);
        }else{
            return AjaxResult.error();
        }

    }

    /**
     * 修改上传文件信息
     */
    @ApiOperation("修改上传文件信息")
    @RequiresPermissions("manage:uploadFile:edit")
    @Log(title = "上传文件信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UploadFileInfo uploadFileInfo) {
        return toAjax(uploadFileInfoService.updateById(uploadFileInfo));
    }

    /**
     * 删除上传文件信息
     */
    @ApiOperation("删除上传文件信息")
    @RequiresPermissions("manage:uploadFile:remove")
    @Log(title = "上传文件信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        for(Long id:ids){
            UploadFileInfo uploadFileInfo = uploadFileInfoService.getById(id);
            if(uploadFileInfo != null)
                remoteFileService.deleteFile(uploadFileInfo.getFileUrl());
        }
        return toAjax(uploadFileInfoService.removeByIds(ids));
    }
}
