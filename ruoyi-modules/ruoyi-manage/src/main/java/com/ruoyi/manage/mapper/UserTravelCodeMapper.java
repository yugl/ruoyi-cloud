package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.UserTravelCode;
import org.apache.ibatis.annotations.Param;

/**
 * 行程码Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface UserTravelCodeMapper extends BaseMapper<UserTravelCode> {

}
