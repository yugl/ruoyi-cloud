package com.ruoyi.manage.controller;

import io.swagger.annotations.Api;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 下载模板文件、帮助文件等
 * wangguiyu
 */
@Api(value = "文件下载控制器", tags = {"文件下载管理"})
@RestController
@RequestMapping("/download")
public class DownLoadFileController {

    @Resource
    private ResourceLoader resourceLoader;

    /**
     * 下载导入模板
     * @param response
     * @filename  模板名称带扩展名  如:user1.xlsx
     */
    @GetMapping(value = "/template")
    public void downloadExcel(HttpServletResponse response,String fileName) {
        InputStream inputStream = null;
        ServletOutputStream servletOutputStream = null;
        try {
            String filename = fileName;
            if("userHealthManageModel.xlsx".equals(fileName))
                filename="导入员工模板.xlsx";//模板导出后的名字
            else if("productInfoModel.xlsx".equals(fileName))
                filename="导入服务商产品模板.xlsx";//模板导出后的名字
            else if("serviceProviderInfoModel.xlsx".equals(fileName))
                filename="导入服务商模板.xlsx";//模板导出后的名字
            else if("exhibitorManageModel.xlsx".equals(fileName))
                filename="导入展商模板.xlsx";//模板导出后的名字
            else if("venueBoothInfoModel.xlsx".equals(fileName))
                filename="导入场馆展位模板.xlsx";//模板导出后的名字

            String path = "fileTemplate/"+fileName;//要下载的文件在resources目录内的全路径
            org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:" + path);
            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.addHeader("charset", "utf-8");
            response.addHeader("Pragma", "no-cache");
            String encodeName = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + encodeName + "\"; filename*=utf-8''" + encodeName);
            inputStream = resource.getInputStream();
            servletOutputStream = response.getOutputStream();
            IOUtils.copy(inputStream, servletOutputStream);
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


