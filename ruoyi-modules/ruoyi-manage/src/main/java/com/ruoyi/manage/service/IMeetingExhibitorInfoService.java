package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingExhibitorInfo;
import com.ruoyi.manage.dto.MeetingExhibitorInfoDto;

/**
 * 会议展商信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingExhibitorInfoService extends IService<MeetingExhibitorInfo> {


    /**
     * 查询会议展商信息列表
     *
     * @param meetingExhibitorInfo 会议展商信息
     * @return 会议展商信息集合
     */
    public List<MeetingExhibitorInfo> selectdList(MeetingExhibitorInfoDto meetingExhibitorInfo);

    /**
     * 通过公司名称精确查找会议ids
     * @param companyName
     * @return
     */
    List<Long> selectMeetingIdsByCompanyName(String companyName);

    /**
     * 查询参加指定会议的所有展商list
     * @param meetingIds
     * @return
     */
    List<MeetingExhibitorInfo> selectListByMeetingIds(List<Long> meetingIds);

    public String checkNameUnique(Long meetingId,String companyName);

}
