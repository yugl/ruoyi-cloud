package com.ruoyi.manage.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.UserNucleinMapper;
import com.ruoyi.manage.domain.UserNuclein;
import com.ruoyi.manage.dto.UserNucleinDto;
import com.ruoyi.manage.service.IUserNucleinService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 核酸检测Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Service
public class UserNucleinServiceImpl extends ServiceImpl<UserNucleinMapper, UserNuclein> implements IUserNucleinService {
    @Autowired
    private UserNucleinMapper userNucleinMapper;

    /**
     * 查询核酸检测列表
     *
     * @param userNuclein 核酸检测
     * @return 核酸检测
     */
    @Override
    public List<UserNuclein> selectdList(UserNucleinDto userNuclein) {
        QueryWrapper<UserNuclein> q = new QueryWrapper<>();
                    if (userNuclein.getPhone() != null   && !userNuclein.getPhone().trim().equals("")){
                        q.eq("phone",userNuclein.getPhone());
                }
                    if (userNuclein.getTestOrg() != null   && !userNuclein.getTestOrg().trim().equals("")){
                        q.eq("test_org",userNuclein.getTestOrg());
                }
                    if (userNuclein.getTestTime() != null  ){
                        q.eq("test_time",userNuclein.getTestTime());
                }
                    if (userNuclein.getUserStatus() != null   && !userNuclein.getUserStatus().trim().equals("")){
                        q.eq("user_status",userNuclein.getUserStatus());
                }
                    if (userNuclein.getCreatedTime() != null  ){
                        q.eq("created_time",userNuclein.getCreatedTime());
                }
        List<UserNuclein> list = this.list(q);
        return list;
    }


}
