package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 问卷题目对象 app_subject_info
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class AppSubjectInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 问卷ID
     */
    @Excel(name = "问卷ID")
    private Long questionnaireId;

    /**
     * 序号
     */
    @Excel(name = "序号")
    private Long sortNo;

    /**
     * 题目
     */
    @Excel(name = "题目")
    private String subject;

    /**
     * 类型 1：单选 2：多选 3：文本
     */
    @Excel(name = "类型 1：单选 2：多选 3：文本")
    private String type;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @TableLogic
    @Excel(name = "状态 0：有效 1：无效")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

}
