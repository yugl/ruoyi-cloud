package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.domain.MeetingMainContractorInfo;
import com.ruoyi.manage.dto.MeetingMainContractorInfoDto;
import com.ruoyi.manage.mapper.MeetingInfoMapper;
import com.ruoyi.manage.mapper.MeetingMainContractorInfoMapper;
import com.ruoyi.manage.service.IMeetingMainContractorInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 会议承办信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingMainContractorInfoServiceImpl extends ServiceImpl<MeetingMainContractorInfoMapper, MeetingMainContractorInfo> implements IMeetingMainContractorInfoService {
    @Autowired
    private MeetingMainContractorInfoMapper meetingMainContractorInfoMapper;

    @Autowired
    private MeetingInfoMapper meetingInfoMapper;

    /**
     * 查询会议承办信息列表
     *
     * @param meetingMainContractorInfo 会议承办信息
     * @return 会议承办信息
     */
    @Override
    public List<MeetingMainContractorInfo> selectdList(MeetingMainContractorInfoDto meetingMainContractorInfo) {
        QueryWrapper<MeetingMainContractorInfo> q = new QueryWrapper<>();
        if (meetingMainContractorInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingMainContractorInfo.getMeetingId());
            //****************
            MeetingInfo meetingInfo = meetingInfoMapper.selectById(meetingMainContractorInfo.getMeetingId());
            q.ne("company_name",meetingInfo.getMainOrg());//只查承办公司,过滤掉主办公司。
            //****************
        }
        if (meetingMainContractorInfo.getCompanyName() != null && !meetingMainContractorInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", meetingMainContractorInfo.getCompanyName());
        }
        if (meetingMainContractorInfo.getTaxpayerIdentifierNum() != null && !meetingMainContractorInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", meetingMainContractorInfo.getTaxpayerIdentifierNum());
        }
        if (meetingMainContractorInfo.getIndustry() != null && !meetingMainContractorInfo.getIndustry().trim().equals("")) {
            q.eq("industry", meetingMainContractorInfo.getIndustry());
        }
        if (meetingMainContractorInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", meetingMainContractorInfo.getIncorporationDate());
        }
        if (meetingMainContractorInfo.getRegisteredCapital() != null && !meetingMainContractorInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", meetingMainContractorInfo.getRegisteredCapital());
        }
        if (meetingMainContractorInfo.getChargePerson() != null && !meetingMainContractorInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", meetingMainContractorInfo.getChargePerson());
        }
        if (meetingMainContractorInfo.getChargePersonPhone() != null && !meetingMainContractorInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", meetingMainContractorInfo.getChargePersonPhone());
        }
        if (meetingMainContractorInfo.getPreventionPerson() != null && !meetingMainContractorInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", meetingMainContractorInfo.getPreventionPerson());
        }
        if (meetingMainContractorInfo.getPreventionPersonPhone() != null && !meetingMainContractorInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", meetingMainContractorInfo.getPreventionPersonPhone());
        }
        if (meetingMainContractorInfo.getAccountManager() != null && !meetingMainContractorInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", meetingMainContractorInfo.getAccountManager());
        }
        if (meetingMainContractorInfo.getAccountManagerPhone() != null && !meetingMainContractorInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", meetingMainContractorInfo.getAccountManagerPhone());
        }
        if (meetingMainContractorInfo.getAccountManagerUsername() != null && !meetingMainContractorInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", meetingMainContractorInfo.getAccountManagerUsername());
        }
        if (meetingMainContractorInfo.getBusinessLicenseUrl() != null && !meetingMainContractorInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", meetingMainContractorInfo.getBusinessLicenseUrl());
        }
        if (meetingMainContractorInfo.getBusinessNature() != null && !meetingMainContractorInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", meetingMainContractorInfo.getBusinessNature());
        }
        if (meetingMainContractorInfo.getStatus() != null && !meetingMainContractorInfo.getStatus().trim().equals("")) {
            q.eq("status", meetingMainContractorInfo.getStatus());
        }
        if (meetingMainContractorInfo.getCreatedBy() != null && !meetingMainContractorInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingMainContractorInfo.getCreatedBy());
        }
        if (meetingMainContractorInfo.getCreatedTime() != null) {
            q.eq("created_time", meetingMainContractorInfo.getCreatedTime());
        }
        if (meetingMainContractorInfo.getUpdatedBy() != null && !meetingMainContractorInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingMainContractorInfo.getUpdatedBy());
        }
        if (meetingMainContractorInfo.getUpdatedTime() != null) {
            q.eq("updated_time", meetingMainContractorInfo.getUpdatedTime());
        }

        List<MeetingMainContractorInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<Long> selectMeetingIdsByCompanyName(String companyName) {
        QueryWrapper<MeetingMainContractorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingMainContractorInfo::getCompanyName, companyName);
        List<MeetingMainContractorInfo> list = this.baseMapper.selectList(q);
        return list.stream().map(item -> item.getMeetingId()).collect(Collectors.toList());
    }

    @Override
    public String checkNameUnique(Long meetingId, String companyName) {
        QueryWrapper<MeetingMainContractorInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingMainContractorInfo::getCompanyName, companyName);
        q.lambda().eq(MeetingMainContractorInfo::getMeetingId, meetingId);
        List<MeetingMainContractorInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

}
