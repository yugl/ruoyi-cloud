package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.MeetingUserHealthManageDto;

import java.util.List;
import java.util.Map;

/**
 * 会议员工健康管理Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingUserHealthManageService extends IService<MeetingUserHealthManage> {


    /**
     * 查询会议员工健康管理列表
     *
     * @param meetingUserHealthManage 会议员工健康管理
     * @return 会议员工健康管理集合
     */
    public List<MeetingUserHealthManage> selectdList(MeetingUserHealthManageDto meetingUserHealthManage);

    /**
     * 统计参会的人员数量
     *
     * @param meetingId
     * @return
     */
    Map<String, Object> statisticsMeetingPersonQuantity(String meetingId);

    AjaxResult findMeetingHealthList(String phone);

    AjaxResult addHealthAndTripCode(Map<String, String> map);

    AjaxResult getHealthAndTripCode(Map<String, String> map);
}
