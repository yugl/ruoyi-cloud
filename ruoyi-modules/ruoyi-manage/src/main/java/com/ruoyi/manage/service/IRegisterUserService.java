package com.ruoyi.manage.service;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysUser;

public interface IRegisterUserService {


    AjaxResult registerUser(String phone, String name, String companyName, String roleName);
}
