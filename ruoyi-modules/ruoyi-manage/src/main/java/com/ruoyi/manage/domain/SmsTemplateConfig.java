package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信模板配置对象 sms_template_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsTemplateConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 模板名称
     */
    @Excel(name = "模板名称")
    private Long templateId;
    /**
     * 模板名称
     */
    @Excel(name = "模板名称")
    private String templateName;

    /**
     * 提醒类型 1：自动 2：人工 3：每天 4：提前
     */
    private String noticeType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 1:平台使用 2：会议使用
     */
    private String type;

    /**
     * 模板内容
     */
    @Excel(name = "模板内容")
    private String templateContent;

    private String useStatus;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @TableLogic
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private Date updatedTime;


}
