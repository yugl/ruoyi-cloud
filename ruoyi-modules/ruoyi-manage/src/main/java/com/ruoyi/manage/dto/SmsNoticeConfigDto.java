package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信使用率提醒对象 sms_notice_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsNoticeConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 短信使用率提醒
     */
    private Long noticeNum;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

}
