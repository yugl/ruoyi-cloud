package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议基本信息Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingInfoMapper extends BaseMapper<MeetingInfo> {


    List<MeetingInfo> findMeetingList(@Param("map") Map<String, String> map);

    MeetingInfo getMeetingInfo(@Param("meetingId") String meetingId);

    List<Map<String, Object>> findMeetingSelect(@Param("phone") String phone);
}
