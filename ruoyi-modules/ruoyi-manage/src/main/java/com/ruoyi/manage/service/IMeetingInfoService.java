package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.dto.MeetingInfoDto;

/**
 * 会议基本信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingInfoService extends IService<MeetingInfo> {


    /**
     * 查询会议基本信息列表
     *
     * @param meetingInfo 会议基本信息
     * @return 会议基本信息集合
     */
    List<MeetingInfo> selectdList(MeetingInfoDto meetingInfo);

    /**
     * 通过公司名称精确查找会议信息
     * @param companyName
     * @return
     */
    List<MeetingInfo> selectdListByCompanyName(String companyName,String useRole,MeetingInfoDto meetingInfo,List<Long> meetingIds);

}
