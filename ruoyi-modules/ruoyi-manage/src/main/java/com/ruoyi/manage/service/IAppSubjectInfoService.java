package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.AppSubjectInfo;
import com.ruoyi.manage.dto.AppSubjectInfoDto;
import com.ruoyi.manage.vo.AppSubjectInfoVo;

import java.util.List;

/**
 * 问卷题目Service接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface IAppSubjectInfoService extends IService<AppSubjectInfo> {


    /**
     * 查询问卷题目列表
     *
     * @param appSubjectInfo 问卷题目
     * @return 问卷题目集合
     */
    public List<AppSubjectInfo> selectdList(AppSubjectInfoDto appSubjectInfo);

    /**
     * 小程序获取问卷调查
     *
     * @param questionnaireId
     * @return
     */
    List<AppSubjectInfoVo> selectSubjectList(Long questionnaireId);
}
