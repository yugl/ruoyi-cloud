package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.VenueInfo;
import com.ruoyi.manage.dto.VenueInfoDto;

import java.util.List;

/**
 * 场馆基本信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IVenueInfoService extends IService<VenueInfo> {


    /**
     * 查询场馆基本信息列表
     *
     * @param venueInfo 场馆基本信息
     * @return 场馆基本信息集合
     */
    public List<VenueInfo> selectdList(VenueInfoDto venueInfo);

    public String checkNameUnique(String venueName);

    List<VenueInfo> selectdListByVenueName(String venueName);

    List<VenueInfo> selectdListByCompanyName(String companyName);



}
