package com.ruoyi.manage.config;

public class CacheNames {

    /**
     * app 验证码缓存Name前缀
     */
    public static final String APP_VERIFICATION = "app:verification:";
}
