package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.UserVaccines;
import com.ruoyi.manage.dto.UserVaccinesDto;
import com.ruoyi.manage.service.IUserVaccinesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 用户疫苗记录Controller
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Api(value = "用户疫苗记录控制器", tags = {"用户疫苗记录管理"})
@RestController
@RequestMapping("/vaccines")
public class UserVaccinesController extends BaseController {
    @Autowired
    private IUserVaccinesService userVaccinesService;

    /**
     * 查询用户疫苗记录列表
     */
    @ApiOperation("查询用户疫苗记录列表")
    @RequiresPermissions("manage:vaccines:list")
    @GetMapping("/list")
    public TableDataInfo list(UserVaccinesDto userVaccines) {
        startPage();
        List<UserVaccines> list = userVaccinesService.selectdList(userVaccines);
        return getDataTable(list);
    }

    /**
     * 导出用户疫苗记录列表
     */
    @ApiOperation("导出用户疫苗记录列表")
    @RequiresPermissions("manage:vaccines:export")
    @Log(title = "用户疫苗记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserVaccinesDto userVaccines) throws IOException {

        startPage();
        List<UserVaccines> list = userVaccinesService.selectdList(userVaccines);
        ExcelUtil<UserVaccines> util = new ExcelUtil<UserVaccines>(UserVaccines.class);
        util.exportExcel(response, list, "用户疫苗记录数据");
    }

    /**
     * 导入用户疫苗记录
     */
    @ApiOperation("导出用户疫苗记录列表")
    @RequiresPermissions("manage:vaccines:importData")
    @Log(title = "用户疫苗记录导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<UserVaccines> util = new ExcelUtil<UserVaccines>(UserVaccines.class);
        List<UserVaccines> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = userVaccinesService.saveOrUpdateBatch(list);
        } else {
            message = userVaccinesService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取用户疫苗记录详细信息
     */
    @ApiOperation("获取用户疫苗记录详细信息")
    @RequiresPermissions("manage:vaccines:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(userVaccinesService.getById(id));
    }

    /**
     * 新增用户疫苗记录
     */
    @ApiOperation("新增用户疫苗记录")
    @RequiresPermissions("manage:vaccines:add")
    @Log(title = "用户疫苗记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserVaccines userVaccines) {
        return toAjax(userVaccinesService.save(userVaccines));
    }

    /**
     * 修改用户疫苗记录
     */
    @ApiOperation("修改用户疫苗记录")
    @RequiresPermissions("manage:vaccines:edit")
    @Log(title = "用户疫苗记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserVaccines userVaccines) {
        return toAjax(userVaccinesService.updateById(userVaccines));
    }

    /**
     * 删除用户疫苗记录
     */
    @ApiOperation("删除用户疫苗记录")
    @RequiresPermissions("manage:vaccines:remove")
    @Log(title = "用户疫苗记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(userVaccinesService.removeByIds(ids));
    }
}
