package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.*;
import com.ruoyi.manage.dto.MeetingInfoDto;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.exception.base.BizException;
import com.ruoyi.manage.service.*;
import com.ruoyi.manage.utils.SmsUtils;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 会议基本信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议基本信息控制器", tags = {"会议基本信息管理"})
@RestController
@RequestMapping("/meetingInfo")
public class MeetingInfoController extends BaseController {
    @Autowired
    private IMeetingInfoService meetingInfoService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private IMainContractorInfoService mainContractorInfoService;

    @Autowired
    private IMeetingMainContractorInfoService meetingMainContractorInfoService;

    @Autowired
    IMeetingMainContractorInfoService iMeetingMainContractorInfoService;
    @Autowired
    IMeetingServiceProviderInfoService iMeetingServiceProviderInfoService;
    @Autowired
    IMeetingExhibitorInfoService iMeetingExhibitorInfoService;
    @Autowired
    IMeetingVenueInfoService iMeetingVenueInfoService;

    @Autowired
    IVenueInfoService iVenueInfoService;

    @Autowired
    SmsUtils smsUtils;
    @Autowired
    RemoteUserService remoteUserService;


    /**
     * 查询会议基本信息列表
     */
    @ApiOperation("查询会议基本信息列表")
    @RequiresPermissions("manage:meetingInfo:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingInfoDto meetingInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();

        // 平台运营业务经理
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())) {
            meetingInfo.setCustomerManagerUsername(SecurityUtils.getUsername());
        }

        // 行政审核人
        if (roles.contains(PersonRole.ADMINISTRATIVE_APPROVER.getIndex())) {
            meetingInfo.setAdministrativeApprover(SecurityUtils.getUsername());
        }

        startPage();
        List<MeetingInfo> list = meetingInfoService.selectdList(meetingInfo);
        return getDataTable(list);
    }

    /**
     * 导出会议基本信息列表
     */
    @ApiOperation("导出会议基本信息列表")
    @RequiresPermissions("manage:meetingInfo:export")
    @Log(title = "会议基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingInfoDto meetingInfo) throws IOException {

        startPage();
        List<MeetingInfo> list = meetingInfoService.selectdList(meetingInfo);
        ExcelUtil<MeetingInfo> util = new ExcelUtil<MeetingInfo>(MeetingInfo.class);
        util.exportExcel(response, list, "会议基本信息数据");
    }

    /**
     * 导入会议基本信息
     */
    @ApiOperation("导出会议基本信息列表")
    @RequiresPermissions("manage:meetingInfo:importData")
    @Log(title = "会议基本信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingInfo> util = new ExcelUtil<MeetingInfo>(MeetingInfo.class);
        List<MeetingInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingInfoService.saveOrUpdateBatch(list);
        } else {
            message = meetingInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议基本信息详细信息
     */
    @ApiOperation("获取会议基本信息详细信息")
    @RequiresPermissions("manage:meetingInfo:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingInfoService.getById(id));
    }

    /**
     * 通过公司名称获取会议基本信息
     */
    @ApiOperation("通过公司名称获取会议基本信息")
    @RequiresPermissions("manage:meetingInfo:query")
    @GetMapping(value = "/companyName/{companyName}")
    public TableDataInfo getInfoByCompanyName(@ApiParam("公司名称") @NotNull(message = "公司名称不能为空且精确匹配")
                                              @PathVariable("companyName") String companyName, @RequestParam("userRole") String userRole
            , MeetingInfoDto meetingInfo) {
        //---附加条件--------------
        List<Long> meetingIds = null;

        if (PersonRole.MAIN.getIndex().equals(userRole) || PersonRole.UNDERTAKER.getIndex().equals(userRole)) {//主办或承办
            meetingIds = iMeetingMainContractorInfoService.selectMeetingIdsByCompanyName(companyName);
        } else if (PersonRole.SERVICE_PROVIDER.getIndex().equals(userRole)) {//服务商
            meetingIds = iMeetingServiceProviderInfoService.selectMeetingIdsByCompanyName(companyName);
        } else if (PersonRole.EXHIBITOR.getIndex().equals(userRole)) {//展商
            meetingIds = iMeetingExhibitorInfoService.selectMeetingIdsByCompanyName(companyName);
        } else if (PersonRole.VENUE.getIndex().equals(userRole)) {//场馆
            meetingIds = iMeetingVenueInfoService.selectMeetingIdsByCompanyName(companyName);
        }

        startPage();
        List<MeetingInfo> list = meetingInfoService.selectdListByCompanyName(companyName, userRole, meetingInfo, meetingIds);
        return getDataTable(list);
    }


    /**
     * 新增会议基本信息
     */
    @ApiOperation("新增会议基本信息")
    @RequiresPermissions("manage:meetingInfo:add")
    @Log(title = "会议基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingInfo meetingInfo) {
        meetingInfo.setCreatedTime(DateUtils.getNowDate());
        meetingInfo.setCreatedBy(SecurityUtils.getUsername());
        meetingInfo.setUpdatedTime(DateUtils.getNowDate());
        boolean b = meetingInfoService.save(meetingInfo);
        if (b) {
            //-----------------------
//            String mainOrg = meetingInfo.getMainOrg();
//            List<MainContractorInfo>  mainContractorInfos = mainContractorInfoService.selectdListByCompanyName(mainOrg);
//            if(mainContractorInfos != null && mainContractorInfos.size()>0){
//                MeetingMainContractorInfo meetingMainContractorInfo = new MeetingMainContractorInfo();
//                BeanUtils.copyBeanProp(meetingMainContractorInfo,mainContractorInfos.get(0));
//                meetingMainContractorInfo.setMeetingId(meetingInfo.getId());
//                meetingMainContractorInfo.setId(null);//id 是自增的
//
//                UpdateWrapper<MeetingMainContractorInfo> updateWrapper = new UpdateWrapper<>();
//                updateWrapper.lambda().eq(MeetingMainContractorInfo::getMeetingId,meetingInfo.getId());
//                updateWrapper.lambda().eq(MeetingMainContractorInfo::getCompanyName,mainOrg);
//                meetingMainContractorInfoService.saveOrUpdate(meetingMainContractorInfo,updateWrapper);//主办公司与会议id关联，记录到表 meeting_main_contractor_info
//            }
            //------------------------
            Map<String, Object> obj = new HashMap<String, Object>();
            obj.put("meetingId", meetingInfo.getId());
            return AjaxResult.success(obj);
        } else {
            return AjaxResult.error();
        }
    }

    /**
     * 修改会议基本信息
     */
    @ApiOperation("修改会议基本信息")
    @RequiresPermissions("manage:meetingInfo:edit")
    @Log(title = "会议基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingInfo meetingInfo) {
        MeetingInfo info = meetingInfoService.getById(meetingInfo.getId());
        meetingInfo.setUpdatedBy(SecurityUtils.getUsername());
        meetingInfo.setUpdatedTime(DateUtils.getNowDate());
        boolean b = meetingInfoService.updateById(meetingInfo);
        if (b) {
            String mainOrg = meetingInfo.getMainOrg();
            List<MainContractorInfo> mainContractorInfos = mainContractorInfoService.selectdListByCompanyName(mainOrg);//主办信息
            VenueInfo venueInfo = iVenueInfoService.getById(meetingInfo.getVenueId());

            if (mainContractorInfos != null && mainContractorInfos.size() > 0) {
                //主办公司与会议关联，保存到表 meeting_main_contractor_info
                MeetingMainContractorInfo meetingMainContractorInfo = new MeetingMainContractorInfo();
                BeanUtils.copyBeanProp(meetingMainContractorInfo, mainContractorInfos.get(0));
                meetingMainContractorInfo.setMeetingId(meetingInfo.getId());
                meetingMainContractorInfo.setId(null);//id 是自增的
                meetingMainContractorInfo.setUpdatedTime(DateUtils.getNowDate());
                UpdateWrapper<MeetingMainContractorInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(MeetingMainContractorInfo::getMeetingId, meetingInfo.getId());
                updateWrapper.lambda().eq(MeetingMainContractorInfo::getCompanyName, mainOrg);
                meetingMainContractorInfoService.saveOrUpdate(meetingMainContractorInfo, updateWrapper);

                //场馆和会议关联,保存到表meeting_venue_info
                MeetingVenueInfo meetingVenueInfo = new MeetingVenueInfo();
                BeanUtils.copyBeanProp(meetingVenueInfo,venueInfo);
                meetingVenueInfo.setMeetingId(meetingInfo.getId());
                meetingVenueInfo.setId(null);//id 是自增的
                meetingVenueInfo.setUpdatedTime(DateUtils.getNowDate());
                UpdateWrapper<MeetingVenueInfo> updateVenueWrapper = new UpdateWrapper<>();
                updateVenueWrapper.lambda().eq(MeetingVenueInfo::getMeetingId, meetingInfo.getId());
                updateVenueWrapper.lambda().eq(MeetingVenueInfo::getCompanyName, venueInfo.getCompanyName());
                iMeetingVenueInfoService.saveOrUpdate(meetingVenueInfo, updateVenueWrapper);

            }
        }

        //防疫申报发送短信提醒
        //申报通过给展会负责人发送短信
        if ("3".equals(meetingInfo.getPreventionDeclareStatus()) && !"3".equals(info.getPreventionDeclareStatus())) {
            List<MainContractorInfo> mainContractorInfos = mainContractorInfoService.selectdListByCompanyName(info.getMainOrg());
            if (mainContractorInfos != null && mainContractorInfos.size() > 0 && StringUtils.isNotEmpty(mainContractorInfos.get(0).getChargePersonPhone())) {
                SmsSendVo smsSendVo = new SmsSendVo();
                smsSendVo.setTemplateId(114L);
                smsSendVo.setMeetingId(info.getId());
                smsSendVo.setName(mainContractorInfos.get(0).getChargePerson());
                smsSendVo.setPhone(mainContractorInfos.get(0).getChargePersonPhone());
                smsSendVo.setType("1");
                AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
                if ((int) ajaxResult.get("code") != 200) {
                    throw new BizException(500, "发送防疫申报成功短信提醒失败！");
                }
            }
        }

        //申报中给政务审批人发送短信
        if ("3".equals(meetingInfo.getMeetingStatus()) && !"3".equals(info.getMeetingStatus())) {
            List<MainContractorInfo> mainContractorInfos = mainContractorInfoService.selectdListByCompanyName(info.getMainOrg());
            if (mainContractorInfos != null && mainContractorInfos.size() > 0 && StringUtils.isNotEmpty(mainContractorInfos.get(0).getChargePersonPhone())) {
                SmsSendVo smsSendVo = new SmsSendVo();
                smsSendVo.setTemplateId(115L);
                smsSendVo.setMeetingId(info.getId());
                smsSendVo.setName(mainContractorInfos.get(0).getChargePerson());
                smsSendVo.setPhone(mainContractorInfos.get(0).getChargePersonPhone());
                smsSendVo.setType("1");
                AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
                if ((int) ajaxResult.get("code") != 200) {
                    throw new BizException(500, "发送审批通过成功短信提醒失败！");
                }
            }
        }

        return toAjax(b);
    }

    /**
     * 删除会议基本信息
     */
    @ApiOperation("删除会议基本信息")
    @RequiresPermissions("manage:meetingInfo:remove")
    @Log(title = "会议基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingInfoService.removeByIds(ids));
    }
}
