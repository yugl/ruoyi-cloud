package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 问卷调查配置对象 app_questionnaire_config
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class AppQuestionnaireConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 问卷名称
     */
    @Excel(name = "问卷名称")
    private String questionnaireName;

    /**
     * 状态 0：启用 1：停用
     */
    @Excel(name = "状态 0：启用 1：停用")
    private String enableStatus;

    /**
     * 状态 0：有效 1：无效
     */
    @TableLogic
    @Excel(name = "状态 0：有效 1：无效")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


}
