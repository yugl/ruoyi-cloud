package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.ServiceProviderInfo;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.service.IServiceProviderInfoService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingServiceProviderInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderInfoDto;
import com.ruoyi.manage.service.IMeetingServiceProviderInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 会议服务商信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议服务商信息控制器", tags = {"会议服务商信息管理"})
@RestController
@RequestMapping("/meetingServiceProvider")
public class MeetingServiceProviderInfoController extends BaseController {
    @Autowired
    private IMeetingServiceProviderInfoService meetingServiceProviderInfoService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private IServiceProviderInfoService serviceProviderInfoService;

    /**
     * 查询会议服务商信息列表
     */
    @ApiOperation("查询会议服务商信息列表")
    @RequiresPermissions("manage:meetingServiceProvider:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingServiceProviderInfoDto meetingServiceProviderInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            meetingServiceProviderInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }
        startPage();
        List<MeetingServiceProviderInfo> list = meetingServiceProviderInfoService.selectdList(meetingServiceProviderInfo);
        return getDataTable(list);
    }

    /**
     * 导出会议服务商信息列表
     */
    @ApiOperation("导出会议服务商信息列表")
    @RequiresPermissions("manage:meetingServiceProvider:export")
    @Log(title = "会议服务商信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingServiceProviderInfoDto meetingServiceProviderInfo) throws IOException {
        startPage();
        List<MeetingServiceProviderInfo> list = meetingServiceProviderInfoService.selectdList(meetingServiceProviderInfo);
        ExcelUtil<MeetingServiceProviderInfo> util = new ExcelUtil<MeetingServiceProviderInfo>(MeetingServiceProviderInfo.class);
        util.exportExcel(response, list, "会议服务商信息数据");
    }

    /**
     * 导入会议服务商信息
     */
    @ApiOperation("导入会议服务商信息列表")
    @RequiresPermissions("manage:meetingServiceProvider:importData")
    @Log(title = "会议服务商信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,Long meetingId) throws Exception {
        ExcelUtil<MeetingServiceProviderInfo> util = new ExcelUtil<MeetingServiceProviderInfo>(MeetingServiceProviderInfo.class);
        List<MeetingServiceProviderInfo> list = util.importExcel(file.getInputStream());
        List<ServiceProviderInfo> serviceList = new ArrayList<>();
        for(MeetingServiceProviderInfo mspi:list){
            mspi.setCreatedTime(DateUtils.getNowDate());
            mspi.setCreatedBy(SecurityUtils.getUsername());
            mspi.setMeetingId(meetingId);

            ServiceProviderInfo spi = new ServiceProviderInfo();
            BeanUtils.copyBeanProp(spi,mspi);
            serviceList.add(spi);
        }

        boolean message = false;
        if (updateSupport) {
            //批量更新会存在问题 如果重复提交，会有重复记录
            //message = meetingServiceProviderInfoService.saveOrUpdateBatch(list);
            //serviceProviderInfoService.saveOrUpdateBatch(serviceList);
            for(MeetingServiceProviderInfo ms:list){
                UpdateWrapper<MeetingServiceProviderInfo>  updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(MeetingServiceProviderInfo::getMeetingId,meetingId);
                updateWrapper.lambda().eq(MeetingServiceProviderInfo::getCompanyName,ms.getCompanyName());
                updateWrapper.lambda().eq(MeetingServiceProviderInfo::getTaxpayerIdentifierNum,ms.getTaxpayerIdentifierNum());
                meetingServiceProviderInfoService.saveOrUpdate(ms,updateWrapper);
            }
            for(ServiceProviderInfo s:serviceList){
                UpdateWrapper<ServiceProviderInfo>  updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(ServiceProviderInfo::getCompanyName,s.getCompanyName());
                updateWrapper.lambda().eq(ServiceProviderInfo::getTaxpayerIdentifierNum,s.getTaxpayerIdentifierNum());
                serviceProviderInfoService.saveOrUpdate(s,updateWrapper);
            }
        } else {
            message = meetingServiceProviderInfoService.saveBatch(list);
            serviceProviderInfoService.saveBatch(serviceList);
        }

        //入库成功，下发短信
        for(MeetingServiceProviderInfo mspi:list){
            registerUserService.registerUser(mspi.getChargePersonPhone(), mspi.getChargePerson(), mspi.getCompanyName(), PersonRole.SERVICE_PROVIDER.getIndex());
        }

        return AjaxResult.success(message);
    }

    /**
     * 获取会议服务商信息详细信息
     */
    @ApiOperation("获取会议服务商信息详细信息")
    @RequiresPermissions("manage:meetingServiceProvider:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingServiceProviderInfoService.getById(id));
    }

    /**
     * 新增会议服务商信息
     */
    @ApiOperation("新增会议服务商信息")
    @RequiresPermissions("manage:meetingServiceProvider:add")
    @Log(title = "会议服务商信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingServiceProviderInfo meetingServiceProviderInfo) {
        if (UserConstants.NOT_UNIQUE.equals(meetingServiceProviderInfoService.checkNameUnique(meetingServiceProviderInfo.getMeetingId(),meetingServiceProviderInfo.getCompanyName()))) {
            return AjaxResult.error("新增会议服务商信息'" + meetingServiceProviderInfo.getCompanyName() + "'失败，公司名称已存在!");
        }
        meetingServiceProviderInfo.setCreatedTime(DateUtils.getNowDate());
        meetingServiceProviderInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(meetingServiceProviderInfo.getChargePersonPhone(), meetingServiceProviderInfo.getChargePerson(), meetingServiceProviderInfo.getCompanyName(), PersonRole.SERVICE_PROVIDER.getIndex());
        return toAjax(meetingServiceProviderInfoService.save(meetingServiceProviderInfo));
    }

    /**
     * 修改会议服务商信息
     */
    @ApiOperation("修改会议服务商信息")
    @RequiresPermissions("manage:meetingServiceProvider:edit")
    @Log(title = "会议服务商信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingServiceProviderInfo meetingServiceProviderInfo) {
        return toAjax(meetingServiceProviderInfoService.updateById(meetingServiceProviderInfo));
    }

    /**
     * 删除会议服务商信息
     */
    @ApiOperation("删除会议服务商信息")
    @RequiresPermissions("manage:meetingServiceProvider:remove")
    @Log(title = "会议服务商信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingServiceProviderInfoService.removeByIds(ids));
    }
}
