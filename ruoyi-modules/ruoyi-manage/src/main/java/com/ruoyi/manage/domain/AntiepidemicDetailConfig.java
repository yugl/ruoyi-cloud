package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 防疫填报详情对象 antiepidemic_detail_config
 *
 * @author ruoyi
 * @date 2022-04-23
 */
@Data
public class AntiepidemicDetailConfig {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 主表ID
     */
    @NotNull(message = "防疫填报ID不能为空")
    @Excel(name = "主表ID")
    private Long antiepidemicId;

    /**
     * 标题
     */
    @NotNull(message = "标题不能为空")
    @Excel(name = "标题")
    private String name;

    /**
     * 1：是否 2：文本
     */
    @Excel(name = "1：是否 2：文本")
    private String type;

    /**
     * 排序
     */
    private int sortNo;

    /**
     * 状态
     */
    @TableLogic
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updatedTime;

}
