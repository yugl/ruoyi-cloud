package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.VenueBoothInfo;
import com.ruoyi.manage.domain.VenueInfo;
import com.ruoyi.manage.dto.VenueBoothInfoDto;

/**
 * 场馆展位信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IVenueBoothInfoService extends IService<VenueBoothInfo> {


    /**
     * 查询场馆展位信息列表
     *
     * @param venueBoothInfo 场馆展位信息
     * @return 场馆展位信息集合
     */
    public List<VenueBoothInfo> selectdList(VenueBoothInfoDto venueBoothInfo);

    public String checkNameUnique(Long venueId,String venueBoothName);

    List<VenueBoothInfo> selectdListByVenueIdAndVenueBoothName(Long venueId,String venueBoothName);

}
