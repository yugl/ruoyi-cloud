package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingDisinfectFile;

/**
 * 会议消毒记录文件Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-31
 */
public interface MeetingDisinfectFileMapper extends BaseMapper<MeetingDisinfectFile> {


}
