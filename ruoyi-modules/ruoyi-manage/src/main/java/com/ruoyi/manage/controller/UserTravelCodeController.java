package com.ruoyi.manage.controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.UserTravelCode;
import com.ruoyi.manage.dto.UserTravelCodeDto;
import com.ruoyi.manage.service.IUserTravelCodeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 行程码Controller
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Api(value = "行程码控制器", tags = {"行程码管理"})
@RestController
@RequestMapping("/travelCode")
public class UserTravelCodeController extends BaseController {
    @Autowired
    private IUserTravelCodeService userTravelCodeService;

/**
 * 查询行程码列表
 */
@ApiOperation("查询行程码列表")
@RequiresPermissions("manage:travelCode:list")
@GetMapping("/list")
    public TableDataInfo list(UserTravelCodeDto userTravelCode) {
        startPage();
        List<UserTravelCode> list = userTravelCodeService.selectdList(userTravelCode);
        return getDataTable(list);
    }

    /**
     * 导出行程码列表
     */
    @ApiOperation("导出行程码列表")
    @RequiresPermissions("manage:travelCode:export")
    @Log(title = "行程码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserTravelCodeDto userTravelCode) throws IOException {

        startPage();
        List<UserTravelCode> list = userTravelCodeService.selectdList(userTravelCode);
        ExcelUtil<UserTravelCode> util = new ExcelUtil<UserTravelCode>(UserTravelCode. class);
        util.exportExcel(response, list, "行程码数据");
    }

    /**
     * 导入行程码
     */
    @ApiOperation("导出行程码列表")
    @RequiresPermissions("manage:travelCode:importData")
    @Log(title = "行程码导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<UserTravelCode> util = new ExcelUtil< UserTravelCode>( UserTravelCode. class);
        List<UserTravelCode> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = userTravelCodeService.saveOrUpdateBatch(list);
        } else {
            message = userTravelCodeService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取行程码详细信息
     */
    @ApiOperation("获取行程码详细信息")
    @RequiresPermissions("manage:travelCode:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(userTravelCodeService.getById(id));
    }

    /**
     * 新增行程码
     */
    @ApiOperation("新增行程码")
    @RequiresPermissions("manage:travelCode:add")
    @Log(title = "行程码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserTravelCode userTravelCode) {
        return toAjax(userTravelCodeService.save(userTravelCode));
    }

    /**
     * 修改行程码
     */
    @ApiOperation("修改行程码")
    @RequiresPermissions("manage:travelCode:edit")
    @Log(title = "行程码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserTravelCode userTravelCode) {
        return toAjax(userTravelCodeService.updateById(userTravelCode));
    }

    /**
     * 删除行程码
     */
    @ApiOperation("删除行程码")
    @RequiresPermissions("manage:travelCode:remove")
    @Log(title = "行程码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(userTravelCodeService.removeByIds(ids));
    }
}
