package com.ruoyi.manage.controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.UserNuclein;
import com.ruoyi.manage.dto.UserNucleinDto;
import com.ruoyi.manage.service.IUserNucleinService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 核酸检测Controller
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Api(value = "核酸检测控制器", tags = {"核酸检测管理"})
@RestController
@RequestMapping("/nuclein")
public class UserNucleinController extends BaseController {
    @Autowired
    private IUserNucleinService userNucleinService;

/**
 * 查询核酸检测列表
 */
@ApiOperation("查询核酸检测列表")
@RequiresPermissions("manage:nuclein:list")
@GetMapping("/list")
    public TableDataInfo list(UserNucleinDto userNuclein) {
        startPage();
        List<UserNuclein> list = userNucleinService.selectdList(userNuclein);
        return getDataTable(list);
    }

    /**
     * 导出核酸检测列表
     */
    @ApiOperation("导出核酸检测列表")
    @RequiresPermissions("manage:nuclein:export")
    @Log(title = "核酸检测", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserNucleinDto userNuclein) throws IOException {

        startPage();
        List<UserNuclein> list = userNucleinService.selectdList(userNuclein);
        ExcelUtil<UserNuclein> util = new ExcelUtil<UserNuclein>(UserNuclein. class);
        util.exportExcel(response, list, "核酸检测数据");
    }

    /**
     * 导入核酸检测
     */
    @ApiOperation("导出核酸检测列表")
    @RequiresPermissions("manage:nuclein:importData")
    @Log(title = "核酸检测导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<UserNuclein> util = new ExcelUtil< UserNuclein>( UserNuclein. class);
        List<UserNuclein> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = userNucleinService.saveOrUpdateBatch(list);
        } else {
            message = userNucleinService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取核酸检测详细信息
     */
    @ApiOperation("获取核酸检测详细信息")
    @RequiresPermissions("manage:nuclein:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(userNucleinService.getById(id));
    }

    /**
     * 新增核酸检测
     */
    @ApiOperation("新增核酸检测")
    @RequiresPermissions("manage:nuclein:add")
    @Log(title = "核酸检测", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserNuclein userNuclein) {
        return toAjax(userNucleinService.save(userNuclein));
    }

    /**
     * 修改核酸检测
     */
    @ApiOperation("修改核酸检测")
    @RequiresPermissions("manage:nuclein:edit")
    @Log(title = "核酸检测", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserNuclein userNuclein) {
        return toAjax(userNucleinService.updateById(userNuclein));
    }

    /**
     * 删除核酸检测
     */
    @ApiOperation("删除核酸检测")
    @RequiresPermissions("manage:nuclein:remove")
    @Log(title = "核酸检测", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(userNucleinService.removeByIds(ids));
    }
}
