package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MeetingServiceProviderInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderInfoDto;
import com.ruoyi.manage.mapper.MeetingServiceProviderInfoMapper;
import com.ruoyi.manage.service.IMeetingServiceProviderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 会议服务商信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingServiceProviderInfoServiceImpl extends ServiceImpl<MeetingServiceProviderInfoMapper, MeetingServiceProviderInfo> implements IMeetingServiceProviderInfoService {
    @Autowired
    private MeetingServiceProviderInfoMapper meetingServiceProviderInfoMapper;

    /**
     * 查询会议服务商信息列表
     *
     * @param meetingServiceProviderInfo 会议服务商信息
     * @return 会议服务商信息
     */
    @Override
    public List<MeetingServiceProviderInfo> selectdList(MeetingServiceProviderInfoDto meetingServiceProviderInfo) {
        QueryWrapper<MeetingServiceProviderInfo> q = new QueryWrapper<>();
        if (meetingServiceProviderInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingServiceProviderInfo.getMeetingId());
        }
        if (meetingServiceProviderInfo.getCompanyName() != null && !meetingServiceProviderInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", meetingServiceProviderInfo.getCompanyName());
        }
        if (meetingServiceProviderInfo.getTaxpayerIdentifierNum() != null && !meetingServiceProviderInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", meetingServiceProviderInfo.getTaxpayerIdentifierNum());
        }
        if (meetingServiceProviderInfo.getIndustry() != null && !meetingServiceProviderInfo.getIndustry().trim().equals("")) {
            q.eq("industry", meetingServiceProviderInfo.getIndustry());
        }
        if (meetingServiceProviderInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", meetingServiceProviderInfo.getIncorporationDate());
        }
        if (meetingServiceProviderInfo.getRegisteredCapital() != null && !meetingServiceProviderInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", meetingServiceProviderInfo.getRegisteredCapital());
        }
        if (meetingServiceProviderInfo.getChargePerson() != null && !meetingServiceProviderInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", meetingServiceProviderInfo.getChargePerson());
        }
        if (meetingServiceProviderInfo.getChargePersonPhone() != null && !meetingServiceProviderInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", meetingServiceProviderInfo.getChargePersonPhone());
        }
        if (meetingServiceProviderInfo.getPreventionPerson() != null && !meetingServiceProviderInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", meetingServiceProviderInfo.getPreventionPerson());
        }
        if (meetingServiceProviderInfo.getPreventionPersonPhone() != null && !meetingServiceProviderInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", meetingServiceProviderInfo.getPreventionPersonPhone());
        }
        if (meetingServiceProviderInfo.getAccountManager() != null && !meetingServiceProviderInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", meetingServiceProviderInfo.getAccountManager());
        }
        if (meetingServiceProviderInfo.getAccountManagerPhone() != null && !meetingServiceProviderInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", meetingServiceProviderInfo.getAccountManagerPhone());
        }
        if (meetingServiceProviderInfo.getAccountManagerUsername() != null && !meetingServiceProviderInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", meetingServiceProviderInfo.getAccountManagerUsername());
        }
        if (meetingServiceProviderInfo.getBusinessLicenseUrl() != null && !meetingServiceProviderInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", meetingServiceProviderInfo.getBusinessLicenseUrl());
        }
        if (meetingServiceProviderInfo.getBusinessNature() != null && !meetingServiceProviderInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", meetingServiceProviderInfo.getBusinessNature());
        }
        if (meetingServiceProviderInfo.getStatus() != null && !meetingServiceProviderInfo.getStatus().trim().equals("")) {
            q.eq("status", meetingServiceProviderInfo.getStatus());
        }
        if (meetingServiceProviderInfo.getCreatedBy() != null && !meetingServiceProviderInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingServiceProviderInfo.getCreatedBy());
        }
        if (meetingServiceProviderInfo.getCreatedTime() != null) {
            q.eq("created_time", meetingServiceProviderInfo.getCreatedTime());
        }
        if (meetingServiceProviderInfo.getUpdatedBy() != null && !meetingServiceProviderInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingServiceProviderInfo.getUpdatedBy());
        }
        if (meetingServiceProviderInfo.getUpdatedTime() != null) {
            q.eq("updated_time", meetingServiceProviderInfo.getUpdatedTime());
        }
        List<MeetingServiceProviderInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<Long> selectMeetingIdsByCompanyName(String companyName) {
        QueryWrapper<MeetingServiceProviderInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingServiceProviderInfo::getCompanyName, companyName);
        List<MeetingServiceProviderInfo> list = this.baseMapper.selectList(q);
        return list.stream().map(item -> item.getMeetingId()).collect(Collectors.toList());
    }

    @Override
    public String checkNameUnique(Long meetingId, String companyName) {
        QueryWrapper<MeetingServiceProviderInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingServiceProviderInfo::getCompanyName, companyName);
        q.lambda().eq(MeetingServiceProviderInfo::getMeetingId, meetingId);
        List<MeetingServiceProviderInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


}
