package com.ruoyi.manage.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.manage.domain.*;
import com.ruoyi.manage.dto.SmsSendRecordDto;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.exception.base.BizException;
import com.ruoyi.manage.mapper.MeetingInfoMapper;
import com.ruoyi.manage.mapper.MeetingUserHealthManageMapper;
import com.ruoyi.manage.mapper.SmsNoticeConfigMapper;
import com.ruoyi.manage.properties.SmsConfigProperties;
import com.ruoyi.manage.service.IMeetingRuleConfigService;
import com.ruoyi.manage.service.IMeetingSmsTemplateService;
import com.ruoyi.manage.service.ISmsTemplateConfigService;
import com.ruoyi.manage.service.impl.SmsSendRecordServiceImpl;
import com.ruoyi.system.api.RemoteInterfaceService;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysDictData;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SmsUtils {

    @Autowired
    RedisService redisService;

    @Autowired
    RemoteInterfaceService remoteInterfaceService;

    @Autowired
    RemoteUserService remoteUserService;

    @Autowired
    ISmsTemplateConfigService smsTemplateConfigService;

    @Autowired
    IMeetingSmsTemplateService meetingSmsTemplateService;

    @Autowired
    SmsConfigProperties smsConfigProperties;

    @Autowired
    MeetingInfoMapper meetingInfoMapper;

    @Autowired
    IMeetingRuleConfigService meetingRuleConfigService;

    @Autowired
    MeetingUserHealthManageMapper meetingUserHealthManageMapper;

    @Autowired
    SmsSendRecordServiceImpl smsSendRecordService;

    @Autowired
    SmsNoticeConfigMapper smsNoticeConfigMapper;

    @Autowired
    ThreadPoolExecutor executor;

    /**
     * 通用发送短信
     *
     * @return
     */
    public AjaxResult smsSend(SmsSendVo smsSendVo) {
        //判断是平台还是会议使用
        String content = null;
        if ("2".equals(smsSendVo.getType())) {
            content = meetingSmsTemplateService.getSmsContent(smsSendVo.getMeetingId(), smsSendVo.getTemplateId());
        } else {
            content = smsTemplateConfigService.getSmsContent(smsSendVo.getTemplateId());
        }
        //判断map中是否有参数
        if (smsSendVo.getMap() != null) {
            content = StrUtil.format(content, smsSendVo.getMap());
        }

        Map<String, Object> paramMap = getParamMap(content, smsSendVo);
        content = StrUtil.format(content, paramMap);
        return smsSend(smsSendVo.getPhone(), content, smsSendVo);
    }

    /**
     * 发送短信
     *
     * @param mobile
     * @param content   短信内容
     * @param smsSendVo
     * @return
     */
    public AjaxResult smsSend(String mobile, String content, SmsSendVo smsSendVo) {
        MultiValueMap smsConfigMap = getSmsConfigMap();
        String smsSign = redisService.getCacheObject(Constants.SYS_CONFIG_KEY + "sms_sign");
        smsConfigMap.add("mobile", mobile);
        smsConfigMap.add("content", smsSign + content);
        String result;
        try {
            result = remoteInterfaceService.invokeRest("1001", "", smsConfigMap);
            log.info("结果：" + result);
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            throw new BizException(500, "接口服务失败");
        }
        JSONObject jsonObject = JSONObject.parseObject(result);
        String returnstatus = jsonObject.getJSONObject("returnsms").getString("returnstatus");
        if (!returnstatus.equals("Success")) {
            //发送失败后，保存发送短信记录
            saveSmsSendRecord(content, smsSendVo, "1");
            throw new BizException(500, "发送短信失败,提示：" + jsonObject.getJSONObject("returnsms").getString("message"));
        } else {
            //发送成功后，保存发送短信记录
            saveSmsSendRecord(content, smsSendVo, "0");
        }
        return AjaxResult.success();
    }

    /**
     * 短信配置信息
     *
     * @return
     */
    private MultiValueMap getSmsConfigMap() {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("action", smsConfigProperties.getAction());
        map.add("userid", smsConfigProperties.getUserid());
        map.add("account", smsConfigProperties.getAccount());
        map.add("password", smsConfigProperties.getPassword());
        return map;
    }

    /**
     * 短信内容数据信息
     */
    private Map<String, Object> getParamMap(String content, SmsSendVo smsSendVo) {
        Map<String, Object> map = new HashMap<>();
        Object cacheObject = redisService.getCacheObject(Constants.SYS_DICT_KEY + "sms_variable");
        List<SysDictData> list = JSON.parseArray(cacheObject.toString(), SysDictData.class);
        for (SysDictData obj : list) {
            if (StringUtils.isNotEmpty(obj.getRemark())) {
                map.put(obj.getDictLabel(), obj.getRemark());
            }
        }

        //封装公司信息
        if (StringUtils.isNotEmpty(smsSendVo.getCompanyName())) {
            R<LoginUser> userInfo = remoteUserService.getUserInfo(smsSendVo.getCompanyName(), SecurityConstants.INNER);
            if (userInfo != null) {
                LoginUser data = userInfo.getData();
                List<SysRole> roles = data.getSysUser().getRoles();
                List<String> roleName = roles.stream().map(SysRole::getRoleName).collect(Collectors.toList());
                map.put("角色名称", String.join(",", roleName));
            }
            map.put("公司名称", smsSendVo.getCompanyName());
        }

        //封装会议信息
        if (smsSendVo.getMeetingId() != null) {
            if (content.indexOf("{核酸规则}") != -1) {
                MeetingRuleConfig meetingRuleConfig = meetingRuleConfigService.getInfo(smsSendVo.getMeetingId());
                map.put("核酸规则", meetingRuleConfig.getNucleicAcidRule());
            }
            if (content.indexOf("{不符合数量人数}") != -1) {
                Long count = meetingUserHealthManageMapper.selectCount(new QueryWrapper<MeetingUserHealthManage>()
                        .isNotNull("health_abnormal_status")
                        .eq("status", "0")
                        .eq("meeting_id", smsSendVo.getMeetingId())
                );
                map.put("不符合数量人数", count);
            }

            if (content.indexOf("{会议名称}") != -1 || content.indexOf("{会议开始时间}") != -1 || content.indexOf("{会议详情地址}") != -1) {
                MeetingInfo meetingInfo = meetingInfoMapper.selectById(smsSendVo.getMeetingId());
                map.put("会议名称", meetingInfo.getMeetingName());
                map.put("会议开始时间", DateUtil.format(meetingInfo.getMeetingStartTime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("会议详情地址", meetingInfo.getH5Url());
            }
        }

        if (content.indexOf("{姓名}") != -1 && StringUtils.isNotEmpty(smsSendVo.getName())) {
            map.put("姓名", smsSendVo.getName());
        }

        //密码
        if (content.indexOf("{密码}") != -1) {
            map.put("密码", PassUtils.genPasswd());
        }
        return map;
    }

    /**
     * 保存发送短信记录
     *
     * @param content   短信内容
     * @param smsSendVo 参数
     * @param status    成功状态 0成功 1失败
     */
    private void saveSmsSendRecord(String content, SmsSendVo smsSendVo, String status) {
        SmsSendRecord obj = new SmsSendRecord();
        obj.setMeetingId(smsSendVo.getMeetingId());
        if (!"3".equals(smsSendVo.getType())) {
            obj.setSmsTemplateName("自定义短信");
        } else {
            obj.setSmsTemplateId(smsSendVo.getTemplateId());
        }
        obj.setPhone(smsSendVo.getPhone());
        obj.setName(smsSendVo.getName());
        obj.setContent(content);
        obj.setStatus(status);
        smsSendRecordService.save(obj);

        //校验会议短信是否超过使用率上限
        if (smsSendVo.getMeetingId() != null) {
            //异步线程
            CompletableFuture.runAsync(() -> {
                MeetingRuleConfig ruleConfig = meetingRuleConfigService.getInfo(smsSendVo.getMeetingId());
                SmsNoticeConfig noticeConfig = smsNoticeConfigMapper.selectOne(new QueryWrapper<SmsNoticeConfig>().eq("sms_key", "sms_use_rate"));
                if (ruleConfig.getSmsTotalNum() != null && StringUtils.isNotEmpty(noticeConfig.getSmsValue())) {
                    //计算当前会议短信使用率
                    Long sendCount = smsSendRecordService.getBaseMapper().selectCount(new QueryWrapper<SmsSendRecord>().eq("meeting_id", smsSendVo.getMeetingId()));
                    BigDecimal sendScale = NumberUtil.round(NumberUtil.mul(NumberUtil.div(sendCount, ruleConfig.getSmsTotalNum()), 100), 0);
                    //查询该会议今天是否已经发送过短信
                    SmsSendRecordDto smsSendRecordDto = new SmsSendRecordDto();
                    smsSendRecordDto.setBeginDate(DateUtil.format(new Date(), "yyyy-MM-dd"));
                    smsSendRecordDto.setSmsTemplateId("111");
                    smsSendRecordDto.setMeetingId(smsSendVo.getMeetingId());
                    smsSendRecordDto.setStatus("0");
                    List<SmsSendRecord> smsSendRecords = smsSendRecordService.selectdList(smsSendRecordDto);
                    //判断短信使用率是否超过上限
                    if (smsSendRecords.size() == 0 && sendScale.intValue() > Integer.parseInt(noticeConfig.getSmsValue())) {
                        MeetingInfo meetingInfo = meetingInfoMapper.selectById(smsSendVo.getMeetingId());
                        smsSendVo.setPhone(meetingInfo.getPreventionOfficerTel());
                        smsSendVo.setName(meetingInfo.getPreventionOfficer());
                        smsSendVo.setTemplateId(111L);
                        AjaxResult ajaxResult = smsSend(smsSendVo);
                    }
                }
            }, executor);
        }
    }

}
