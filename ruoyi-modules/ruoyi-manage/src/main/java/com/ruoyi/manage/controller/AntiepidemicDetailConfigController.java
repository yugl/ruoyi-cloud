package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;
import com.ruoyi.manage.dto.AntiepidemicDetailConfigDto;
import com.ruoyi.manage.service.IAntiepidemicDetailConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 防疫填报详情Controller
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Api(value = "防疫填报详情控制器", tags = {"防疫填报详情管理"})
@RestController
@RequestMapping("/antiepidemicDetail")
public class AntiepidemicDetailConfigController extends BaseController {
    @Autowired
    private IAntiepidemicDetailConfigService antiepidemicDetailConfigService;

    /**
     * 查询防疫填报详情列表
     */
    @ApiOperation("查询防疫填报详情列表")
    @RequiresPermissions("manage:antiepidemicDetail:list")
    @GetMapping("/list")
    public AjaxResult list(AntiepidemicDetailConfigDto antiepidemicDetailConfig) {
        List<AntiepidemicDetailConfig> list = antiepidemicDetailConfigService.selectdList(antiepidemicDetailConfig);
        return AjaxResult.success(list);
    }

    /**
     * 导出防疫填报详情列表
     */
    @ApiOperation("导出防疫填报详情列表")
    @RequiresPermissions("manage:antiepidemicDetail:export")
    @Log(title = "防疫填报详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AntiepidemicDetailConfigDto antiepidemicDetailConfig) throws IOException {
        startPage();
        List<AntiepidemicDetailConfig> list = antiepidemicDetailConfigService.selectdList(antiepidemicDetailConfig);
        ExcelUtil<AntiepidemicDetailConfig> util = new ExcelUtil<AntiepidemicDetailConfig>(AntiepidemicDetailConfig.class);
        util.exportExcel(response, list, "防疫填报详情数据");
    }

    /**
     * 导入防疫填报详情
     */
    @ApiOperation("导出防疫填报详情列表")
    @RequiresPermissions("manage:antiepidemicDetail:importData")
    @Log(title = "防疫填报详情导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AntiepidemicDetailConfig> util = new ExcelUtil<AntiepidemicDetailConfig>(AntiepidemicDetailConfig.class);
        List<AntiepidemicDetailConfig> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = antiepidemicDetailConfigService.saveOrUpdateBatch(list);
        } else {
            message = antiepidemicDetailConfigService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取防疫填报详情详细信息
     */
    @ApiOperation("获取防疫填报详情详细信息")
    @RequiresPermissions("manage:antiepidemicDetail:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(antiepidemicDetailConfigService.getById(id));
    }

    /**
     * 新增防疫填报详情
     */
    @ApiOperation("新增防疫填报详情")
    @RequiresPermissions("manage:antiepidemicDetail:add")
    @Log(title = "防疫填报详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AntiepidemicDetailConfig antiepidemicDetailConfig) {
        return toAjax(antiepidemicDetailConfigService.save(antiepidemicDetailConfig));
    }

    /**
     * 修改防疫填报详情
     */
    @ApiOperation("修改防疫填报详情")
    @RequiresPermissions("manage:antiepidemicDetail:edit")
    @Log(title = "防疫填报详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AntiepidemicDetailConfig antiepidemicDetailConfig) {
        return toAjax(antiepidemicDetailConfigService.updateById(antiepidemicDetailConfig));
    }

    /**
     * 删除防疫填报详情
     */
    @ApiOperation("删除防疫填报详情")
    @RequiresPermissions("manage:antiepidemicDetail:remove")
    @Log(title = "防疫填报详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(antiepidemicDetailConfigService.removeByIds(ids));
    }
}
