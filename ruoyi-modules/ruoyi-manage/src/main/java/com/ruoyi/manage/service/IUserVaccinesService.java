package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.UserVaccines;
import com.ruoyi.manage.dto.UserVaccinesDto;

/**
 * 用户疫苗记录Service接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface IUserVaccinesService extends IService<UserVaccines> {


    /**
     * 查询用户疫苗记录列表
     *
     * @param userVaccines 用户疫苗记录
     * @return 用户疫苗记录集合
     */
    public List<UserVaccines> selectdList(UserVaccinesDto userVaccines);

}
