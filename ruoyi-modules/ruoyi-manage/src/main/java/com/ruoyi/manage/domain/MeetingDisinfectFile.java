package com.ruoyi.manage.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议消毒记录文件对象 meeting_disinfect_file
 *
 * @author ruoyi
 * @date 2022-03-31
 */
@Data
public class MeetingDisinfectFile implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 消毒记录主表ID
     */
    @Excel(name = "消毒记录主表ID")
    private Long disinfectId;

    private String url;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;


}
