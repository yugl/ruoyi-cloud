package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AppPrivacyAggreement;
import com.ruoyi.manage.dto.AppPrivacyAggreementDto;
import com.ruoyi.manage.service.IAppPrivacyAggreementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 隐私协议设置Controller
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Api(value = "隐私协议设置控制器", tags = {"隐私协议设置管理"})
@RestController
@RequestMapping("/aggreement")
public class AppPrivacyAggreementController extends BaseController {
    @Autowired
    private IAppPrivacyAggreementService appPrivacyAggreementService;

    /**
     * 查询隐私协议设置列表
     */
    @ApiOperation("查询隐私协议设置列表")
    @RequiresPermissions("manage:aggreement:list")
    @GetMapping("/list")
    public TableDataInfo list(AppPrivacyAggreementDto appPrivacyAggreement) {
        startPage();
        List<AppPrivacyAggreement> list = appPrivacyAggreementService.selectdList(appPrivacyAggreement);
        return getDataTable(list);
    }

    /**
     * 导出隐私协议设置列表
     */
    @ApiOperation("导出隐私协议设置列表")
    @RequiresPermissions("manage:aggreement:export")
    @Log(title = "隐私协议设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppPrivacyAggreementDto appPrivacyAggreement) throws IOException {
        startPage();
        List<AppPrivacyAggreement> list = appPrivacyAggreementService.selectdList(appPrivacyAggreement);
        ExcelUtil<AppPrivacyAggreement> util = new ExcelUtil<AppPrivacyAggreement>(AppPrivacyAggreement.class);
        util.exportExcel(response, list, "隐私协议设置数据");
    }

    /**
     * 导入隐私协议设置
     */
    @ApiOperation("导出隐私协议设置列表")
    @RequiresPermissions("manage:aggreement:importData")
    @Log(title = "隐私协议设置导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AppPrivacyAggreement> util = new ExcelUtil<AppPrivacyAggreement>(AppPrivacyAggreement.class);
        List<AppPrivacyAggreement> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = appPrivacyAggreementService.saveOrUpdateBatch(list);
        } else {
            message = appPrivacyAggreementService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取隐私协议设置详细信息
     */
    @ApiOperation("获取隐私协议设置详细信息")
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo() {
        return appPrivacyAggreementService.getInfo();
    }

    /**
     * 新增隐私协议设置
     */
    @ApiOperation("新增隐私协议设置")
    @RequiresPermissions("manage:aggreement:add")
    @Log(title = "隐私协议设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppPrivacyAggreement appPrivacyAggreement) {
        return toAjax(appPrivacyAggreementService.save(appPrivacyAggreement));
    }

    /**
     * 修改隐私协议设置
     */
    @ApiOperation("修改隐私协议设置")
    @RequiresPermissions("manage:aggreement:edit")
    @Log(title = "隐私协议设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppPrivacyAggreement appPrivacyAggreement) {
        return toAjax(appPrivacyAggreementService.updateById(appPrivacyAggreement));
    }

    /**
     * 删除隐私协议设置
     */
    @ApiOperation("删除隐私协议设置")
    @RequiresPermissions("manage:aggreement:remove")
    @Log(title = "隐私协议设置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(appPrivacyAggreementService.removeByIds(ids));
    }
}
