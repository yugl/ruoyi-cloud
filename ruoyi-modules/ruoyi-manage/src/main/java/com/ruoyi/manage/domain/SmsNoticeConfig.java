package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信使用率提醒对象 sms_notice_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsNoticeConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 短信配置key
     */
    private String smsKey;

    /**
     * 短信配置值
     */
    @Excel(name = "短信使用率提醒")
    private String smsValue;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


}
