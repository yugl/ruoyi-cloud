package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppUserQuestionnaireInfo;
import com.ruoyi.manage.dto.AppUserQuestionnaireInfoDto;

/**
 * 用户问卷调查信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IAppUserQuestionnaireInfoService extends IService<AppUserQuestionnaireInfo> {


    /**
     * 查询用户问卷调查信息列表
     *
     * @param appUserQuestionnaireInfo 用户问卷调查信息
     * @return 用户问卷调查信息集合
     */
    public List<AppUserQuestionnaireInfo> selectdList(AppUserQuestionnaireInfoDto appUserQuestionnaireInfo);

    AjaxResult addQuestionnaire(AppUserQuestionnaireInfoDto dto);

    AjaxResult getInfo(Long id);
}
