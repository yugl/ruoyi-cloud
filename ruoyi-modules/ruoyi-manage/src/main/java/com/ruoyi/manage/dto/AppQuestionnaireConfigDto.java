package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 问卷调查配置对象 app_questionnaire_config
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class AppQuestionnaireConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 问卷名称
     */
    private String questionnaireName;

    /**
     * 启用状态 0：启用 1：不启用
     */
    private String enableStatus;

}
