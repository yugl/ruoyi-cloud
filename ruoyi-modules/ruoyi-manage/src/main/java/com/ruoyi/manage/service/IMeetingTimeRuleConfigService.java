package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingTimeRuleConfig;
import com.ruoyi.manage.dto.MeetingTimeRuleConfigDto;

/**
 * 会议入场时间规则Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingTimeRuleConfigService extends IService<MeetingTimeRuleConfig> {


    /**
     * 查询会议入场时间规则列表
     *
     * @param meetingTimeRuleConfig 会议入场时间规则
     * @return 会议入场时间规则集合
     */
    public List<MeetingTimeRuleConfig> selectdList(MeetingTimeRuleConfigDto meetingTimeRuleConfig);

    AjaxResult add(MeetingTimeRuleConfig meetingTimeRuleConfig);
}
