package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AppSubjectDetailInfo;
import com.ruoyi.manage.dto.AppSubjectDetailInfoDto;
import com.ruoyi.manage.service.IAppSubjectDetailInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 问卷题目详情Controller
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Api(value = "问卷题目详情控制器", tags = {"问卷题目详情管理"})
@RestController
@RequestMapping("/subjectDetailInfo")
public class AppSubjectDetailInfoController extends BaseController {
    @Autowired
    private IAppSubjectDetailInfoService appSubjectDetailInfoService;

    /**
     * 查询问卷题目详情列表
     */
    @ApiOperation("查询问卷题目详情列表")
    @GetMapping("/list")
    public TableDataInfo list(AppSubjectDetailInfoDto appSubjectDetailInfo) {
        startPage();
        List<AppSubjectDetailInfo> list = appSubjectDetailInfoService.selectdList(appSubjectDetailInfo);
        return getDataTable(list);
    }

    /**
     * 导出问卷题目详情列表
     */
    @ApiOperation("导出问卷题目详情列表")
    @Log(title = "问卷题目详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppSubjectDetailInfoDto appSubjectDetailInfo) throws IOException {

        startPage();
        List<AppSubjectDetailInfo> list = appSubjectDetailInfoService.selectdList(appSubjectDetailInfo);
        ExcelUtil<AppSubjectDetailInfo> util = new ExcelUtil<AppSubjectDetailInfo>(AppSubjectDetailInfo.class);
        util.exportExcel(response, list, "问卷题目详情数据");
    }

    /**
     * 导入问卷题目详情
     */
    @ApiOperation("导出问卷题目详情列表")
    @Log(title = "问卷题目详情导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AppSubjectDetailInfo> util = new ExcelUtil<AppSubjectDetailInfo>(AppSubjectDetailInfo.class);
        List<AppSubjectDetailInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = appSubjectDetailInfoService.saveOrUpdateBatch(list);
        } else {
            message = appSubjectDetailInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取问卷题目详情详细信息
     */
    @ApiOperation("获取问卷题目详情详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(appSubjectDetailInfoService.getById(id));
    }

    /**
     * 新增问卷题目详情
     */
    @ApiOperation("新增问卷题目详情")
    @Log(title = "问卷题目详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppSubjectDetailInfo appSubjectDetailInfo) {
        return toAjax(appSubjectDetailInfoService.save(appSubjectDetailInfo));
    }

    /**
     * 修改问卷题目详情
     */
    @ApiOperation("修改问卷题目详情")
    @Log(title = "问卷题目详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppSubjectDetailInfo appSubjectDetailInfo) {
        return toAjax(appSubjectDetailInfoService.updateById(appSubjectDetailInfo));
    }

    /**
     * 删除问卷题目详情
     */
    @ApiOperation("删除问卷题目详情")
    @Log(title = "问卷题目详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(appSubjectDetailInfoService.removeByIds(ids));
    }
}
