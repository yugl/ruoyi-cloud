package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 行程码对象 user_travel_code
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Data
public class UserTravelCodeDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 地址
     */
    private String address;
    /**
     * 途径日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date travelDate;
    /**
     * 地区风险（1：低风险地区 2：中风险地区 3：高风险地区）
     */
    private String riskLevel;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

}
