package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 问卷题目详情对象 app_subject_detail_info
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class AppSubjectDetailInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主表ID
     */
    private Long subjectId;
}
