package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.ExhibitorInfo;
import com.ruoyi.manage.domain.MeetingExhibitorInfo;
import com.ruoyi.manage.domain.MeetingVenueBoothInfo;
import com.ruoyi.manage.dto.MeetingExhibitorInfoDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IExhibitorInfoService;
import com.ruoyi.manage.service.IMeetingExhibitorInfoService;
import com.ruoyi.manage.service.IMeetingVenueBoothInfoService;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 会议展商信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议展商信息控制器", tags = {"会议展商信息管理"})
@RestController
@RequestMapping("/meetingExhibitor")
public class MeetingExhibitorInfoController extends BaseController {
    @Autowired
    private IMeetingExhibitorInfoService meetingExhibitorInfoService;

    @Autowired
    private IExhibitorInfoService exhibitorInfoService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private IMeetingVenueBoothInfoService meetingVenueBoothInfoService;

    /**
     * 查询会议展商信息列表
     */
    @ApiOperation("查询会议展商信息列表")
    @RequiresPermissions("manage:meetingExhibitor:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingExhibitorInfoDto meetingExhibitorInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            meetingExhibitorInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }

        startPage();
        List<MeetingExhibitorInfo> list = meetingExhibitorInfoService.selectdList(meetingExhibitorInfo);
        for(MeetingExhibitorInfo info:list){
            List<MeetingVenueBoothInfo> booths = meetingVenueBoothInfoService.selectdLisbByExhibitorName(info.getMeetingId(),info.getCompanyName());
            if(booths != null && booths.size() >0){
                info.setBoothName(booths.get(0).getBoothName());
                info.setBoothOrderNum(booths.get(0).getBoothOrderNum());
            }
        }

        return getDataTable(list);
    }

    /**
     * 导出会议展商信息列表
     */
    @ApiOperation("导出会议展商信息列表")
    @RequiresPermissions("manage:meetingExhibitor:export")
    @Log(title = "会议展商信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingExhibitorInfoDto meetingExhibitorInfo) throws IOException {
        startPage();
        List<MeetingExhibitorInfo> list = meetingExhibitorInfoService.selectdList(meetingExhibitorInfo);
        ExcelUtil<MeetingExhibitorInfo> util = new ExcelUtil<MeetingExhibitorInfo>(MeetingExhibitorInfo.class);
        util.exportExcel(response, list, "会议展商信息数据");
    }

    /**
     * 导入会议展商信息
     */
    @ApiOperation("导入会议展商信息列表")
    @RequiresPermissions("manage:meetingExhibitor:importData")
    @Log(title = "会议展商信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,Long meetingId) throws Exception {
        ExcelUtil<MeetingExhibitorInfo> util = new ExcelUtil<MeetingExhibitorInfo>(MeetingExhibitorInfo.class);
        List<MeetingExhibitorInfo> list = util.importExcel(file.getInputStream());
        List<ExhibitorInfo> exhibitorList = new ArrayList<>();
        for(MeetingExhibitorInfo mei:list){
            mei.setCreatedTime(DateUtils.getNowDate());
            mei.setCreatedBy(SecurityUtils.getUsername());
            mei.setMeetingId(meetingId);

            ExhibitorInfo ei = new ExhibitorInfo();
            BeanUtils.copyBeanProp(ei,mei);
            exhibitorList.add(ei);
        }
        boolean message = false;
        if (updateSupport) {
            //批量更新会存在问题 如果重复提交，会有重复记录
            //message = meetingExhibitorInfoService.saveOrUpdateBatch(list);
            //exhibitorInfoService.saveOrUpdateBatch(exhibitorList);
            for(MeetingExhibitorInfo me:list){
                UpdateWrapper<MeetingExhibitorInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(MeetingExhibitorInfo::getMeetingId,meetingId);
                updateWrapper.lambda().eq(MeetingExhibitorInfo::getCompanyName,me.getCompanyName());
                updateWrapper.lambda().eq(MeetingExhibitorInfo::getTaxpayerIdentifierNum,me.getTaxpayerIdentifierNum());
                meetingExhibitorInfoService.saveOrUpdate(me,updateWrapper);
            }

            for(ExhibitorInfo e:exhibitorList){
                UpdateWrapper<ExhibitorInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(ExhibitorInfo::getCompanyName,e.getCompanyName());
                updateWrapper.lambda().eq(ExhibitorInfo::getTaxpayerIdentifierNum,e.getTaxpayerIdentifierNum());
                exhibitorInfoService.saveOrUpdate(e,updateWrapper);
            }

        } else {
            message = meetingExhibitorInfoService.saveBatch(list);
            exhibitorInfoService.saveBatch(exhibitorList);
        }

        //入库成功，下发短信
        for(MeetingExhibitorInfo mei:list){
            registerUserService.registerUser(mei.getChargePersonPhone(), mei.getChargePerson(), mei.getCompanyName(), PersonRole.EXHIBITOR.getIndex());
        }

        return AjaxResult.success(message);
    }

    /**
     * 获取会议展商信息详细信息
     */
    @ApiOperation("获取会议展商信息详细信息")
    @RequiresPermissions("manage:meetingExhibitor:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {

        MeetingExhibitorInfo meetingExhibitor = meetingExhibitorInfoService.getById(id);
        List<MeetingVenueBoothInfo> booths = meetingVenueBoothInfoService.selectdLisbByExhibitorName(meetingExhibitor.getMeetingId(),meetingExhibitor.getCompanyName());
        if(booths != null && booths.size() >0){
            meetingExhibitor.setBoothName(booths.get(0).getBoothName());
            meetingExhibitor.setBoothOrderNum(booths.get(0).getBoothOrderNum());
        }
        return AjaxResult.success(meetingExhibitor);
    }

    /**
     * 新增会议展商信息
     */
    @ApiOperation("新增会议展商信息")
    @RequiresPermissions("manage:meetingExhibitor:add")
    @Log(title = "会议展商信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingExhibitorInfo meetingExhibitorInfo) {
        if (UserConstants.NOT_UNIQUE.equals(meetingExhibitorInfoService.checkNameUnique(meetingExhibitorInfo.getMeetingId(),meetingExhibitorInfo.getCompanyName()))) {
            return AjaxResult.error("新增会议展商信息'" + meetingExhibitorInfo.getCompanyName() + "'失败，公司名称已存在!");
        }
        //-----------与会议展馆表关联-----------------------------
        String boothName = meetingExhibitorInfo.getBoothName();
        Long meetingId = meetingExhibitorInfo.getMeetingId();
        String exhibitorCopmanyName = meetingExhibitorInfo.getCompanyName();
        MeetingVenueBoothInfo meetingVenueBoothInfo = new MeetingVenueBoothInfo();
        meetingVenueBoothInfo.setExhibitorName(exhibitorCopmanyName);
        UpdateWrapper<MeetingVenueBoothInfo> updateWrapper = new UpdateWrapper();
        updateWrapper.lambda().eq(MeetingVenueBoothInfo::getMeetingId,meetingId);//通过 meetingId 和 boothName 修改 exhibitor_name字段
        updateWrapper.lambda().eq(MeetingVenueBoothInfo::getBoothName,boothName);
        meetingVenueBoothInfoService.update(meetingVenueBoothInfo,updateWrapper);
        //-----------------------------------------------------

        meetingExhibitorInfo.setCreatedTime(DateUtils.getNowDate());
        meetingExhibitorInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(meetingExhibitorInfo.getChargePersonPhone(), meetingExhibitorInfo.getChargePerson(), meetingExhibitorInfo.getCompanyName(), PersonRole.EXHIBITOR.getIndex());
        return toAjax(meetingExhibitorInfoService.save(meetingExhibitorInfo));
    }

    /**
     * 修改会议展商信息
     */
    @ApiOperation("修改会议展商信息")
    @RequiresPermissions("manage:meetingExhibitor:edit")
    @Log(title = "会议展商信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingExhibitorInfo meetingExhibitorInfo) {
        Long id = meetingExhibitorInfo.getId();
        MeetingExhibitorInfo meetingExhibitorInfo0 = meetingExhibitorInfoService.getById(id);
        //-----------清除此前与会议展馆表的关联-----------------------------
        String boothName0 = meetingExhibitorInfo0.getBoothName();
        Long meetingId0 = meetingExhibitorInfo0.getMeetingId();
        MeetingVenueBoothInfo meetingVenueBoothInfo0 = new MeetingVenueBoothInfo();
        meetingVenueBoothInfo0.setExhibitorName("");//展商字段清空
        UpdateWrapper<MeetingVenueBoothInfo> updateWrapper0 = new UpdateWrapper();
        updateWrapper0.lambda().eq(MeetingVenueBoothInfo::getMeetingId,meetingId0);//通过 meetingId 和 boothName 修改 exhibitor_name字段
        updateWrapper0.lambda().eq(MeetingVenueBoothInfo::getBoothName,boothName0);
        meetingVenueBoothInfoService.update(meetingVenueBoothInfo0,updateWrapper0);
        //-----------------------------------------------------

        //-----------重新与会议展馆表关联-----------------------------
        String boothName = meetingExhibitorInfo.getBoothName();
        Long meetingId = meetingExhibitorInfo.getMeetingId();
        String exhibitorCopmanyName = meetingExhibitorInfo.getCompanyName();
        MeetingVenueBoothInfo meetingVenueBoothInfo = new MeetingVenueBoothInfo();
        meetingVenueBoothInfo.setExhibitorName(exhibitorCopmanyName);//展商字段
        UpdateWrapper<MeetingVenueBoothInfo> updateWrapper = new UpdateWrapper();
        updateWrapper.lambda().eq(MeetingVenueBoothInfo::getMeetingId,meetingId);//通过 meetingId 和 boothName 修改 exhibitor_name字段
        updateWrapper.lambda().eq(MeetingVenueBoothInfo::getBoothName,boothName);
        meetingVenueBoothInfoService.update(meetingVenueBoothInfo,updateWrapper);
        //-----------------------------------------------------

        return toAjax(meetingExhibitorInfoService.updateById(meetingExhibitorInfo));
    }

    /**
     * 删除会议展商信息
     */
    @ApiOperation("删除会议展商信息")
    @RequiresPermissions("manage:meetingExhibitor:remove")
    @Log(title = "会议展商信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        for(Long id:ids){
            MeetingExhibitorInfo meetingExhibitorInfo = meetingExhibitorInfoService.getById(id);
            //-----------清除与会议展馆表关联-----------------------------
            String boothName = meetingExhibitorInfo.getBoothName();
            Long meetingId = meetingExhibitorInfo.getMeetingId();
            //String exhibitorCopmanyName = meetingExhibitorInfo.getCompanyName();
            MeetingVenueBoothInfo meetingVenueBoothInfo = new MeetingVenueBoothInfo();
            meetingVenueBoothInfo.setExhibitorName("");//展商字段清空
            UpdateWrapper<MeetingVenueBoothInfo> updateWrapper = new UpdateWrapper();
            updateWrapper.lambda().eq(MeetingVenueBoothInfo::getMeetingId,meetingId);//通过 meetingId 和 boothName 修改 exhibitor_name字段
            updateWrapper.lambda().eq(MeetingVenueBoothInfo::getBoothName,boothName);
            meetingVenueBoothInfoService.update(meetingVenueBoothInfo,updateWrapper);
            //-----------------------------------------------------
        }
        return toAjax(meetingExhibitorInfoService.removeByIds(ids));
    }
}
