package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.UserHealthManage;
import com.ruoyi.manage.dto.UserHealthManageDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IUserHealthManageService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * 员工健康管理Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "员工健康管理控制器", tags = {"员工健康管理管理"})
@RestController
@RequestMapping("/userHealthManage")
public class UserHealthManageController extends BaseController {
    @Autowired
    private IUserHealthManageService userHealthManageService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询员工健康管理列表
     */
    @ApiOperation("查询员工健康管理列表")
    @RequiresPermissions("manage:userHealthManage:list")
    @GetMapping("/list")
    public TableDataInfo list(UserHealthManageDto userHealthManage) {
        startPage();
        List<UserHealthManage> list = userHealthManageService.selectdList(userHealthManage);
        return getDataTable(list);
    }

    /**
     * 导出员工健康管理列表
     */
    @ApiOperation("导出员工健康管理列表")
    @RequiresPermissions("manage:userHealthManage:export")
    @Log(title = "员工健康管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserHealthManageDto userHealthManage) throws IOException {

        startPage();
        List<UserHealthManage> list = userHealthManageService.selectdList(userHealthManage);
        ExcelUtil<UserHealthManage> util = new ExcelUtil<UserHealthManage>(UserHealthManage.class);
        util.exportExcel(response, list, "员工健康管理数据");
    }

    /**
     * 导入员工健康管理
     */
    @ApiOperation("导出员工健康管理列表")
    @RequiresPermissions("manage:userHealthManage:importData")
    @Log(title = "员工健康管理导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,String userRole) throws Exception {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        ExcelUtil<UserHealthManage> util = new ExcelUtil<UserHealthManage>(UserHealthManage.class);
        List<UserHealthManage> list = util.importExcel(file.getInputStream());
        for(UserHealthManage uhm:list){
            uhm.setCreatedTime(DateUtils.getNowDate());
            uhm.setCreatedBy(SecurityUtils.getUsername());
            uhm.setUserRole(userRole);
            uhm.setCompanyName(SecurityUtils.getUsername());//登录名即为公司名
            uhm.setUserRoleName(PersonRole.getName(userRole));
        }
        boolean message = false;
        if (updateSupport) {
            message = userHealthManageService.saveOrUpdateBatch(list);
        } else {
            message = userHealthManageService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取员工健康管理详细信息
     */
    @ApiOperation("获取员工健康管理详细信息")
    @RequiresPermissions("manage:userHealthManage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("id") Long id) {
        return AjaxResult.success(userHealthManageService.getById(id));
    }


    /**
     * 新增员工健康管理
     */
    @ApiOperation("新增员工健康管理")
    @RequiresPermissions("manage:userHealthManage:add")
    @Log(title = "员工健康管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserHealthManage userHealthManage) {
        if (UserConstants.NOT_UNIQUE.equals(userHealthManageService.checkNameUnique(userHealthManage.getIdCard(),userHealthManage.getCompanyName()))) {
            return AjaxResult.error("新增人员'" + userHealthManage.getName() + "'失败，用户已存在");
        }
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        String userRole = userHealthManage.getUserRole();
        //if(StringUtils.isNotBlank(userRole) && roles.contains(userRole)){
            userHealthManage.setCreatedTime(DateUtils.getNowDate());
            userHealthManage.setCreatedBy(SecurityUtils.getUsername());
            //userHealthManage.setUserRoleName(PersonRole.getName(userRole));
            userHealthManage.setUserRoleName(PersonRole.getName(userRole));
            return toAjax(userHealthManageService.save(userHealthManage));
        //} else
        //    return AjaxResult.error("指定的员工角色不正确,与当前用户不符！");
    }


    /**
     * 修改员工健康管理
     */
    @ApiOperation("修改员工健康管理")
    @RequiresPermissions("manage:userHealthManage:edit")
    @Log(title = "员工健康管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserHealthManage userHealthManage) {
        return toAjax(userHealthManageService.updateById(userHealthManage));
    }

    /**
     * 删除员工健康管理
     */
    @ApiOperation("删除员工健康管理")
    @RequiresPermissions("manage:userHealthManage:remove")
    @Log(title = "员工健康管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(userHealthManageService.removeByIds(ids));
    }
}
