package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AntiepidemicConfig;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;
import com.ruoyi.manage.dto.AntiepidemicConfigDto;
import com.ruoyi.manage.mapper.AntiepidemicConfigMapper;
import com.ruoyi.manage.service.IAntiepidemicConfigService;
import com.ruoyi.manage.service.IAntiepidemicDetailConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 防疫填报模板Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Service
public class AntiepidemicConfigServiceImpl extends ServiceImpl<AntiepidemicConfigMapper, AntiepidemicConfig> implements IAntiepidemicConfigService {
    @Autowired
    private AntiepidemicConfigMapper antiepidemicConfigMapper;

    @Autowired
    private IAntiepidemicDetailConfigService antiepidemicDetailConfigService;

    /**
     * 查询防疫填报模板列表
     *
     * @param antiepidemicConfig 防疫填报模板
     * @return 防疫填报模板
     */
    @Override
    public List<AntiepidemicConfig> selectdList(AntiepidemicConfigDto antiepidemicConfig) {
        QueryWrapper<AntiepidemicConfig> q = new QueryWrapper<>();
        if (antiepidemicConfig.getAntiepidemicName() != null && !antiepidemicConfig.getAntiepidemicName().trim().equals("")) {
            q.like("antiepidemic_name", antiepidemicConfig.getAntiepidemicName());
        }
        if (antiepidemicConfig.getEnableStatus() != null && !antiepidemicConfig.getEnableStatus().trim().equals("")) {
            q.eq("status", antiepidemicConfig.getEnableStatus());
        }
        q.orderByDesc("created_time");
        List<AntiepidemicConfig> list = this.list(q);
        return list;
    }

    /**
     * 查询防疫填报下拉
     *
     * @return
     */
    @Override
    public AjaxResult findSelect() {
        QueryWrapper<AntiepidemicConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id as value", "antiepidemic_name as label")
                .eq("enable_status", "0")
                .eq("status", "0");
        List<Map<String, Object>> list = this.getBaseMapper().selectMaps(queryWrapper);
        return AjaxResult.success(list);
    }


    /**
     * 更新启用状态
     *
     * @param dto
     * @return
     */
    @Override
    public AjaxResult changeEnableStatus(AntiepidemicConfigDto dto) {
        AntiepidemicConfig config = this.getById(dto.getId());
        config.setEnableStatus(dto.getEnableStatus());
        boolean result = this.updateById(config);
        if (result) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error(500, "操作失败");
        }
    }

    /**
     * 获取防疫填报模板详细信息
     *
     * @param id
     * @return
     */
    @Override
    public AjaxResult getInfo(Long id) {
        AntiepidemicConfig config = this.getById(id);
        List<AntiepidemicDetailConfig> antiepidemicDetailConfigList = antiepidemicDetailConfigService.getBaseMapper().selectList(new QueryWrapper<AntiepidemicDetailConfig>()
                .eq("antiepidemic_id", id)
                .eq("status", "0")
                .orderByAsc("sort_no")
        );
        config.setAntiepidemicDetailConfigList(antiepidemicDetailConfigList);
        return AjaxResult.success(config);
    }

    /**
     * 新增防疫上报
     *
     * @param antiepidemicConfig
     * @return
     */
    @Transactional
    @Override
    public AjaxResult addOrUpdate(AntiepidemicConfig antiepidemicConfig) {
        if (antiepidemicConfig.getAntiepidemicDetailConfigList().size() > 0) {
            List<AntiepidemicDetailConfig> collect = antiepidemicConfig.getAntiepidemicDetailConfigList().stream().filter(entity -> StringUtils.isEmpty(entity.getName())).collect(Collectors.toList());
            if (collect.size() > 0) {
                return AjaxResult.error(500, "标题不能为空");
            }
        }

        boolean save = this.saveOrUpdate(antiepidemicConfig);
        if (save) {
            List<AntiepidemicDetailConfig> list = antiepidemicConfig.getAntiepidemicDetailConfigList();
            list.stream().forEach(entity -> entity.setAntiepidemicId(antiepidemicConfig.getId()));
            antiepidemicDetailConfigService.saveOrUpdateBatch(list);
        }
        return AjaxResult.success();
    }

    @Transactional
    @Override
    public AjaxResult remove(List<Long> ids) {
        boolean remove = removeByIds(ids);
        if (remove) {
            antiepidemicDetailConfigService.remove(new QueryWrapper<AntiepidemicDetailConfig>().in("antiepidemic_id", ids));
        }
        return AjaxResult.success();
    }


}
