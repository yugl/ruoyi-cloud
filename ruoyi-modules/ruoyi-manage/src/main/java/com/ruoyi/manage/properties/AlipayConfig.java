package com.ruoyi.manage.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 阿里config
 */
@Data
@Component
@ConfigurationProperties(prefix = "app.alipay")
public class AlipayConfig {

    private String url;//	支付宝网关（固定）
    private String appid;//	APPID 即创建小程序后生成。
    private String appPrivateKey;//开发者私钥，由开发者自己生成。
    private String charset;//编码集，支持 GBK/UTF-8。
    private String publicKey;//支付宝公钥，由支付宝生成。
    private String signType;//签名字符串所使用的签名算法类型，目前支持 RSA2 和 RSA，推荐使用 RSA2。
}
