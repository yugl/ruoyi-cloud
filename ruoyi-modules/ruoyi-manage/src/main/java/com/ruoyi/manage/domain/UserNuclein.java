package com.ruoyi.manage.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 核酸检测对象 user_nuclein
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Data
public class UserNuclein implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phone;

    /**
     * 身份证号
     */
    @Excel(name = "身份证号")
    private String idCard;

    /**
     * 核酸检测机构
     */
    @Excel(name = "核酸检测机构")
    private String testOrg;

    /**
     * 核酸时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "核酸时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date testTime;

    /**
     * 核酸结果（1：阴性 2：阳性）
     */
    @Excel(name = "核酸结果", readConverterExp = "1=：阴性,2=：阳性")
    private String userStatus;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;


}
