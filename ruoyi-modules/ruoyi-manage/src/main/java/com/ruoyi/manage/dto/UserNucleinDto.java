package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 核酸检测对象 user_nuclein
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Data
public class UserNucleinDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 核酸检测机构
     */
    private String testOrg;
    /**
     * 核酸时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date testTime;
    /**
     * 核酸结果（1：阴性 2：阳性）
     */
    private String userStatus;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

}
