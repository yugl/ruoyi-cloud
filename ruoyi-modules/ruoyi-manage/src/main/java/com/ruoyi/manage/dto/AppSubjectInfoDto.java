package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 问卷题目对象 app_subject_info
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class AppSubjectInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主表ID
     */
    private Long questionnaireId;
    /**
     * 题目
     */
    private String subject;

    private String type;
}
