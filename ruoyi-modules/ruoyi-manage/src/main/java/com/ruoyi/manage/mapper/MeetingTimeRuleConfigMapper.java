package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingTimeRuleConfig;

/**
 * 会议入场时间规则Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingTimeRuleConfigMapper extends BaseMapper<MeetingTimeRuleConfig> {


}
