package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppPrivacyAggreement;

/**
 * 隐私协议设置Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-14
 */
public interface AppPrivacyAggreementMapper extends BaseMapper<AppPrivacyAggreement> {


}
