package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingInfoApprovalOpinion;


/**
 * 政务审核意见Mapper接口
 *
 * @author wangguiyu
 * @date 2022-05-04
 */
public interface MeetingInfoApprovalOpinionMapper extends BaseMapper<MeetingInfoApprovalOpinion> {


}
