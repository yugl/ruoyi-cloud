package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingDisinfectRecordInfo;
import com.ruoyi.manage.domain.UserAntiepidemicRecord;
import com.ruoyi.manage.dto.MeetingDisinfectRecordInfoDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议消毒记录Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingDisinfectRecordInfoMapper extends BaseMapper<MeetingDisinfectRecordInfo> {

    List<Map<String, Object>> selectdList(@Param("dto") MeetingDisinfectRecordInfoDto meetingDisinfectRecordInfoDto);

    List<MeetingDisinfectRecordInfo> findDisinfectRecordList(@Param("dto") Map<String, String> map);
}
