package com.ruoyi.manage.dto;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;
    
/**
 * 会议入场时间规则对象 meeting_time_rule_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
                @Data
        public class MeetingTimeRuleConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;
                /** 会议ID */
                private Long meetingId;
                /** 角色 */
                private String userRole;
                /** 开始日期 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date startDate;
                /** 结束日期 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date endDate;
                /** 开始时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date startTime;
                /** 结束时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date endTime;
                /** 状态 0：有效
1：无效 */
                private String status;
                /** 创建人 */
                private String createdBy;
                /** 创建时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date createdTime;
                /** 更新人 */
                private String updatedBy;
                /** 更新时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date updatedTime;

}
