package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.SmsSendRecord;
import com.ruoyi.manage.dto.SmsSendRecordDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 短信管理Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface SmsSendRecordMapper extends BaseMapper<SmsSendRecord> {


    List<SmsSendRecord> selectdList(@Param("dto") SmsSendRecordDto dto);
}
