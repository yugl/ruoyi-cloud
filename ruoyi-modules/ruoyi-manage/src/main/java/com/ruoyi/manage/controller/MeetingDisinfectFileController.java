package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingDisinfectFile;
import com.ruoyi.manage.dto.MeetingDisinfectFileDto;
import com.ruoyi.manage.service.IMeetingDisinfectFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 会议消毒记录文件Controller
 *
 * @author ruoyi
 * @date 2022-03-31
 */
@Api(value = "会议消毒记录文件控制器", tags = {"会议消毒记录文件管理"})
@RestController
@RequestMapping("/meetingDisinfectFile")
public class MeetingDisinfectFileController extends BaseController {
    @Autowired
    private IMeetingDisinfectFileService meetingDisinfectFileService;

    /**
     * 查询会议消毒记录文件列表
     */
    @ApiOperation("查询会议消毒记录文件列表")
    @PostMapping("/list")
    public TableDataInfo list(@RequestBody MeetingDisinfectFileDto meetingDisinfectFile) {
        startPage();
        List<MeetingDisinfectFile> list = meetingDisinfectFileService.selectdList(meetingDisinfectFile);
        return getDataTable(list);
    }

    /**
     * 导出会议消毒记录文件列表
     */
    @ApiOperation("导出会议消毒记录文件列表")
    @Log(title = "会议消毒记录文件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingDisinfectFileDto meetingDisinfectFile) throws IOException {

        startPage();
        List<MeetingDisinfectFile> list = meetingDisinfectFileService.selectdList(meetingDisinfectFile);
        ExcelUtil<MeetingDisinfectFile> util = new ExcelUtil<MeetingDisinfectFile>(MeetingDisinfectFile.class);
        util.exportExcel(response, list, "会议消毒记录文件数据");
    }

    /**
     * 导入会议消毒记录文件
     */
    @ApiOperation("导出会议消毒记录文件列表")
    @Log(title = "会议消毒记录文件导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingDisinfectFile> util = new ExcelUtil<MeetingDisinfectFile>(MeetingDisinfectFile.class);
        List<MeetingDisinfectFile> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingDisinfectFileService.saveOrUpdateBatch(list);
        } else {
            message = meetingDisinfectFileService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议消毒记录文件详细信息
     */
    @ApiOperation("获取会议消毒记录文件详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingDisinfectFileService.getById(id));
    }

    /**
     * 新增会议消毒记录文件
     */
    @ApiOperation("新增会议消毒记录文件")
    @Log(title = "会议消毒记录文件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingDisinfectFile meetingDisinfectFile) {
        return toAjax(meetingDisinfectFileService.save(meetingDisinfectFile));
    }

    /**
     * 修改会议消毒记录文件
     */
    @ApiOperation("修改会议消毒记录文件")
    @Log(title = "会议消毒记录文件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingDisinfectFile meetingDisinfectFile) {
        return toAjax(meetingDisinfectFileService.updateById(meetingDisinfectFile));
    }

    /**
     * 删除会议消毒记录文件
     */
    @ApiOperation("删除会议消毒记录文件")
    @Log(title = "会议消毒记录文件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingDisinfectFileService.removeByIds(ids));
    }
}
