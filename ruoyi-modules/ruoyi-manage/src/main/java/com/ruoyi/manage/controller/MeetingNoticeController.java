package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.SecurityUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingNotice;
import com.ruoyi.manage.dto.MeetingNoticeDto;
import com.ruoyi.manage.service.IMeetingNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 会议通知公告Controller
 *
 * @author ruoyi
 * @date 2022-04-01
 */
@Api(value = "会议通知公告控制器", tags = {"会议通知公告管理"})
@RestController
@RequestMapping("/meetingNotice")
public class MeetingNoticeController extends BaseController {

    @Autowired
    private IMeetingNoticeService meetingNoticeService;

    /**
     * app端 会议通知公告列表
     */
    @PostMapping("/appNoticeList")
    public AjaxResult appList(@RequestBody Map<String, String> map) {
        return meetingNoticeService.appList(map);
    }

    /**
     * app端 获取会议通知公告详细信息
     */
    @PostMapping(value = "/getNoticeById")
    public AjaxResult getNoticeById(@RequestBody Map<String, String> map) {
        return meetingNoticeService.getNoticeById(map);
    }

    /**
     * 根据当前用户 查询报名过的会议通知公告
     */
    @ApiOperation("查询会议通知公告列表")
    @GetMapping("/listByUser")
    public TableDataInfo listByUser(MeetingNoticeDto meetingNotice) {
        startPage();
        List<MeetingNotice> list = meetingNoticeService.listByUser(meetingNotice);
        return getDataTable(list);
    }

    /**
     * 查询会议通知公告列表
     */
    @ApiOperation("查询会议通知公告列表")
    @RequiresPermissions("manage:meetingNotice:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingNoticeDto meetingNotice) {
        startPage();
        List<MeetingNotice> list = meetingNoticeService.selectdList(meetingNotice);
        return getDataTable(list);
    }

    /**
     * 导出会议通知公告列表
     */
    @ApiOperation("导出会议通知公告列表")
    @RequiresPermissions("manage:meetingNotice:export")
    @Log(title = "会议通知公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingNoticeDto meetingNotice) throws IOException {
        startPage();
        List<MeetingNotice> list = meetingNoticeService.selectdList(meetingNotice);
        ExcelUtil<MeetingNotice> util = new ExcelUtil<MeetingNotice>(MeetingNotice.class);
        util.exportExcel(response, list, "会议通知公告数据");
    }

    /**
     * 导入会议通知公告
     */
    @ApiOperation("导出会议通知公告列表")
    @RequiresPermissions("manage:meetingNotice:importData")
    @Log(title = "会议通知公告导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingNotice> util = new ExcelUtil<MeetingNotice>(MeetingNotice.class);
        List<MeetingNotice> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingNoticeService.saveOrUpdateBatch(list);
        } else {
            message = meetingNoticeService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议通知公告详细信息
     */
    @ApiOperation("获取会议通知公告详细信息")
    @RequiresPermissions("manage:meetingNotice:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingNoticeService.getById(id));
    }

    /**
     * 新增会议通知公告
     */
    @ApiOperation("新增会议通知公告")
    @RequiresPermissions("manage:meetingNotice:add")
    @Log(title = "会议通知公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody MeetingNotice meetingNotice) {
        return toAjax(meetingNoticeService.save(meetingNotice));
    }

    /**
     * 修改会议通知公告
     */
    @ApiOperation("修改会议通知公告")
    @RequiresPermissions("manage:meetingNotice:edit")
    @Log(title = "会议通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody MeetingNotice meetingNotice) {
        return toAjax(meetingNoticeService.updateById(meetingNotice));
    }

    /**
     * 删除会议通知公告
     */
    @ApiOperation("删除会议通知公告")
    @RequiresPermissions("manage:meetingNotice:remove")
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingNoticeService.removeByIds(ids));
    }

    /**
     * 根据当前用户查询会议下拉
     *
     * @return
     */
    @PostMapping("/findMeetingList")
    public AjaxResult findMeetingList() {
        String username = SecurityUtils.getUsername();
        List<Map<String, String>> list = meetingNoticeService.findMeetingList(username);
        return AjaxResult.success(list);
    }
}
