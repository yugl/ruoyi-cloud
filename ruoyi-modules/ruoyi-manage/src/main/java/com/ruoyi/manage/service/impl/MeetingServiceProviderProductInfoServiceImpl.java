package com.ruoyi.manage.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.MeetingServiceProviderProductInfoMapper;
import com.ruoyi.manage.domain.MeetingServiceProviderProductInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderProductInfoDto;
import com.ruoyi.manage.service.IMeetingServiceProviderProductInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 会议服务商产品信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingServiceProviderProductInfoServiceImpl extends ServiceImpl<MeetingServiceProviderProductInfoMapper, MeetingServiceProviderProductInfo> implements IMeetingServiceProviderProductInfoService {
    @Autowired
    private MeetingServiceProviderProductInfoMapper meetingServiceProviderProductInfoMapper;

    /**
     * 查询会议服务商产品信息列表
     *
     * @param meetingServiceProviderProductInfo 会议服务商产品信息
     * @return 会议服务商产品信息
     */
    @Override
    public List<MeetingServiceProviderProductInfo> selectdList(MeetingServiceProviderProductInfoDto meetingServiceProviderProductInfo) {
        QueryWrapper<MeetingServiceProviderProductInfo> q = new QueryWrapper<>();
                    if (meetingServiceProviderProductInfo.getMeetingId() != null  ){
                        q.eq("meeting_id",meetingServiceProviderProductInfo.getMeetingId());
                }
                    if (meetingServiceProviderProductInfo.getCompanyName() != null   && !meetingServiceProviderProductInfo.getCompanyName().trim().equals("")){
                        q.like("company_name",meetingServiceProviderProductInfo.getCompanyName());
                }
                    if (meetingServiceProviderProductInfo.getProductName() != null   && !meetingServiceProviderProductInfo.getProductName().trim().equals("")){
                        q.like("product_name",meetingServiceProviderProductInfo.getProductName());
                }
                    if (meetingServiceProviderProductInfo.getProductCode() != null   && !meetingServiceProviderProductInfo.getProductCode().trim().equals("")){
                        q.eq("product_code",meetingServiceProviderProductInfo.getProductCode());
                }
                    if (meetingServiceProviderProductInfo.getProductExplain() != null   && !meetingServiceProviderProductInfo.getProductExplain().trim().equals("")){
                        q.eq("product_explain",meetingServiceProviderProductInfo.getProductExplain());
                }
                    if (meetingServiceProviderProductInfo.getUseMeeting() != null   && !meetingServiceProviderProductInfo.getUseMeeting().trim().equals("")){
                        q.eq("use_meeting",meetingServiceProviderProductInfo.getUseMeeting());
                }
                    if (meetingServiceProviderProductInfo.getUseStartTime() != null  ){
                        q.eq("use_start_time",meetingServiceProviderProductInfo.getUseStartTime());
                }
                    if (meetingServiceProviderProductInfo.getUseEndTime() != null  ){
                        q.eq("use_end_time",meetingServiceProviderProductInfo.getUseEndTime());
                }
                    if (meetingServiceProviderProductInfo.getStatus() != null   && !meetingServiceProviderProductInfo.getStatus().trim().equals("")){
                        q.eq("status",meetingServiceProviderProductInfo.getStatus());
                }
                    if (meetingServiceProviderProductInfo.getCreatedBy() != null   && !meetingServiceProviderProductInfo.getCreatedBy().trim().equals("")){
                        q.eq("created_by",meetingServiceProviderProductInfo.getCreatedBy());
                }
                    if (meetingServiceProviderProductInfo.getCreatedTime() != null  ){
                        q.eq("created_time",meetingServiceProviderProductInfo.getCreatedTime());
                }
                    if (meetingServiceProviderProductInfo.getUpdatedBy() != null   && !meetingServiceProviderProductInfo.getUpdatedBy().trim().equals("")){
                        q.eq("updated_by",meetingServiceProviderProductInfo.getUpdatedBy());
                }
                    if (meetingServiceProviderProductInfo.getUpdatedTime() != null  ){
                        q.eq("updated_time",meetingServiceProviderProductInfo.getUpdatedTime());
                }
        List<MeetingServiceProviderProductInfo> list = this.list(q);
        return list;
    }


}
