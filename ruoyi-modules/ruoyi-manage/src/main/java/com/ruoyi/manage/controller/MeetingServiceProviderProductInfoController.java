package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.MeetingServiceProviderProductInfo;
import com.ruoyi.manage.domain.ServiceProviderProductInfo;
import com.ruoyi.manage.dto.MeetingServiceProviderProductInfoDto;
import com.ruoyi.manage.service.IMeetingServiceProviderProductInfoService;
import com.ruoyi.manage.service.IServiceProviderProductInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 会议服务商产品信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议服务商产品信息控制器", tags = {"会议服务商产品信息管理"})
@RestController
@RequestMapping("/meetingServiceProviderProduct")
public class MeetingServiceProviderProductInfoController extends BaseController {
    @Autowired
    private IMeetingServiceProviderProductInfoService meetingServiceProviderProductInfoService;

    @Autowired
    private IServiceProviderProductInfoService serviceProviderProductInfoService;

    /**
     * 查询会议服务商产品信息列表
     */
    @ApiOperation("查询会议服务商产品信息列表")
    @RequiresPermissions("manage:meetingServiceProviderProduct:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingServiceProviderProductInfoDto meetingServiceProviderProductInfo) {
        startPage();
        List<MeetingServiceProviderProductInfo> list = meetingServiceProviderProductInfoService.selectdList(meetingServiceProviderProductInfo);
        return getDataTable(list);
    }

    /**
     * 导出会议服务商产品信息列表
     */
    @ApiOperation("导出会议服务商产品信息列表")
    @RequiresPermissions("manage:meetingServiceProviderProduct:export")
    @Log(title = "会议服务商产品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingServiceProviderProductInfoDto meetingServiceProviderProductInfo) throws IOException {

        startPage();
        List<MeetingServiceProviderProductInfo> list = meetingServiceProviderProductInfoService.selectdList(meetingServiceProviderProductInfo);
        ExcelUtil<MeetingServiceProviderProductInfo> util = new ExcelUtil<MeetingServiceProviderProductInfo>(MeetingServiceProviderProductInfo.class);
        util.exportExcel(response, list, "会议服务商产品信息数据");
    }

    /**
     * 导入会议服务商产品信息
     * companyName: 指的服务商公司名称
     */
    @ApiOperation("导入会议服务商产品信息列表")
    @RequiresPermissions("manage:meetingServiceProviderProduct:importData")
    @Log(title = "会议服务商产品信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,Long meetingId,String companyName) throws Exception {
        ExcelUtil<MeetingServiceProviderProductInfo> util = new ExcelUtil<MeetingServiceProviderProductInfo>(MeetingServiceProviderProductInfo.class);
        List<MeetingServiceProviderProductInfo> list = util.importExcel(file.getInputStream());
        List<ServiceProviderProductInfo> serviceProductList = new ArrayList<>();
        for(MeetingServiceProviderProductInfo mspp : list){
            mspp.setCreatedTime(DateUtils.getNowDate());
            mspp.setCreatedBy(SecurityUtils.getUsername());
            mspp.setMeetingId(meetingId);
            mspp.setCompanyName(companyName);

            ServiceProviderProductInfo sppi = new ServiceProviderProductInfo();
            BeanUtils.copyBeanProp(sppi,mspp);
            serviceProductList.add(sppi);
        }

        boolean message = false;
        if (updateSupport) {
            //批量更新会存在问题 如果重复提交，会有重复记录
            //message = meetingServiceProviderProductInfoService.saveOrUpdateBatch(list);
            //serviceProviderProductInfoService.saveOrUpdateBatch(serviceProductList);
            for(MeetingServiceProviderProductInfo mp:list){
                UpdateWrapper<MeetingServiceProviderProductInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(MeetingServiceProviderProductInfo::getMeetingId,meetingId);
                updateWrapper.lambda().eq(MeetingServiceProviderProductInfo::getCompanyName,mp.getCompanyName());
                updateWrapper.lambda().eq(MeetingServiceProviderProductInfo::getProductCode,mp.getProductCode());
                updateWrapper.lambda().eq(MeetingServiceProviderProductInfo::getProductName,mp.getProductName());
                meetingServiceProviderProductInfoService.saveOrUpdate(mp,updateWrapper);
            }

            for(ServiceProviderProductInfo p:serviceProductList){
                UpdateWrapper<ServiceProviderProductInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(ServiceProviderProductInfo::getCompanyName,p.getCompanyName());
                updateWrapper.lambda().eq(ServiceProviderProductInfo::getProductCode,p.getProductCode());
                updateWrapper.lambda().eq(ServiceProviderProductInfo::getProductName,p.getProductName());
                serviceProviderProductInfoService.saveOrUpdate(p,updateWrapper);
            }

        } else {
            message = meetingServiceProviderProductInfoService.saveBatch(list);
            serviceProviderProductInfoService.saveBatch(serviceProductList);

        }
        return AjaxResult.success(message);
    }



    /**
     * 获取会议服务商产品信息详细信息
     */
    @ApiOperation("获取会议服务商产品信息详细信息")
    @RequiresPermissions("manage:meetingServiceProviderProduct:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingServiceProviderProductInfoService.getById(id));
    }

    /**
     * 新增会议服务商产品信息
     */
    @ApiOperation("新增会议服务商产品信息")
    @RequiresPermissions("manage:meetingServiceProviderProduct:add")
    @Log(title = "会议服务商产品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingServiceProviderProductInfo meetingServiceProviderProductInfo) {
        meetingServiceProviderProductInfo.setCreatedTime(DateUtils.getNowDate());
        meetingServiceProviderProductInfo.setCreatedBy(SecurityUtils.getUsername());
        return toAjax(meetingServiceProviderProductInfoService.save(meetingServiceProviderProductInfo));
    }

    /**
     * 修改会议服务商产品信息
     */
    @ApiOperation("修改会议服务商产品信息")
    @RequiresPermissions("manage:meetingServiceProviderProduct:edit")
    @Log(title = "会议服务商产品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingServiceProviderProductInfo meetingServiceProviderProductInfo) {
        return toAjax(meetingServiceProviderProductInfoService.updateById(meetingServiceProviderProductInfo));
    }

    /**
     * 删除会议服务商产品信息
     */
    @ApiOperation("删除会议服务商产品信息")
    @RequiresPermissions("manage:meetingServiceProviderProduct:remove")
    @Log(title = "会议服务商产品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingServiceProviderProductInfoService.removeByIds(ids));
    }
}
