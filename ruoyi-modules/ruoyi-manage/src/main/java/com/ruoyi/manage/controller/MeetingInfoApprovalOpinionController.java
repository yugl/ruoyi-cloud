package com.ruoyi.manage.controller;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.manage.domain.MeetingInfoApprovalOpinion;
import com.ruoyi.manage.dto.MeetingInfoApprovalOpinionDto;
import com.ruoyi.manage.service.IMeetingInfoApprovalOpinionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 政务审核意见Controller
 *
 * @author wangguiyu
 * @date 2022-05-04
 */
@Api(value = "政务审核意见控制器", tags = {"政务审核意见管理"})
@RestController
@RequestMapping("/opinion")
public class MeetingInfoApprovalOpinionController extends BaseController {
    @Autowired
    private IMeetingInfoApprovalOpinionService meetingInfoApprovalOpinionService;

/**
 * 查询政务审核意见列表
 */
@ApiOperation("查询政务审核意见列表")
// @RequiresPermissions("manage:opinion:list")
@GetMapping("/list")
    public TableDataInfo list(MeetingInfoApprovalOpinionDto meetingInfoApprovalOpinion) {
        startPage();
        List<MeetingInfoApprovalOpinion> list = meetingInfoApprovalOpinionService.selectdList(meetingInfoApprovalOpinion);
        return getDataTable(list);
    }

    /**
     * 导出政务审核意见列表
     */
    @ApiOperation("导出政务审核意见列表")
    @RequiresPermissions("manage:opinion:export")
    @Log(title = "政务审核意见", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingInfoApprovalOpinionDto meetingInfoApprovalOpinion) throws IOException {

        startPage();
        List<MeetingInfoApprovalOpinion> list = meetingInfoApprovalOpinionService.selectdList(meetingInfoApprovalOpinion);
        ExcelUtil<MeetingInfoApprovalOpinion> util = new ExcelUtil<MeetingInfoApprovalOpinion>(MeetingInfoApprovalOpinion. class);
        util.exportExcel(response, list, "政务审核意见数据");
    }

    /**
     * 导入政务审核意见
     */
    @ApiOperation("导出政务审核意见列表")
    @RequiresPermissions("manage:opinion:importData")
    @Log(title = "政务审核意见导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingInfoApprovalOpinion> util = new ExcelUtil< MeetingInfoApprovalOpinion>( MeetingInfoApprovalOpinion. class);
        List<MeetingInfoApprovalOpinion> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingInfoApprovalOpinionService.saveOrUpdateBatch(list);
        } else {
            message = meetingInfoApprovalOpinionService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取政务审核意见详细信息
     */
    @ApiOperation("获取政务审核意见详细信息")
    @RequiresPermissions("manage:opinion:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingInfoApprovalOpinionService.getById(id));
    }

    /**
     * 新增政务审核意见
     */
    @ApiOperation("新增政务审核意见")
    @RequiresPermissions("manage:opinion:add")
    @Log(title = "政务审核意见", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingInfoApprovalOpinion meetingInfoApprovalOpinion) {
        meetingInfoApprovalOpinion.setCreatedTime(DateUtils.getNowDate());
        return toAjax(meetingInfoApprovalOpinionService.save(meetingInfoApprovalOpinion));
    }

    /**
     * 修改政务审核意见
     */
    @ApiOperation("修改政务审核意见")
    @RequiresPermissions("manage:opinion:edit")
    @Log(title = "政务审核意见", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingInfoApprovalOpinion meetingInfoApprovalOpinion) {
        return toAjax(meetingInfoApprovalOpinionService.updateById(meetingInfoApprovalOpinion));
    }

    /**
     * 删除政务审核意见
     */
    @ApiOperation("删除政务审核意见")
    @RequiresPermissions("manage:opinion:remove")
    @Log(title = "政务审核意见", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingInfoApprovalOpinionService.removeByIds(ids));
    }
}
