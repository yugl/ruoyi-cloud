package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 场馆基本信息对象 venue_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class VenueInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 场馆名称
     */
    private String venueName;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 场馆面积
     */
    private String venueArea;
    /**
     * 负责人
     */
    private String chargePerson;
    /**
     * 负责人联系方式
     */
    private String chargePersonPhone;
    /**
     * 防疫联系人
     */
    private String preventionPerson;
    /**
     * 防疫联系方式
     */
    private String preventionPersonPhone;
    /**
     * 展位数量
     */
    private Long boothNum;
    /**
     * 剩余展位数量
     */
    private Long surplusBoothNum;
    /**
     * 营业执照URL
     */
    private String businessLicenseUrl;

    /**
     * 客户经理
     */
    private String accountManager;
    /**
     * 客户经理联系方式
     */
    private String accountManagerPhone;

    /**
     * 客户经理运营平台帐号
     */
    private String accountManagerUsername;

}
