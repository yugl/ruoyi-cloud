package com.ruoyi.manage.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 人工发送短信对象 meeting_sms_phone
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Data
public class MeetingSmsPhoneDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议id
     */
    @NotNull(message = "会议ID不能为空")
    private Long meetingId;


}
