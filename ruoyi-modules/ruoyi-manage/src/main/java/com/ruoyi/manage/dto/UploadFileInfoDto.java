package com.ruoyi.manage.dto;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;
    
/**
 * 上传文件信息对象 upload_file_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
                @Data
        public class UploadFileInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
                /** 会议ID */
                private Long meetingId;
                /** 公司名称 */
                private String companyName;
                /** 文件名称 */
                private String fileName;
                /** 文件大小 */
                private Long fileSize;
                /** 文件格式 */
                private String fileExt;
                /** 文件类型 1：防疫材料
2：场馆防疫材料
3：展位防疫材料
4：防疫方案
5：消毒记录 */
                private String fileType;
                /** 文件URL */
                private String fileUrl;
                /** 文件存储ID */
                private String fastdfsId;
                /** 状态 0：有效
1：无效 */
                private String status;
                /** 创建人 */
                private String createdBy;
                /** 创建时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date createdTime;
                /** 更新人 */
                private String updatedBy;
                /** 更新时间 */
                        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                private Date updatedTime;

}
