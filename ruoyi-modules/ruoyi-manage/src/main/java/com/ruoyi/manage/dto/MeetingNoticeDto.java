package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议通知公告对象 meeting_notice
 *
 * @author ruoyi
 * @date 2022-04-01
 */
@Data
public class MeetingNoticeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 会议名称
     */
    private String meetingName;
    /**
     * 会议id
     */
    private Long meetingId;
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告内容
     */
    private String noticeContent;
    /**
     * 有效开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 有效结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 公告状态（0待发布 1已发布 2已撤回 3删除）
     */
    private String status;
    /**
     * 创建者
     */
    private String createdBy;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    /**
     * 更新者
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    private String beginDate;

    private String endDate;

}
