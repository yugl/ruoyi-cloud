package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.manage.domain.AppSubjectDetailInfo;
import com.ruoyi.manage.dto.AppSubjectDetailInfoDto;
import com.ruoyi.manage.mapper.AppSubjectDetailInfoMapper;
import com.ruoyi.manage.service.IAppSubjectDetailInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 问卷题目详情Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Service
public class AppSubjectDetailInfoServiceImpl extends ServiceImpl<AppSubjectDetailInfoMapper, AppSubjectDetailInfo> implements IAppSubjectDetailInfoService {
    @Autowired
    private AppSubjectDetailInfoMapper appSubjectDetailInfoMapper;

    /**
     * 查询问卷题目详情列表
     *
     * @param appSubjectDetailInfo 问卷题目详情
     * @return 问卷题目详情
     */
    @Override
    public List<AppSubjectDetailInfo> selectdList(AppSubjectDetailInfoDto appSubjectDetailInfo) {
        QueryWrapper<AppSubjectDetailInfo> q = new QueryWrapper<>();
        if (appSubjectDetailInfo.getSubjectId() != null) {
            q.eq("subject_id", appSubjectDetailInfo.getSubjectId());
        } else {
            return new ArrayList<AppSubjectDetailInfo>();
        }
        q.orderByAsc("sort_no");
        List<AppSubjectDetailInfo> list = this.list(q);
        return list;
    }


}
