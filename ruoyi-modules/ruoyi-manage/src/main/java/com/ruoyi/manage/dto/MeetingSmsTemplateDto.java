package com.ruoyi.manage.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 会议短信模板对象 meeting_sms_template
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class MeetingSmsTemplateDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    private Long meetingId;

    /**
     * 短信模板ID
     */
    private Long templateId;

    /**
     * 启用状态 0：启用 1：不启用
     */
    private String useStatus;

}
