package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MeetingVenueInfo;
import com.ruoyi.manage.dto.MeetingVenueInfoDto;
import com.ruoyi.manage.mapper.MeetingVenueInfoMapper;
import com.ruoyi.manage.service.IMeetingVenueInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 会议场馆信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingVenueInfoServiceImpl extends ServiceImpl<MeetingVenueInfoMapper, MeetingVenueInfo> implements IMeetingVenueInfoService {
    @Autowired
    private MeetingVenueInfoMapper meetingVenueInfoMapper;

    /**
     * 查询会议场馆信息列表
     *
     * @param meetingVenueInfo 会议场馆信息
     * @return 会议场馆信息
     */
    @Override
    public List<MeetingVenueInfo> selectdList(MeetingVenueInfoDto meetingVenueInfo) {
        QueryWrapper<MeetingVenueInfo> q = new QueryWrapper<>();
        if (meetingVenueInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingVenueInfo.getMeetingId());
        }
        if (meetingVenueInfo.getVenueName() != null && !meetingVenueInfo.getVenueName().trim().equals("")) {
            q.like("venue_name", meetingVenueInfo.getVenueName());
        }
        if (meetingVenueInfo.getCompanyName() != null && !meetingVenueInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", meetingVenueInfo.getCompanyName());
        }
        if (meetingVenueInfo.getVenueArea() != null && !meetingVenueInfo.getVenueArea().trim().equals("")) {
            q.eq("venue_area", meetingVenueInfo.getVenueArea());
        }
        if (meetingVenueInfo.getChargePerson() != null && !meetingVenueInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", meetingVenueInfo.getChargePerson());
        }
        if (meetingVenueInfo.getChargePersonPhone() != null && !meetingVenueInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", meetingVenueInfo.getChargePersonPhone());
        }
        if (meetingVenueInfo.getPreventionPerson() != null && !meetingVenueInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", meetingVenueInfo.getPreventionPerson());
        }
        if (meetingVenueInfo.getPreventionPersonPhone() != null && !meetingVenueInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", meetingVenueInfo.getPreventionPersonPhone());
        }
        if (meetingVenueInfo.getBoothNum() != null) {
            q.eq("booth_num", meetingVenueInfo.getBoothNum());
        }
        if (meetingVenueInfo.getSurplusBoothNum() != null) {
            q.eq("surplus_booth_num", meetingVenueInfo.getSurplusBoothNum());
        }
        if (meetingVenueInfo.getBusinessLicenseUrl() != null && !meetingVenueInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", meetingVenueInfo.getBusinessLicenseUrl());
        }
        if (meetingVenueInfo.getStatus() != null && !meetingVenueInfo.getStatus().trim().equals("")) {
            q.eq("status", meetingVenueInfo.getStatus());
        }
        if (meetingVenueInfo.getCreatedBy() != null && !meetingVenueInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingVenueInfo.getCreatedBy());
        }
        if (meetingVenueInfo.getCreatedTime() != null) {
            q.eq("created_time", meetingVenueInfo.getCreatedTime());
        }
        if (meetingVenueInfo.getUpdatedBy() != null && !meetingVenueInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingVenueInfo.getUpdatedBy());
        }
        if (meetingVenueInfo.getUpdatedTime() != null) {
            q.eq("updated_time", meetingVenueInfo.getUpdatedTime());
        }
        if (meetingVenueInfo.getAccountManager() != null && !meetingVenueInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", meetingVenueInfo.getAccountManager());
        }
        if (meetingVenueInfo.getAccountManagerPhone() != null && !meetingVenueInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", meetingVenueInfo.getAccountManagerPhone());
        }
        if (meetingVenueInfo.getAccountManagerUsername() != null && !meetingVenueInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", meetingVenueInfo.getAccountManagerUsername());
        }
        List<MeetingVenueInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<Long> selectMeetingIdsByCompanyName(String companyName) {
        QueryWrapper<MeetingVenueInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingVenueInfo::getCompanyName, companyName);
        List<MeetingVenueInfo> list = this.baseMapper.selectList(q);
        return list.stream().map(item -> item.getMeetingId()).collect(Collectors.toList());
    }

    @Override
    public String checkNameUnique(Long meetingId, String companyName) {
        QueryWrapper<MeetingVenueInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingVenueInfo::getCompanyName, companyName);
        q.lambda().eq(MeetingVenueInfo::getMeetingId, meetingId);
        List<MeetingVenueInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


}
