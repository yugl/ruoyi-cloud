package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.manage.domain.AppSubjectInfo;
import com.ruoyi.manage.dto.AppSubjectInfoDto;
import com.ruoyi.manage.service.IAppSubjectInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 问卷题目Controller
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Api(value = "问卷题目控制器", tags = {"问卷题目管理"})
@RestController
@RequestMapping("/subjectInfo")
public class AppSubjectInfoController extends BaseController {

    @Autowired
    private IAppSubjectInfoService appSubjectInfoService;

    /**
     * 查询问卷题目列表
     */
    @ApiOperation("查询问卷题目列表")
    @GetMapping("/list")
    public TableDataInfo list(AppSubjectInfoDto appSubjectInfo) {
        startPage();
        List<AppSubjectInfo> list = appSubjectInfoService.selectdList(appSubjectInfo);
        return getDataTable(list);
    }

    /**
     * 导出问卷题目列表
     */
    @ApiOperation("导出问卷题目列表")
    @Log(title = "问卷题目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppSubjectInfoDto appSubjectInfo) throws IOException {

        startPage();
        List<AppSubjectInfo> list = appSubjectInfoService.selectdList(appSubjectInfo);
        ExcelUtil<AppSubjectInfo> util = new ExcelUtil<AppSubjectInfo>(AppSubjectInfo.class);
        util.exportExcel(response, list, "问卷题目数据");
    }

    /**
     * 导入问卷题目
     */
    @ApiOperation("导出问卷题目列表")
    @Log(title = "问卷题目导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AppSubjectInfo> util = new ExcelUtil<AppSubjectInfo>(AppSubjectInfo.class);
        List<AppSubjectInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = appSubjectInfoService.saveOrUpdateBatch(list);
        } else {
            message = appSubjectInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取问卷题目详细信息
     */
    @ApiOperation("获取问卷题目详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(appSubjectInfoService.getById(id));
    }

    /**
     * 新增问卷题目
     */
    @ApiOperation("新增问卷题目")
    @Log(title = "问卷题目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppSubjectInfo appSubjectInfo) {
        return toAjax(appSubjectInfoService.save(appSubjectInfo));
    }

    /**
     * 修改问卷题目
     */
    @ApiOperation("修改问卷题目")
    @Log(title = "问卷题目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppSubjectInfo appSubjectInfo) {
        return toAjax(appSubjectInfoService.updateById(appSubjectInfo));
    }

    /**
     * 删除问卷题目
     */
    @ApiOperation("删除问卷题目")
    @Log(title = "问卷题目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(appSubjectInfoService.removeByIds(ids));
    }
}
