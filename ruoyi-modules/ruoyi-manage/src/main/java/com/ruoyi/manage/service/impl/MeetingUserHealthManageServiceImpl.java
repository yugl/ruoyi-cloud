package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.MeetingUserHealthManageDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.mapper.MeetingUserHealthManageMapper;
import com.ruoyi.manage.service.IMeetingUserHealthManageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会议员工健康管理Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingUserHealthManageServiceImpl extends ServiceImpl<MeetingUserHealthManageMapper, MeetingUserHealthManage> implements IMeetingUserHealthManageService {
    @Autowired
    private MeetingUserHealthManageMapper meetingUserHealthManageMapper;

    /**
     * 查询会议员工健康管理列表
     *
     * @param meetingUserHealthManage 会议员工健康管理
     * @return 会议员工健康管理
     */
    @Override
    public List<MeetingUserHealthManage> selectdList(MeetingUserHealthManageDto meetingUserHealthManage) {
        /*QueryWrapper<MeetingUserHealthManage> q = new QueryWrapper<>();
        if (meetingUserHealthManage.getMeetingId() != null && !meetingUserHealthManage.getMeetingId().trim().equals("")) {
            q.eq("meeting_id", meetingUserHealthManage.getMeetingId());
        }
        if (meetingUserHealthManage.getName() != null && !meetingUserHealthManage.getName().trim().equals("")) {
            q.like("name", meetingUserHealthManage.getName());
        }
        if (meetingUserHealthManage.getCompanyName() != null && !meetingUserHealthManage.getCompanyName().trim().equals("")) {
            q.like("company_name", meetingUserHealthManage.getCompanyName());
        }
        if (meetingUserHealthManage.getUserRole() != null && !meetingUserHealthManage.getUserRole().trim().equals("")) {
            q.eq("user_role", meetingUserHealthManage.getUserRole());
        }
        if (meetingUserHealthManage.getHealthAbnormalStatus() != null && !meetingUserHealthManage.getHealthAbnormalStatus().trim().equals("")) {
            q.eq("health_abnormal_status", meetingUserHealthManage.getHealthAbnormalStatus());
        }*/
        List<MeetingUserHealthManage> list = meetingUserHealthManageMapper.selectdList(meetingUserHealthManage);
        return list;
    }

    /**
     * 统计参会的人员数量
     *
     * @param meetingId
     * @return
     */
    @Override
    public Map<String, Object> statisticsMeetingPersonQuantity(String meetingId) {
        MeetingUserHealthManageDto dto = new MeetingUserHealthManageDto();
        dto.setMeetingId(meetingId);
        /*List<MeetingUserHealthManage> userHealthManageList = this.baseMapper.selectList(
                new QueryWrapper<MeetingUserHealthManage>().eq("meeting_id", meetingId)
        );*/
        List<MeetingUserHealthManage> userHealthManageList = meetingUserHealthManageMapper.selectdList(dto);
        //服务商人员数量
        Integer serviceProviderCount = 0;
        //展商人员数量
        Integer exhibitorCount = 0;
        //观众人员数量
        Integer audienceCount = 0;

        //主办人员数量
        Integer mainCount = 0;
        //承办人员数量
        Integer undertakerCount = 0;
        //场馆人员数量
        Integer venueCount = 0;

        Map<String, Object> map = new HashMap<>();
        if (userHealthManageList != null && userHealthManageList.size() > 0) {
            for (MeetingUserHealthManage obj : userHealthManageList) {
                String userRole = obj.getUserRole();
                if (PersonRole.SERVICE_PROVIDER.getIndex().equals(userRole)) {
                    serviceProviderCount += 1;
                } else if (PersonRole.EXHIBITOR.getIndex().equals(userRole)) {
                    exhibitorCount += 1;
                } else if (PersonRole.AUDIENCE.getIndex().equals(userRole)) {
                    audienceCount += 1;
                } else if (PersonRole.MAIN.getIndex().equals(userRole)) {
                    mainCount += 1;
                } else if (PersonRole.UNDERTAKER.getIndex().equals(userRole)) {
                    undertakerCount += 1;
                } else if (PersonRole.VENUE.getIndex().equals(userRole)) {
                    venueCount += 1;
                }
            }
        }
        map.put("serviceProviderCount", serviceProviderCount);
        map.put("exhibitorCount", exhibitorCount);
        map.put("audienceCount", audienceCount);

        map.put("mainCount", mainCount);
        map.put("undertakerCount", undertakerCount);
        map.put("venueCount", venueCount);

        return map;
    }

    /**
     * app端 根据手机号获取正在参会的会议或即将进行的会议
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult findMeetingHealthList(String phone) {
        List<Map<String, Object>> list = meetingUserHealthManageMapper.findMeetingHealthList(phone);
        return AjaxResult.success(list);
    }

    /**
     * 上传健康码 行程码图片
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult addHealthAndTripCode(Map<String, String> map) {
        String phone = map.get("phone");
        String meetingId = map.get("meetingId");
        String healthImgUrl = map.get("healthImgUrl");
        String tripImgUrl = map.get("tripImgUrl");

        MeetingUserHealthManage meetingUserHealthManage = meetingUserHealthManageMapper.selectOne(new QueryWrapper<MeetingUserHealthManage>()
                .eq("meeting_id", meetingId)
                .eq("phone", phone)
                .eq("status", "0")
        );
        if (meetingUserHealthManage == null) {
            return AjaxResult.error(500, "您未参加该会议");
        }
        meetingUserHealthManage.setHealthImgUrl(healthImgUrl);
        meetingUserHealthManage.setTripImgUrl(tripImgUrl);
        meetingUserHealthManageMapper.updateById(meetingUserHealthManage);
        return AjaxResult.success();
    }

    /**
     * 根据会议ID获取健康码图片 行程码图片
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult getHealthAndTripCode(Map<String, String> map) {
        String phone = map.get("phone");
        String meetingId = map.get("meetingId");
        MeetingUserHealthManage entity = meetingUserHealthManageMapper.selectOne(new QueryWrapper<MeetingUserHealthManage>()
                .eq("meeting_id", meetingId)
                .eq("phone", phone)
                .eq("status", "0")
        );
        if (entity == null) {
            return AjaxResult.error(500, "您未参加该会议");
        }
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("healthImgUrl", entity.getHealthImgUrl());
        resultMap.put("tripImgUrl", entity.getTripImgUrl());
        return AjaxResult.success(resultMap);
    }


}
