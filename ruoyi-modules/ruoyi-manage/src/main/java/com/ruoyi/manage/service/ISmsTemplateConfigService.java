package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsTemplateConfig;
import com.ruoyi.manage.dto.SmsTemplateConfigDto;

import java.util.List;

/**
 * 短信模板配置Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface ISmsTemplateConfigService extends IService<SmsTemplateConfig> {


    /**
     * 查询短信模板配置列表
     *
     * @param smsTemplateConfig 短信模板配置
     * @return 短信模板配置集合
     */
    public List<SmsTemplateConfig> selectdList(SmsTemplateConfigDto smsTemplateConfig);

    AjaxResult findTemplateSelect();

    AjaxResult add(SmsTemplateConfig smsTemplateConfig);

    public String getSmsContent(Long templateId);

    AjaxResult changeEnableStatus(SmsTemplateConfigDto dto);
}
