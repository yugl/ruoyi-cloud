package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.manage.domain.MeetingSmsTemplate;
import com.ruoyi.manage.domain.SmsTemplateConfig;
import com.ruoyi.manage.dto.MeetingSmsTemplateDto;
import com.ruoyi.manage.mapper.MeetingSmsTemplateMapper;
import com.ruoyi.manage.service.IMeetingSmsTemplateService;
import com.ruoyi.manage.service.ISmsTemplateConfigService;
import com.ruoyi.system.api.RemoteUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 会议短信模板Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Service
public class MeetingSmsTemplateServiceImpl extends ServiceImpl<MeetingSmsTemplateMapper, MeetingSmsTemplate> implements IMeetingSmsTemplateService {
    @Autowired
    private MeetingSmsTemplateMapper meetingSmsTemplateMapper;

    @Autowired
    ISmsTemplateConfigService smsTemplateConfigService;

    @Autowired
    RemoteUserService remoteUserService;

    @Autowired
    RedisService redisService;

    /**
     * 查询会议短信模板列表
     *
     * @param meetingSmsTemplate 会议短信模板
     * @return 会议短信模板
     */
    @Override
    public List<MeetingSmsTemplate> selectdList(Long meetingId) {
        QueryWrapper<MeetingSmsTemplate> q = new QueryWrapper<>();
        if (meetingId != null) {
            q.like("meeting_id", meetingId);
        }
        q.eq("status", "0");
        q.orderByDesc("created_time");
        List<MeetingSmsTemplate> list = this.list(q);
        return list;
    }

    /**
     * 会议短信新增
     *
     * @param id
     * @return
     */
    @Transactional
    @Override
    public AjaxResult add(MeetingSmsTemplateDto dto) {
        Long count = this.getBaseMapper().selectCount(new QueryWrapper<MeetingSmsTemplate>()
                .eq("status", "0")
                .eq("template_id", dto.getTemplateId())
                .eq("meeting_id", dto.getMeetingId())
        );
        if (count != null && count > 0) {
            return AjaxResult.error(500, "该短信模板已存在");
        }
        SmsTemplateConfig smsTemplateConfig = smsTemplateConfigService.getOne(new QueryWrapper<SmsTemplateConfig>()
                .eq("template_id", dto.getTemplateId()).eq("status", "0"));
        MeetingSmsTemplate meetingSmsTemplate = new MeetingSmsTemplate();
        meetingSmsTemplate.setMeetingId(dto.getMeetingId());
        meetingSmsTemplate.setTemplateId(smsTemplateConfig.getTemplateId());
        meetingSmsTemplate.setTemplateContent(smsTemplateConfig.getTemplateContent());
        meetingSmsTemplate.setNoticeType(smsTemplateConfig.getNoticeType());
        meetingSmsTemplate.setUseStatus(smsTemplateConfig.getUseStatus());
        meetingSmsTemplate.setRemark(smsTemplateConfig.getRemark());
        meetingSmsTemplate.setTemplateName(smsTemplateConfig.getTemplateName());
        this.save(meetingSmsTemplate);
        return AjaxResult.success();
    }

    /**
     * 获取明细
     *
     * @param id
     * @return
     */
    @Override
    public AjaxResult getInfo(Long id) {
        MeetingSmsTemplate meetingSmsTemplate = this.getById(id);
        SmsTemplateConfig smsTemplateConfig = smsTemplateConfigService.getById(meetingSmsTemplate.getTemplateId());
        meetingSmsTemplate.setTemplateName(smsTemplateConfig.getTemplateName());
        return AjaxResult.success(meetingSmsTemplate);
    }

    /**
     * 获取人工会议短信模板下拉
     *
     * @param meetingId
     * @return
     */
    @Override
    public AjaxResult findMeetingSmsSelect(Long meetingId) {
        List<Map<String, Object>> list = meetingSmsTemplateMapper.findMeetingSmsSelect(meetingId);
        return AjaxResult.success(list);
    }

    /**
     * 替换会议短信模板中的变量
     *
     * @param meetingId  会议ID
     * @param templateId 模板ID
     * @return
     */
    @Override
    public String getSmsContent(Long meetingId, Long templateId) {
        MeetingSmsTemplate meetingSmsTemplate = this.getBaseMapper().selectOne(new QueryWrapper<MeetingSmsTemplate>()
                .eq("meeting_id", meetingId)
                .eq("template_id", templateId)
                .eq("status", "0")
                .eq("use_status", "1")
        );
        if (meetingSmsTemplate == null || StringUtils.isEmpty(meetingSmsTemplate.getTemplateContent())) {
            return null;
        }
        return meetingSmsTemplate.getTemplateContent();
    }

    @Override
    public AjaxResult changeEnableStatus(MeetingSmsTemplateDto dto) {
        MeetingSmsTemplate config = this.getById(dto.getId());
        config.setUseStatus(dto.getUseStatus());
        boolean result = this.updateById(config);
        if (result) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error(500, "操作失败");
        }

    }


}
