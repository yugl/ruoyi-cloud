package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingSmsPhone;

/**
 * 人工发送短信Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-04
 */
public interface MeetingSmsPhoneMapper extends BaseMapper<MeetingSmsPhone> {


}
