package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 隐私协议设置对象 app_privacy_aggreement
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Data
public class AppPrivacyAggreementDto implements Serializable {
    private static final long serialVersionUID = 1L;

}
