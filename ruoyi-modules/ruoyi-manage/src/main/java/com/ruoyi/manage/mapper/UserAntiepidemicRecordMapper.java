package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.UserAntiepidemicRecord;
import com.ruoyi.manage.dto.UserAntiepidemicRecordDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 员工防疫记录Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface UserAntiepidemicRecordMapper extends BaseMapper<UserAntiepidemicRecord> {


    List<UserAntiepidemicRecord> selectdList(@Param("dto") UserAntiepidemicRecordDto dto);

    List<Map<String, Object>> findAntiepidemicList(@Param("dto") UserAntiepidemicRecordDto dto);

}
