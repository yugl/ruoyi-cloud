package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 短信模板配置对象 sms_template_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsTemplateConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 模板ID
     */
    private Integer templateId;
    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 启用状态 0：启用 1：不启用
     */
    private String useStatus;

}
