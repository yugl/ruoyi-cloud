package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppQuestionnaireConfig;
import com.ruoyi.manage.dto.AppQuestionnaireConfigDto;

/**
 * 问卷调查配置Service接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface IAppQuestionnaireConfigService extends IService<AppQuestionnaireConfig> {


    /**
     * 查询问卷调查配置列表
     *
     * @param appQuestionnaireConfig 问卷调查配置
     * @return 问卷调查配置集合
     */
    public List<AppQuestionnaireConfig> selectdList(AppQuestionnaireConfigDto appQuestionnaireConfig);

    AjaxResult getAppQuestionnaire(String meetingId);

    AjaxResult selectList();

    AjaxResult changeEnableStatus(AppQuestionnaireConfigDto dto);
}
