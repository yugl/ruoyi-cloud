package com.ruoyi.manage.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 行程码对象 user_travel_code
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Data
public class UserTravelCode implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phone;

    /**
     * 身份证号
     */
    @Excel(name = "身份证号")
    private String idCard;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * 途径日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "途径日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date travelDate;

    /**
     * 地区风险（1：低风险地区 2：中风险地区 3：高风险地区）
     */
    @Excel(name = "地区风险", readConverterExp = "1=：低风险地区,2=：中风险地区,3=：高风险地区")
    private String riskLevel;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;


}
