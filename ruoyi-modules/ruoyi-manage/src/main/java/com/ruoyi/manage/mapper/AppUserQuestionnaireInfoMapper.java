package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppUserQuestionnaireInfo;
import com.ruoyi.manage.dto.AppUserQuestionnaireInfoDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户问卷调查信息Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface AppUserQuestionnaireInfoMapper extends BaseMapper<AppUserQuestionnaireInfo> {


    List<AppUserQuestionnaireInfo> selectdList(@Param("dto") AppUserQuestionnaireInfoDto dto);
}
