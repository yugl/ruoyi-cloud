package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 防疫填报详情对象 antiepidemic_detail_config
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Data
public class AntiepidemicDetailConfigDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主表ID
     */
    private Long antiepidemicId;
    /**
     * 标题
     */
    private String name;

}
