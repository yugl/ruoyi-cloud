package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingDisinfectFile;
import com.ruoyi.manage.domain.MeetingDisinfectRecordInfo;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.AppDisinfectRecordInfoDto;
import com.ruoyi.manage.dto.MeetingDisinfectFileDto;
import com.ruoyi.manage.dto.MeetingDisinfectRecordInfoDto;
import com.ruoyi.manage.mapper.MeetingDisinfectRecordInfoMapper;
import com.ruoyi.manage.service.IMeetingDisinfectFileService;
import com.ruoyi.manage.service.IMeetingDisinfectRecordInfoService;
import com.ruoyi.manage.service.IMeetingUserHealthManageService;
import com.ruoyi.system.api.RemoteFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 会议消毒记录Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingDisinfectRecordInfoServiceImpl extends ServiceImpl<MeetingDisinfectRecordInfoMapper, MeetingDisinfectRecordInfo> implements IMeetingDisinfectRecordInfoService {
    @Autowired
    private MeetingDisinfectRecordInfoMapper meetingDisinfectRecordInfoMapper;

    @Autowired
    private IMeetingUserHealthManageService iMeetingUserHealthManageService;

    @Autowired
    IMeetingDisinfectFileService meetingDisinfectFileService;

    @Autowired
    RemoteFileService remoteFileService;


    /**
     * 查询会议消毒记录列表
     *
     * @param meetingDisinfectRecordInfo 会议消毒记录
     * @return 会议消毒记录
     */
    @Override
    public List<Map<String, Object>> selectdList(MeetingDisinfectRecordInfoDto meetingDisinfectRecordInfo) {
        return meetingDisinfectRecordInfoMapper.selectdList(meetingDisinfectRecordInfo);
        /*QueryWrapper<MeetingDisinfectRecordInfo> q = new QueryWrapper<>();
        if (meetingDisinfectRecordInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingDisinfectRecordInfo.getMeetingId());
        }
        if (!StringUtils.isEmpty(meetingDisinfectRecordInfo.getCompanyName())) {
            q.eq("company_name", meetingDisinfectRecordInfo.getCompanyName());
        }
        if (!StringUtils.isEmpty(meetingDisinfectRecordInfo.getDisinfectPerson())) {
            q.eq("disinfect_person", meetingDisinfectRecordInfo.getDisinfectPerson());
        }
        List<MeetingDisinfectRecordInfo> list = this.list(q);
        return list;*/
    }

    /**
     * 移动端 消毒记录填报
     *
     * @param AppDisinfectRecordInfoDto dto
     * @return
     */
    @Transactional
    @Override
    public AjaxResult disinfectAdd(AppDisinfectRecordInfoDto dto) {
        MeetingUserHealthManage userHealthManage = iMeetingUserHealthManageService.getBaseMapper().selectOne(
                new QueryWrapper<MeetingUserHealthManage>()
                        .eq("meeting_id", dto.getMeetingId())
                        .eq("phone", dto.getPhone())
                        .eq("status", "0")
        );
        if (userHealthManage == null) {
            return AjaxResult.error(500, "您还未报名该会议！");
        }

        //保存消毒记录主表
        MeetingDisinfectRecordInfo disinfectRecordInfo = new MeetingDisinfectRecordInfo();
        disinfectRecordInfo.setMeetingId(dto.getMeetingId());
        disinfectRecordInfo.setMeetingName(dto.getMeetingName());
        disinfectRecordInfo.setPhone(dto.getPhone());
        disinfectRecordInfo.setDisinfectAddress(dto.getDisinfectAddress());
        disinfectRecordInfo.setDisinfectTime(new Date());
        disinfectRecordInfo.setCreatedTime(new Date());
        this.getBaseMapper().insert(disinfectRecordInfo);

        //保存消毒文件
        ArrayList<MeetingDisinfectFile> list = new ArrayList<>();
        for (String url : dto.getFileList()) {
            MeetingDisinfectFile meetingDisinfectFile = new MeetingDisinfectFile();
            meetingDisinfectFile.setUrl(url);
            meetingDisinfectFile.setDisinfectId(disinfectRecordInfo.getId());
            meetingDisinfectFile.setCreatedTime(new Date());
            list.add(meetingDisinfectFile);
        }
        meetingDisinfectFileService.saveBatch(list);
        return AjaxResult.success("上报成功！");

    }

    @Override
    public List<MeetingDisinfectRecordInfo> findDisinfectRecordList(Map<String, String> map) {
        return meetingDisinfectRecordInfoMapper.findDisinfectRecordList(map);
    }

    @Override
    public AjaxResult findDisinfectRecordById(String id) {
        MeetingDisinfectFileDto dto = new MeetingDisinfectFileDto();
        dto.setDisinfectId(Long.parseLong(id));
        List<MeetingDisinfectFile> list = meetingDisinfectFileService.selectdList(dto);
        return AjaxResult.success(list);
    }


}
