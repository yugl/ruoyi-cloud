package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingSmsTemplate;
import com.ruoyi.manage.dto.MeetingSmsTemplateDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议短信模板Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface MeetingSmsTemplateMapper extends BaseMapper<MeetingSmsTemplate> {

    List<Map<String, Object>> selectdList(@Param("dto") MeetingSmsTemplateDto meetingSmsTemplate);

    List<Map<String, Object>> findMeetingSmsSelect(@Param("meetingId") Long meetingId);
}

