package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.manage.domain.AppSubjectInfo;
import com.ruoyi.manage.dto.AppSubjectInfoDto;
import com.ruoyi.manage.mapper.AppSubjectInfoMapper;
import com.ruoyi.manage.service.IAppSubjectInfoService;
import com.ruoyi.manage.vo.AppSubjectInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 问卷题目Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Service
public class AppSubjectInfoServiceImpl extends ServiceImpl<AppSubjectInfoMapper, AppSubjectInfo> implements IAppSubjectInfoService {
    @Autowired
    private AppSubjectInfoMapper appSubjectInfoMapper;

    /**
     * 查询问卷题目列表
     *
     * @param appSubjectInfo 问卷题目
     * @return 问卷题目
     */
    @Override
    public List<AppSubjectInfo> selectdList(AppSubjectInfoDto appSubjectInfo) {
        QueryWrapper<AppSubjectInfo> q = new QueryWrapper<>();
        if (appSubjectInfo.getQuestionnaireId() != null) {
            q.eq("questionnaire_id", appSubjectInfo.getQuestionnaireId());
        }
        if (appSubjectInfo.getSubject() != null && !appSubjectInfo.getSubject().trim().equals("")) {
            q.like("subject", appSubjectInfo.getSubject());
        }
        if (appSubjectInfo.getType() != null && !appSubjectInfo.getType().trim().equals("")) {
            q.like("type", appSubjectInfo.getType());
        }
        q.orderByAsc("sort_no");
        List<AppSubjectInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<AppSubjectInfoVo> selectSubjectList(Long questionnaireId) {
        return appSubjectInfoMapper.selectSubjectList(questionnaireId);
    }


}
