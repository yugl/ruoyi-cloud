package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppUserEnrollInfo;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.dto.AppUserEnrollInfoDto;

import java.util.List;
import java.util.Map;

/**
 * app报名信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IAppUserEnrollInfoService extends IService<AppUserEnrollInfo> {


    /**
     * 查询app报名信息列表
     *
     * @param appUserEnrollInfo app报名信息
     * @return app报名信息集合
     */
    public List<AppUserEnrollInfo> selectdList(AppUserEnrollInfoDto appUserEnrollInfo);

    /**
     * 我的参会码
     *
     * @param phone
     * @return
     */
    AjaxResult getCode(String phone);

    /**
     * 报名展会
     *
     * @param appUserEnrollInfoDto
     * @return
     */
    AjaxResult add(AppUserEnrollInfoDto appUserEnrollInfoDto);

    /**
     * 移动端 根据手机号 获取报名的会议
     *
     * @param map
     * @return
     */
    List<MeetingInfo> findMeetingList(Map<String, String> map);

    /**
     * 根据会议ID查询
     *
     * @param map
     * @return
     */
    AjaxResult getMeetingInfo(Map<String, String> map);

    /**
     * 会议名称下拉
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> findMeetingSelect(String phone);

    /**
     * 文件base64
     *
     * @param imgBase64
     * @return
     */
    AjaxResult upFileBase64(Map<String, String> map);
}
