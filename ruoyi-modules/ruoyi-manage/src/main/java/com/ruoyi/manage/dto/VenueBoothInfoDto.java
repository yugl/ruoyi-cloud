package com.ruoyi.manage.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 场馆展位信息对象 venue_booth_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class VenueBoothInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 场馆ID
     */
    private Long venueId;
    /**
     * 展馆名称
     */
    private String boothName;
    /**
     * 展厅/展位号
     */
    private Long boothOrderNum;
    /**
     * 面积
     */
    private Long boothArea;

    /**
     * 参展商
     */
    private String exhibitorName;
    /**
     * 责任人
     */
    private String chargePerson;
    /**
     * 责任人联系电话
     */
    private String chargePersonPhone;
    /**
     * 防疫负责人
     */
    private String preventionPerson;
    /**
     * 防疫负责人联系电话
     */
    private String preventionPersonPhone;

}
