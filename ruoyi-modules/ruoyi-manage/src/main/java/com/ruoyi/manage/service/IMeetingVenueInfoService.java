package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingVenueInfo;
import com.ruoyi.manage.dto.MeetingVenueInfoDto;

/**
 * 会议场馆信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingVenueInfoService extends IService<MeetingVenueInfo> {


    /**
     * 查询会议场馆信息列表
     *
     * @param meetingVenueInfo 会议场馆信息
     * @return 会议场馆信息集合
     */
    public List<MeetingVenueInfo> selectdList(MeetingVenueInfoDto meetingVenueInfo);

    /**
     * 通过公司名称精确查找会议ids
     * @param companyName
     * @return
     */
    List<Long> selectMeetingIdsByCompanyName(String companyName);

    public String checkNameUnique(Long meetingId,String companyName);

}
