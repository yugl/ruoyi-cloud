package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;
import com.ruoyi.manage.dto.AntiepidemicDetailConfigDto;

/**
 * 防疫填报详情Service接口
 *
 * @author ruoyi
 * @date 2022-04-12
 */
public interface IAntiepidemicDetailConfigService extends IService<AntiepidemicDetailConfig> {


    /**
     * 查询防疫填报详情列表
     *
     * @param antiepidemicDetailConfig 防疫填报详情
     * @return 防疫填报详情集合
     */
    public List<AntiepidemicDetailConfig> selectdList(AntiepidemicDetailConfigDto antiepidemicDetailConfig);

}
