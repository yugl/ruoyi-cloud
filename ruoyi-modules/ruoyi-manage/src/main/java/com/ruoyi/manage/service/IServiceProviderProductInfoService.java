package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.ServiceProviderProductInfo;
import com.ruoyi.manage.dto.ServiceProviderProductInfoDto;

/**
 * 服务商产品信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IServiceProviderProductInfoService extends IService<ServiceProviderProductInfo> {


    /**
     * 查询服务商产品信息列表
     *
     * @param serviceProviderProductInfo 服务商产品信息
     * @return 服务商产品信息集合
     */
    public List<ServiceProviderProductInfo> selectdList(ServiceProviderProductInfoDto serviceProviderProductInfo);

    /**
     *
     * @param useMeeting    产品使用的会议名称
     * @param companyName   产品对应的服务提供商公司名称
     * @param productName   产品名称
     * 这三个属性确定产品的唯一性
     * @return
     */
    String checkNameUnique(String useMeeting, String companyName,String productName);

}
