package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AntiepidemicDetailConfig;

/**
 * 防疫填报详情Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-12
 */
public interface AntiepidemicDetailConfigMapper extends BaseMapper<AntiepidemicDetailConfig> {


}
