package com.ruoyi.manage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户问卷调查信息对象 app_user_questionnaire_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class AppUserQuestionnaireInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    private Long meetingId;
    /**
     * 会议名称
     */
    @NotNull(message = "会议名称不能为空")
    private String meetingName;

    /**
     * 问卷调查id
     */
    @NotNull(message = "问卷调查ID不能为空")
    private Long questionnaireId;
    /**
     * 问卷名称
     */
    private String questionnaireName;
    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    private String phone;
    /**
     * 姓名
     */
    private String name;

    /**
     * 题目
     */
    private String subject;

    private List<Date> dateList;


    @NotNull(message = "必须填写问卷")
    private List<SubjectVo> list;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class SubjectVo implements Serializable {
        private String subject;
        private List<String> answer;
    }


}
