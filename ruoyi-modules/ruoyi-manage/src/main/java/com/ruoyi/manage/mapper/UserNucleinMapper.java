package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.UserNuclein;

/**
 * 核酸检测Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface UserNucleinMapper extends BaseMapper<UserNuclein> {


}
