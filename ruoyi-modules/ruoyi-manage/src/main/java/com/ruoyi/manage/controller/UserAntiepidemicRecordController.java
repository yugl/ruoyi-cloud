package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.UserAntiepidemicRecord;
import com.ruoyi.manage.dto.UserAntiepidemicRecordDto;
import com.ruoyi.manage.service.IUserAntiepidemicRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 员工防疫记录Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "员工防疫记录控制器", tags = {"员工防疫记录管理"})
@RestController
@RequestMapping("/userAntiepidemicRecord")
public class UserAntiepidemicRecordController extends BaseController {
    @Autowired
    private IUserAntiepidemicRecordService userAntiepidemicRecordService;

    /**
     * 查询员工防疫记录列表
     */
    @ApiOperation("查询员工防疫记录列表")
    @PostMapping("/list")
    public AjaxResult list(@RequestBody UserAntiepidemicRecordDto userAntiepidemicRecord) {
        List<UserAntiepidemicRecord> list = userAntiepidemicRecordService.selectdList(userAntiepidemicRecord);
        return AjaxResult.success(list);
    }

    /**
     * 导出员工防疫记录列表
     */
    @ApiOperation("导出员工防疫记录列表")
    @RequiresPermissions("manage:userAntiepidemicRecord:export")
    @Log(title = "员工防疫记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserAntiepidemicRecordDto userAntiepidemicRecord) throws IOException {

        startPage();
        List<UserAntiepidemicRecord> list = userAntiepidemicRecordService.selectdList(userAntiepidemicRecord);
        ExcelUtil<UserAntiepidemicRecord> util = new ExcelUtil<UserAntiepidemicRecord>(UserAntiepidemicRecord.class);
        util.exportExcel(response, list, "员工防疫记录数据");
    }

    /**
     * 导入员工防疫记录
     */
    @ApiOperation("导出员工防疫记录列表")
    @RequiresPermissions("manage:userAntiepidemicRecord:importData")
    @Log(title = "员工防疫记录导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<UserAntiepidemicRecord> util = new ExcelUtil<UserAntiepidemicRecord>(UserAntiepidemicRecord.class);
        List<UserAntiepidemicRecord> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = userAntiepidemicRecordService.saveOrUpdateBatch(list);
        } else {
            message = userAntiepidemicRecordService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取员工防疫记录详细信息
     */
    @ApiOperation("获取员工防疫记录详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("id") Long id) {
        return AjaxResult.success(userAntiepidemicRecordService.getById(id));
    }

    /**
     * 通过身份证号获取员工防疫记录详细信息
     */
    @ApiOperation("获取员工防疫记录详细信息")
    @RequiresPermissions("manage:userAntiepidemicRecord:query")
    @GetMapping(value = "/idCard/{idCard}")
    public AjaxResult getInfoByIdCard(@ApiParam("身份证号") @NotNull(message = "身份证号不能为空") @PathVariable("idCard") String idCard) {
        return AjaxResult.success(userAntiepidemicRecordService.getInfoByIdCard(idCard));
    }

    /**
     * 新增员工防疫记录
     */
    @ApiOperation("新增员工防疫记录")
    @RequiresPermissions("manage:userAntiepidemicRecord:add")
    @Log(title = "员工防疫记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserAntiepidemicRecord userAntiepidemicRecord) {
        return toAjax(userAntiepidemicRecordService.save(userAntiepidemicRecord));
    }

    /**
     * 修改员工防疫记录
     */
    @ApiOperation("修改员工防疫记录")
    @RequiresPermissions("manage:userAntiepidemicRecord:edit")
    @Log(title = "员工防疫记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserAntiepidemicRecord userAntiepidemicRecord) {
        return toAjax(userAntiepidemicRecordService.updateById(userAntiepidemicRecord));
    }

    /**
     * 删除员工防疫记录
     */
    @ApiOperation("删除员工防疫记录")
    @RequiresPermissions("manage:userAntiepidemicRecord:remove")
    @Log(title = "员工防疫记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(userAntiepidemicRecordService.removeByIds(ids));
    }
}
