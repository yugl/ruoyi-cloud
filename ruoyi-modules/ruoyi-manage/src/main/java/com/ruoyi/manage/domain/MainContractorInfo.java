package com.ruoyi.manage.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 主承办基本信息对象 main_contractor_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MainContractorInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 公司名称
     */
    @Excel(name = "公司名称")
    private String companyName;

    /**
     * 纳税人识别号
     */
    @Excel(name = "纳税人识别号")
    private String taxpayerIdentifierNum;

    /**
     * 行业
     */
    @Excel(name = "行业")
    private String industry;

    /**
     * 成立日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date incorporationDate;

    /**
     * 注册资本
     */
    @Excel(name = "注册资本")
    private String registeredCapital;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String chargePerson;

    /**
     * 负责人联系方式
     */
    @Excel(name = "负责人联系方式")
    private String chargePersonPhone;

    /**
     * 负责人职务
     */
    @Excel(name = "负责人职务")
    private String chargePersonPost;

    /**
     * 防疫负责人
     */
    @Excel(name = "防疫负责人")
    private String preventionPerson;

    /**
     * 防疫负责人联系方式
     */
    @Excel(name = "防疫负责人联系方式")
    private String preventionPersonPhone;

    /**
     * 客户经理
     */
    @Excel(name = "客户经理")
    private String accountManager;

    /**
     * 客户经理联系方式
     */
    @Excel(name = "客户经理联系方式")
    private String accountManagerPhone;

    /**
     * 营业执照URL
     */
    @Excel(name = "营业执照URL")
    private String businessLicenseUrl;

    /**
     * 经营范围
     */
    @Excel(name = "经营范围")
    private String businessNature;

    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


    /**
     * 客户经理运营平台帐号
     */
    @Excel(name = "客户经理运营平台帐号")
    private String accountManagerUsername;

}
