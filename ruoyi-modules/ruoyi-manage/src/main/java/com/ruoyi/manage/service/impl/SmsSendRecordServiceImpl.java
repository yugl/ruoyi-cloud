package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsSendRecord;
import com.ruoyi.manage.dto.SmsSendRecordDto;
import com.ruoyi.manage.mapper.SmsSendRecordMapper;
import com.ruoyi.manage.service.ISmsSendRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 短信管理Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class SmsSendRecordServiceImpl extends ServiceImpl<SmsSendRecordMapper, SmsSendRecord> implements ISmsSendRecordService {
    @Autowired
    private SmsSendRecordMapper smsSendRecordMapper;

    /**
     * 查询短信发送记录列表
     *
     * @param dto 短信发送记录
     * @return 短信发送记录集合
     */
    @Override
    public List<SmsSendRecord> selectdList(SmsSendRecordDto dto) {
        return smsSendRecordMapper.selectdList(dto);

      /*  QueryWrapper<SmsSendRecord> q = new QueryWrapper<>();
        if (dto.getMeetingId() != null) {
            q.eq("meeting_id", dto.getMeetingId());
        }
        if (dto.getMeetingName() != null && !dto.getMeetingName().trim().equals("")) {
            q.like("meeting_name", dto.getMeetingName());
        }
        if (dto.getSmsTemplateId() != null && !dto.getSmsTemplateId().trim().equals("")) {
            q.like("sms_template_id", dto.getSmsTemplateId());
        }
        if (dto.getSmsTemplateName() != null && !dto.getSmsTemplateName().trim().equals("")) {
            q.like("sms_template_name", dto.getSmsTemplateName());
        }
        if (dto.getPhone() != null && !dto.getPhone().trim().equals("")) {
            q.like("phone", dto.getPhone());
        }
        if (dto.getName() != null && !dto.getName().trim().equals("")) {
            q.like("name", dto.getName());
        }
        List<SmsSendRecord> list = this.list(q);
        return list;*/
    }

    @Override
    public AjaxResult countSmsSend(Long meetingId) {
        Long count = this.getBaseMapper().selectCount(new QueryWrapper<SmsSendRecord>()
                .eq("meeting_id", meetingId)
                .eq("status", "0")
        );
        return AjaxResult.success(count);
    }


}
