package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingVenueBoothInfo;
import com.ruoyi.manage.dto.MeetingVenueBoothInfoDto;

/**
 * 会议场馆展位信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingVenueBoothInfoService extends IService<MeetingVenueBoothInfo> {


    /**
     * 查询会议场馆展位信息列表
     *
     * @param meetingVenueBoothInfo 会议场馆展位信息
     * @return 会议场馆展位信息集合
     */
    public List<MeetingVenueBoothInfo> selectdList(MeetingVenueBoothInfoDto meetingVenueBoothInfo);
    public String checkNameUnique(Long venueId,String venueBoothName);
    public List<MeetingVenueBoothInfo> selectdListEx(MeetingVenueBoothInfoDto meetingVenueBoothInfo);
    public List<MeetingVenueBoothInfo> selectdLisbByExhibitorName(Long meetingId,String exhibitorName);

}
