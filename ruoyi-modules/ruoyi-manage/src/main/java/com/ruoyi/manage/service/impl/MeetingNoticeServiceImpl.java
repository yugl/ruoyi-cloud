package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.domain.MeetingNotice;
import com.ruoyi.manage.dto.MeetingNoticeDto;
import com.ruoyi.manage.mapper.MeetingInfoMapper;
import com.ruoyi.manage.mapper.MeetingNoticeMapper;
import com.ruoyi.manage.service.IMeetingNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 会议通知公告Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-01
 */
@Service
public class MeetingNoticeServiceImpl extends ServiceImpl<MeetingNoticeMapper, MeetingNotice> implements IMeetingNoticeService {
    @Autowired
    private MeetingNoticeMapper meetingNoticeMapper;

    @Autowired
    MeetingInfoMapper meetingInfoMapper;

    /**
     * 查询会议通知公告列表
     *
     * @param meetingNotice 会议通知公告
     * @return 会议通知公告
     */
    @Override
    public List<MeetingNotice> selectdList(MeetingNoticeDto meetingNotice) {
        meetingNotice.setCreatedBy(SecurityUtils.getUsername());
        return meetingNoticeMapper.selectdList(meetingNotice);
    }

    @Override
    public List<MeetingNotice> listByUser(MeetingNoticeDto meetingNotice) {
        String username = SecurityUtils.getUsername();

        return meetingNoticeMapper.selectdList(meetingNotice);
    }

    /**
     * app端 获取公告内容
     *
     * @param map
     * @return
     */
    @Override
    public AjaxResult appList(Map<String, String> map) {
        QueryWrapper<MeetingNotice> queryWrapper = new QueryWrapper<MeetingNotice>()
                .eq("status", "2")
                .le("start_time", new Date())
                .ge("end_time", new Date())
                .orderByDesc("created_time");
        if (StringUtils.isNotEmpty(map.get("noticeTitle"))) {
            queryWrapper.like("notice_title", map.get("noticeTitle"));
        }
        List<MeetingNotice> meetingNotices = this.getBaseMapper().selectList(queryWrapper);
        return AjaxResult.success(meetingNotices);
    }

    /**
     * app端 获取公告内容详情
     *
     * @param map
     * @return
     */
    @Override
    public AjaxResult getNoticeById(Map<String, String> map) {
        MeetingNotice meetingNotice = this.getById(map.get("id"));
        return AjaxResult.success(meetingNotice);
    }

    @Override
    public List<Map<String, String>> findMeetingList(String username) {
        List<MeetingInfo> meetingInfos = meetingInfoMapper.selectList(new QueryWrapper<MeetingInfo>()
                .eq("status", "0")
                .eq("main_org", username)
                .orderByDesc("created_time")
        );
        List<Map<String, String>> list = new ArrayList<>();
        list = meetingInfos.stream().map(meetingInfo -> {
            HashMap<String, String> map1 = new HashMap<>();
            map1.put("label", meetingInfo.getMeetingName());
            map1.put("value", meetingInfo.getId() + "");
            return map1;
        }).collect(Collectors.toList());
        return list;
    }


}
