package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.ServiceProviderProductInfo;
import com.ruoyi.manage.dto.ServiceProviderProductInfoDto;
import com.ruoyi.manage.mapper.ServiceProviderProductInfoMapper;
import com.ruoyi.manage.service.IServiceProviderProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 服务商产品信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class ServiceProviderProductInfoServiceImpl extends ServiceImpl<ServiceProviderProductInfoMapper, ServiceProviderProductInfo> implements IServiceProviderProductInfoService {
    @Autowired
    private ServiceProviderProductInfoMapper serviceProviderProductInfoMapper;

    /**
     * 查询服务商产品信息列表
     *
     * @param serviceProviderProductInfo 服务商产品信息
     * @return 服务商产品信息
     */
    @Override
    public List<ServiceProviderProductInfo> selectdList(ServiceProviderProductInfoDto serviceProviderProductInfo) {
        QueryWrapper<ServiceProviderProductInfo> q = new QueryWrapper<>();
                    if (serviceProviderProductInfo.getCompanyName() != null   && !serviceProviderProductInfo.getCompanyName().trim().equals("")){
                        q.like("company_name",serviceProviderProductInfo.getCompanyName());
                }
                    if (serviceProviderProductInfo.getProductName() != null   && !serviceProviderProductInfo.getProductName().trim().equals("")){
                        q.like("product_name",serviceProviderProductInfo.getProductName());
                }
                    if (serviceProviderProductInfo.getProductCode() != null   && !serviceProviderProductInfo.getProductCode().trim().equals("")){
                        q.eq("product_code",serviceProviderProductInfo.getProductCode());
                }
                    if (serviceProviderProductInfo.getProductExplain() != null   && !serviceProviderProductInfo.getProductExplain().trim().equals("")){
                        q.eq("product_explain",serviceProviderProductInfo.getProductExplain());
                }
                    if (serviceProviderProductInfo.getUseMeeting() != null   && !serviceProviderProductInfo.getUseMeeting().trim().equals("")){
                        q.eq("use_meeting",serviceProviderProductInfo.getUseMeeting());
                }
                    if (serviceProviderProductInfo.getUseStartTime() != null  ){
                        q.eq("use_start_time",serviceProviderProductInfo.getUseStartTime());
                }
                    if (serviceProviderProductInfo.getUseEndTime() != null  ){
                        q.eq("use_end_time",serviceProviderProductInfo.getUseEndTime());
                }
        List<ServiceProviderProductInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(String useMeeting, String companyName, String productName) {
        QueryWrapper<ServiceProviderProductInfo> q = new QueryWrapper<>();
        q.lambda().eq(ServiceProviderProductInfo::getUseMeeting,useMeeting);
        q.lambda().eq(ServiceProviderProductInfo::getCompanyName,companyName);
        q.lambda().eq(ServiceProviderProductInfo::getProductName,productName);
        List<ServiceProviderProductInfo> list = this.baseMapper.selectList(q);
        if(list!=null && list.size()>0){
            return UserConstants.NOT_UNIQUE;
        }else{
            return UserConstants.UNIQUE;
        }


    }


}
