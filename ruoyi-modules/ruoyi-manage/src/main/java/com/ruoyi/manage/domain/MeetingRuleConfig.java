package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 会议入场规则配置对象 meeting_rule_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MeetingRuleConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 门禁规则 1：扫码+人脸识别
     * 2：身份证+人脸识别
     * 3：扫码
     * 4：身份证
     */
    @Excel(name = "门禁规则 1：扫码+人脸识别 2：身份证+人脸识别 3：扫码 4：身份证")
    private String entranceGuardRule;

    /**
     * 行程码禁入规则 1：高风险地区
     * 2：中风险地区
     * 3：低风险地区
     */
    @Excel(name = "行程码禁入规则 1：高风险地区 2：中风险地区 3：低风险地区")
    private String travelCodeRule;

    /**
     * 核酸规则（几天以内）
     */
    @Excel(name = "核酸规则", readConverterExp = "几=天以内")
    private Long nucleicAcidRule;

    /**
     * 疫苗规则（几次疫苗）
     */
    @Excel(name = "疫苗规则", readConverterExp = "几=次疫苗")
    private Long vaccinesRule;

    /**
     * 疫情数据提前刷新天数
     */
    @Excel(name = "疫情数据提前刷新天数")
    private Long advanceRefreshDay;

    /**
     * 刷新间隔时间
     */
    @Excel(name = "刷新间隔时间")
    private Long refreshTime;

    /**
     * 开放短信数量
     */
    @Excel(name = "开放短信数量")
    private Long smsTotalNum;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @TableLogic
    @Excel(name = "状态 0：有效 1：无效")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 问卷ID
     */
    private Long questionnaireId;
    /**
     * 观众承诺书 0：打开 1：关闭
     */
    private String isAudienceCommitment;
    /**
     * 观众承诺书
     */
    private String audienceCommitment;

    /**
     * 防疫填报ID
     */
    private Long antiepidemicId;

    @TableField(exist = false)
    private List<Integer> travelCodeRuleArr;

    @TableField(exist = false)
    private List<Integer> entranceGuardRuleArr;
}
