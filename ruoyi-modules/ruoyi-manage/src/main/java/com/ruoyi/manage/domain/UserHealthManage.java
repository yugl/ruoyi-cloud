package com.ruoyi.manage.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 员工健康管理对象 user_health_manage
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class UserHealthManage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 公司名称
     */
    @Excel(name = "公司名称")
    private String companyName;

    /**
     * 角色
     */
    @Excel(name = "角色")
    private String userRole;

    /**
     * 角色名称
     */
    @Excel(name = "角色名称")
    private String userRoleName;

    /**
     * 职务
     */
    @Excel(name = "职务")
    private String userPost;

    /**
     * 电话
     */
    @Excel(name = "电话")
    private String phone;

    /**
     * 身份证号
     */
    @Excel(name = "身份证号")
    private String idCard;

    /**
     * 健康异常状态 1：健康码异常
     * 2：行程码异常
     * 3：核酸异常
     * 4：疫苗异常
     */
    @Excel(name = "健康异常状态 1：健康码异常 2：行程码异常 3：核酸异常 4：疫苗异常")
    private String healthAbnormalStatus;

    /**
     * 健康码状态 1：正常
     * 2：异常
     */
    @Excel(name = "健康码状态 1：正常 2：异常")
    private String healthCodeStatus;

    /**
     * 行程码状态 1：正常
     * 2：异常
     */
    @Excel(name = "行程码状态 1：正常 2：异常")
    private String tripCodeStatus;

    /**
     * 最近核酸记录
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最近核酸记录", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date latelyNucleicAcid;

    /**
     * 核酸检测机构
     */
    @Excel(name = "核酸检测机构")
    private String nucleicAcidOrg;

    /**
     * 最近疫苗记录
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最近疫苗记录", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date latelyVaccines;

    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;
}
