package com.ruoyi.manage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * app报名信息对象 app_user_enroll_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class AppUserEnrollInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    @ApiModelProperty(name = "phone", value = "手机号")
    private String phone;
    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    @ApiModelProperty(name = "meetingId", value = "会议ID")
    private Long meetingId;
    /**
     * 报名来源 1：微信小程序 2：支付宝小程序 3：H5
     */
    @NotNull(message = "报名来源不能为空")
    @ApiModelProperty(name = "enrollSource", value = "报名来源 1：微信小程序 2：支付宝小程序 3：H5")
    private String enrollSource;

}
