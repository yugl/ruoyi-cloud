package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.UploadFileInfo;
import com.ruoyi.manage.dto.UploadFileInfoDto;

/**
 * 上传文件信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IUploadFileInfoService extends IService<UploadFileInfo> {


    /**
     * 查询上传文件信息列表
     *
     * @param uploadFileInfo 上传文件信息
     * @return 上传文件信息集合
     */
    public List<UploadFileInfo> selectdList(UploadFileInfoDto uploadFileInfo);

}
