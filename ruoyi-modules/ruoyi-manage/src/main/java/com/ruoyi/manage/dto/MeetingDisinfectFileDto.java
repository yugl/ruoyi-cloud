package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 会议消毒记录文件对象 meeting_disinfect_file
 *
 * @author ruoyi
 * @date 2022-03-31
 */
@Data
public class MeetingDisinfectFileDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 消毒记录主表ID
     */
    private Long disinfectId;

}
