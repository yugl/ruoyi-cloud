package com.ruoyi.manage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 会议消毒记录对象 meeting_disinfect_record_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MeetingDisinfectRecordInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    @ApiModelProperty(name = "meetingId", value = "会议ID")
    private Long meetingId;
    /**
     * 公司名称
     */
    @ApiModelProperty(name = "companyName", value = "公司名称")
    private String companyName;
    /**
     * 消毒人员姓名
     */
    @ApiModelProperty(name = "disinfectPerson", value = "消毒人员姓名")
    private String disinfectPerson;
}
