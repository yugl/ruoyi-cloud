package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.UserTravelCode;
import com.ruoyi.manage.dto.UserTravelCodeDto;

import java.util.List;

/**
 * 行程码Service接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface IUserTravelCodeService extends IService<UserTravelCode> {


    /**
     * 查询行程码列表
     *
     * @param userTravelCode 行程码
     * @return 行程码集合
     */
    public List<UserTravelCode> selectdList(UserTravelCodeDto userTravelCode);

    /**
     * 用户疫情防疫数据
     *
     * @param phone
     * @return
     */
    AjaxResult getUserHealthInfo(String phone);
}
