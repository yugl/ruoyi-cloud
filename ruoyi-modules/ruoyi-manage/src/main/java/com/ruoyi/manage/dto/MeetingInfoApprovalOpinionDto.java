package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 政务审核意见对象 meeting_info_approval_opinion
 *
 * @author wangguiyu
 * @date 2022-05-04
 */
@Data
public class MeetingInfoApprovalOpinionDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议id
     */
    private Long meetingId;
    /**
     * 防疫申报状态:3：申报通过 4：申报未通过
     */
    private String meetingApprovalStatus;
    /**
     * 审批意见
     */
    private String meetingApprovalOpinion;
    /**
     * 审批单位
     */
    private String administrativeApprover;
    /**
     * 记录创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

}
