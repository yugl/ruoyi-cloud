package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 隐私协议设置对象 app_privacy_aggreement
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Data
public class AppPrivacyAggreement implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 隐私协议
     */
    @Excel(name = "隐私协议")
    private String privacyAgreement;

    @TableLogic
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 修改人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "修改人")
    private String updatedBy;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;


}
