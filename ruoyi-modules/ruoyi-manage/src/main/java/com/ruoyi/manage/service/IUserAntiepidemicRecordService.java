package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.UserAntiepidemicRecord;
import com.ruoyi.manage.dto.UserAntiepidemicRecordDto;

import java.util.List;
import java.util.Map;

/**
 * 员工防疫记录Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IUserAntiepidemicRecordService extends IService<UserAntiepidemicRecord> {


    /**
     * 查询员工防疫记录列表
     *
     * @param userAntiepidemicRecord 员工防疫记录
     * @return 员工防疫记录集合
     */
    public List<UserAntiepidemicRecord> selectdList(UserAntiepidemicRecordDto userAntiepidemicRecord);

    AjaxResult add(UserAntiepidemicRecordDto userAntiepidemicRecordDto);

    UserAntiepidemicRecord getInfoByIdCard(String idCard);

    AjaxResult findAntiepidemic(String meetingId);

    List<Map<String,Object>> findAntiepidemicList(UserAntiepidemicRecordDto dto);

    AjaxResult findAntiepidemicById(String id);
}
