package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsNoticeConfig;
import com.ruoyi.manage.mapper.SmsNoticeConfigMapper;
import com.ruoyi.manage.service.ISmsNoticeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 短信使用率提醒Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class SmsNoticeConfigServiceImpl extends ServiceImpl<SmsNoticeConfigMapper, SmsNoticeConfig> implements ISmsNoticeConfigService {
    @Autowired
    private SmsNoticeConfigMapper smsNoticeConfigMapper;

    /**
     * 根据key查询
     *
     * @param smsKey
     * @return
     */
    @Override
    public AjaxResult getBySmsKey(String smsKey) {
        SmsNoticeConfig noticeConfig = this.getBaseMapper().selectOne(new QueryWrapper<SmsNoticeConfig>().eq("sms_key", smsKey));
        if (noticeConfig != null) {
            return AjaxResult.success("操作成功", noticeConfig.getSmsValue());
        }
        return AjaxResult.error(500, "未获取到数据");
    }

    @Transactional
    @Override
    public AjaxResult updateSmsValue(String smsKey, String smsValue) {
        SmsNoticeConfig noticeConfig = smsNoticeConfigMapper.selectOne(new QueryWrapper<SmsNoticeConfig>()
                .eq("sms_key", smsKey)
        );
        if (noticeConfig != null) {
            noticeConfig.setSmsValue(smsValue);
            smsNoticeConfigMapper.updateById(noticeConfig);
            return AjaxResult.success();
        }
        return AjaxResult.error(500, "更新失败");
    }
}
