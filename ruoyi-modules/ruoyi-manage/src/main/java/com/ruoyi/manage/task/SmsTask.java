package com.ruoyi.manage.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.mapper.MeetingInfoMapper;
import com.ruoyi.manage.mapper.MeetingUserHealthManageMapper;
import com.ruoyi.manage.utils.SmsUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 短信定时任务
 */
@Slf4j
@Component
public class SmsTask {

    @Autowired
    MeetingInfoMapper meetingInfoMapper;

    @Autowired
    SmsUtils smsUtils;

    @Autowired
    MeetingUserHealthManageMapper meetingUserHealthManageMapper;


    /**
     * 短信定时任务 查询所有符合校验的会议，存在不符合的人数发送短信
     *
     * @param param
     * @return
     */
    @XxlJob("sendSmsEpidemicJobHandler")
    public ReturnT<String> sendSmsJobHandler(String param) throws Exception {
        List<MeetingInfo> meetingInfos = meetingInfoMapper.selectList(new QueryWrapper<MeetingInfo>()
                .eq("status", "0")
                .le("meeting_end_time", new Date())
                .isNotNull("meeting_end_time")
                .isNotNull("meeting_start_time")
        );
        //过滤出正在进行的会议 或者会议开始前三天的会议
        List<MeetingInfo> collect = meetingInfos.stream().filter(meetingInfo -> {
            Date meetingStartTime = meetingInfo.getMeetingStartTime();
            Date meetingEndTime = meetingInfo.getMeetingEndTime();
            meetingStartTime = DateUtil.offsetDay(meetingStartTime, -3);
            return DateUtil.isIn(new Date(), meetingStartTime, meetingEndTime);
        }).collect(Collectors.toList());
        try {
            for (MeetingInfo obj : collect) {
                Long count = meetingUserHealthManageMapper.selectCount(new QueryWrapper<MeetingUserHealthManage>()
                        .isNotNull("health_abnormal_status")
                        .eq("status", "0")
                        .eq("meeting_id", obj.getId())
                );
                if (count > 0 && StringUtils.isNotEmpty(obj.getPreventionOfficerTel())) {
                    SmsSendVo smsSendVo = new SmsSendVo();
                    smsSendVo.setPhone(obj.getPreventionOfficerTel());
                    smsSendVo.setName(obj.getPreventionOfficer());
                    smsSendVo.setMeetingId(obj.getId());
                    smsSendVo.setTemplateId(105L);
                    AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
                    if ((int) ajaxResult.get("code") != 200) {
                        XxlJobHelper.log("会议名称：" + obj.getMeetingName() + "发送短信失败，" + ajaxResult.get("msg"));
                    }
                }
            }
        } catch (Exception e) {
            XxlJobHelper.log(e);
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 短信定时任务 会议提醒
     *
     * @param param
     * @return
     */
    @XxlJob("sendNoticeMeetingJobHandler")
    public ReturnT<String> sendNoticeMeetingJobHandler(String param) throws Exception {
        MeetingInfo meetingInfo = meetingInfoMapper.selectById(param);
        List<MeetingUserHealthManage> list = meetingUserHealthManageMapper.selectList(new QueryWrapper<MeetingUserHealthManage>()
                .eq("meeting_id", param)
                .eq("status", "0")
        );
        try {
            for (MeetingUserHealthManage obj : list) {
                if (StringUtils.isNotEmpty(obj.getPhone())) {
                    SmsSendVo smsSendVo = new SmsSendVo();
                    smsSendVo.setPhone(obj.getPhone());
                    smsSendVo.setName(obj.getName());
                    smsSendVo.setMeetingId(obj.getId());
                    smsSendVo.setTemplateId(106L);
                    AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
                    if ((int) ajaxResult.get("code") != 200) {
                        XxlJobHelper.log("会议名称：" + meetingInfo.getMeetingName() + "发送短信失败，" + ajaxResult.get("msg"));
                    }
                }
            }
        } catch (Exception e) {
            XxlJobHelper.log(e);
        }
        return ReturnT.SUCCESS;
    }
}
