package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.MeetingUserHealthManageDto;
import com.ruoyi.manage.service.IMeetingUserHealthManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 会议员工健康管理Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议员工健康管理控制器", tags = {"会议员工健康管理管理"})
@RestController
@RequestMapping("/meetingUserHealthManage")
public class MeetingUserHealthManageController extends BaseController {
    @Autowired
    private IMeetingUserHealthManageService meetingUserHealthManageService;

    /**
     * 统计展会人员数量
     *
     * @return
     */
    @RequiresPermissions("manage:meetingUserHealthManage:list")
    @GetMapping("/statistic/{meetingId}")
    public AjaxResult statisticsMeetingPersonQuantity(@PathVariable("meetingId") String meetingId) {
        Map<String, Object> map = meetingUserHealthManageService.statisticsMeetingPersonQuantity(meetingId);
        return AjaxResult.success(map);
    }

    /**
     * 查询会议员工健康管理列表
     */
    @ApiOperation("查询会议员工健康管理列表")
    @RequiresPermissions("manage:meetingUserHealthManage:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingUserHealthManageDto meetingUserHealthManage) {
        startPage();
        List<MeetingUserHealthManage> list = meetingUserHealthManageService.selectdList(meetingUserHealthManage);
        return getDataTable(list);
    }

    /**
     * 导出会议员工健康管理列表
     */
    @ApiOperation("导出会议员工健康管理列表")
    @RequiresPermissions("manage:meetingUserHealthManage:export")
    @Log(title = "会议员工健康管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingUserHealthManageDto meetingUserHealthManage) throws IOException {
        startPage();
        List<MeetingUserHealthManage> list = meetingUserHealthManageService.selectdList(meetingUserHealthManage);
        ExcelUtil<MeetingUserHealthManage> util = new ExcelUtil<MeetingUserHealthManage>(MeetingUserHealthManage.class);
        util.exportExcel(response, list, "会议员工健康管理数据");
    }

    /**
     * 导入会议员工健康管理
     */
    @ApiOperation("导出会议员工健康管理列表")
    @RequiresPermissions("manage:meetingUserHealthManage:importData")
    @Log(title = "会议员工健康管理导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingUserHealthManage> util = new ExcelUtil<MeetingUserHealthManage>(MeetingUserHealthManage.class);
        List<MeetingUserHealthManage> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingUserHealthManageService.saveOrUpdateBatch(list);
        } else {
            message = meetingUserHealthManageService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议员工健康管理详细信息
     */
    @ApiOperation("获取会议员工健康管理详细信息")
    @RequiresPermissions("manage:meetingUserHealthManage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingUserHealthManageService.getById(id));
    }

    /**
     * 新增会议员工健康管理
     */
    @ApiOperation("新增会议员工健康管理")
    @RequiresPermissions("manage:meetingUserHealthManage:add")
    @Log(title = "会议员工健康管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingUserHealthManage meetingUserHealthManage) {
        return toAjax(meetingUserHealthManageService.save(meetingUserHealthManage));
    }

    /**
     * 修改会议员工健康管理
     */
    @ApiOperation("修改会议员工健康管理")
    @RequiresPermissions("manage:meetingUserHealthManage:edit")
    @Log(title = "会议员工健康管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingUserHealthManage meetingUserHealthManage) {
        return toAjax(meetingUserHealthManageService.updateById(meetingUserHealthManage));
    }

    /**
     * 删除会议员工健康管理
     */
    @ApiOperation("删除会议员工健康管理")
    @RequiresPermissions("manage:meetingUserHealthManage:remove")
    @Log(title = "会议员工健康管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingUserHealthManageService.removeByIds(ids));
    }
}
