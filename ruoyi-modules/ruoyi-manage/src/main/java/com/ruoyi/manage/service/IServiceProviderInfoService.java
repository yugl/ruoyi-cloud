package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.ServiceProviderInfo;
import com.ruoyi.manage.dto.ServiceProviderInfoDto;

import java.util.List;

/**
 * 服务商基本信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IServiceProviderInfoService extends IService<ServiceProviderInfo> {


    /**
     * 查询服务商基本信息列表
     *
     * @param serviceProviderInfo 服务商基本信息
     * @return 服务商基本信息集合
     */
    public List<ServiceProviderInfo> selectdList(ServiceProviderInfoDto serviceProviderInfo);

    public String checkNameUnique(String companyName);

    List<ServiceProviderInfo> selectdListByCompanyName(String companyName);

}
