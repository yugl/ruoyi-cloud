package com.ruoyi.manage.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.manage.domain.MeetingDisinfectRecordInfo;
import com.ruoyi.manage.domain.MeetingInfo;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.dto.AppDisinfectRecordInfoDto;
import com.ruoyi.manage.dto.AppUserEnrollInfoDto;
import com.ruoyi.manage.dto.AppUserQuestionnaireInfoDto;
import com.ruoyi.manage.dto.UserAntiepidemicRecordDto;
import com.ruoyi.manage.service.*;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.domain.SysFile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * app报名信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "app报名信息控制器", tags = {"app报名信息管理"})
@RestController
@RequestMapping("/appEnroll")
public class AppUserEnrollInfoController extends BaseController {

    @Autowired
    private IAppUserEnrollInfoService appUserEnrollInfoService;
    @Autowired
    IUserAntiepidemicRecordService iUserAntiepidemicRecordService;
    @Autowired
    IAppQuestionnaireConfigService appQuestionnaireConfigService;
    @Autowired
    private IAppUserQuestionnaireInfoService appUserQuestionnaireInfoService;
    @Autowired
    private IMeetingDisinfectRecordInfoService meetingDisinfectRecordInfoService;
    @Autowired
    IUserTravelCodeService travelCodeService;
    @Autowired
    RemoteFileService remoteFileService;

    @Autowired
    IMeetingUserHealthManageService meetingUserHealthManageService;

    @Autowired
    IMeetingRuleConfigService meetingRuleConfigService;

    /**
     * 根据手机号 查询报名过的会议
     *
     * @param map
     * @return
     */
    @PostMapping("/findMeetingList")
    public TableDataInfo findMeetingList(@RequestBody Map<String, String> map) {
        startPage();
        List<MeetingInfo> list = appUserEnrollInfoService.findMeetingList(map);
        return getDataTable(list);
    }

    /**
     * 会议名称下拉
     *
     * @param map
     * @return
     */
    @PostMapping("/findMeetingSelect")
    public AjaxResult findMeetingSelect(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("phone"))) {
            return AjaxResult.error(500, "手机号不能为空");
        }
        List<Map<String, Object>> list = appUserEnrollInfoService.findMeetingSelect(map.get("phone"));
        return AjaxResult.success(list);
    }


    /**
     * 根据会议ID查询
     *
     * @param map
     * @return
     */
    @PostMapping("/getMeetingInfo")
    public AjaxResult getMeetingInfo(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("meetingId"))) {
            return AjaxResult.error(500, "会议ID不能为空");
        }
        return appUserEnrollInfoService.getMeetingInfo(map);
    }


    /**
     * 获取app报名信息详细信息
     */
    @ApiOperation("我的参会码")
    @PostMapping(value = "/getInfo")
    public AjaxResult getInfo(@RequestBody Map<String, String> map) {
        return appUserEnrollInfoService.getCode(map.get("phone"));
    }

    /**
     * 填写报名信息
     */
    @ApiOperation("填写报名信息")
    @PostMapping("/enroll")
    public AjaxResult add(@RequestBody AppUserEnrollInfoDto appUserEnrollInfoDto) {
        return appUserEnrollInfoService.add(appUserEnrollInfoDto);
    }

    /**
     * 防疫信息填报列表
     */
    @ApiOperation("防疫信息填报列表")
    @PostMapping("/findAntiepidemic")
    public AjaxResult findAntiepidemic(@Valid @RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("meetingId"))) {
            return AjaxResult.error(500, "会议ID不能为空");
        }
        return iUserAntiepidemicRecordService.findAntiepidemic(map.get("meetingId"));
    }

    /**
     * 防疫信息填报
     */
    @ApiOperation("防疫信息填报")
    @PostMapping("/updata")
    public AjaxResult add(@Valid @RequestBody UserAntiepidemicRecordDto userAntiepidemicRecordDto) {
        return iUserAntiepidemicRecordService.add(userAntiepidemicRecordDto);
    }

    /**
     * 上传采集头像
     */
    @ApiOperation("上传采集头像")
    @PostMapping("/upImage")
    public AjaxResult upImage(@RequestParam("file") MultipartFile file) {
        R<SysFile> r = remoteFileService.upload(file);
        if (r.getCode() == 200) {
            return AjaxResult.success("操作成功", r.getData().getUrl());
        } else {
            return AjaxResult.error(500, "上传失败");
        }
    }

    /**
     * 上传采集头像
     */
    @ApiOperation("上传采集头像")
    @PostMapping("/upFileBase64")
    public AjaxResult upFileBase64(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("file"))) {
            return AjaxResult.error(500, "文件不能为空");
        }
        return appUserEnrollInfoService.upFileBase64(map);
    }

    /**
     * 消毒信息填报
     */
    @ApiOperation("消毒信息填报")
    @PostMapping("/disinfectAdd")
    public AjaxResult disinfectAdd(@Valid @RequestBody AppDisinfectRecordInfoDto dto) {
        return meetingDisinfectRecordInfoService.disinfectAdd(dto);
    }


    /**
     * 获取调查问卷
     *
     * @param meetingId
     * @return
     */
    @PostMapping("/getAppQuestionnaire")
    public AjaxResult getAppQuestionnaire(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("meetingId"))) {
            return AjaxResult.error(500, "会议ID不能为空");
        }
        return appQuestionnaireConfigService.getAppQuestionnaire(map.get("meetingId"));
    }

    /**
     * 填写调查问卷
     *
     * @param dto
     * @return
     */
    @PostMapping("/addQuestionnaire")
    public AjaxResult addQuestionnaire(@Valid @RequestBody AppUserQuestionnaireInfoDto dto) {
        return appUserQuestionnaireInfoService.addQuestionnaire(dto);
    }


    /**
     * TODO 疫情防疫数据
     *
     * @param phone
     * @return
     */
    @PostMapping("/getUserHealthInfo")
    public AjaxResult getUserHealthInfo(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("phone"))) {
            return AjaxResult.error(500, "手机号不能为空");
        }
        return travelCodeService.getUserHealthInfo(map.get("phone"));
    }

    /**
     * 根据手机号 防疫信息填报列表
     *
     * @param map
     * @return
     */
    @PostMapping("/findAntiepidemicList")
    public TableDataInfo findAntiepidemicList(@RequestBody UserAntiepidemicRecordDto dto) {
        startPage();
        List<Map<String, Object>> list = iUserAntiepidemicRecordService.findAntiepidemicList(dto);
        return getDataTable(list);
    }

    /**
     * 防疫信息填报详情
     *
     * @param phone
     * @return
     */
    @PostMapping("/findAntiepidemicById")
    public AjaxResult findAntiepidemicById(@RequestBody Map<String, String> map) {
        return iUserAntiepidemicRecordService.findAntiepidemicById(map.get("id"));
    }


    /**
     * 根据手机号 消毒记录
     *
     * @param map
     * @return
     */
    @PostMapping("/findDisinfectRecordList")
    public TableDataInfo findDisinfectRecordList(@RequestBody Map<String, String> map) {
        startPage();
        List<MeetingDisinfectRecordInfo> list = meetingDisinfectRecordInfoService.findDisinfectRecordList(map);
        return getDataTable(list);
    }

    /**
     * 防疫信息填报详情
     *
     * @param phone
     * @return
     */
    @PostMapping("/findDisinfectRecordById")
    public AjaxResult findDisinfectRecordById(@RequestBody Map<String, String> map) {
        return meetingDisinfectRecordInfoService.findDisinfectRecordById(map.get("id"));
    }


    /**
     * 获取会议名称下拉
     *
     * @param phone
     * @return
     */
    @PostMapping("/findMeetingHealthList")
    public AjaxResult findMeetingHealthList(@RequestBody Map<String, String> map) {
        return meetingUserHealthManageService.findMeetingHealthList(map.get("phone"));
    }


    /**
     * 上传健康码 行程码图片
     *
     * @param phone
     * @return
     */
    @PostMapping("/addHealthAndTripCode")
    public AjaxResult addHealthAndTripCode(@RequestBody Map<String, String> map) {
        return meetingUserHealthManageService.addHealthAndTripCode(map);
    }

    /**
     * 获取健康码图片 行程码图片
     *
     * @param phone
     * @return
     */
    @PostMapping("/getHealthAndTripCode")
    public AjaxResult getHealthAndTripCode(@RequestBody Map<String, String> map) {
        return meetingUserHealthManageService.getHealthAndTripCode(map);
    }


    /**
     * 获取承诺书
     *
     * @param phone
     * @return
     */
    @PostMapping("/getAudienceCommitment")
    public AjaxResult getAudienceCommitment(@RequestBody Map<String, String> map) {
        if (StringUtils.isEmpty(map.get("meetingId"))) {
            return AjaxResult.error(500, "会议ID不能为空");
        }
        MeetingRuleConfig meetingRuleConfig = meetingRuleConfigService.getInfo(Long.parseLong(map.get("meetingId")));
        HashMap<String, String> result = new HashMap<>();
        if (meetingRuleConfig == null) {
            result.put("isAudienceCommitment", "1");
            result.put("audienceCommitment", null);
            return AjaxResult.success(result);
        }
        result.put("isAudienceCommitment", meetingRuleConfig.getIsAudienceCommitment());
        result.put("audienceCommitment", meetingRuleConfig.getAudienceCommitment());
        return AjaxResult.success(result);
    }
}
