package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 员工防疫记录对象 user_antiepidemic_record
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class UserAntiepidemicRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    private Long antiepidemicId;

    private Long meetingId;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phone;

    private String idCard;

    private String name;

    private String title;

    private String answer;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createdTime;

    @TableField(exist = false)
    private String meetingName;

    @TableField(exist = false)
    private String meetingLogo;

    private String identify;

}
