package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsSendRecord;
import com.ruoyi.manage.dto.SmsSendRecordDto;

import java.util.List;

/**
 * 短信管理Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface ISmsSendRecordService extends IService<SmsSendRecord> {


    /**
     * 查询短信发送记录列表
     *
     * @param dto 短信发送记录
     * @return 短信发送记录集合
     */
    public List<SmsSendRecord> selectdList(SmsSendRecordDto dto);

    AjaxResult countSmsSend(Long meetingId);
}
