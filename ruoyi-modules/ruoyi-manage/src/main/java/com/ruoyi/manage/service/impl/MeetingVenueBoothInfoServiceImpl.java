package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.MeetingVenueBoothInfo;
import com.ruoyi.manage.dto.MeetingVenueBoothInfoDto;
import com.ruoyi.manage.mapper.MeetingVenueBoothInfoMapper;
import com.ruoyi.manage.service.IMeetingVenueBoothInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会议场馆展位信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class MeetingVenueBoothInfoServiceImpl extends ServiceImpl<MeetingVenueBoothInfoMapper, MeetingVenueBoothInfo> implements IMeetingVenueBoothInfoService {
    @Autowired
    private MeetingVenueBoothInfoMapper meetingVenueBoothInfoMapper;

    /**
     * 查询会议场馆展位信息列表
     *
     * @param meetingVenueBoothInfo 会议场馆展位信息
     * @return 会议场馆展位信息
     */
    @Override
    public List<MeetingVenueBoothInfo> selectdList(MeetingVenueBoothInfoDto meetingVenueBoothInfo) {
        QueryWrapper<MeetingVenueBoothInfo> q = new QueryWrapper<>();
        if (meetingVenueBoothInfo.getMeetingId() != null) {
            q.eq("meeting_id", meetingVenueBoothInfo.getMeetingId());
        }
        if (meetingVenueBoothInfo.getVenueId() != null) {
            q.eq("venue_id", meetingVenueBoothInfo.getVenueId());
        }
        if (meetingVenueBoothInfo.getBoothName() != null && !meetingVenueBoothInfo.getBoothName().trim().equals("")) {
            q.like("booth_name", meetingVenueBoothInfo.getBoothName());
        }
        if (meetingVenueBoothInfo.getBoothOrderNum() != null && !meetingVenueBoothInfo.getBoothOrderNum().trim().equals("")) {
            q.eq("booth_order_num", meetingVenueBoothInfo.getBoothOrderNum());
        }
        if (meetingVenueBoothInfo.getExhibitorName() != null && !meetingVenueBoothInfo.getExhibitorName().trim().equals("")) {
            q.like("exhibitor_name", meetingVenueBoothInfo.getExhibitorName());
        }
        if (meetingVenueBoothInfo.getChargePerson() != null && !meetingVenueBoothInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", meetingVenueBoothInfo.getChargePerson());
        }
        if (meetingVenueBoothInfo.getChargePersonPhone() != null && !meetingVenueBoothInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", meetingVenueBoothInfo.getChargePersonPhone());
        }
        if (meetingVenueBoothInfo.getPreventionPerson() != null && !meetingVenueBoothInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", meetingVenueBoothInfo.getPreventionPerson());
        }
        if (meetingVenueBoothInfo.getPreventionPersonPhone() != null && !meetingVenueBoothInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", meetingVenueBoothInfo.getPreventionPersonPhone());
        }
        if (meetingVenueBoothInfo.getStatus() != null && !meetingVenueBoothInfo.getStatus().trim().equals("")) {
            q.eq("status", meetingVenueBoothInfo.getStatus());
        }
        if (meetingVenueBoothInfo.getCreatedBy() != null && !meetingVenueBoothInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", meetingVenueBoothInfo.getCreatedBy());
        }
        if (meetingVenueBoothInfo.getCreatedTime() != null) {
            q.eq("created_time", meetingVenueBoothInfo.getCreatedTime());
        }
        if (meetingVenueBoothInfo.getUpdatedBy() != null && !meetingVenueBoothInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", meetingVenueBoothInfo.getUpdatedBy());
        }
        if (meetingVenueBoothInfo.getUpdatedTime() != null) {
            q.eq("updated_time", meetingVenueBoothInfo.getUpdatedTime());
        }
        List<MeetingVenueBoothInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(Long venueId, String venueBoothName) {
        QueryWrapper<MeetingVenueBoothInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingVenueBoothInfo::getBoothName, venueBoothName);
        q.lambda().eq(MeetingVenueBoothInfo::getVenueId, venueId);
        List<MeetingVenueBoothInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public List<MeetingVenueBoothInfo> selectdListEx(MeetingVenueBoothInfoDto meetingVenueBoothInfo) {
        QueryWrapper<MeetingVenueBoothInfo> q = new QueryWrapper<>();

        if (meetingVenueBoothInfo.getExhibitorName() != null && !meetingVenueBoothInfo.getExhibitorName().trim().equals("")) {
            q.eq("meeting_id", meetingVenueBoothInfo.getMeetingId());
            q.eq("exhibitor_name", meetingVenueBoothInfo.getExhibitorName()).or().isNull("exhibitor_name").or().eq("exhibitor_name", "");
        } else {
            q.eq("meeting_id", meetingVenueBoothInfo.getMeetingId());
            q.isNull("exhibitor_name").or().eq("exhibitor_name", "");
        }
        List<MeetingVenueBoothInfo> list = this.list(q);
        return list;
    }

    @Override
    public List<MeetingVenueBoothInfo> selectdLisbByExhibitorName(Long meetingId, String exhibitorName) {
        QueryWrapper<MeetingVenueBoothInfo> q = new QueryWrapper<>();
        q.lambda().eq(MeetingVenueBoothInfo::getMeetingId, meetingId);
        q.lambda().eq(MeetingVenueBoothInfo::getExhibitorName, exhibitorName);
        List<MeetingVenueBoothInfo> list = this.list(q);
        return list;
    }


}
