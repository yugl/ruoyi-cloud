package com.ruoyi.manage.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "app.wx")
public class WxConfig {

    private String url;
    private String appid;
    private String secret;
}
