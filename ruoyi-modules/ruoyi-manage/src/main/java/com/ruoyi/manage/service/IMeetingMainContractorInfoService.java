package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingMainContractorInfo;
import com.ruoyi.manage.dto.MeetingMainContractorInfoDto;

import java.util.List;

/**
 * 会议承办信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingMainContractorInfoService extends IService<MeetingMainContractorInfo> {


    /**
     * 查询会议承办信息列表
     *
     * @param meetingMainContractorInfo 会议承办信息
     * @return 会议承办信息集合
     */
    List<MeetingMainContractorInfo> selectdList(MeetingMainContractorInfoDto meetingMainContractorInfo);

    /**
     * 通过公司名称精确查找会议ids
     * @param companyName
     * @return
     */
    List<Long> selectMeetingIdsByCompanyName(String companyName);

    public String checkNameUnique(Long meetingId,String companyName);

}
