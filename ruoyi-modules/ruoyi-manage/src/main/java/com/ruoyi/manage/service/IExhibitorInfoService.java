package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.ExhibitorInfo;
import com.ruoyi.manage.domain.VenueInfo;
import com.ruoyi.manage.dto.ExhibitorInfoDto;

/**
 * 展商基本信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IExhibitorInfoService extends IService<ExhibitorInfo> {


    /**
     * 查询展商基本信息列表
     *
     * @param exhibitorInfo 展商基本信息
     * @return 展商基本信息集合
     */
    public List<ExhibitorInfo> selectdList(ExhibitorInfoDto exhibitorInfo);

    public String checkNameUnique(String companyName);
    List<ExhibitorInfo> selectdListByCompanyName(String companyName);

}
