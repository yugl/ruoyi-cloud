package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppSubjectDetailInfo;

/**
 * 问卷题目详情Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface AppSubjectDetailInfoMapper extends BaseMapper<AppSubjectDetailInfo> {


}
