package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.VenueInfo;
import com.ruoyi.manage.dto.VenueInfoDto;
import com.ruoyi.manage.mapper.VenueInfoMapper;
import com.ruoyi.manage.service.IVenueInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 场馆基本信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class VenueInfoServiceImpl extends ServiceImpl<VenueInfoMapper, VenueInfo> implements IVenueInfoService {
    //@Autowired
    //private VenueInfoMapper venueInfoMapper;

    /**
     * 查询场馆基本信息列表
     *
     * @param venueInfo 场馆基本信息
     * @return 场馆基本信息
     */
    @Override
    public List<VenueInfo> selectdList(VenueInfoDto venueInfo) {
        QueryWrapper<VenueInfo> q = new QueryWrapper<>();
        if (venueInfo.getVenueName() != null && !venueInfo.getVenueName().trim().equals("")) {
            q.like("venue_name", venueInfo.getVenueName());
        }
        if (venueInfo.getCompanyName() != null && !venueInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", venueInfo.getCompanyName());
        }
        if (venueInfo.getVenueArea() != null && !venueInfo.getVenueArea().trim().equals("")) {
            q.eq("venue_area", venueInfo.getVenueArea());
        }
        if (venueInfo.getChargePerson() != null && !venueInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", venueInfo.getChargePerson());
        }
        if (venueInfo.getChargePersonPhone() != null && !venueInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", venueInfo.getChargePersonPhone());
        }
        if (venueInfo.getPreventionPerson() != null && !venueInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", venueInfo.getPreventionPerson());
        }
        if (venueInfo.getPreventionPersonPhone() != null && !venueInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", venueInfo.getPreventionPersonPhone());
        }
        if (venueInfo.getBoothNum() != null) {
            q.eq("booth_num", venueInfo.getBoothNum());
        }
        if (venueInfo.getSurplusBoothNum() != null) {
            q.eq("surplus_booth_num", venueInfo.getSurplusBoothNum());
        }
        if (venueInfo.getBusinessLicenseUrl() != null && !venueInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", venueInfo.getBusinessLicenseUrl());
        }
        if (venueInfo.getAccountManager() != null && !venueInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", venueInfo.getAccountManager());
        }
        if (venueInfo.getAccountManagerPhone() != null && !venueInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", venueInfo.getAccountManagerPhone());
        }
        if (venueInfo.getAccountManagerUsername() != null && !venueInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", venueInfo.getAccountManagerUsername());
        }
        List<VenueInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(String venueName) {
        QueryWrapper<VenueInfo> q = new QueryWrapper<>();
        q.lambda().eq(VenueInfo::getVenueName, venueName);
        List<VenueInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 如果 venueName 为null,查询所有场馆
     *
     * @param venueName
     * @return
     */
    @Override
    public List<VenueInfo> selectdListByVenueName(String venueName) {
        QueryWrapper<VenueInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(VenueInfo::getVenueName);
        if (venueName != null)
            q.lambda().eq(VenueInfo::getVenueName, venueName);
        List<VenueInfo> list = this.baseMapper.selectList(q);
        return list;
    }

    @Override
    public List<VenueInfo> selectdListByCompanyName(String companyName) {
        QueryWrapper<VenueInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(VenueInfo::getCompanyName);
        if (companyName != null)
            q.lambda().eq(VenueInfo::getCompanyName, companyName);
        List<VenueInfo> list = this.baseMapper.selectList(q);
        return list;
    }


}
