package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议基本信息对象 meeting_info
 *
 * @author 于观礼
 * @date 2022-03-23
 */
@Data
public class MeetingInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议名称
     */
    private String meetingName;
    /**
     * 会议类别 1：展会 2：会议
     */
    private String meetingCategory;
    /**
     * 会议类别名称
     */
    private String meetingCategoryName;
    /**
     * 会议状态 1：待申请 2：审批中 3：审批完成 4：进行中 5：已完成
     */
    private String meetingStatus;
    /**
     * 会议状态名称
     */
    private String meetingStatusName;
    /**
     * 防疫申报状态 1：未申报 2：申报中 3：申报通过 4：申报未通过
     */
    private String preventionDeclareStatus;
    /**
     * 防疫申报状态名称
     */
    private String preventionDeclareStatusName;
    /**
     * 会议开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date meetingStartTime;
    /**
     * 会议结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date meetingEndTime;
    /**
     * 场馆ID
     */
    private Long venueId;
    /**
     * 场馆名称
     */
    private String venueName;
    /**
     * 主办单位
     */
    private String mainOrg;
    /**
     * 会议地点
     */
    private String meetingAddress;
    /**
     * 预计展商数量
     */
    private Long expectExhibitorNum;
    /**
     * 预计观众数量
     */
    private Long expectAudienceNum;
    /**
     * 会议logo
     */
    private String meetingLogo;
    /**
     * 会议banner
     */
    private String meetingBanner;
    /**
     * 会议介绍
     */
    private String meetingIntroduce;
    /**
     * 会议流程
     */
    private String meetingFlow;
    /**
     * 审批状态 1：待审批 2：已审批 3：审批通过 4：审批不通过
     */
    private String approvalStatus;
    /**
     * 审批意见
     */
    private String approvalOpinion;
    /**
     * 微信小程序二维码url
     */
    private String wxQrCodeUrl;
    /**
     * 支付宝小程序二维码url
     */
    private String zfbQrCodeUrl;
    /**
     * H5网页地址
     */
    private String h5Url;
    /**
     * 会议防疫要求
     */
    private String meetingPreventionRequirements;
    /**
     * 客服联系人
     */
    private String customerServiceContact;
    /**
     * 客服联系人电话
     */
    private String customerServiceContactTel;
    /**
     * 客户经理
     */
    private String customerManager;
    /**
     * 客户经理电话
     */
    private String customerManagerTel;
    /**
     * 防疫负责人
     */
    private String preventionOfficer;
    /**
     * 防疫负责人电话
     */
    private String preventionOfficerTel;

    /** 客户经理运营平台帐号 */
    private String customerManagerUsername;

    /**
     * 行政审批人
     */
    private String administrativeApprover;

}
