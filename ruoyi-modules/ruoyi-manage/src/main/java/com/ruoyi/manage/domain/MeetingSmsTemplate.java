package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议短信模板对象 meeting_sms_template
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Data
public class MeetingSmsTemplate implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 短信模板ID
     */
    @Excel(name = "短信模板ID")
    private Long templateId;

    private String templateName;

    /**
     * 状态(0：有效 1：无效)
     */
    @TableLogic
    @Excel(name = "状态(0：有效 1：无效)")
    private String status;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 提醒类型 1：自动 2：人工 3：每天 4：提前
     */
    private String noticeType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 模板内容
     */
    @Excel(name = "模板内容")
    private String templateContent;

    private String useStatus;
}
