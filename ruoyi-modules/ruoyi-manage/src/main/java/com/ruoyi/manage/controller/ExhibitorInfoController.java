package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.ExhibitorInfo;
import com.ruoyi.manage.dto.ExhibitorInfoDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IExhibitorInfoService;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * 展商基本信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "展商基本信息控制器", tags = {"展商基本信息管理"})
@RestController
@RequestMapping("/exhibitor")
public class ExhibitorInfoController extends BaseController {
    @Autowired
    private IExhibitorInfoService exhibitorInfoService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private TokenService tokenService;


    /**
     * 查询展商基本信息列表
     */
    @ApiOperation("查询展商基本信息列表")
    @RequiresPermissions("manage:exhibitor:list")
    @GetMapping("/list")
    public TableDataInfo list(ExhibitorInfoDto exhibitorInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            exhibitorInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }
        startPage();
        List<ExhibitorInfo> list = exhibitorInfoService.selectdList(exhibitorInfo);
        return getDataTable(list);
    }

    /**
     * 导出展商基本信息列表
     */
    @ApiOperation("导出展商基本信息列表")
    @RequiresPermissions("manage:exhibitor:export")
    @Log(title = "展商基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExhibitorInfoDto exhibitorInfo) throws IOException {
        startPage();
        List<ExhibitorInfo> list = exhibitorInfoService.selectdList(exhibitorInfo);
        ExcelUtil<ExhibitorInfo> util = new ExcelUtil<ExhibitorInfo>(ExhibitorInfo.class);
        util.exportExcel(response, list, "展商基本信息数据");
    }

    /**
     * 导入展商基本信息
     */
    @ApiOperation("导出展商基本信息列表")
    @RequiresPermissions("manage:exhibitor:importData")
    @Log(title = "展商基本信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<ExhibitorInfo> util = new ExcelUtil<ExhibitorInfo>(ExhibitorInfo.class);
        List<ExhibitorInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = exhibitorInfoService.saveOrUpdateBatch(list);
        } else {
            message = exhibitorInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取展商基本信息详细信息
     */
    @ApiOperation("获取展商基本信息详细信息")
    @RequiresPermissions("manage:exhibitor:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(exhibitorInfoService.getById(id));
    }


    /**
     * 通过公司名称获取展商信息
     */
    @ApiOperation("通过公司名称获取会议展商信息")
    @RequiresPermissions("manage:exhibitor:query")
    @GetMapping(value = "/companyName/{companyName}")
    public AjaxResult getInfoByCompanyName(@ApiParam("公司名称") @NotNull(message = "公司名称不能为空且精确匹配")
                                           @PathVariable("companyName") String companyName) {
        return AjaxResult.success(exhibitorInfoService.selectdListByCompanyName(companyName));
    }

    /**
     * 获取所有展商
     */
    @ApiOperation("获取所有展商")
    @RequiresPermissions("manage:exhibitor:query")
    @GetMapping(value = "/exhibitors")
    public AjaxResult getAll() {
        return AjaxResult.success(exhibitorInfoService.selectdListByCompanyName(null));
    }



    /**
     * 新增展商基本信息
     */
    @ApiOperation("新增展商基本信息")
    @RequiresPermissions("manage:exhibitor:add")
    @Log(title = "展商基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExhibitorInfo exhibitorInfo) {
        if (UserConstants.NOT_UNIQUE.equals(exhibitorInfoService.checkNameUnique(exhibitorInfo.getCompanyName()))) {
            return AjaxResult.error("新增展商信息'" + exhibitorInfo.getCompanyName() + "'失败，公司名称已存在");
        }
        exhibitorInfo.setCreatedTime(DateUtils.getNowDate());
        exhibitorInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(exhibitorInfo.getChargePersonPhone(), exhibitorInfo.getChargePerson(), exhibitorInfo.getCompanyName(), PersonRole.EXHIBITOR.getIndex());
        return toAjax(exhibitorInfoService.save(exhibitorInfo));
    }

    /**
     * 修改展商基本信息
     */
    @ApiOperation("修改展商基本信息")
    @RequiresPermissions("manage:exhibitor:edit")
    @Log(title = "展商基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExhibitorInfo exhibitorInfo) {
        return toAjax(exhibitorInfoService.updateById(exhibitorInfo));
    }

    /**
     * 删除展商基本信息
     */
    @ApiOperation("删除展商基本信息")
    @RequiresPermissions("manage:exhibitor:remove")
    @Log(title = "展商基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(exhibitorInfoService.removeByIds(ids));
    }
}
