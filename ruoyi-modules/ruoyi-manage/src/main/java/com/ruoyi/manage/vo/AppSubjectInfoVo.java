package com.ruoyi.manage.vo;

import lombok.Data;

import java.util.List;

@Data
public class AppSubjectInfoVo {

    /**
     * 序号
     */
    private Long sortNo;

    /**
     * 题目
     */
    private String subject;

    /**
     * 类型 1：单选 2：多选 3：文本
     */
    private String type;

    private List<AppSubjectDetailVo> detailInfoList;


}
