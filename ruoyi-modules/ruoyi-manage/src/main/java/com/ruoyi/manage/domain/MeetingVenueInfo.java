package com.ruoyi.manage.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 会议场馆信息对象 meeting_venue_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MeetingVenueInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 场馆名称
     */
    @Excel(name = "场馆名称")
    private String venueName;

    /**
     * 公司名称
     */
    @Excel(name = "公司名称")
    private String companyName;

    /**
     * 场馆面积
     */
    @Excel(name = "场馆面积")
    private String venueArea;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String chargePerson;

    /**
     * 负责人联系方式
     */
    @Excel(name = "负责人联系方式")
    private String chargePersonPhone;

    /**
     * 防疫联系人
     */
    @Excel(name = "防疫联系人")
    private String preventionPerson;

    /**
     * 防疫联系人联系方式
     */
    @Excel(name = "防疫联系人联系方式")
    private String preventionPersonPhone;

    /**
     * 展位数量
     */
    @Excel(name = "展位数量")
    private Long boothNum;

    /**
     * 剩余展位数量
     */
    @Excel(name = "剩余展位数量")
    private Long surplusBoothNum;

    /**
     * 营业执照
     */
    @Excel(name = "营业执照")
    private String businessLicenseUrl;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @Excel(name = "状态 0：有效 1：无效")
    private String status;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 客户经理
     */
    @Excel(name = "客户经理")
    private String accountManager;

    /**
     * 客户经理联系方式
     */
    @Excel(name = "客户经理联系方式")
    private String accountManagerPhone;

    /** 客户经理运营平台帐号 */
    @Excel(name = "客户经理运营平台帐号")
    private String accountManagerUsername;
}
