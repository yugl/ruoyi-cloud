package com.ruoyi.manage.enumeration;

public enum RoleEnum {

    MAIN(101, "hostOrganizer"),
    UNDERTAKER(106, "undertakeOrganizer"),
    SERVICE_PROVIDER(104, "serviceProvider"),
    EXHIBITOR(103, "exhibitor"),
    VENUE(102, "venue");

    private Long name;
    private String index;

    // 构造方法
    private RoleEnum(long name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static Long getName(String index) {
        for (RoleEnum c : RoleEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public long getName() {
        return name;
    }

    public void setName(long name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
