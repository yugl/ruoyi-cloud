package com.ruoyi.manage.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 实名认证信息对象 app_user_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
@ApiModel("app用户实名认证信息")
public class AppUserInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 姓名
     */
    @NotNull(message = "姓名不能为空")
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;
    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    @ApiModelProperty(name = "phone", value = "手机号")
    private String phone;
    /**
     * 身份证号
     */
    @NotNull(message = "身份证号不能为空")
    @ApiModelProperty(name = "idCard", value = "身份证号")
    private String idCard;
    /**
     * 公司名称
     */
    @NotNull(message = "公司名称不能为空")
    @ApiModelProperty(name = "companyName", value = "公司名称")
    private String companyName;
    /**
     * 数据来源 1：微信小程序
     * 2：支付宝小程序
     * 3：H5
     */
    @NotNull(message = "数据来源不能为空")
    @ApiModelProperty(name = "dataSource", value = "1：微信小程序 2：支付宝小程序 3：H5")
    private String dataSource;

    @ApiModelProperty(name = "url", value = "采集头像")
    private String url;

    /**
     * openid
     */
    @NotNull(message = "openid不能为空")
    @ApiModelProperty(name = "openid", value = "openid")
    private String openid;

    /**
     * verifyCode
     */
    @NotNull(message = "验证码不能为空")
    private String verifyCode;

}
