package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.ServiceProviderProductInfo;
import com.ruoyi.manage.dto.ServiceProviderProductInfoDto;
import com.ruoyi.manage.service.IServiceProviderProductInfoService;
import io.seata.common.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 服务商产品信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "服务商产品信息控制器", tags = {"服务商产品信息管理"})
@RestController
@RequestMapping("/serviceProviderProduct")
public class ServiceProviderProductInfoController extends BaseController {
    @Autowired
    private IServiceProviderProductInfoService serviceProviderProductInfoService;

    /**
     * 查询服务商产品信息列表
     */
    @ApiOperation("查询服务商产品信息列表")
    @RequiresPermissions("manage:serviceProviderProduct:list")
    @GetMapping("/list")
    public TableDataInfo list(ServiceProviderProductInfoDto serviceProviderProductInfo) {
        startPage();
        List<ServiceProviderProductInfo> list = serviceProviderProductInfoService.selectdList(serviceProviderProductInfo);
        return getDataTable(list);
    }

    /**
     * 导出服务商产品信息列表
     */
    @ApiOperation("导出服务商产品信息列表")
    @RequiresPermissions("manage:serviceProviderProduct:export")
    @Log(title = "服务商产品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ServiceProviderProductInfoDto serviceProviderProductInfo) throws IOException {

        startPage();
        List<ServiceProviderProductInfo> list = serviceProviderProductInfoService.selectdList(serviceProviderProductInfo);
        ExcelUtil<ServiceProviderProductInfo> util = new ExcelUtil<ServiceProviderProductInfo>(ServiceProviderProductInfo.class);
        util.exportExcel(response, list, "服务商产品信息数据");
    }

    /**
     * 导入服务商产品信息
     */
    @ApiOperation("导入服务商产品信息列表")
    @RequiresPermissions("manage:serviceProviderProduct:importData")
    @Log(title = "服务商产品信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<ServiceProviderProductInfo> util = new ExcelUtil<ServiceProviderProductInfo>(ServiceProviderProductInfo.class);
        List<ServiceProviderProductInfo> list = util.importExcel(file.getInputStream());
        for(ServiceProviderProductInfo sppi:list){
            if(StringUtils.isBlank(sppi.getCompanyName()))
                sppi.setCompanyName(SecurityUtils.getUsername());//登录名即为公司名
            sppi.setCreatedBy(SecurityUtils.getUsername());
            sppi.setCreatedTime(DateUtils.getNowDate());
        }
        boolean message = false;
        if (updateSupport) {
            message = serviceProviderProductInfoService.saveOrUpdateBatch(list);
        } else {
            message = serviceProviderProductInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取服务商产品信息详细信息
     */
    @ApiOperation("获取服务商产品信息详细信息")
    @RequiresPermissions("manage:serviceProviderProduct:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(serviceProviderProductInfoService.getById(id));
    }

    /**
     * 新增服务商产品信息
     */
    @ApiOperation("新增服务商产品信息")
    @RequiresPermissions("manage:serviceProviderProduct:add")
    @Log(title = "服务商产品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ServiceProviderProductInfo serviceProviderProductInfo) {
        if (UserConstants.NOT_UNIQUE.equals(serviceProviderProductInfoService.checkNameUnique(serviceProviderProductInfo.getUseMeeting(),serviceProviderProductInfo.getCompanyName(),serviceProviderProductInfo.getProductName()))) {
            return AjaxResult.error("新增服务商产品信息'" + serviceProviderProductInfo.getProductName() + "'失败，产品名称已存在");
        }
        serviceProviderProductInfo.setCreatedTime(DateUtils.getNowDate());
        serviceProviderProductInfo.setCreatedBy(SecurityUtils.getUsername());
        return toAjax(serviceProviderProductInfoService.save(serviceProviderProductInfo));
    }

    /**
     * 修改服务商产品信息
     */
    @ApiOperation("修改服务商产品信息")
    @RequiresPermissions("manage:serviceProviderProduct:edit")
    @Log(title = "服务商产品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ServiceProviderProductInfo serviceProviderProductInfo) {
        return toAjax(serviceProviderProductInfoService.updateById(serviceProviderProductInfo));
    }

    /**
     * 删除服务商产品信息
     */
    @ApiOperation("删除服务商产品信息")
    @RequiresPermissions("manage:serviceProviderProduct:remove")
    @Log(title = "服务商产品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(serviceProviderProductInfoService.removeByIds(ids));
    }
}
