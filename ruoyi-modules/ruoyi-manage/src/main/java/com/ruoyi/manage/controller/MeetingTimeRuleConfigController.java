package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingTimeRuleConfig;
import com.ruoyi.manage.dto.MeetingTimeRuleConfigDto;
import com.ruoyi.manage.service.IMeetingTimeRuleConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 会议入场时间规则Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议入场时间规则控制器", tags = {"会议入场时间规则管理"})
@RestController
@RequestMapping("/meetingTimeRuleConfig")
public class MeetingTimeRuleConfigController extends BaseController {
    @Autowired
    private IMeetingTimeRuleConfigService meetingTimeRuleConfigService;

    /**
     * 查询会议入场时间规则列表
     */
    @ApiOperation("查询会议入场时间规则列表")
    @GetMapping("/list")
    public TableDataInfo list(MeetingTimeRuleConfigDto meetingTimeRuleConfig) {
        startPage();
        List<MeetingTimeRuleConfig> list = meetingTimeRuleConfigService.selectdList(meetingTimeRuleConfig);
        return getDataTable(list);
    }

    /**
     * 导出会议入场时间规则列表
     */
    @ApiOperation("导出会议入场时间规则列表")
    @Log(title = "会议入场时间规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingTimeRuleConfigDto meetingTimeRuleConfig) throws IOException {

        startPage();
        List<MeetingTimeRuleConfig> list = meetingTimeRuleConfigService.selectdList(meetingTimeRuleConfig);
        ExcelUtil<MeetingTimeRuleConfig> util = new ExcelUtil<MeetingTimeRuleConfig>(MeetingTimeRuleConfig.class);
        util.exportExcel(response, list, "会议入场时间规则数据");
    }

    /**
     * 导入会议入场时间规则
     */
    @ApiOperation("导出会议入场时间规则列表")
    @Log(title = "会议入场时间规则导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MeetingTimeRuleConfig> util = new ExcelUtil<MeetingTimeRuleConfig>(MeetingTimeRuleConfig.class);
        List<MeetingTimeRuleConfig> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = meetingTimeRuleConfigService.saveOrUpdateBatch(list);
        } else {
            message = meetingTimeRuleConfigService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取会议入场时间规则详细信息
     */
    @ApiOperation("获取会议入场时间规则详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingTimeRuleConfigService.getById(id));
    }

    /**
     * 新增会议入场时间规则
     */
    @ApiOperation("新增会议入场时间规则")
    @Log(title = "会议入场时间规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingTimeRuleConfig meetingTimeRuleConfig) {
        return meetingTimeRuleConfigService.add(meetingTimeRuleConfig);
    }

    /**
     * 修改会议入场时间规则
     */
    @ApiOperation("修改会议入场时间规则")
    @Log(title = "会议入场时间规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingTimeRuleConfig meetingTimeRuleConfig) {
        return toAjax(meetingTimeRuleConfigService.updateById(meetingTimeRuleConfig));
    }

    /**
     * 删除会议入场时间规则
     */
    @ApiOperation("删除会议入场时间规则")
    @Log(title = "会议入场时间规则", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingTimeRuleConfigService.removeByIds(ids));
    }
}
