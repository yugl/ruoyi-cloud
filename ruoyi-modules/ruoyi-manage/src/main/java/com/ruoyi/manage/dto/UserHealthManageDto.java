package com.ruoyi.manage.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 员工健康管理对象 user_health_manage
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class UserHealthManageDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 姓名
     */
    private String name;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 角色
     */
    private String userRole;
    /**
     * 职务
     */
    private String userPost;
    /**
     * 电话
     */
    private String phone;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 健康异常状态 1：健康码异常
     * 2：行程码异常
     * 3：核酸异常
     * 4：疫苗异常
     */
    private List<String> healthAbnormalStatus;
    /**
     * 健康码状态 1：正常
     * 2：异常
     */
    private String healthCodeStatus;
    /**
     * 行程码状态 1：正常
     * 2：异常
     */
    private String tripCodeStatus;
    /**
     * 最近核酸记录
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date latelyNucleicAcid;
    /**
     * 核酸检测机构
     */
    private String nucleicAcidOrg;
    /**
     * 最近疫苗记录
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date latelyVaccines;

}
