package com.ruoyi.manage.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.SmsTemplateConfig;
import com.ruoyi.manage.dto.AppQuestionnaireConfigDto;
import com.ruoyi.manage.dto.SmsTemplateConfigDto;
import com.ruoyi.manage.service.ISmsTemplateConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 短信模板配置Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "短信模板配置控制器", tags = {"短信模板配置管理"})
@RestController
@RequestMapping("/smsTemplateConfig")
public class SmsTemplateConfigController extends BaseController {
    @Autowired
    private ISmsTemplateConfigService smsTemplateConfigService;

    /**
     * 更新启用状态
     *
     * @return
     */
    @PostMapping("changeEnableStatus")
    public AjaxResult changeEnableStatus(@RequestBody SmsTemplateConfigDto dto) {
        return smsTemplateConfigService.changeEnableStatus(dto);
    }

    /**
     * 查询短信模板下拉
     *
     * @return
     */
    @GetMapping("findSmsTemplateSelect")
    public AjaxResult findSmsTemplateSelect() {
        return smsTemplateConfigService.findTemplateSelect();
    }

    /**
     * 查询短信模板配置列表
     */
    @ApiOperation("查询短信模板配置列表")
    @RequiresPermissions("manage:smsTemplateConfig:list")
    @PostMapping("/list")
    public TableDataInfo list(SmsTemplateConfigDto smsTemplateConfig) {
        startPage();
        List<SmsTemplateConfig> list = smsTemplateConfigService.selectdList(smsTemplateConfig);
        return getDataTable(list);
    }

    /**
     * 获取短信模板配置详细信息
     */
    @ApiOperation("获取短信模板配置详细信息")
    @RequiresPermissions("manage:smsTemplateConfig:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(smsTemplateConfigService.getById(id));
    }

    /**
     * 新增短信模板配置
     */
    @ApiOperation("新增短信模板配置")
    @RequiresPermissions("manage:smsTemplateConfig:add")
    @Log(title = "短信模板配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsTemplateConfig smsTemplateConfig) {
        return smsTemplateConfigService.add(smsTemplateConfig);
    }

    /**
     * 修改短信模板配置
     */
    @ApiOperation("修改短信模板配置")
    @RequiresPermissions("manage:smsTemplateConfig:edit")
    @Log(title = "短信模板配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsTemplateConfig smsTemplateConfig) {
        return toAjax(smsTemplateConfigService.updateById(smsTemplateConfig));
    }

    /**
     * 删除短信模板配置
     */
    @ApiOperation("删除短信模板配置")
    @RequiresPermissions("manage:smsTemplateConfig:remove")
    @Log(title = "短信模板配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(smsTemplateConfigService.removeByIds(ids));
    }
}
