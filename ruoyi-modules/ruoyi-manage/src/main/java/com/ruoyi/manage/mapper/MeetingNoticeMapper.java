package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingNotice;
import com.ruoyi.manage.dto.MeetingNoticeDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 会议通知公告Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-01
 */
public interface MeetingNoticeMapper extends BaseMapper<MeetingNotice> {


    List<MeetingNotice> selectdList(@Param("dto") MeetingNoticeDto meetingNotice);
}
