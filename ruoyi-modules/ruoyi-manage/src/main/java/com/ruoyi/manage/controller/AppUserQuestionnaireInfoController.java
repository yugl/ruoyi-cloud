package com.ruoyi.manage.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AppUserQuestionnaireInfo;
import com.ruoyi.manage.dto.AppUserQuestionnaireInfoDto;
import com.ruoyi.manage.service.IAppUserQuestionnaireInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 用户问卷调查信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "用户问卷调查信息控制器", tags = {"用户问卷调查信息管理"})
@RestController
@RequestMapping("/appQuestionnaire")
public class AppUserQuestionnaireInfoController extends BaseController {
    @Autowired
    private IAppUserQuestionnaireInfoService appUserQuestionnaireInfoService;

    /**
     * 查询用户问卷调查信息列表
     */
    @ApiOperation("查询用户问卷调查信息列表")
    @GetMapping("/list")
    public TableDataInfo list(AppUserQuestionnaireInfoDto appUserQuestionnaireInfo) {
        startPage();
        List<AppUserQuestionnaireInfo> list = appUserQuestionnaireInfoService.selectdList(appUserQuestionnaireInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户问卷调查信息列表
     */
    @ApiOperation("导出用户问卷调查信息列表")
    @Log(title = "用户问卷调查信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppUserQuestionnaireInfoDto appUserQuestionnaireInfo) throws IOException {

        startPage();
        List<AppUserQuestionnaireInfo> list = appUserQuestionnaireInfoService.selectdList(appUserQuestionnaireInfo);
        ExcelUtil<AppUserQuestionnaireInfo> util = new ExcelUtil<AppUserQuestionnaireInfo>(AppUserQuestionnaireInfo.class);
        util.exportExcel(response, list, "用户问卷调查信息数据");
    }

    /**
     * 导入用户问卷调查信息
     */
    @ApiOperation("导出用户问卷调查信息列表")
    @Log(title = "用户问卷调查信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AppUserQuestionnaireInfo> util = new ExcelUtil<AppUserQuestionnaireInfo>(AppUserQuestionnaireInfo.class);
        List<AppUserQuestionnaireInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = appUserQuestionnaireInfoService.saveOrUpdateBatch(list);
        } else {
            message = appUserQuestionnaireInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取用户问卷调查信息详细信息
     */
    @ApiOperation("获取用户问卷调查信息详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return appUserQuestionnaireInfoService.getInfo(id);
    }

    /**
     * 新增用户问卷调查信息
     */
    @ApiOperation("新增用户问卷调查信息")
    @Log(title = "用户问卷调查信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppUserQuestionnaireInfo appUserQuestionnaireInfo) {
        return toAjax(appUserQuestionnaireInfoService.save(appUserQuestionnaireInfo));
    }

    /**
     * 修改用户问卷调查信息
     */
    @ApiOperation("修改用户问卷调查信息")
    @Log(title = "用户问卷调查信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppUserQuestionnaireInfo appUserQuestionnaireInfo) {
        return toAjax(appUserQuestionnaireInfoService.updateById(appUserQuestionnaireInfo));
    }

    /**
     * 删除用户问卷调查信息
     */
    @ApiOperation("删除用户问卷调查信息")
    @Log(title = "用户问卷调查信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(appUserQuestionnaireInfoService.removeByIds(ids));
    }
}
