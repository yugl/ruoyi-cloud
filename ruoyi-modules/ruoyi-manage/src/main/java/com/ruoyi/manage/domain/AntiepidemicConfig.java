package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 防疫填报模板对象 antiepidemic_config
 *
 * @author ruoyi
 * @date 2022-04-23
 */
@Data
public class AntiepidemicConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 防疫填报名称
     */
    @NotNull(message = "防疫填报名称不能为空")
    @Excel(name = "防疫填报名称")
    private String antiepidemicName;

    /**
     * 启用状态 0：启用 1：不启用
     */
    private String enableStatus;

    /**
     * 0:有效 1：无效
     */
    @TableLogic
    @Excel(name = "0:有效 1：无效")
    private String status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Excel(name = "创建人")
    private String createdBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updatedTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updatedBy;

    @TableField(exist = false)
    private List<AntiepidemicDetailConfig> antiepidemicDetailConfigList;


}
