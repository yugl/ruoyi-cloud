package com.ruoyi.manage.service.impl;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.enumeration.DeptEnum;
import com.ruoyi.manage.enumeration.RoleEnum;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.utils.PassUtils;
import com.ruoyi.manage.utils.SmsUtils;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class RegisterUserServiceImpl implements IRegisterUserService {

    @Autowired
    SmsUtils smsUtils;

    @Autowired
    RemoteUserService remoteUserService;

    /**
     * 注册用户
     *
     * @param user         用户信息
     * @param userRoleName 角色权限字符
     * @return
     */
    @Override
    public AjaxResult registerUser(String phone, String name, String companyName, String roleName) {
        SysUser user = new SysUser();
        user.setUserName(companyName);
        user.setPhonenumber(phone);
        user.setNickName(companyName);

        String passwd = PassUtils.genPasswd();
        log.info("随机密码：" + passwd);
        user.setPassword(SecurityUtils.encryptPassword(passwd));
        Long[] arr = new Long[]{RoleEnum.getName(roleName)};
        user.setDeptId(DeptEnum.getName(roleName));
        user.setRoleIds(arr);
        R<Boolean> r = remoteUserService.registerUserInfo(user, SecurityConstants.INNER);
        if (r.getCode() == 500) {
            return AjaxResult.error(500, r.getMsg());
        }
        try {
            SmsSendVo smsSendVo = new SmsSendVo();
            smsSendVo.setTemplateId(101L);
            smsSendVo.setCompanyName(companyName);
            smsSendVo.setName(name);
            smsSendVo.setPhone(user.getPhonenumber());
            Map<String, Object> map = new HashMap<>();
            map.put("密码", passwd);
            smsSendVo.setMap(map);
            return smsUtils.smsSend(smsSendVo);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(500, "发送开通账号短信失败");
        }
    }

}
