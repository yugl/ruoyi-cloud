package com.ruoyi.manage.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.MeetingExhibitorInfo;
import com.ruoyi.manage.domain.MeetingVenueBoothInfo;
import com.ruoyi.manage.domain.VenueBoothInfo;
import com.ruoyi.manage.dto.MeetingVenueBoothInfoDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IMeetingVenueBoothInfoService;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.service.IVenueBoothInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 会议场馆展位信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "会议场馆展位信息控制器", tags = {"会议场馆展位信息管理"})
@RestController
@RequestMapping("/meetingVenueBooth")
public class MeetingVenueBoothInfoController extends BaseController {
    @Autowired
    private IMeetingVenueBoothInfoService meetingVenueBoothInfoService;

    @Autowired
    private IVenueBoothInfoService venueBoothInfoService;

    /**
     * 查询会议场馆展位信息列表
     */
    @ApiOperation("查询会议场馆展位信息列表")
    @RequiresPermissions("manage:meetingVenueBooth:list")
    @GetMapping("/list")
    public TableDataInfo list(MeetingVenueBoothInfoDto meetingVenueBoothInfo) {
        startPage();
        List<MeetingVenueBoothInfo> list = meetingVenueBoothInfoService.selectdList(meetingVenueBoothInfo);
        return getDataTable(list);
    }

    /**
     * 查询未被占用的会议场馆展位信息列表
     */
    @ApiOperation("查询会议场馆展位信息列表")
    @RequiresPermissions("manage:meetingVenueBooth:list")
    @GetMapping("/idle/list")
    public AjaxResult listEx(MeetingVenueBoothInfoDto meetingVenueBoothInfo) {
        //startPage();
        List<MeetingVenueBoothInfo> list = meetingVenueBoothInfoService.selectdListEx(meetingVenueBoothInfo);
        return AjaxResult.success(list);
    }

    /**
     * 导出会议场馆展位信息列表
     */
    @ApiOperation("导出会议场馆展位信息列表")
    @RequiresPermissions("manage:meetingVenueBooth:export")
    @Log(title = "会议场馆展位信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MeetingVenueBoothInfoDto meetingVenueBoothInfo) throws IOException {

        startPage();
        List<MeetingVenueBoothInfo> list = meetingVenueBoothInfoService.selectdList(meetingVenueBoothInfo);
        ExcelUtil<MeetingVenueBoothInfo> util = new ExcelUtil<MeetingVenueBoothInfo>(MeetingVenueBoothInfo.class);
        util.exportExcel(response, list, "会议场馆展位信息数据");
    }

    /**
     * 导入会议场馆展位信息
     */
    @ApiOperation("导入会议场馆展位信息列表")
    @RequiresPermissions("manage:meetingVenueBooth:importData")
    @Log(title = "会议场馆展位信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,Long meetingId,Long venueId) throws Exception {
        ExcelUtil<MeetingVenueBoothInfo> util = new ExcelUtil<MeetingVenueBoothInfo>(MeetingVenueBoothInfo.class);
        List<MeetingVenueBoothInfo> list = util.importExcel(file.getInputStream());
        List<VenueBoothInfo> venueBoothList = new ArrayList<>();
        for(MeetingVenueBoothInfo mvbi:list){
            mvbi.setCreatedTime(DateUtils.getNowDate());
            mvbi.setCreatedBy(SecurityUtils.getUsername());
            mvbi.setMeetingId(meetingId);
            mvbi.setVenueId(venueId);

            VenueBoothInfo vbi = new VenueBoothInfo();
            BeanUtils.copyBeanProp(vbi,mvbi);
            venueBoothList.add(vbi);
        }
        boolean message = false;
        if (updateSupport) {
            //批量保存或更新，因为 createTime 不同，多次提交会重复
            //message = meetingVenueBoothInfoService.saveOrUpdateBatch(list);
            //venueBoothInfoService.saveOrUpdateBatch(venueBoothList);

            // 逐条保存或更新 MeetingVenueBoothInfo
            for(MeetingVenueBoothInfo mv:list){
                UpdateWrapper<MeetingVenueBoothInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(MeetingVenueBoothInfo::getMeetingId,meetingId);
                updateWrapper.lambda().eq(MeetingVenueBoothInfo::getVenueId,venueId);
                updateWrapper.lambda().eq(MeetingVenueBoothInfo::getBoothName,mv.getBoothName());
                updateWrapper.lambda().eq(MeetingVenueBoothInfo::getBoothOrderNum,mv.getBoothOrderNum());
                meetingVenueBoothInfoService.saveOrUpdate(mv,updateWrapper);
            }

            // 逐条保存或更新 VenueBoothInfo
            for(VenueBoothInfo v:venueBoothList){
                UpdateWrapper<VenueBoothInfo> updateWrapper = new UpdateWrapper<>();
                updateWrapper.lambda().eq(VenueBoothInfo::getVenueId,venueId);
                updateWrapper.lambda().eq(VenueBoothInfo::getBoothName,v.getBoothName());
                updateWrapper.lambda().eq(VenueBoothInfo::getBoothOrderNum,v.getBoothOrderNum());
                venueBoothInfoService.saveOrUpdate(v,updateWrapper);
            }

        } else {
            message = meetingVenueBoothInfoService.saveBatch(list);
            venueBoothInfoService.saveBatch(venueBoothList);
        }

        return AjaxResult.success(message);
    }

    /**
     * 获取会议场馆展位信息详细信息
     */
    @ApiOperation("获取会议场馆展位信息详细信息")
    @RequiresPermissions("manage:meetingVenueBooth:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(meetingVenueBoothInfoService.getById(id));
    }


    /**
     * 新增会议场馆展位信息
     */
    @ApiOperation("新增会议场馆展位信息")
    @RequiresPermissions("manage:meetingVenueBooth:add")
    @Log(title = "会议场馆展位信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingVenueBoothInfo meetingVenueBoothInfo) {
        if (UserConstants.NOT_UNIQUE.equals(meetingVenueBoothInfoService.checkNameUnique(meetingVenueBoothInfo.getVenueId(),meetingVenueBoothInfo.getBoothName()))){
            return AjaxResult.error("新增展馆信息'" + meetingVenueBoothInfo.getBoothName() + "'失败，展馆名称已存在");
        }
        meetingVenueBoothInfo.setCreatedTime(DateUtils.getNowDate());
        meetingVenueBoothInfo.setCreatedBy(SecurityUtils.getUsername());
        return toAjax(meetingVenueBoothInfoService.save(meetingVenueBoothInfo));
    }


    /**
     * 修改会议场馆展位信息
     */
    @ApiOperation("修改会议场馆展位信息")
    @RequiresPermissions("manage:meetingVenueBooth:edit")
    @Log(title = "会议场馆展位信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingVenueBoothInfo meetingVenueBoothInfo) {
        return toAjax(meetingVenueBoothInfoService.updateById(meetingVenueBoothInfo));
    }

    /**
     * 删除会议场馆展位信息
     */
    @ApiOperation("删除会议场馆展位信息")
    @RequiresPermissions("manage:meetingVenueBooth:remove")
    @Log(title = "会议场馆展位信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingVenueBoothInfoService.removeByIds(ids));
    }
}
