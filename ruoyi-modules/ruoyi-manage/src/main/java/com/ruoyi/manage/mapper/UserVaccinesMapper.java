package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.UserVaccines;

/**
 * 用户疫苗记录Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-03
 */
public interface UserVaccinesMapper extends BaseMapper<UserVaccines> {


}
