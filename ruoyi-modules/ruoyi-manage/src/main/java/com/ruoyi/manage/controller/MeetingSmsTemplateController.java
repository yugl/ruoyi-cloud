package com.ruoyi.manage.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingSmsTemplate;
import com.ruoyi.manage.dto.MeetingSmsTemplateDto;
import com.ruoyi.manage.dto.SmsTemplateConfigDto;
import com.ruoyi.manage.service.IMeetingSmsTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 会议短信模板Controller
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Api(value = "会议短信模板控制器", tags = {"会议短信模板管理"})
@RestController
@RequestMapping("/meetingSmsTemplate")
public class MeetingSmsTemplateController extends BaseController {

    @Autowired
    private IMeetingSmsTemplateService meetingSmsTemplateService;

    /**
     * 更新启用状态
     *
     * @return
     */
    @PostMapping("changeEnableStatus")
    public AjaxResult changeEnableStatus(@RequestBody MeetingSmsTemplateDto dto) {
        return meetingSmsTemplateService.changeEnableStatus(dto);
    }


    /**
     * 查询人工会议短信模板下拉
     *
     * @return
     */
    @PostMapping("findMeetingSmsSelect")
    public AjaxResult findMeetingSmsSelect(@RequestParam("meetingId") Long meetingId) {
        return meetingSmsTemplateService.findMeetingSmsSelect(meetingId);
    }

    /**
     * 查询会议短信模板列表
     */
    @ApiOperation("查询会议短信模板列表")
    @GetMapping("/list/{meetingId}")
    public AjaxResult list(@PathVariable Long meetingId) {
        List<MeetingSmsTemplate> list = meetingSmsTemplateService.selectdList(meetingId);
        return AjaxResult.success(list);
    }

    /**
     * 获取会议短信模板详细信息
     */
    @ApiOperation("获取会议短信模板详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return meetingSmsTemplateService.getInfo(id);
    }

    /**
     * 新增会议短信模板
     */
    @ApiOperation("新增会议短信模板")
    @Log(title = "会议短信模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingSmsTemplateDto dto) {
        return meetingSmsTemplateService.add(dto);
    }

    /**
     * 修改会议短信模板
     */
    @ApiOperation("修改会议短信模板")
    @Log(title = "会议短信模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingSmsTemplate meetingSmsTemplate) {
        return toAjax(meetingSmsTemplateService.updateById(meetingSmsTemplate));
    }

    /**
     * 删除会议短信模板
     */
    @ApiOperation("删除会议短信模板")
    @Log(title = "会议短信模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingSmsTemplateService.removeByIds(ids));
    }
}
