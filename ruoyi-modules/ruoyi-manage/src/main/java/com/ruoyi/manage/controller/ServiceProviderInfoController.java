package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.manage.domain.ServiceProviderInfo;
import com.ruoyi.manage.dto.ServiceProviderInfoDto;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.service.IServiceProviderInfoService;
import com.ruoyi.system.api.model.LoginUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * 服务商基本信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "服务商基本信息控制器", tags = {"服务商基本信息管理"})
@RestController
@RequestMapping("/serviceProvider")
public class ServiceProviderInfoController extends BaseController {
    @Autowired
    private IServiceProviderInfoService serviceProviderInfoService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询服务商基本信息列表
     */
    @ApiOperation("查询服务商基本信息列表")
    @RequiresPermissions("manage:serviceProvider:list")
    @GetMapping("/list")
    public TableDataInfo list(ServiceProviderInfoDto serviceProviderInfo) {
        LoginUser user = tokenService.getLoginUser();
        Set<String> roles = user.getRoles();
        if (roles.contains(PersonRole.PLATFORM_SERVICE.getIndex())){
            serviceProviderInfo.setAccountManagerUsername(SecurityUtils.getUsername());
        }
        startPage();
        List<ServiceProviderInfo> list = serviceProviderInfoService.selectdList(serviceProviderInfo);
        return getDataTable(list);
    }

    /**
     * 导出服务商基本信息列表
     */
    @ApiOperation("导出服务商基本信息列表")
    @RequiresPermissions("manage:serviceProvider:export")
    @Log(title = "服务商基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ServiceProviderInfoDto serviceProviderInfo) throws IOException {

        startPage();
        List<ServiceProviderInfo> list = serviceProviderInfoService.selectdList(serviceProviderInfo);
        ExcelUtil<ServiceProviderInfo> util = new ExcelUtil<ServiceProviderInfo>(ServiceProviderInfo.class);
        util.exportExcel(response, list, "服务商基本信息数据");
    }

    /**
     * 导入服务商基本信息
     */
    @ApiOperation("导出服务商基本信息列表")
    @RequiresPermissions("manage:serviceProvider:importData")
    @Log(title = "服务商基本信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<ServiceProviderInfo> util = new ExcelUtil<ServiceProviderInfo>(ServiceProviderInfo.class);
        List<ServiceProviderInfo> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = serviceProviderInfoService.saveOrUpdateBatch(list);
        } else {
            message = serviceProviderInfoService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取服务商基本信息详细信息
     */
    @ApiOperation("获取服务商基本信息详细信息")
    @RequiresPermissions("manage:serviceProvider:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(serviceProviderInfoService.getById(id));
    }

    /**
     * 通过公司名称获取服务商信息
     */
    @ApiOperation("通过公司名称获取会议服务商信息")
    @RequiresPermissions("manage:serviceProvider:query")
    @GetMapping(value = "/companyName/{companyName}")
    public AjaxResult getInfoByCompanyName(@ApiParam("公司名称") @NotNull(message = "公司名称不能为空且精确匹配")
                                           @PathVariable("companyName") String companyName) {
        return AjaxResult.success(serviceProviderInfoService.selectdListByCompanyName(companyName));
    }

    /**
     * 获取所有展商
     */
    @ApiOperation("获取所有展商")
    @RequiresPermissions("manage:serviceProvider:query")
    @GetMapping(value = "/serviceProviders")
    public AjaxResult getAll() {
        return AjaxResult.success(serviceProviderInfoService.selectdListByCompanyName(null));
    }


    /**
     * 新增服务商基本信息
     */
    @ApiOperation("新增服务商基本信息")
    @RequiresPermissions("manage:serviceProvider:add")
    @Log(title = "服务商基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ServiceProviderInfo serviceProviderInfo) {
        if (UserConstants.NOT_UNIQUE.equals(serviceProviderInfoService.checkNameUnique(serviceProviderInfo.getCompanyName()))) {
            return AjaxResult.error("新增服务商信息'" + serviceProviderInfo.getCompanyName() + "'失败，公司名称已存在");
        }
        serviceProviderInfo.setCreatedTime(DateUtils.getNowDate());
        serviceProviderInfo.setCreatedBy(SecurityUtils.getUsername());
        registerUserService.registerUser(serviceProviderInfo.getChargePersonPhone(), serviceProviderInfo.getChargePerson(), serviceProviderInfo.getCompanyName(), PersonRole.SERVICE_PROVIDER.getIndex());
        return toAjax(serviceProviderInfoService.save(serviceProviderInfo));
    }

    /**
     * 修改服务商基本信息
     */
    @ApiOperation("修改服务商基本信息")
    @RequiresPermissions("manage:serviceProvider:edit")
    @Log(title = "服务商基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ServiceProviderInfo serviceProviderInfo) {
        return toAjax(serviceProviderInfoService.updateById(serviceProviderInfo));
    }

    /**
     * 删除服务商基本信息
     */
    @ApiOperation("删除服务商基本信息")
    @RequiresPermissions("manage:serviceProvider:remove")
    @Log(title = "服务商基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(serviceProviderInfoService.removeByIds(ids));
    }
}
