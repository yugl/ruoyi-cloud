package com.ruoyi.manage.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户问卷调查信息对象 app_user_questionnaire_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class AppUserQuestionnaireInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 会议ID
     */
    @Excel(name = "会议ID")
    private Long meetingId;

    /**
     * 会议名称
     */
    @Excel(name = "会议名称")
    private String meetingName;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phone;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 题目
     */
    @Excel(name = "题目")
    private String subject;

    @Excel(name = "答案")
    private String answer;

    /**
     * 问卷调查id
     */
    private Long questionnaireId;

    /**
     * 状态 0：有效
     * 1：无效
     */
    @Excel(name = "状态 0：有效1：无效")
    private String status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 问卷名称
     */
    @TableField(exist = false)
    private String questionnaireName;

}
