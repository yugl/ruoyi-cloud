package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.UserTravelCode;
import com.ruoyi.manage.dto.UserTravelCodeDto;
import com.ruoyi.manage.mapper.UserTravelCodeMapper;
import com.ruoyi.manage.service.IUserTravelCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 行程码Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-03
 */
@Service
public class UserTravelCodeServiceImpl extends ServiceImpl<UserTravelCodeMapper, UserTravelCode> implements IUserTravelCodeService {
    @Autowired
    private UserTravelCodeMapper userTravelCodeMapper;

    /**
     * 查询行程码列表
     *
     * @param userTravelCode 行程码
     * @return 行程码
     */
    @Override
    public List<UserTravelCode> selectdList(UserTravelCodeDto userTravelCode) {
        QueryWrapper<UserTravelCode> q = new QueryWrapper<>();
        if (userTravelCode.getPhone() != null && !userTravelCode.getPhone().trim().equals("")) {
            q.eq("phone", userTravelCode.getPhone());
        }
        if (userTravelCode.getAddress() != null && !userTravelCode.getAddress().trim().equals("")) {
            q.eq("address", userTravelCode.getAddress());
        }
        if (userTravelCode.getTravelDate() != null) {
            q.eq("travel_date", userTravelCode.getTravelDate());
        }
        if (userTravelCode.getRiskLevel() != null && !userTravelCode.getRiskLevel().trim().equals("")) {
            q.eq("risk_level", userTravelCode.getRiskLevel());
        }
        if (userTravelCode.getCreatedTime() != null) {
            q.eq("created_time", userTravelCode.getCreatedTime());
        }
        List<UserTravelCode> list = this.list(q);
        return list;
    }

    /**
     * 用户疫情防疫数据
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult getUserHealthInfo(String phone) {
        Map<String, Object> map = new HashMap<>();
        map.put("nucleinResult", 1);
        map.put("distanceDay", 20);
        map.put("testOrg", "北京波高医学检验所");
        map.put("testDate", "2022-03-15");
        map.put("riskLevel", "1");
        map.put("travelAddress", "北京市");
        return AjaxResult.success(map);
    }


}
