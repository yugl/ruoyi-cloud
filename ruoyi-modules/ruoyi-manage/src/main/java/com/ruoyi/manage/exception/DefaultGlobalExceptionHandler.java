package com.ruoyi.manage.exception;

import cn.hutool.core.date.DateException;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.exception.base.BizException;
import com.ruoyi.manage.exception.code.ExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * 全局controller异常处理类
 */
@RestControllerAdvice(basePackages = {"com.ruoyi.manage.controller"})
@ResponseBody
@Slf4j
public class DefaultGlobalExceptionHandler {

    /**
     * 业务异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(BizException.class)
    public AjaxResult bizException(BizException ex, HttpServletRequest request) {
        log.warn("BizException:", ex);
        return AjaxResult.error(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public AjaxResult httpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest request) {
        log.warn("HttpMessageNotReadableException:", ex);
        String message = ex.getMessage();
        try {
            Throwable cause = ex.getCause();
            if (cause instanceof InvalidFormatException) {
                return AjaxResult.error(500, "数值格式不正确！");
            }
        } catch (Exception e) {
            return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), ExceptionCode.PARAM_EX.getMsg());
        }

        try {
            Throwable cause = ex.getCause().getCause();
            if (cause instanceof DateException) {
                return AjaxResult.error(500, "日期格式不正确！");
            }
        } catch (Exception e) {
            return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), ExceptionCode.PARAM_EX.getMsg());
        }
        if (StrUtil.containsAny(message, "Could not read document:")) {
            String msg = String.format("无法正确的解析json类型的参数：%s", StrUtil.subBetween(message, "Could not read document:", " at "));
            return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), msg);
        }
        return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), ExceptionCode.PARAM_EX.getMsg());
    }

    @ExceptionHandler(BindException.class)
    public AjaxResult bindException(BindException ex, HttpServletRequest request) {
        log.warn("BindException:", ex);
        try {
            String msgs = ex.getBindingResult().getFieldError().getDefaultMessage();
            if (StrUtil.isNotEmpty(msgs)) {
                return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), msgs);
            }
        } catch (Exception ee) {
        }
        StringBuilder msg = new StringBuilder();
        List<FieldError> fieldErrors = ex.getFieldErrors();
        fieldErrors.forEach((oe) -> msg.append("参数:[").append(oe.getObjectName()).append(".").append(oe.getField()).append("]的传入值:[").append(oe.getRejectedValue()).append("]与预期的字段类型不匹配."));
        return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), msg.toString());
    }


    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public AjaxResult methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, HttpServletRequest request) {
        log.warn("MethodArgumentTypeMismatchException:", ex);
        MethodArgumentTypeMismatchException eee = (MethodArgumentTypeMismatchException) ex;
        StringBuilder msg = new StringBuilder("参数：[").append(eee.getName()).append("]的传入值：[").append(eee.getValue()).append("]与预期的字段类型：[").append(eee.getRequiredType().getName()).append("]不匹配");
        return AjaxResult.error(ExceptionCode.PARAM_EX.getCode(), msg.toString());
    }

    @ExceptionHandler(IllegalStateException.class)
    public AjaxResult illegalStateException(IllegalStateException ex, HttpServletRequest request) {
        log.warn("IllegalStateException:", ex);
        return AjaxResult.error(ExceptionCode.ILLEGALA_ARGUMENT_EX.getCode(), ExceptionCode.ILLEGALA_ARGUMENT_EX.getMsg());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public AjaxResult missingServletRequestParameterException(MissingServletRequestParameterException ex, HttpServletRequest request) {
        log.warn("MissingServletRequestParameterException:", ex);
        StringBuilder msg = new StringBuilder();
        msg.append("缺少必须的[").append(ex.getParameterType()).append("]类型的参数[").append(ex.getParameterName()).append("]");
        return AjaxResult.error(ExceptionCode.ILLEGALA_ARGUMENT_EX.getCode(), msg.toString());
    }

    @ExceptionHandler(NullPointerException.class)
    public AjaxResult nullPointerException(NullPointerException ex, HttpServletRequest request) {
        log.warn("NullPointerException:", ex);
        return AjaxResult.error(ExceptionCode.NULL_POINT_EX.getCode(), ExceptionCode.NULL_POINT_EX.getMsg());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public AjaxResult illegalArgumentException(IllegalArgumentException ex, HttpServletRequest request) {
        log.warn("IllegalArgumentException:", ex);
        return AjaxResult.error(500, ex.getMessage());
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public AjaxResult httpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex, HttpServletRequest request) {
        log.warn("HttpMediaTypeNotSupportedException:", ex);
        MediaType contentType = ex.getContentType();
        if (contentType != null) {
            StringBuilder msg = new StringBuilder();
            msg.append("请求类型(Content-Type)[").append(contentType.toString()).append("] 与实际接口的请求类型不匹配");
            return AjaxResult.error(ExceptionCode.MEDIA_TYPE_EX.getCode(), msg.toString());
        }
        return AjaxResult.error(ExceptionCode.MEDIA_TYPE_EX.getCode(), "无效的Content-Type类型");
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    public AjaxResult missingServletRequestPartException(MissingServletRequestPartException ex, HttpServletRequest request) {
        log.warn("MissingServletRequestPartException:", ex);
        return AjaxResult.error(ExceptionCode.REQUIRED_FILE_PARAM_EX.getCode(), ExceptionCode.REQUIRED_FILE_PARAM_EX.getMsg());
    }

    @ExceptionHandler(ServletException.class)
    public AjaxResult servletException(ServletException ex, HttpServletRequest request) {
        log.warn("ServletException:", ex);
        String msg = "UT010016: Not a multi part request";
        if (msg.equalsIgnoreCase(ex.getMessage())) {
            return AjaxResult.error(ExceptionCode.REQUIRED_FILE_PARAM_EX.getCode(), ExceptionCode.REQUIRED_FILE_PARAM_EX.getMsg());
        }
        return AjaxResult.error(ExceptionCode.SYSTEM_BUSY.getCode(), ex.getMessage());
    }

    @ExceptionHandler(MultipartException.class)
    public AjaxResult multipartException(MultipartException ex, HttpServletRequest request) {
        log.warn("MultipartException:", ex);
        return AjaxResult.error(ExceptionCode.REQUIRED_FILE_PARAM_EX.getCode(), ExceptionCode.REQUIRED_FILE_PARAM_EX.getMsg());
    }

    /**
     * jsr 规范中的验证异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public AjaxResult constraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        log.warn("ConstraintViolationException:", ex);
        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        String message = violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(";"));
        return AjaxResult.error(ExceptionCode.BASE_VALID_PARAM.getCode(), message);
    }

    /**
     * spring 封装的参数验证异常， 在conttoller中没有写result参数时，会进入
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.warn("MethodArgumentNotValidException:", ex);
        return AjaxResult.error(ExceptionCode.BASE_VALID_PARAM.getCode(), ex.getBindingResult().getFieldError().getDefaultMessage());
    }

    /**
     * 其他异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public AjaxResult otherExceptionHandler(Exception ex, HttpServletRequest request) {
        log.warn("Exception:", ex);
        if (ex.getCause() instanceof BizException) {
            return this.bizException((BizException) ex.getCause(), request);
        }
        return AjaxResult.error(ExceptionCode.SYSTEM_BUSY.getCode(), ExceptionCode.SYSTEM_BUSY.getMsg());
    }


    /**
     * 返回状态码:405
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public AjaxResult handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex, HttpServletRequest request) {
        log.warn("HttpRequestMethodNotSupportedException:", ex);
        return AjaxResult.error(ExceptionCode.METHOD_NOT_ALLOWED.getCode(), ExceptionCode.METHOD_NOT_ALLOWED.getMsg());
    }


    @ExceptionHandler(SQLException.class)
    public AjaxResult sqlException(SQLException ex, HttpServletRequest request) {
        log.warn("SQLException:", ex);
        return AjaxResult.error(ExceptionCode.SQL_EX.getCode(), ExceptionCode.SQL_EX.getMsg());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public AjaxResult dataIntegrityViolationException(DataIntegrityViolationException ex, HttpServletRequest request) {
        log.warn("DataIntegrityViolationException:", ex);
        return AjaxResult.error(ExceptionCode.SQL_EX.getCode(), ExceptionCode.SQL_EX.getMsg());
    }

}
