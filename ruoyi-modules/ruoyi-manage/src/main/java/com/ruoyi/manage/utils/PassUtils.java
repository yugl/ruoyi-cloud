package com.ruoyi.manage.utils;

import org.apache.commons.lang.RandomStringUtils;

public class PassUtils {
    /**
     * 生成随机密码
     *
     * @return
     */
    public static String genPasswd() {
        return RandomStringUtils.randomNumeric(6);
       /* String textString = "";
        String reference = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm!@#*";
        StringBuffer buffer = new StringBuffer(reference);
        for (int i = 0; i < 8; i++) {
            int ran = (int) (Math.random() * 65);
            textString += buffer.charAt(ran);
        }
        return textString.toString();*/
    }
}
