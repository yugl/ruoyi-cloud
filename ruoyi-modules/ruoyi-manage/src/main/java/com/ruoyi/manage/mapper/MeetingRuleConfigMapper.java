package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.dto.MeetingRuleConfigDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议入场规则配置Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingRuleConfigMapper extends BaseMapper<MeetingRuleConfig> {


    /**
     * 短信管理列表
     *
     * @param meetingRuleConfig
     * @return
     */
    List<Map<String, Object>> selectSmsList(@Param("dto") MeetingRuleConfigDto meetingRuleConfig);

}
