package com.ruoyi.manage.service.impl;

import java.util.List;

import com.ruoyi.manage.domain.MeetingInfoApprovalOpinion;
import com.ruoyi.manage.dto.MeetingInfoApprovalOpinionDto;
import com.ruoyi.manage.mapper.MeetingInfoApprovalOpinionMapper;
import com.ruoyi.manage.service.IMeetingInfoApprovalOpinionService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 政务审核意见Service业务层处理
 *
 * @author wangguiyu
 * @date 2022-05-04
 */
@Service
public class MeetingInfoApprovalOpinionServiceImpl extends ServiceImpl<MeetingInfoApprovalOpinionMapper, MeetingInfoApprovalOpinion> implements IMeetingInfoApprovalOpinionService {
    @Autowired
    private MeetingInfoApprovalOpinionMapper meetingInfoApprovalOpinionMapper;

    /**
     * 查询政务审核意见列表
     *
     * @param meetingInfoApprovalOpinion 政务审核意见
     * @return 政务审核意见
     */
    @Override
    public List<MeetingInfoApprovalOpinion> selectdList(MeetingInfoApprovalOpinionDto meetingInfoApprovalOpinion) {
        QueryWrapper<MeetingInfoApprovalOpinion> q = new QueryWrapper<>();
        if (meetingInfoApprovalOpinion.getMeetingId() != null) {
            q.eq("meeting_id", meetingInfoApprovalOpinion.getMeetingId());
        }
        if (meetingInfoApprovalOpinion.getMeetingApprovalStatus() != null && !meetingInfoApprovalOpinion.getMeetingApprovalStatus().trim().equals("")) {
            q.eq("meeting_approval_status", meetingInfoApprovalOpinion.getMeetingApprovalStatus());
        }
        if (meetingInfoApprovalOpinion.getMeetingApprovalOpinion() != null && !meetingInfoApprovalOpinion.getMeetingApprovalOpinion().trim().equals("")) {
            q.eq("meeting_approval_opinion", meetingInfoApprovalOpinion.getMeetingApprovalOpinion());
        }
        if (meetingInfoApprovalOpinion.getAdministrativeApprover() != null && !meetingInfoApprovalOpinion.getAdministrativeApprover().trim().equals("")) {
            q.eq("administrative_approver", meetingInfoApprovalOpinion.getAdministrativeApprover());
        }
        if (meetingInfoApprovalOpinion.getCreatedTime() != null ) {
            q.eq("created_time", meetingInfoApprovalOpinion.getCreatedTime());
        }
        List<MeetingInfoApprovalOpinion> list = this.list(q);
        return list;
    }


}
