package com.ruoyi.manage.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 联通短信配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "sms")
public class SmsConfigProperties {
    private String action;

    private String userid;

    private String account;

    private String password;
}
