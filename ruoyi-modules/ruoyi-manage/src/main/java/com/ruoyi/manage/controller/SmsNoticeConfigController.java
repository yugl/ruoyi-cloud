package com.ruoyi.manage.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.service.ISmsNoticeConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 短信使用率提醒Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "短信使用率提醒控制器", tags = {"短信使用率提醒管理"})
@RestController
@RequestMapping("/smsNoticeConfig")
public class SmsNoticeConfigController extends BaseController {
    @Autowired
    private ISmsNoticeConfigService smsNoticeConfigService;


    /**
     * 获取短信使用率提醒详细信息
     */
    @ApiOperation("获取短信使用率提醒详细信息")
    @RequiresPermissions("manage:smsNoticeConfig:query")
    @GetMapping(value = "/{smsKey}")
    public AjaxResult getBySmsKey(@NotNull(message = "key值不能为空")
                                  @PathVariable("smsKey") String smsKey) {
        return smsNoticeConfigService.getBySmsKey(smsKey);
    }


    /**
     * 修改短信使用率提醒
     */
    @ApiOperation("修改短信使用率提醒")
    @RequiresPermissions("manage:smsNoticeConfig:edit")
    @PutMapping("/{smsKey}")
    public AjaxResult edit(@PathVariable("smsKey") String smsKey,
                           @Valid @NotNull(message = "smsValue不能为空") @RequestParam("smsValue") String smsValue) {
        return smsNoticeConfigService.updateSmsValue(smsKey, smsValue);
    }

}
