package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingUserHealthManage;
import com.ruoyi.manage.dto.MeetingUserHealthManageDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议员工健康管理Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingUserHealthManageMapper extends BaseMapper<MeetingUserHealthManage> {


    List<Map<String, Object>> findMeetingHealthList(@Param("phone") String phone);

    List<MeetingUserHealthManage> selectdList(@Param("dto") MeetingUserHealthManageDto dto);
}
