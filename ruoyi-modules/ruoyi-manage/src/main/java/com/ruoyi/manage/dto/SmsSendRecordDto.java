package com.ruoyi.manage.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 短信管理对象 sms_manage
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class SmsSendRecordDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议ID
     */
    @NotNull(message = "会议ID不能为空")
    private Long meetingId;
    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 模板ID
     */
    private String smsTemplateId;

    /**
     * 模板名称
     */
    private String smsTemplateName;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 姓名
     */
    private String name;

    private String beginDate;

    private String endDate;

    private String status;


}
