package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingNotice;
import com.ruoyi.manage.dto.MeetingNoticeDto;

import java.util.List;
import java.util.Map;

/**
 * 会议通知公告Service接口
 *
 * @author ruoyi
 * @date 2022-04-01
 */
public interface IMeetingNoticeService extends IService<MeetingNotice> {


    /**
     * 查询会议通知公告列表
     *
     * @param meetingNotice 会议通知公告
     * @return 会议通知公告集合
     */
    public List<MeetingNotice> selectdList(MeetingNoticeDto meetingNotice);

    List<MeetingNotice> listByUser(MeetingNoticeDto meetingNotice);

    AjaxResult appList(Map<String, String> map);

    AjaxResult getNoticeById(Map<String, String> map);

    List<Map<String, String>> findMeetingList(String username);
}
