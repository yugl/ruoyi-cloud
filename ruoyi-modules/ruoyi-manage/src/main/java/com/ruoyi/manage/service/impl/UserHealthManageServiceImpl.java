package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.UserHealthManage;
import com.ruoyi.manage.dto.UserHealthManageDto;
import com.ruoyi.manage.mapper.UserHealthManageMapper;
import com.ruoyi.manage.service.IUserHealthManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 员工健康管理Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class UserHealthManageServiceImpl extends ServiceImpl<UserHealthManageMapper, UserHealthManage> implements IUserHealthManageService {
    @Autowired
    private UserHealthManageMapper userHealthManageMapper;

    /**
     * 查询员工健康管理列表
     *
     * @param userHealthManage 员工健康管理
     * @return 员工健康管理
     */
    @Override
    public List<UserHealthManage> selectdList(UserHealthManageDto userHealthManage) {
        QueryWrapper<UserHealthManage> q = new QueryWrapper<>();
        if (userHealthManage.getName() != null && !userHealthManage.getName().trim().equals("")) {
            q.like("name", userHealthManage.getName());
        }
        if (userHealthManage.getCompanyName() != null && !userHealthManage.getCompanyName().trim().equals("")) {
            q.like("company_name", userHealthManage.getCompanyName());
        }
        if (userHealthManage.getUserRole() != null && !userHealthManage.getUserRole().trim().equals("")) {
            q.eq("user_role", userHealthManage.getUserRole());
        }
        if (userHealthManage.getUserPost() != null && !userHealthManage.getUserPost().trim().equals("")) {
            q.eq("user_post", userHealthManage.getUserPost());
        }
        if (userHealthManage.getPhone() != null && !userHealthManage.getPhone().trim().equals("")) {
            q.eq("phone", userHealthManage.getPhone());
        }
        if (userHealthManage.getIdCard() != null && !userHealthManage.getIdCard().trim().equals("")) {
            q.eq("id_card", userHealthManage.getIdCard());
        }
//                    if (userHealthManage.getHealthAbnormalStatus() != null   && !userHealthManage.getHealthAbnormalStatus().trim().equals("")){
//                        q.eq("health_abnormal_status",userHealthManage.getHealthAbnormalStatus());
//                }
        if (userHealthManage.getHealthAbnormalStatus() != null && userHealthManage.getHealthAbnormalStatus().size()>0) {
            q.in("health_abnormal_status",userHealthManage.getHealthAbnormalStatus());
        }
        if (userHealthManage.getHealthCodeStatus() != null && !userHealthManage.getHealthCodeStatus().trim().equals("")) {
            q.eq("health_code_status", userHealthManage.getHealthCodeStatus());
        }
        if (userHealthManage.getTripCodeStatus() != null && !userHealthManage.getTripCodeStatus().trim().equals("")) {
            q.eq("trip_code_status", userHealthManage.getTripCodeStatus());
        }
        if (userHealthManage.getLatelyNucleicAcid() != null) {
            q.eq("lately_nucleic_acid", userHealthManage.getLatelyNucleicAcid());
        }
        if (userHealthManage.getNucleicAcidOrg() != null && !userHealthManage.getNucleicAcidOrg().trim().equals("")) {
            q.eq("nucleic_acid_org", userHealthManage.getNucleicAcidOrg());
        }
        if (userHealthManage.getLatelyVaccines() != null) {
            q.eq("lately_vaccines", userHealthManage.getLatelyVaccines());
        }
        List<UserHealthManage> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(String idCard, String companyName) {
        QueryWrapper<UserHealthManage> q = new QueryWrapper<>();
        q.lambda().eq(UserHealthManage::getIdCard, idCard);
        q.lambda().eq(UserHealthManage::getCompanyName, companyName);
        List<UserHealthManage> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


}
