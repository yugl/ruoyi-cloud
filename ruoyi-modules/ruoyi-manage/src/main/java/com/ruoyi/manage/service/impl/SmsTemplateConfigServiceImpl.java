package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsTemplateConfig;
import com.ruoyi.manage.dto.SmsTemplateConfigDto;
import com.ruoyi.manage.mapper.SmsTemplateConfigMapper;
import com.ruoyi.manage.service.ISmsTemplateConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 短信模板配置Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class SmsTemplateConfigServiceImpl extends ServiceImpl<SmsTemplateConfigMapper, SmsTemplateConfig> implements ISmsTemplateConfigService {
    @Autowired
    private SmsTemplateConfigMapper smsTemplateConfigMapper;

    /**
     * 查询短信模板配置列表
     *
     * @param smsTemplateConfig 短信模板配置
     * @return 短信模板配置
     */
    @Override
    public List<SmsTemplateConfig> selectdList(SmsTemplateConfigDto smsTemplateConfig) {
        QueryWrapper<SmsTemplateConfig> q = new QueryWrapper<>();
        if (smsTemplateConfig.getTemplateId() != null) {
            q.like("template_id", smsTemplateConfig.getTemplateId());
        }
        if (smsTemplateConfig.getTemplateName() != null && !smsTemplateConfig.getTemplateName().trim().equals("")) {
            q.like("template_name", smsTemplateConfig.getTemplateName());
        }
        q.orderByAsc("template_id");
        List<SmsTemplateConfig> list = this.list(q);
        return list;
    }

    /**
     * 获取短信模板下拉
     *
     * @return
     */
    @Override
    public AjaxResult findTemplateSelect() {
        QueryWrapper<SmsTemplateConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("template_id as value", "template_name as label")
                .eq("status", "0")
                .eq("type", "2")
                .eq("use_status", "1");
        List<Map<String, Object>> list = this.getBaseMapper().selectMaps(queryWrapper);
        return AjaxResult.success(list);
    }

    @Transactional
    @Override
    public AjaxResult add(SmsTemplateConfig smsTemplateConfig) {
        Long count = this.getBaseMapper().selectCount(
                new QueryWrapper<SmsTemplateConfig>()
                        .eq("template_id", smsTemplateConfig.getTemplateId())
                        .eq("status", "0"));
        if (count > 0) {
            return AjaxResult.error(500, "已存在该短信模板");
        }
        this.save(smsTemplateConfig);
        return AjaxResult.success();
    }

    public String getSmsContent(Long templateId) {
        SmsTemplateConfig smsTemplateConfig = smsTemplateConfigMapper.selectOne(new QueryWrapper<SmsTemplateConfig>()
                .eq("template_id", templateId)
                .eq("use_status", "1")
                .eq("status", "0")
        );
        return smsTemplateConfig.getTemplateContent();
    }

    @Override
    public AjaxResult changeEnableStatus(SmsTemplateConfigDto dto) {
        SmsTemplateConfig config = this.getById(dto.getId());
        config.setUseStatus(dto.getUseStatus());
        boolean result = this.updateById(config);
        if (result) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error(500, "操作失败");
        }


    }

}
