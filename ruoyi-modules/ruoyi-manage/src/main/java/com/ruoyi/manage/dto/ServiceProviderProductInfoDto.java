package com.ruoyi.manage.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 服务商产品信息对象 service_provider_product_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class ServiceProviderProductInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品编号
     */
    private String productCode;
    /**
     * 产品说明
     */
    private String productExplain;
    /**
     * 使用会议
     */
    private String useMeeting;
    /**
     * 使用开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date useStartTime;
    /**
     * 使用结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date useEndTime;

}
