package com.ruoyi.manage.utils;

import cn.hutool.core.util.URLUtil;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.domain.SysFile;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.UUID;

@Component
public class ImageUtil {

    @Autowired
    RemoteFileService remoteFileService;

    /**
     * 压缩图片到指定大小
     *
     * @param url
     * @return
     * @throws IOException
     */
    public String compressImg(String url) throws IOException {
        InputStream inputStream = URLUtil.getStream(new URL(url));
        String extName = url.substring(url.length() - 3);
        File tempFile = File.createTempFile(UUID.randomUUID().toString(), "." + extName);
        compressUnderSize(inputStream, 30 * 1024, tempFile, extName);
        R<SysFile> upload = remoteFileService.upload(getMulFile(tempFile));
        return upload.getData().getUrl();
    }

    /**
     * 将图片压缩到指定大小以内
     *
     * @param srcImgData 源图片数据
     * @param maxSize    目的图片大小
     * @return
     * @author CY
     * @date 2020年11月18日
     */
    public static void compressUnderSize(InputStream inputStream, long maxSize, File zipFile, String extName) throws IOException {
        byte[] data = getByteByPic(inputStream, extName);
        byte[] imgData = Arrays.copyOf(data, data.length);
        do {
            try {
                imgData = compress(imgData, 0.9);
            } catch (IOException e) {
                throw new IllegalStateException("压缩图片过程中出错,请及时联系管理员!", e);
            }
        } while (imgData.length > maxSize);
        byteToImage(imgData, zipFile);
    }

    /**
     * 获取图片文件字节
     *
     * @param imageFile
     * @return
     * @throws IOException
     * @author CY
     * @date 2020年11月18日
     */
    public static byte[] getByteByPic(InputStream inStream, String extName) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(inStream);
        BufferedImage bm = ImageIO.read(bis);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bm, extName, bos);
        bos.flush();
        byte[] data = bos.toByteArray();
        return data;
    }

    /**
     * 按照宽高比例压缩
     *
     * @param srcImgData 待压缩图片输入流
     * @param scale      压缩刻度
     * @return
     * @throws IOException
     * @author CY
     * @date 2020年11月18日
     */
    public static byte[] compress(byte[] srcImgData, double scale) throws IOException {
        BufferedImage bi = ImageIO.read(new ByteArrayInputStream(srcImgData));
        int width = (int) (bi.getWidth() * scale); // 源图宽度
        int height = (int) (bi.getHeight() * scale); // 源图高度
        Image image = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = tag.getGraphics();
        g.setColor(Color.RED);
        g.drawImage(image, 0, 0, null); // 绘制处理后的图
        g.dispose();
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        ImageIO.write(tag, "JPEG", bOut);
        return bOut.toByteArray();
    }

    /**
     * byte数组转图片
     *
     * @param data
     * @param path
     * @author CY
     * @date 2020年11月18日
     */
    public static void byteToImage(byte[] data, File zipFile) {
        if (data.length < 3)
            return;
        try {
            FileImageOutputStream imageOutput = new FileImageOutputStream(zipFile);
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //file 转换为 MultipartFile
    private static MultipartFile getMulFile(File file) {
        FileItemFactory factory = new DiskFileItemFactory(16, null);
        FileItem item = factory.createItem("file", "text/plain", true, file.getName());
        int bytesRead = 0;
        byte[] buffer = new byte[8192];
        try {
            FileInputStream fis = new FileInputStream(file);
            OutputStream os = item.getOutputStream();
            while ((bytesRead = fis.read(buffer, 0, 8192))
                    != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MultipartFile mfile = new CommonsMultipartFile(item);
        return mfile;
    }

}