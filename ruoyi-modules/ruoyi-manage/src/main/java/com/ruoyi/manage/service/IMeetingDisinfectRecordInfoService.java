package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingDisinfectRecordInfo;
import com.ruoyi.manage.dto.AppDisinfectRecordInfoDto;
import com.ruoyi.manage.dto.MeetingDisinfectRecordInfoDto;

import java.util.List;
import java.util.Map;

/**
 * 会议消毒记录Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingDisinfectRecordInfoService extends IService<MeetingDisinfectRecordInfo> {


    /**
     * 查询会议消毒记录列表
     *
     * @param meetingDisinfectRecordInfo 会议消毒记录
     * @return 会议消毒记录集合
     */
    public List<Map<String, Object>> selectdList(MeetingDisinfectRecordInfoDto meetingDisinfectRecordInfo);


    AjaxResult disinfectAdd(AppDisinfectRecordInfoDto dto);

    List<MeetingDisinfectRecordInfo> findDisinfectRecordList(Map<String, String> map);

    AjaxResult findDisinfectRecordById(String id);
}
