package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingRuleConfig;
import com.ruoyi.manage.dto.MeetingRuleConfigDto;

import java.util.List;
import java.util.Map;

/**
 * 会议入场规则配置Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IMeetingRuleConfigService extends IService<MeetingRuleConfig> {


    /**
     * 短信管理列表
     *
     * @param meetingRuleConfig 会议入场规则配置
     * @return 短信管理列表
     */
    public List<Map<String, Object>> selectdList(MeetingRuleConfigDto meetingRuleConfig);

    AjaxResult checkUserRule(Long meetingId, String idCard);

    MeetingRuleConfig getInfo(Long meetingId);

    AjaxResult add(MeetingRuleConfig meetingRuleConfig);
}
