package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AppQuestionnaireConfig;
import com.ruoyi.manage.dto.AntiepidemicConfigDto;
import com.ruoyi.manage.dto.AppQuestionnaireConfigDto;
import com.ruoyi.manage.service.IAppQuestionnaireConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 问卷调查配置Controller
 *
 * @author ruoyi
 * @date 2022-03-27
 */
@Api(value = "问卷调查控制器", tags = {"问卷调查管理"})
@RestController
@RequestMapping("/questionnaireConfig")
public class AppQuestionnaireConfigController extends BaseController {
    @Autowired
    private IAppQuestionnaireConfigService appQuestionnaireConfigService;

    /**
     * 问卷下拉
     */
    @ApiOperation("问卷下拉")
    @GetMapping("/selectList")
    public AjaxResult selectList() {
        return appQuestionnaireConfigService.selectList();
    }

    /**
     * 更新启用状态
     *
     * @return
     */
    @PostMapping("changeEnableStatus")
    public AjaxResult changeEnableStatus(@RequestBody AppQuestionnaireConfigDto dto) {
        return appQuestionnaireConfigService.changeEnableStatus(dto);
    }

    /**
     * 查询问卷调查配置列表
     */
    @ApiOperation("查询问卷调查配置列表")
    @RequiresPermissions("manage:questionnaireConfig:list")
    @GetMapping("/list")
    public TableDataInfo list(AppQuestionnaireConfigDto appQuestionnaireConfig) {
        startPage();
        List<AppQuestionnaireConfig> list = appQuestionnaireConfigService.selectdList(appQuestionnaireConfig);
        return getDataTable(list);
    }

    /**
     * 导出问卷调查配置列表
     */
    @ApiOperation("导出问卷调查配置列表")
    @RequiresPermissions("manage:questionnaireConfig:export")
    @Log(title = "问卷调查配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppQuestionnaireConfigDto appQuestionnaireConfig) throws IOException {
        startPage();
        List<AppQuestionnaireConfig> list = appQuestionnaireConfigService.selectdList(appQuestionnaireConfig);
        ExcelUtil<AppQuestionnaireConfig> util = new ExcelUtil<AppQuestionnaireConfig>(AppQuestionnaireConfig.class);
        util.exportExcel(response, list, "问卷调查配置数据");
    }

    /**
     * 导入问卷调查配置
     */
    @ApiOperation("导出问卷调查配置列表")
    @RequiresPermissions("manage:questionnaireConfig:importData")
    @Log(title = "问卷调查配置导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AppQuestionnaireConfig> util = new ExcelUtil<AppQuestionnaireConfig>(AppQuestionnaireConfig.class);
        List<AppQuestionnaireConfig> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = appQuestionnaireConfigService.saveOrUpdateBatch(list);
        } else {
            message = appQuestionnaireConfigService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取问卷调查配置详细信息
     */
    @ApiOperation("获取问卷调查配置详细信息")
    @RequiresPermissions("manage:questionnaireConfig:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(appQuestionnaireConfigService.getById(id));
    }

    /**
     * 新增问卷调查配置
     */
    @ApiOperation("新增问卷调查配置")
    @RequiresPermissions("manage:questionnaireConfig:add")
    @Log(title = "问卷调查配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppQuestionnaireConfig appQuestionnaireConfig) {
        boolean save = appQuestionnaireConfigService.save(appQuestionnaireConfig);
        return AjaxResult.success(appQuestionnaireConfig);
    }

    /**
     * 修改问卷调查配置
     */
    @ApiOperation("修改问卷调查配置")
    @RequiresPermissions("manage:questionnaireConfig:edit")
    @Log(title = "问卷调查配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppQuestionnaireConfig appQuestionnaireConfig) {
        return toAjax(appQuestionnaireConfigService.updateById(appQuestionnaireConfig));
    }

    /**
     * 删除问卷调查配置
     */
    @ApiOperation("删除问卷调查配置")
    @RequiresPermissions("manage:questionnaireConfig:remove")
    @Log(title = "问卷调查配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(appQuestionnaireConfigService.removeByIds(ids));
    }
}
