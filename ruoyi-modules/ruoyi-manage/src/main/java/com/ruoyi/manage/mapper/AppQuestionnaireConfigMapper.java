package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppQuestionnaireConfig;

/**
 * 问卷调查配置Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface AppQuestionnaireConfigMapper extends BaseMapper<AppQuestionnaireConfig> {


}
