package com.ruoyi.manage.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.*;
import com.ruoyi.manage.dto.AppUserEnrollInfoDto;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.exception.base.BizException;
import com.ruoyi.manage.mapper.*;
import com.ruoyi.manage.service.IAppUserEnrollInfoService;
import com.ruoyi.manage.utils.SmsUtils;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.domain.SysFile;
import com.ruoyi.system.api.domain.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * app报名信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class AppUserEnrollInfoServiceImpl extends ServiceImpl<AppUserEnrollInfoMapper, AppUserEnrollInfo> implements IAppUserEnrollInfoService {

    @Autowired
    private AppUserEnrollInfoMapper appUserEnrollInfoMapper;
    @Autowired
    MeetingInfoMapper meetingInfoMapper;
    @Autowired
    private AppUserInfoMapper appUserInfoMapper;
    @Autowired
    RemoteFileService remoteFileService;
    @Autowired
    UserHealthManageMapper userHealthManageMapper;
    @Autowired
    MeetingUserHealthManageMapper meetingUserHealthManageMapper;
    @Autowired
    SmsUtils smsUtils;

    /**
     * 查询app报名信息列表
     *
     * @param appUserEnrollInfo app报名信息
     * @return app报名信息
     */
    @Override
    public List<AppUserEnrollInfo> selectdList(AppUserEnrollInfoDto appUserEnrollInfo) {
        QueryWrapper<AppUserEnrollInfo> q = new QueryWrapper<>();
        if (appUserEnrollInfo.getPhone() != null && !appUserEnrollInfo.getPhone().trim().equals("")) {
            q.eq("phone", appUserEnrollInfo.getPhone());
        }
        if (appUserEnrollInfo.getMeetingId() != null) {
            q.eq("meeting_id", appUserEnrollInfo.getMeetingId());
        }
        if (appUserEnrollInfo.getEnrollSource() != null && !appUserEnrollInfo.getEnrollSource().trim().equals("")) {
            q.eq("enroll_source", appUserEnrollInfo.getEnrollSource());
        }
        List<AppUserEnrollInfo> list = this.list(q);
        return list;
    }

    /**
     * 我的参会码
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult getCode(String phone) {
        List<AppUserEnrollInfo> list = this.getBaseMapper().selectList(new QueryWrapper<AppUserEnrollInfo>()
                .eq("phone", phone)
                .eq("status", "0")
                .orderByDesc("created_time")
        );
        if (list == null || list.size() == 0) {
            return AjaxResult.error(500, "未获取到参会码");
        }
        ArrayList<Map<String, Object>> resultList = new ArrayList<>();
        for (AppUserEnrollInfo one : list) {
            HashMap<String, Object> map = new HashMap<>();
            Long meetingId = one.getMeetingId();
            MeetingInfo meetingInfo = meetingInfoMapper.selectById(meetingId);
            if (meetingInfo == null) {
                continue;
            }
            if (meetingInfo.getMeetingEndTime() != null) {
                if (meetingInfo == null || DateTime.now().isAfter(meetingInfo.getMeetingEndTime())) {
                    continue;
                }
            }
            map.put("codeSrc", one.getQrCodeUrl());
            map.put("title", meetingInfo.getMeetingName());
            map.put("startDate", DateUtil.format(meetingInfo.getMeetingStartTime(), "yyyy-MM-dd HH:mm"));
            map.put("endDate", DateUtil.format(meetingInfo.getMeetingEndTime(), "yyyy-MM-dd HH:mm"));
            String date = DateUtil.format(meetingInfo.getMeetingStartTime(), "yyyy-MM-dd HH:mm") +
                    "-" + DateUtil.format(meetingInfo.getMeetingEndTime(), "yyyy-MM-dd HH:mm");
            map.put("date", date);
            map.put("address", meetingInfo.getMeetingAddress());
            map.put("name", one.getName());
            map.put("state", one.getQrCodeStatus());
            resultList.add(map);
        }
        return AjaxResult.success(resultList);
    }

    /**
     * 报名
     *
     * @param appUserEnrollInfoDto
     * @return
     */
    @Transactional
    @Override
    public AjaxResult add(AppUserEnrollInfoDto appUserEnrollInfoDto) {
        //TODO 用户核算检查是否异常
        MeetingUserHealthManage one = meetingUserHealthManageMapper.selectOne(new QueryWrapper<MeetingUserHealthManage>()
                .eq("phone", appUserEnrollInfoDto.getPhone())
                .eq("meeting_id", appUserEnrollInfoDto.getMeetingId())
                .eq("status", "0")
        );
        if (one != null) {
            return AjaxResult.error(500, "您已报名会议");
        }
        //判断是否已实名认证
        List<AppUserInfo> appUserInfos = appUserInfoMapper.selectList(new QueryWrapper<AppUserInfo>()
                .eq("phone", appUserEnrollInfoDto.getPhone())
                .eq("status", "0")
        );
        if (appUserInfos.isEmpty()) {
            return AjaxResult.error(500, "您还未实名认证");
        }

        //保存报名信息
        AppUserEnrollInfo info = new AppUserEnrollInfo();
        BeanUtils.copyProperties(appUserEnrollInfoDto, info);
        AppUserInfo appUserInfo = appUserInfos.get(0);
        //封装二维码信息
        Map<String, Object> map = new HashMap<>();
        map.put("phone", appUserEnrollInfoDto.getPhone());
        map.put("name", appUserInfos.get(0).getName());
        map.put("meetingId", appUserEnrollInfoDto.getMeetingId());
        R<String> r = remoteFileService.qrCodeUrl(JSON.toJSONString(map));
        info.setName(appUserInfo.getName());
        info.setQrCodeUrl(r.getData());
        info.setEnrollTime(new Date());
        info.setCreatedTime(new Date());
        this.save(info);

        //将报名信息保存到会议健康人员管理表
        UserHealthManage userHealthManage = userHealthManageMapper.selectOne(
                new QueryWrapper<UserHealthManage>()
                        .eq("phone", appUserEnrollInfoDto.getPhone())
                        .eq("status", "0")
        );

        //判断是否为平台运营人员
        List<SysUser> sysUserList = appUserEnrollInfoMapper.selectSysuser(appUserEnrollInfoDto.getPhone());
        //判断人员是否为观众
        MeetingUserHealthManage meetingUserHealthManage = new MeetingUserHealthManage();
        meetingUserHealthManage.setMeetingId(appUserEnrollInfoDto.getMeetingId());
        meetingUserHealthManage.setCreatedTime(new Date());
        if (userHealthManage == null) {
            meetingUserHealthManage.setName(appUserInfo.getName());
            meetingUserHealthManage.setCompanyName(appUserInfo.getCompanyName());
            meetingUserHealthManage.setPhone(appUserInfo.getPhone());
            meetingUserHealthManage.setIdCard(appUserInfo.getIdCard());
            if (sysUserList.size() > 0) {
                meetingUserHealthManage.setUserRole(PersonRole.PLATFORM.getIndex());
            } else {
                meetingUserHealthManage.setUserRole(PersonRole.AUDIENCE.getIndex());
            }
        } else {
            //TODO 判断公司在这次会议中是什么角色
            BeanUtils.copyProperties(userHealthManage, meetingUserHealthManage, "id", "created_by", "updated_by", "updated_time");
        }
        meetingUserHealthManageMapper.insert(meetingUserHealthManage);

        //报名成功发送短信
        SmsSendVo smsSendVo = new SmsSendVo();
        smsSendVo.setTemplateId(113L);
        smsSendVo.setMeetingId(appUserEnrollInfoDto.getMeetingId());
        smsSendVo.setName(appUserInfo.getName());
        smsSendVo.setPhone(appUserEnrollInfoDto.getPhone());
        smsSendVo.setType("1");
        AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);
        if ((int) ajaxResult.get("code") != 200) {
            throw new BizException(500, "发送报名会议成功短信失败！");
        }
        return AjaxResult.success("报名成功");
    }

    /**
     * 移动端 根据手机号 获取报名的会议
     *
     * @param map
     * @return
     */
    @Override
    public List<MeetingInfo> findMeetingList(Map<String, String> map) {
        List<MeetingInfo> list = meetingInfoMapper.findMeetingList(map);
        return list;
    }

    /**
     * 根据会议ID查询
     *
     * @param map
     * @return
     */
    @Override
    public AjaxResult getMeetingInfo(Map<String, String> map) {
        MeetingInfo meetingInfo = meetingInfoMapper.getMeetingInfo(map.get("meetingId"));
        if (meetingInfo == null) {
            return AjaxResult.error(500, "没有该会议");
        }
        if (StringUtils.isNotEmpty(map.get("phone"))) {
            /*AppUserEnrollInfo appUserEnrollInfo = appUserEnrollInfoMapper.selectOne(new QueryWrapper<AppUserEnrollInfo>()
                    .eq("phone", map.get("phone"))
                    .eq("meeting_id", map.get("meetingId"))
                    .eq("status", "0")
            );*/
            MeetingUserHealthManage one = meetingUserHealthManageMapper.selectOne(new QueryWrapper<MeetingUserHealthManage>()
                    .eq("phone", map.get("phone"))
                    .eq("meeting_id", map.get("meetingId"))
                    .eq("status", "0")
            );
            if (one != null) {
                meetingInfo.setIsAlreadyEnroll("1");
            } else {
                meetingInfo.setIsAlreadyEnroll("2");
            }
        } else {
            meetingInfo.setIsAlreadyEnroll("2");
        }
        return AjaxResult.success(meetingInfo);
    }

    /**
     * 会议名称下拉
     *
     * @param map
     * @return
     */
    @Override
    public List<Map<String, Object>> findMeetingSelect(String phone) {
        List<Map<String, Object>> list = meetingInfoMapper.findMeetingSelect(phone);
        return list;
    }


    /**
     * 上传文件base64
     *
     * @param imgBase64
     * @return
     */
    @Override
    public AjaxResult upFileBase64(Map<String, String> map) {
        R<SysFile> r = remoteFileService.upFileBase64(map.get("file"));
        if (r.getCode() == 200) {
            return AjaxResult.success("操作成功", r.getData().getUrl());
        } else {
            return AjaxResult.error(500, "上传失败");
        }
    }

}
