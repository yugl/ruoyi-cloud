package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppPrivacyAggreement;
import com.ruoyi.manage.dto.AppPrivacyAggreementDto;
import com.ruoyi.manage.mapper.AppPrivacyAggreementMapper;
import com.ruoyi.manage.service.IAppPrivacyAggreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 隐私协议设置Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Service
public class AppPrivacyAggreementServiceImpl extends ServiceImpl<AppPrivacyAggreementMapper, AppPrivacyAggreement> implements IAppPrivacyAggreementService {
    @Autowired
    private AppPrivacyAggreementMapper appPrivacyAggreementMapper;

    /**
     * 查询隐私协议设置列表
     *
     * @param appPrivacyAggreement 隐私协议设置
     * @return 隐私协议设置
     */
    @Override
    public List<AppPrivacyAggreement> selectdList(AppPrivacyAggreementDto appPrivacyAggreement) {
        QueryWrapper<AppPrivacyAggreement> q = new QueryWrapper<>();
        List<AppPrivacyAggreement> list = this.list(q);
        return list;
    }

    @Override
    public AjaxResult getInfo() {
        List<AppPrivacyAggreement> list = baseMapper.selectList(new QueryWrapper<AppPrivacyAggreement>());
        if (list == null || list.size() == 0) {
            return AjaxResult.success();
        }
        return AjaxResult.success(list.get(0));
    }


}
