package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 会议入场规则配置对象 meeting_rule_config
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class MeetingRuleConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 会议名称
     */
    private String meetingName;
}
