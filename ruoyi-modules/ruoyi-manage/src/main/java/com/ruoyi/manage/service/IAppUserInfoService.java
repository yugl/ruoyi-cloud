package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.AppUserInfo;
import com.ruoyi.manage.dto.AppUserInfoDto;

import java.util.List;

/**
 * 实名认证信息Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IAppUserInfoService extends IService<AppUserInfo> {


    /**
     * 查询实名认证信息列表
     *
     * @param appUserInfo 实名认证信息
     * @return 实名认证信息集合
     */
    public List<AppUserInfo> selectdList(AppUserInfoDto appUserInfo);

    /**
     * 根据前台传来的code获取openid，如果有加上手机号
     *
     * @param code
     * @param sourceType
     * @return
     */
    AjaxResult getUserInfo(String code, String sourceType);

    /**
     * 实名认证
     *
     * @param appUserInfoDto
     * @return
     */
    AjaxResult add(AppUserInfoDto appUserInfoDto);

    /**
     * app验证码
     *
     * @param phone
     * @return
     */
    AjaxResult verifyCode(String phone);

    /**
     * 获取用户协议
     *
     * @return
     */
    AjaxResult userAgreement();
}
