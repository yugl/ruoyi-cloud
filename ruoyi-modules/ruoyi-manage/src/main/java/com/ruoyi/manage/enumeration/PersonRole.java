package com.ruoyi.manage.enumeration;

/**
 * 人员健康管理角色
 */
public enum PersonRole {
    MAIN("主办单位", "hostOrganizer"),
    UNDERTAKER("承办单位", "undertakeOrganizer"),
    SERVICE_PROVIDER("服务商", "serviceProvider"),
    EXHIBITOR("展商", "exhibitor"),
    VENUE("场馆", "venue"),

    AUDIENCE("观众", "audience"),
    PLATFORM("平台运营人员", "platformPerson"),

    ADMIN("超级管理员", "admin"),
    COMMON("普通角色", "common"),
    PLATFORM_ADMIN("平台运营管理员", "platformAdmin"),
    PLATFORM_SERVICE("平台运营业务经理", "platformService"),
    ADMINISTRATIVE_APPROVER("行政审核人", "administrativeApprover");

    private String name;
    private String index;

    // 构造方法
    private PersonRole(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (PersonRole c : PersonRole.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
