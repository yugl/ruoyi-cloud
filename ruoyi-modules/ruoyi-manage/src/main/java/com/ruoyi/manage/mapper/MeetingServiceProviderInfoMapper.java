package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.MeetingServiceProviderInfo;

/**
 * 会议服务商信息Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface MeetingServiceProviderInfoMapper extends BaseMapper<MeetingServiceProviderInfo> {


}
