package com.ruoyi.manage.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.MeetingSmsPhone;
import com.ruoyi.manage.dto.MeetingSmsPhoneDto;
import com.ruoyi.manage.service.IMeetingSmsPhoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 人工发送短信Controller
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Api(value = "人工发送短信控制器", tags = {"人工发送短信管理"})
@RestController
@RequestMapping("/meetingSmsPhone")
public class MeetingSmsPhoneController extends BaseController {
    @Autowired
    private IMeetingSmsPhoneService meetingSmsPhoneService;

    /**
     * 查询人工发送短信列表
     */
    @ApiOperation("查询人工发送短信列表")
    @RequiresPermissions("manage:meetingSmsPhone:list")
    @PostMapping("/list")
    public AjaxResult list(MeetingSmsPhoneDto meetingSmsPhone) {
        return meetingSmsPhoneService.selectdList(meetingSmsPhone);
    }

    /**
     * 导入手机号
     */
    @ApiOperation("导入手机号")
    @RequiresPermissions("manage:meetingSmsPhone:importData")
    @PostMapping("/importData")
    public AjaxResult importData(@RequestPart("file") MultipartFile file,
                                 @RequestParam("meetingId") Long meetingId) {
        return meetingSmsPhoneService.importData(file, meetingId);
    }

    /**
     * 发送短信
     */
    @ApiOperation("发送短信")
    @RequiresPermissions("manage:meetingSmsPhone:smsSend")
    @PostMapping("/smsSend")
    public AjaxResult smsSend(@RequestParam("meetingId") Long meetingId, @RequestParam("templateId") Long templateId) {
        return meetingSmsPhoneService.smsSend(meetingId, templateId);
    }

    /**
     * 新增人工发送短信
     */
    @ApiOperation("新增人工发送短信")
    @RequiresPermissions("manage:meetingSmsPhone:add")
    @Log(title = "人工发送短信", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MeetingSmsPhone meetingSmsPhone) {
        return toAjax(meetingSmsPhoneService.save(meetingSmsPhone));
    }

    /**
     * 修改人工发送短信
     */
    @ApiOperation("修改人工发送短信")
    @RequiresPermissions("manage:meetingSmsPhone:edit")
    @Log(title = "人工发送短信", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MeetingSmsPhone meetingSmsPhone) {
        return toAjax(meetingSmsPhoneService.updateById(meetingSmsPhone));
    }

    /**
     * 删除人工发送短信
     */
    @ApiOperation("删除人工发送短信")
    @RequiresPermissions("manage:meetingSmsPhone:remove")
    @Log(title = "人工发送短信", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(meetingSmsPhoneService.removeByIds(ids));
    }


    /**
     * 自定义短信内容发送短信
     */
    @ApiOperation("自定义短信内容发送短信")
    @PostMapping("/customSmsSend")
    public AjaxResult customSmsSend(@RequestBody Map<String, Object> map) {
        Long meetingId = Long.parseLong(map.get("meetingId") + "");
        String smsContent = map.get("smsContent") + "";
        return meetingSmsPhoneService.customSmsSend(meetingId, smsContent);
    }
}
