package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.manage.config.CacheNames;
import com.ruoyi.manage.domain.AppPrivacyAggreement;
import com.ruoyi.manage.domain.AppUserInfo;
import com.ruoyi.manage.domain.UserHealthManage;
import com.ruoyi.manage.dto.AppUserInfoDto;
import com.ruoyi.manage.dto.SmsSendVo;
import com.ruoyi.manage.enumeration.PersonRole;
import com.ruoyi.manage.mapper.AppPrivacyAggreementMapper;
import com.ruoyi.manage.mapper.AppUserEnrollInfoMapper;
import com.ruoyi.manage.mapper.AppUserInfoMapper;
import com.ruoyi.manage.mapper.UserHealthManageMapper;
import com.ruoyi.manage.service.IAppUserInfoService;
import com.ruoyi.manage.utils.AppUtils;
import com.ruoyi.manage.utils.ImageUtil;
import com.ruoyi.manage.utils.SmsUtils;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.RemoteInterfaceService;
import com.ruoyi.system.api.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 实名认证信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Slf4j
@Service
public class AppUserInfoServiceImpl extends ServiceImpl<AppUserInfoMapper, AppUserInfo> implements IAppUserInfoService {

    @Autowired
    private AppUserInfoMapper appUserInfoMapper;
    @Autowired
    private UserHealthManageMapper userHealthManageMapper;
    @Autowired
    AppPrivacyAggreementMapper appPrivacyAggreementMapper;
    @Autowired
    AppUtils appUtils;
    @Autowired
    RemoteFileService remoteFileService;
    @Autowired
    RedisService redisService;
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    ImageUtil imageUtil;
    @Autowired
    RemoteInterfaceService remoteInterfaceService;

    @Autowired
    AppUserEnrollInfoMapper appUserEnrollInfoMapper;

    /**
     * 查询实名认证信息列表
     *
     * @param appUserInfo 实名认证信息
     * @return 实名认证信息
     */
    @Override
    public List<AppUserInfo> selectdList(AppUserInfoDto appUserInfo) {
        QueryWrapper<AppUserInfo> q = new QueryWrapper<>();
        if (appUserInfo.getName() != null && !appUserInfo.getName().trim().equals("")) {
            q.like("name", appUserInfo.getName());
        }
        if (appUserInfo.getPhone() != null && !appUserInfo.getPhone().trim().equals("")) {
            q.eq("phone", appUserInfo.getPhone());
        }
        if (appUserInfo.getIdCard() != null && !appUserInfo.getIdCard().trim().equals("")) {
            q.eq("id_card", appUserInfo.getIdCard());
        }
        if (appUserInfo.getCompanyName() != null && !appUserInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", appUserInfo.getCompanyName());
        }
        if (appUserInfo.getDataSource() != null && !appUserInfo.getDataSource().trim().equals("")) {
            q.eq("data_source", appUserInfo.getDataSource());
        }
        List<AppUserInfo> list = this.list(q);
        return list;
    }

    /**
     * 根据code获取openid
     *
     * @param code
     * @param sourceType
     * @return
     */
    @Override
    public AjaxResult getUserInfo(String code, String sourceType) {
        String openid = null;
        if ("1".equals(sourceType)) {
            openid = appUtils.getWxOpenid(code);
        } else if ("2".equals(sourceType)) {
            openid = appUtils.getAlipayOpenid(code);
        }
        if (StringUtils.isEmpty(openid)) {
            return AjaxResult.error(500, "获取用户openId失败");
        }
        AppUserInfo appUserInfo = this.getOne(new QueryWrapper<AppUserInfo>().eq("openid", openid).eq("status", "0"));
        if (appUserInfo != null) {
            UserHealthManage userHealthManage = userHealthManageMapper.selectOne(new QueryWrapper<UserHealthManage>()
                    .eq("phone", appUserInfo.getPhone())
                    .eq("status", "0")
            );
            if (userHealthManage != null) {
                appUserInfo.setRoles(userHealthManage.getUserRole());
            } else {
                //判断是否为平台运营人员
                List<SysUser> sysUserList = appUserEnrollInfoMapper.selectSysuser(appUserInfo.getPhone());
                if (sysUserList.size() > 0) {
                    appUserInfo.setRoles(PersonRole.PLATFORM.getIndex());
                } else {
                    appUserInfo.setRoles(PersonRole.AUDIENCE.getIndex());
                }
            }
            return AjaxResult.success(appUserInfo);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("openid", openid);
            map.put("roles", PersonRole.AUDIENCE.getIndex());
            return AjaxResult.success(map);
        }
    }

    /**
     * 实名认证
     *
     * @param appUserInfoDto
     * @return
     */
    @Override
    public AjaxResult add(AppUserInfoDto appUserInfoDto) {
        //校验验证码是否有效
        String verifyCode = appUserInfoDto.getVerifyCode();
        Object cacheObject = redisService.getCacheObject(CacheNames.APP_VERIFICATION + appUserInfoDto.getPhone());
        if (cacheObject == null || !verifyCode.equals(cacheObject.toString())) {
            return AjaxResult.error(500, "验证码无效");
        }

        if(StringUtils.isNotEmpty(appUserInfoDto.getUrl())){
            //TODO 验证身份信息
            HashMap<String, String> map = new HashMap<>();
            map.put("name", appUserInfoDto.getName());
            map.put("idCard", appUserInfoDto.getIdCard());
            map.put("url", appUserInfoDto.getUrl());

            //图片压缩
            try {
                String compressImgUrl = imageUtil.compressImg(appUserInfoDto.getUrl());
                map.put("url", compressImgUrl);
                appUserInfoDto.setUrl(compressImgUrl);
            } catch (IOException e) {
                e.printStackTrace();
                return AjaxResult.error("图片压缩程序异常");
            }

            AjaxResult ajaxResult = remoteInterfaceService.authIdentity(map);
            if ((Integer) ajaxResult.get("code") == 500) {
                return ajaxResult;
            }
        }


        AppUserInfo appUserInfo = new AppUserInfo();
        BeanUtils.copyProperties(appUserInfoDto, appUserInfo);
        appUserInfo.setHeadPortraitUrl(appUserInfoDto.getUrl());
        AppUserInfo appUserInfo1 = appUserInfoMapper.selectOne(new QueryWrapper<AppUserInfo>()
                .eq("openid", appUserInfoDto.getOpenid())
                .eq("status", "0")
        );
        if (appUserInfo1 != null) {
            appUserInfo.setId(appUserInfo1.getId());
            appUserInfo.setUpdatedTime(new Date());
        } else {
            appUserInfo.setCreatedTime(new Date());
        }
        this.saveOrUpdate(appUserInfo);
        //查询用户的角色
        List<UserHealthManage> userHealthManages = userHealthManageMapper.selectList(new QueryWrapper<UserHealthManage>()
                .eq("phone", appUserInfoDto.getPhone())
                .eq("id_card", appUserInfoDto.getIdCard())
                .eq("status", "0")
        );
        if (userHealthManages.isEmpty()) {
            return AjaxResult.success("认证成功", PersonRole.AUDIENCE.getIndex());
        } else {
            UserHealthManage userHealthManage = userHealthManages.get(0);
            return AjaxResult.success("认证成功", userHealthManage.getUserRole());
        }
    }

    /**
     * 短信验证码
     *
     * @param phone
     * @return
     */
    @Override
    public AjaxResult verifyCode(String phone) {
        String verifyCode = RandomStringUtils.randomNumeric(6);
        Map<String, Object> map = new HashMap<>();
        map.put("验证码", verifyCode);
//        AjaxResult ajaxResult = smsUtils.smsSend(phone, 109L, map);

        SmsSendVo smsSendVo = new SmsSendVo();
        smsSendVo.setPhone(phone);
        smsSendVo.setTemplateId(109L);
        smsSendVo.setMap(map);
        AjaxResult ajaxResult = smsUtils.smsSend(smsSendVo);

        if ((int) ajaxResult.get("code") == 500) {
            return ajaxResult;
        }
        redisService.setCacheObject(CacheNames.APP_VERIFICATION + phone, verifyCode, 300L, TimeUnit.SECONDS);
        return AjaxResult.success("验证码发送成功");
    }

    /**
     * 获取用户协议
     *
     * @return
     */
    @Override
    public AjaxResult userAgreement() {
        AppPrivacyAggreement appPrivacyAggreement = appPrivacyAggreementMapper.selectOne(new QueryWrapper<AppPrivacyAggreement>().eq("status", "0"));
        return AjaxResult.success("操作成功", appPrivacyAggreement.getPrivacyAgreement());
    }

    public static void main(String[] args) {
        String verifyCode = RandomStringUtils.randomNumeric(6);
        System.out.println(verifyCode);
    }


}

