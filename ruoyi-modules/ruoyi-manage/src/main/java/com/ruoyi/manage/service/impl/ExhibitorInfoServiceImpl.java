package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.ExhibitorInfo;
import com.ruoyi.manage.dto.ExhibitorInfoDto;
import com.ruoyi.manage.mapper.ExhibitorInfoMapper;
import com.ruoyi.manage.service.IExhibitorInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 展商基本信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class ExhibitorInfoServiceImpl extends ServiceImpl<ExhibitorInfoMapper, ExhibitorInfo> implements IExhibitorInfoService {
    @Autowired
    private ExhibitorInfoMapper exhibitorInfoMapper;

    /**
     * 查询展商基本信息列表
     *
     * @param exhibitorInfo 展商基本信息
     * @return 展商基本信息
     */
    @Override
    public List<ExhibitorInfo> selectdList(ExhibitorInfoDto exhibitorInfo) {
        QueryWrapper<ExhibitorInfo> q = new QueryWrapper<>();
        if (exhibitorInfo.getCompanyName() != null && !exhibitorInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", exhibitorInfo.getCompanyName());
        }
        if (exhibitorInfo.getTaxpayerIdentifierNum() != null && !exhibitorInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", exhibitorInfo.getTaxpayerIdentifierNum());
        }
        if (exhibitorInfo.getIndustry() != null && !exhibitorInfo.getIndustry().trim().equals("")) {
            q.eq("industry", exhibitorInfo.getIndustry());
        }
        if (exhibitorInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", exhibitorInfo.getIncorporationDate());
        }
        if (exhibitorInfo.getRegisteredCapital() != null && !exhibitorInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", exhibitorInfo.getRegisteredCapital());
        }
        if (exhibitorInfo.getChargePerson() != null && !exhibitorInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", exhibitorInfo.getChargePerson());
        }
        if (exhibitorInfo.getChargePersonPhone() != null && !exhibitorInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", exhibitorInfo.getChargePersonPhone());
        }
        if (exhibitorInfo.getPreventionPerson() != null && !exhibitorInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", exhibitorInfo.getPreventionPerson());
        }
        if (exhibitorInfo.getPreventionPersonPhone() != null && !exhibitorInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", exhibitorInfo.getPreventionPersonPhone());
        }
        if (exhibitorInfo.getAccountManager() != null && !exhibitorInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", exhibitorInfo.getAccountManager());
        }
        if (exhibitorInfo.getAccountManagerPhone() != null && !exhibitorInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", exhibitorInfo.getAccountManagerPhone());
        }
        if (exhibitorInfo.getAccountManagerUsername() != null && !exhibitorInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", exhibitorInfo.getAccountManagerUsername());
        }
        if (exhibitorInfo.getBusinessLicenseUrl() != null && !exhibitorInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", exhibitorInfo.getBusinessLicenseUrl());
        }
        if (exhibitorInfo.getBusinessNature() != null && !exhibitorInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", exhibitorInfo.getBusinessNature());
        }
        List<ExhibitorInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(String companyName) {
        QueryWrapper<ExhibitorInfo> q = new QueryWrapper<>();
        q.lambda().eq(ExhibitorInfo::getCompanyName, companyName);
        List<ExhibitorInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public List<ExhibitorInfo> selectdListByCompanyName(String companyName) {
        QueryWrapper<ExhibitorInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(ExhibitorInfo::getCompanyName);
        if (companyName != null)
            q.lambda().eq(ExhibitorInfo::getCompanyName, companyName);
        List<ExhibitorInfo> list = this.baseMapper.selectList(q);
        return list;
    }


}
