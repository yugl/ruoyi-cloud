package com.ruoyi.manage.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.UploadFileInfoMapper;
import com.ruoyi.manage.domain.UploadFileInfo;
import com.ruoyi.manage.dto.UploadFileInfoDto;
import com.ruoyi.manage.service.IUploadFileInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 上传文件信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class UploadFileInfoServiceImpl extends ServiceImpl<UploadFileInfoMapper, UploadFileInfo> implements IUploadFileInfoService {
    @Autowired
    private UploadFileInfoMapper uploadFileInfoMapper;

    /**
     * 查询上传文件信息列表
     *
     * @param uploadFileInfo 上传文件信息
     * @return 上传文件信息
     */
    @Override
    public List<UploadFileInfo> selectdList(UploadFileInfoDto uploadFileInfo) {
        QueryWrapper<UploadFileInfo> q = new QueryWrapper<>();
                    if (uploadFileInfo.getMeetingId() != null  ){
                        q.eq("meeting_id",uploadFileInfo.getMeetingId());
                }
                    if (uploadFileInfo.getCompanyName() != null   && !uploadFileInfo.getCompanyName().trim().equals("")){
                        q.eq("company_name",uploadFileInfo.getCompanyName());
                }
                    if (uploadFileInfo.getFileName() != null   && !uploadFileInfo.getFileName().trim().equals("")){
                        q.like("file_name",uploadFileInfo.getFileName());
                }
                    if (uploadFileInfo.getFileSize() != null  ){
                        q.eq("file_size",uploadFileInfo.getFileSize());
                }
                    if (uploadFileInfo.getFileExt() != null   && !uploadFileInfo.getFileExt().trim().equals("")){
                        q.eq("file_ext",uploadFileInfo.getFileExt());
                }
                    if (uploadFileInfo.getFileType() != null   && !uploadFileInfo.getFileType().trim().equals("")){
                        q.eq("file_type",uploadFileInfo.getFileType());
                }
                    if (uploadFileInfo.getFileUrl() != null   && !uploadFileInfo.getFileUrl().trim().equals("")){
                        q.eq("file_url",uploadFileInfo.getFileUrl());
                }
                    if (uploadFileInfo.getFastdfsId() != null   && !uploadFileInfo.getFastdfsId().trim().equals("")){
                        q.eq("fastdfs_id",uploadFileInfo.getFastdfsId());
                }
                    if (uploadFileInfo.getStatus() != null   && !uploadFileInfo.getStatus().trim().equals("")){
                        q.eq("status",uploadFileInfo.getStatus());
                }
                    if (uploadFileInfo.getCreatedBy() != null   && !uploadFileInfo.getCreatedBy().trim().equals("")){
                        q.eq("created_by",uploadFileInfo.getCreatedBy());
                }
                    if (uploadFileInfo.getCreatedTime() != null  ){
                        q.eq("created_time",uploadFileInfo.getCreatedTime());
                }
                    if (uploadFileInfo.getUpdatedBy() != null   && !uploadFileInfo.getUpdatedBy().trim().equals("")){
                        q.eq("updated_by",uploadFileInfo.getUpdatedBy());
                }
                    if (uploadFileInfo.getUpdatedTime() != null  ){
                        q.eq("updated_time",uploadFileInfo.getUpdatedTime());
                }
        List<UploadFileInfo> list = this.list(q);
        return list;
    }


}
