package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.UserHealthManage;
import com.ruoyi.manage.dto.UserHealthManageDto;

/**
 * 员工健康管理Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface IUserHealthManageService extends IService<UserHealthManage> {


    /**
     * 查询员工健康管理列表
     *
     * @param userHealthManage 员工健康管理
     * @return 员工健康管理集合
     */
    public List<UserHealthManage> selectdList(UserHealthManageDto userHealthManage);

    public String checkNameUnique(String idCard,String companyName);

}
