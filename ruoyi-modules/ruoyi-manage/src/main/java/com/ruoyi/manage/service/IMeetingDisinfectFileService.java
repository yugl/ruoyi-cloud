package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.manage.domain.MeetingDisinfectFile;
import com.ruoyi.manage.dto.MeetingDisinfectFileDto;

/**
 * 会议消毒记录文件Service接口
 *
 * @author ruoyi
 * @date 2022-03-31
 */
public interface IMeetingDisinfectFileService extends IService<MeetingDisinfectFile> {


    /**
     * 查询会议消毒记录文件列表
     *
     * @param meetingDisinfectFile 会议消毒记录文件
     * @return 会议消毒记录文件集合
     */
    public List<MeetingDisinfectFile> selectdList(MeetingDisinfectFileDto meetingDisinfectFile);

}
