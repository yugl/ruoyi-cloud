package com.ruoyi.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.manage.domain.ServiceProviderInfo;
import com.ruoyi.manage.dto.ServiceProviderInfoDto;
import com.ruoyi.manage.mapper.ServiceProviderInfoMapper;
import com.ruoyi.manage.service.IServiceProviderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 服务商基本信息Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Service
public class ServiceProviderInfoServiceImpl extends ServiceImpl<ServiceProviderInfoMapper, ServiceProviderInfo> implements IServiceProviderInfoService {
    @Autowired
    private ServiceProviderInfoMapper serviceProviderInfoMapper;

    /**
     * 查询服务商基本信息列表
     *
     * @param serviceProviderInfo 服务商基本信息
     * @return 服务商基本信息
     */
    @Override
    public List<ServiceProviderInfo> selectdList(ServiceProviderInfoDto serviceProviderInfo) {
        QueryWrapper<ServiceProviderInfo> q = new QueryWrapper<>();
        if (serviceProviderInfo.getCompanyName() != null && !serviceProviderInfo.getCompanyName().trim().equals("")) {
            q.like("company_name", serviceProviderInfo.getCompanyName());
        }
        if (serviceProviderInfo.getTaxpayerIdentifierNum() != null && !serviceProviderInfo.getTaxpayerIdentifierNum().trim().equals("")) {
            q.eq("taxpayer_identifier_num", serviceProviderInfo.getTaxpayerIdentifierNum());
        }
        if (serviceProviderInfo.getIndustry() != null && !serviceProviderInfo.getIndustry().trim().equals("")) {
            q.eq("industry", serviceProviderInfo.getIndustry());
        }
        if (serviceProviderInfo.getIncorporationDate() != null) {
            q.eq("incorporation_date", serviceProviderInfo.getIncorporationDate());
        }
        if (serviceProviderInfo.getRegisteredCapital() != null && !serviceProviderInfo.getRegisteredCapital().trim().equals("")) {
            q.eq("registered_capital", serviceProviderInfo.getRegisteredCapital());
        }
        if (serviceProviderInfo.getChargePerson() != null && !serviceProviderInfo.getChargePerson().trim().equals("")) {
            q.eq("charge_person", serviceProviderInfo.getChargePerson());
        }
        if (serviceProviderInfo.getChargePersonPhone() != null && !serviceProviderInfo.getChargePersonPhone().trim().equals("")) {
            q.eq("charge_person_phone", serviceProviderInfo.getChargePersonPhone());
        }
        if (serviceProviderInfo.getPreventionPerson() != null && !serviceProviderInfo.getPreventionPerson().trim().equals("")) {
            q.eq("prevention_person", serviceProviderInfo.getPreventionPerson());
        }
        if (serviceProviderInfo.getPreventionPersonPhone() != null && !serviceProviderInfo.getPreventionPersonPhone().trim().equals("")) {
            q.eq("prevention_person_phone", serviceProviderInfo.getPreventionPersonPhone());
        }
        if (serviceProviderInfo.getAccountManager() != null && !serviceProviderInfo.getAccountManager().trim().equals("")) {
            q.eq("account_manager", serviceProviderInfo.getAccountManager());
        }
        if (serviceProviderInfo.getAccountManagerPhone() != null && !serviceProviderInfo.getAccountManagerPhone().trim().equals("")) {
            q.eq("account_manager_phone", serviceProviderInfo.getAccountManagerPhone());
        }
        if (serviceProviderInfo.getAccountManagerUsername() != null && !serviceProviderInfo.getAccountManagerUsername().trim().equals("")) {
            q.eq("account_manager_username", serviceProviderInfo.getAccountManagerUsername());
        }
        if (serviceProviderInfo.getBusinessLicenseUrl() != null && !serviceProviderInfo.getBusinessLicenseUrl().trim().equals("")) {
            q.eq("business_license_url", serviceProviderInfo.getBusinessLicenseUrl());
        }
        if (serviceProviderInfo.getBusinessNature() != null && !serviceProviderInfo.getBusinessNature().trim().equals("")) {
            q.eq("business_nature", serviceProviderInfo.getBusinessNature());
        }
        if (serviceProviderInfo.getStatus() != null && !serviceProviderInfo.getStatus().trim().equals("")) {
            q.eq("status", serviceProviderInfo.getStatus());
        }
        if (serviceProviderInfo.getCreatedBy() != null && !serviceProviderInfo.getCreatedBy().trim().equals("")) {
            q.eq("created_by", serviceProviderInfo.getCreatedBy());
        }
        if (serviceProviderInfo.getCreatedTime() != null) {
            q.eq("created_time", serviceProviderInfo.getCreatedTime());
        }
        if (serviceProviderInfo.getUpdatedBy() != null && !serviceProviderInfo.getUpdatedBy().trim().equals("")) {
            q.eq("updated_by", serviceProviderInfo.getUpdatedBy());
        }
        if (serviceProviderInfo.getUpdatedTime() != null) {
            q.eq("updated_time", serviceProviderInfo.getUpdatedTime());
        }
        List<ServiceProviderInfo> list = this.list(q);
        return list;
    }

    @Override
    public String checkNameUnique(String companyName) {
        QueryWrapper<ServiceProviderInfo> q = new QueryWrapper<>();
        q.lambda().eq(ServiceProviderInfo::getCompanyName, companyName);
        List<ServiceProviderInfo> list = this.baseMapper.selectList(q);
        if (list != null && list.size() > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public List<ServiceProviderInfo> selectdListByCompanyName(String companyName) {
        QueryWrapper<ServiceProviderInfo> q = new QueryWrapper<>();
        q.lambda().orderByAsc(ServiceProviderInfo::getCompanyName);
        if (companyName != null)
            q.lambda().eq(ServiceProviderInfo::getCompanyName, companyName);
        List<ServiceProviderInfo> list = this.baseMapper.selectList(q);
        return list;
    }


}
