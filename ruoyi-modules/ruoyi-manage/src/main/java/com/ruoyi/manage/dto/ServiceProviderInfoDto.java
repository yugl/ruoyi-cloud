package com.ruoyi.manage.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.core.annotation.Excel;

/**
 * 服务商基本信息对象 service_provider_info
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Data
public class ServiceProviderInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 纳税人识别号
     */
    private String taxpayerIdentifierNum;
    /**
     * 行业
     */
    private String industry;
    /**
     * 成立日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date incorporationDate;
    /**
     * 注册资本
     */
    private String registeredCapital;
    /**
     * 负责人
     */
    private String chargePerson;
    /**
     * 负责人联系方式
     */
    private String chargePersonPhone;
    /**
     * 防疫负责人
     */
    private String preventionPerson;
    /**
     * 防疫负责人联系方式
     */
    private String preventionPersonPhone;
    /**
     * 客户经理
     */
    private String accountManager;
    /**
     * 客户经理联系方式
     */
    private String accountManagerPhone;
    /**
     * 营业执照URL
     */
    private String businessLicenseUrl;
    /**
     * 经营范围
     */
    private String businessNature;
    /**
     * 状态 0：有效
     * 1：无效
     */
    private String status;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 客户经理运营平台帐号
     */
    private String accountManagerUsername;

}
