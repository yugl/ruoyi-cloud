package com.ruoyi.manage.controller;

import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.manage.dto.AppUserInfoDto;
import com.ruoyi.manage.service.IAppUserInfoService;
import com.ruoyi.manage.service.IRegisterUserService;
import com.ruoyi.manage.utils.ImageUtil;
import com.ruoyi.manage.utils.SignUtil;
import com.ruoyi.system.api.RemoteInterfaceService;
import com.ruoyi.system.api.RemoteUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

/**
 * 实名认证信息Controller
 *
 * @author 于观礼
 * @date 2022-03-12
 */
@Api(value = "实名认证信息控制器", tags = {"实名认证信息管理"})
@RestController
@RequestMapping("/appUser")
public class AppUserInfoController extends BaseController {

    @Autowired
    private IAppUserInfoService appUserInfoService;

    @Autowired
    RemoteUserService remoteUserService;

    @Autowired
    RedisService redisService;

    @Autowired
    IRegisterUserService registerUserService;

    @Autowired
    SignUtil signUtil;

    @Autowired
    RemoteInterfaceService remoteInterfaceService;

    @Autowired
    ImageUtil imageUtil;

    /**
     * 根据前台传来的code获取openid，如果有加上手机号
     */
    @ApiOperation("根据code获取用户信息")
    @PostMapping("/getUserInfo")
    public AjaxResult getUserInfo(@RequestBody Map<String, String> map) {
        String code = map.get("code");
        String source = map.get("source");
        if (StringUtils.isEmpty(code)) {
            return AjaxResult.error(500, "code不能为空！");
        }
        if (StringUtils.isEmpty(source)) {
            return AjaxResult.error(500, "来源不能为空！");
        }
        return appUserInfoService.getUserInfo(code, source);
    }


    /**
     * 跳过人脸识别
     */
    @ApiOperation("跳过人脸识别")
    @Log(title = "跳过人脸识别", businessType = BusinessType.INSERT)
    @GetMapping("/skipFace")
    public AjaxResult skipFace() {
        String skip_face = redisService.getCacheObject(Constants.SYS_CONFIG_KEY + "skip_face");
        if (skip_face.equals("true")) {
            return AjaxResult.success("操作成功", true);
        } else {
            return AjaxResult.success("操作成功", false);
        }
    }


    /**
     * 新增实名认证信息
     */
    @ApiOperation("新增实名认证信息")
    @Log(title = "实名认证信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@Valid @RequestBody AppUserInfoDto appUserInfoDto) {
        return appUserInfoService.add(appUserInfoDto);
    }

    /**
     * 短信验证码
     */
    @ApiOperation("短信验证码")
    @PostMapping("/verifyCode")
    public AjaxResult verifyCode(@RequestBody Map<String, String> map) {
        String phone = map.get("phone");
        if (StringUtils.isEmpty(phone)) {
            return AjaxResult.error(500, "手机号不能为空");
        }
        return appUserInfoService.verifyCode(phone);
    }

    /**
     * 获取用户协议
     */
    @ApiOperation("用户协议")
    @GetMapping("/userAgreement")
    public AjaxResult userAgreement() {
        return appUserInfoService.userAgreement();
    }

    /**
     * H5页面分享
     */
    @ApiOperation("H5页面分享")
    @PostMapping("/signShare")
    public AjaxResult signShare(@RequestBody Map<String, String> map) {
        String url = map.get("url");
        if (StringUtils.isEmpty(url)) {
            return AjaxResult.error(500, "分享地址不能为空");
        }
        return signUtil.getResult(url);
    }


    /**
     * 注册用户
     *
     * @param phone       手机号
     * @param name        姓名
     * @param companyName 公司名称
     * @param roleName    角色
     * @return
     */
    @RequestMapping("/registerUser")
    public AjaxResult registerUser(@RequestParam("phone") String phone,
                                   @RequestParam("name") String name,
                                   @RequestParam("companyName") String companyName,
                                   @RequestParam("roleName") String roleName) {
        return registerUserService.registerUser(phone, name, companyName, roleName);
    }

    /**
     * 注册用户
     *
     * @param phone       手机号
     * @return
     */
    @RequestMapping("/test")
    public AjaxResult test(@RequestParam("phone") String phone) {
        return appUserInfoService.verifyCode(phone);
    }


    @PostMapping("/auth")
    public AjaxResult auth(@RequestBody Map<String, String> map) {
        String url = map.get("url");
        try {
            String compressImg = imageUtil.compressImg(url);
            System.out.println("压缩后图片地址：" + compressImg);
            map.put("url", compressImg);
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.error("图片压缩程序异常");
        }
        return remoteInterfaceService.authIdentity(map);
    }
}
