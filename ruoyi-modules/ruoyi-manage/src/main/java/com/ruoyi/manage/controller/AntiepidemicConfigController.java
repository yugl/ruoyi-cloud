package com.ruoyi.manage.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.manage.domain.AntiepidemicConfig;
import com.ruoyi.manage.dto.AntiepidemicConfigDto;
import com.ruoyi.manage.service.IAntiepidemicConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 防疫填报模板Controller
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Api(value = "防疫填报模板控制器", tags = {"防疫填报模板管理"})
@RestController
@RequestMapping("/antiepidemicConfig")
public class AntiepidemicConfigController extends BaseController {
    @Autowired
    private IAntiepidemicConfigService antiepidemicConfigService;

    /**
     * 查询防疫填报模板下拉
     *
     * @return
     */
    @GetMapping("findSelect")
    public AjaxResult findSelect() {
        return antiepidemicConfigService.findSelect();
    }

    /**
     * 更新启用状态
     *
     * @return
     */
    @PostMapping("changeEnableStatus")
    public AjaxResult changeEnableStatus(@RequestBody AntiepidemicConfigDto dto) {
        return antiepidemicConfigService.changeEnableStatus(dto);
    }

    /**
     * 查询防疫填报模板列表
     */
    @ApiOperation("查询防疫填报模板列表")
    @RequiresPermissions("manage:antiepidemicConfig:list")
    @GetMapping("/list")
    public TableDataInfo list(AntiepidemicConfigDto antiepidemicConfig) {
        startPage();
        List<AntiepidemicConfig> list = antiepidemicConfigService.selectdList(antiepidemicConfig);
        return getDataTable(list);
    }

    /**
     * 导出防疫填报模板列表
     */
    @ApiOperation("导出防疫填报模板列表")
    @RequiresPermissions("manage:antiepidemicConfig:export")
    @Log(title = "防疫填报模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AntiepidemicConfigDto antiepidemicConfig) throws IOException {
        startPage();
        List<AntiepidemicConfig> list = antiepidemicConfigService.selectdList(antiepidemicConfig);
        ExcelUtil<AntiepidemicConfig> util = new ExcelUtil<AntiepidemicConfig>(AntiepidemicConfig.class);
        util.exportExcel(response, list, "防疫填报模板数据");
    }

    /**
     * 导入防疫填报模板
     */
    @ApiOperation("导出防疫填报模板列表")
    @RequiresPermissions("manage:antiepidemicConfig:importData")
    @Log(title = "防疫填报模板导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<AntiepidemicConfig> util = new ExcelUtil<AntiepidemicConfig>(AntiepidemicConfig.class);
        List<AntiepidemicConfig> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = antiepidemicConfigService.saveOrUpdateBatch(list);
        } else {
            message = antiepidemicConfigService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取防疫填报模板详细信息
     */
    @ApiOperation("获取防疫填报模板详细信息")
    @RequiresPermissions("manage:antiepidemicConfig:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return antiepidemicConfigService.getInfo(id);
    }

    /**
     * 新增防疫填报模板
     */
    @ApiOperation("新增防疫填报模板")
    @RequiresPermissions("manage:antiepidemicConfig:add")
    @Log(title = "防疫填报模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody AntiepidemicConfig antiepidemicConfig) {
        return antiepidemicConfigService.addOrUpdate(antiepidemicConfig);
    }

    /**
     * 修改防疫填报模板
     */
    @ApiOperation("修改防疫填报模板")
    @RequiresPermissions("manage:antiepidemicConfig:edit")
    @Log(title = "防疫填报模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody AntiepidemicConfig antiepidemicConfig) {
        return antiepidemicConfigService.addOrUpdate(antiepidemicConfig);
    }

    /**
     * 删除防疫填报模板
     */
    @ApiOperation("删除防疫填报模板")
    @RequiresPermissions("manage:antiepidemicConfig:remove")
    @Log(title = "防疫填报模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return antiepidemicConfigService.remove(ids);
    }
}
