package com.ruoyi.manage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 防疫填报模板对象 antiepidemic_config
 *
 * @author ruoyi
 * @date 2022-04-12
 */
@Data
public class AntiepidemicConfigDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 防疫填报名称
     */
    private String antiepidemicName;
    /**
     * 启用状态 0：启用 1：不启用
     */
    private String enableStatus;

}
