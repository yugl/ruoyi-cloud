package com.ruoyi.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AppSubjectInfo;
import com.ruoyi.manage.vo.AppSubjectInfoVo;

import java.util.List;

/**
 * 问卷题目Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-27
 */
public interface AppSubjectInfoMapper extends BaseMapper<AppSubjectInfo> {


    List<AppSubjectInfoVo> selectSubjectList(Long questionnaireId);
}
