package com.ruoyi.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.MeetingSmsPhone;
import com.ruoyi.manage.dto.MeetingSmsPhoneDto;
import org.springframework.web.multipart.MultipartFile;

/**
 * 人工发送短信Service接口
 *
 * @author ruoyi
 * @date 2022-04-04
 */
public interface IMeetingSmsPhoneService extends IService<MeetingSmsPhone> {


    /**
     * 查询人工发送短信列表
     *
     * @param meetingSmsPhone 人工发送短信
     * @return 人工发送短信集合
     */
    public AjaxResult selectdList(MeetingSmsPhoneDto meetingSmsPhone);

    AjaxResult importData(MultipartFile file, Long meetingId);

    AjaxResult smsSend(Long meetingId, Long templateId);

    AjaxResult customSmsSend(Long meetingId, String smsContent);
}
