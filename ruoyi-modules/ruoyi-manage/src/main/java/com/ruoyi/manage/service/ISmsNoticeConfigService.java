package com.ruoyi.manage.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.manage.domain.SmsNoticeConfig;
import com.ruoyi.manage.dto.SmsNoticeConfigDto;

/**
 * 短信使用率提醒Service接口
 *
 * @author 于观礼
 * @date 2022-03-12
 */
public interface ISmsNoticeConfigService extends IService<SmsNoticeConfig> {

    AjaxResult getBySmsKey(String smsKey);

    AjaxResult updateSmsValue(String smsKey, String smsValue);
}
