package com.ruoyi.manage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.manage.domain.AntiepidemicConfig;

/**
 * 防疫填报模板Mapper接口
 *
 * @author ruoyi
 * @date 2022-04-12
 */
public interface AntiepidemicConfigMapper extends BaseMapper<AntiepidemicConfig> {


}
