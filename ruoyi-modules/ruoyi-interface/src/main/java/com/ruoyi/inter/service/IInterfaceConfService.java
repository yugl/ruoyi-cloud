package com.ruoyi.inter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.dto.InterfaceConfDto;

import java.util.List;

/**
 * 接口配置Service接口
 *
 * @author 于观礼
 * @date 2022-03-25
 */
public interface IInterfaceConfService extends IService<InterfaceConf> {


    /**
     * 查询接口配置列表
     *
     * @param interfaceConf 接口配置
     * @return 接口配置集合
     */
    public List<InterfaceConf> selectdList(InterfaceConfDto interfaceConf);

}
