package com.ruoyi.inter.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 接口配置对象 interface_conf
 *
 * @author 于观礼
 * @date 2022-03-25
 */
@Data
public class InterfaceConfDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 接口编号
     */
    private String code;
    /**
     * 系统模块
     */
    private String module;
    /**
     * 接口名称
     */
    private String name;
    /**
     * 接口类型（ws/rest）
     */
    private String type;

}
