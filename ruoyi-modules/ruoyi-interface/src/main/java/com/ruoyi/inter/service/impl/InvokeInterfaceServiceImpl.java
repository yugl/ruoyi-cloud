package com.ruoyi.inter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.service.IInterfaceLogService;
import com.ruoyi.inter.service.InvokeInterfaceService;
import com.ruoyi.inter.util.IOUtils;
import com.ruoyi.inter.util.WsServiceClient;
import com.ruoyi.inter.util.XmlToJsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Slf4j
@Service
public class InvokeInterfaceServiceImpl implements InvokeInterfaceService {

    @Autowired
    WsServiceClient wsServiceClient;
    @Autowired
    @Qualifier("restTemplateLong")
    RestTemplate restTemplate;
    @Autowired
    @Qualifier("ifRestTemplate")
    private RestTemplate ifRestTemplate;
    @Autowired
    @Qualifier("ifNLBHttpsRestTemplate")
    private RestTemplate ifNLBHttpsRestTemplate;
    @Autowired
    IInterfaceLogService iInterfaceLogService;

    /**
     * 只允许rest form表单调用外部接口
     *
     * @param action String
     * @param header json 格式的参数
     * @param map    form表单提交
     * @return String 返回结果
     */
    @Override
    public String invokeRest(InterfaceConf interfaceConf, String header, MultiValueMap<String, Object> map) {
        HttpHeaders headers = createHttpHeaders(interfaceConf, header, MediaType.APPLICATION_FORM_URLENCODED);
        //构造实体对象
        HttpEntity<MultiValueMap<String, Object>> param = new HttpEntity<>(map, headers);
        //发起请求,服务地址，请求参数，返回消息体的数据类型
        String result = StringUtils.EMPTY;
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(interfaceConf.getUrl(), param, String.class);
            saveLog(interfaceConf, JSONObject.toJSONString(map), response);
            result = response.getBody();
            return JSONUtil.xmlToJson(result).toString();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            iInterfaceLogService.saveInterfaceLog(interfaceConf, JSON.toJSONString(map), result, "1", IOUtils.printExceptionMsg(e));
        }
        return result;
    }

    /**
     * rest调用外部接口
     *
     * @param interfaceConf 调用接口配置信息
     * @param header        格式的参数 调用外部 restful接口使用
     * @param body          格式的参数 调用外部 restful接口使用
     * @return String 返回结果
     */
    @Override
    public String invokeRest(InterfaceConf interfaceConf, String header, String body) {
        HttpMethod httpMethod = HttpMethod.resolve(interfaceConf.getMethod().toUpperCase());
        HttpHeaders httpHeaders = createHttpHeaders(interfaceConf, header, MediaType.APPLICATION_JSON);
        String result = StringUtils.EMPTY;
        try {
            HttpEntity entity = new HttpEntity(body, httpHeaders);
            ResponseEntity<String> responseEntity;
            if (interfaceConf.getUrl().startsWith("https")) {
                responseEntity = ifNLBHttpsRestTemplate.exchange(interfaceConf.getUrl(), httpMethod, entity, String.class);
            } else {
                responseEntity = ifRestTemplate.exchange(interfaceConf.getUrl(), httpMethod, entity, String.class);
            }
            saveLog(interfaceConf, body, responseEntity);
        } catch (RestClientException e) {
            log.error(e.getMessage(), e);
            iInterfaceLogService.saveInterfaceLog(interfaceConf, body, result, "1", IOUtils.printExceptionMsg(e));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("STATUS", "E");
            jsonObject.put("MSG", "服务异常，请稍后重试！");
            return jsonObject.toJSONString();
        }
        return result;
    }

    /**
     * 调用webservice通用服务
     *
     * @param code
     * @param queryXml
     * @return
     */
    @Override
    public String invokeWs(InterfaceConf interfaceConf, String code, String queryXml) {
        String targetUrl = interfaceConf.getUrl();
        String method = interfaceConf.getMethod();
        String userName = interfaceConf.getUserName();
        String password = interfaceConf.getPassword();

        StringBuffer sb = new StringBuffer(queryXml);
        String xml = wsServiceClient.invokeWsMultiParam(targetUrl, method, userName, password, sb);
        //调用
        JSONObject jsonObject = null;
        try {
            jsonObject = XmlToJsonUtils.xml2Json(xml);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return jsonObject.toJSONString();
    }


    /**
     * 创建表头参数
     *
     * @param mediaType
     * @return
     */
    private HttpHeaders createHttpHeaders(InterfaceConf interfaceConf, String header, MediaType mediaType) {
        //构造http请求头
        HttpHeaders httpHeaders = new HttpHeaders();
        // 设置请求头参数
        httpHeaders.setContentType(mediaType);
        if (!StringUtils.isBlank(header)) {
            try {
                JSONObject object = JSONObject.parseObject(header);
                if (!object.isEmpty()) {
                    for (Map.Entry<String, Object> entry : object.entrySet()) {
                        httpHeaders.add(entry.getKey(), entry.getValue().toString());
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        // 获取basic加密账号密码
        if (StringUtils.isNotEmpty(interfaceConf.getUserName()) && StringUtils.isNotEmpty(interfaceConf.getPassword())) {
            String basicStr = "Basic " + Base64.encodeBase64String((interfaceConf.getUserName() + ":" + interfaceConf.getPassword()).getBytes());
            httpHeaders.add("Authorization", basicStr);
        }
        return httpHeaders;
    }

    /**
     * 保存接口日志
     *
     * @param interfaceConf
     * @param body
     * @param responseEntity
     * @return
     */
    private void saveLog(InterfaceConf interfaceConf, String body, ResponseEntity<String> responseEntity) {
        int statusCode = responseEntity.getStatusCodeValue();
        if (statusCode == HttpStatus.OK.value()) {
            String result = responseEntity.getBody();
            if (JSONUtil.isTypeJSON(result)) {
                JSONObject object = JSONObject.parseObject(result);
                if ((object.containsKey("code") && object.getInteger("code") == HttpStatus.OK.value())
                        || (object.containsKey("CODE") && object.getInteger("CODE") == HttpStatus.OK.value())
                        || ("S".equals(object.getString("Success")))
                        || "S".equalsIgnoreCase(object.getString("status"))
                        || "S".equalsIgnoreCase(object.getString("STATUS"))) {
                    iInterfaceLogService.saveInterfaceLog(interfaceConf, body, result, "0", null);
                } else {
                    iInterfaceLogService.saveInterfaceLog(interfaceConf, body, result, "1", null);
                }
            } else {
                cn.hutool.json.JSONObject jsonObject = JSONUtil.xmlToJson(result);
                if ((jsonObject.containsKey("returnsms") && jsonObject.getJSONObject("returnsms").getStr("returnstatus").equals("Success"))) {
                    iInterfaceLogService.saveInterfaceLog(interfaceConf, body, result, "0", null);
                } else {
                    iInterfaceLogService.saveInterfaceLog(interfaceConf, body, result, "1", null);
                }
            }
        } else {
            iInterfaceLogService.saveInterfaceLog(interfaceConf, body, responseEntity.getBody(), "1", null);
        }
    }
}
