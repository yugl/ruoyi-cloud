package com.ruoyi.inter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.domain.InterfaceLog;
import com.ruoyi.inter.dto.InterfaceLogDto;

import java.util.List;

/**
 * 接口日志Service接口
 *
 * @author ruoyi
 * @date 2022-03-28
 */
public interface IInterfaceLogService extends IService<InterfaceLog> {


    /**
     * 查询接口日志列表
     *
     * @param interfaceLog 接口日志
     * @return 接口日志集合
     */
    public List<InterfaceLog> selectdList(InterfaceLogDto interfaceLog);

    void saveInterfaceLog(InterfaceConf interfaceConf, String inParams, String result, String status, String remark);

    void saveInterfaceLog(String code, String inParams, String result, String status, String remark);
}
