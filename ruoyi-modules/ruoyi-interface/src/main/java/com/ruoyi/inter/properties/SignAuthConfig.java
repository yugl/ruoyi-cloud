package com.ruoyi.inter.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "sign.config")
public class SignAuthConfig {

    //签名目录
    private String signatureDir;
    //请求地址
    private String request_url;
    //认证地址
    private String auth_url;
    //客户号
    private String customerNumber;
    //应用名称
    private String appName;
}
