package com.ruoyi.inter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.inter.domain.InterfaceConf;

/**
 * 接口配置Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-25
 */
public interface InterfaceConfMapper extends BaseMapper<InterfaceConf> {


}
