package com.ruoyi.inter.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class RestTemplateConfig {

    @Bean(value = "restTemplateLong")
    public RestTemplate restTemplate() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(10 * 1000);// ms
        factory.setConnectTimeout(3000);// ms
        RestTemplate restTemplate = new RestTemplate(factory);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        return restTemplate;
    }

    @Bean("ifRestTemplate")
    @LoadBalanced
    public RestTemplate ifRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return toSupportUtf8(restTemplate);
    }

    @Bean("ifNLBHttpsRestTemplate")
    public RestTemplate ifNLBHttpsRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(new HttpsClientRequestFactory());
        return toSupportUtf8(restTemplate);
    }

    private RestTemplate toSupportUtf8(RestTemplate restTemplate) {
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        messageConverters.remove(1);
        HttpMessageConverter<?> converter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
        messageConverters.add(1, converter);
        restTemplate.setMessageConverters(messageConverters);
        return restTemplate;
    }
}
