package com.ruoyi.inter.controller;

import cn.hutool.core.img.ImgUtil;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.inter.util.AuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Map;

/**
 * 身份实名认证接口
 */
@Slf4j
@RequestMapping("/api/identity")
@RestController
public class IdentityAuthController {

    @Autowired
    AuthUtil authUtil;

    /**
     * 身份认证
     *
     * @param map
     * @return
     */
    @RequestMapping("/auth")
    public AjaxResult identityAuth(@RequestBody Map<String, String> map) {
        try {
            return authUtil.auth(map);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            log.error("认证服务程序异常");
            return AjaxResult.error(500, "认证服务程序异常");
        }
    }

    public static void main(String[] args) {
        ImgUtil.compress(new File("d:\\1.jpg"), new File("d:\\2.jpg"), 0.5f);
    }

}
