package com.ruoyi.inter.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口配置对象 interface_conf
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Data
public class InterfaceConf implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 接口编号
     */
    @Excel(name = "接口编号")
    private String code;

    /**
     * 对接系统
     */
    @Excel(name = "对接系统")
    private String module;

    /**
     * 接口名称
     */
    @Excel(name = "接口名称")
    private String name;

    /**
     * 目标URL
     */
    @Excel(name = "目标URL")
    private String url;

    /**
     * 接口类型（ws/rest）
     */
    @Excel(name = "接口类型", readConverterExp = "w=s/rest")
    private String type;

    /**
     * 请求方式
     */
    @Excel(name = "请求方式")
    private String requestMode;

    /**
     * 方法名
     */
    @Excel(name = "方法名")
    private String method;

    /**
     * 认证用户名
     */
    @Excel(name = "认证用户名")
    private String userName;

    /**
     * 认证密码
     */
    @Excel(name = "认证密码")
    private String password;

    /**
     * 状态（0有效 1无效）
     */
    @TableLogic
    private String status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createdTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updatedTime;


}
