package com.ruoyi.inter.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口日志对象 interface_log
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Data
public class InterfaceLogDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 接口编号
     */
    private String code;
    /**
     * 对接系统
     */
    private String module;
    /**
     * 入参
     */
    private String inParams;
    /**
     * 结果
     */
    private String result;
    /**
     * 调用时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date invokeTime;
    /**
     * 状态
     */
    private String status;

}
