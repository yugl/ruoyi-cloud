package com.ruoyi.inter.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class IOUtils {

    /**
     * 打印异常的详细信息
     *
     * @param exception
     * @return
     */
    public static String printExceptionMsg(Exception exception) {
        StringWriter stringWriter = null;
        PrintWriter printWriter = null;
        try {
            stringWriter = new StringWriter();
            printWriter = new PrintWriter(stringWriter);
            exception.printStackTrace(printWriter);
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stringWriter.close();
                printWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
