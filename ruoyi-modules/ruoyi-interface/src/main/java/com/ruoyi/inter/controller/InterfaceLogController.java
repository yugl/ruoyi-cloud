package com.ruoyi.inter.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.inter.domain.InterfaceLog;
import com.ruoyi.inter.dto.InterfaceLogDto;
import com.ruoyi.inter.service.IInterfaceLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 接口日志Controller
 *
 * @author ruoyi
 * @date 2022-03-28
 */
@Api(value = "接口日志控制器", tags = {"接口日志管理"})
@RestController
@RequestMapping("/interfaceLog")
public class InterfaceLogController extends BaseController {
    @Autowired
    private IInterfaceLogService interfaceLogService;

    /**
     * 查询接口日志列表
     */
    @ApiOperation("查询接口日志列表")
    @RequiresPermissions("interface:interfaceLog:list")
    @GetMapping("/list")
    public TableDataInfo list(InterfaceLogDto interfaceLog) {
        startPage();
        List<InterfaceLog> list = interfaceLogService.selectdList(interfaceLog);
        return getDataTable(list);
    }

    /**
     * 导出接口日志列表
     */
    @ApiOperation("导出接口日志列表")
    @RequiresPermissions("interface:interfaceLog:export")
    @Log(title = "接口日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InterfaceLogDto interfaceLog) throws IOException {

        startPage();
        List<InterfaceLog> list = interfaceLogService.selectdList(interfaceLog);
        ExcelUtil<InterfaceLog> util = new ExcelUtil<InterfaceLog>(InterfaceLog.class);
        util.exportExcel(response, list, "接口日志数据");
    }

    /**
     * 导入接口日志
     */
    @ApiOperation("导出接口日志列表")
    @RequiresPermissions("interface:interfaceLog:importData")
    @Log(title = "接口日志导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<InterfaceLog> util = new ExcelUtil<InterfaceLog>(InterfaceLog.class);
        List<InterfaceLog> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = interfaceLogService.saveOrUpdateBatch(list);
        } else {
            message = interfaceLogService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取接口日志详细信息
     */
    @ApiOperation("获取接口日志详细信息")
    @RequiresPermissions("interface:interfaceLog:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(interfaceLogService.getById(id));
    }

    /**
     * 新增接口日志
     */
    @ApiOperation("新增接口日志")
    @RequiresPermissions("interface:interfaceLog:add")
    @Log(title = "接口日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InterfaceLog interfaceLog) {
        return toAjax(interfaceLogService.save(interfaceLog));
    }

    /**
     * 修改接口日志
     */
    @ApiOperation("修改接口日志")
    @RequiresPermissions("interface:interfaceLog:edit")
    @Log(title = "接口日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InterfaceLog interfaceLog) {
        return toAjax(interfaceLogService.updateById(interfaceLog));
    }

    /**
     * 删除接口日志
     */
    @ApiOperation("删除接口日志")
    @RequiresPermissions("interface:interfaceLog:remove")
    @Log(title = "接口日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(interfaceLogService.removeByIds(ids));
    }
}
