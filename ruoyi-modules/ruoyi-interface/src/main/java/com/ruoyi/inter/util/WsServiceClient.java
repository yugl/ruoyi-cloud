package com.ruoyi.inter.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 调用CRM系统webservice接口工具类
 */
@Component
public class WsServiceClient {

    @Autowired
    @Qualifier(value = "restTemplateLong")
    private RestTemplate restTemplate;
    private static RestTemplate staticRestTemplate1;

    public WsServiceClient() {
    }

    @PostConstruct
    public void init() {
        staticRestTemplate1 = this.restTemplate;
    }

    /**
     * 调用crm系统websservice接口
     *
     * @param url      接口访问地址
     * @param method   接口函数名称
     * @param username 用户名
     * @param password 密码
     * @param queryXml 传入参数组装的xml
     * @return
     */
    public static String invokeWsMultiParam(String url, String method, String username, String password, StringBuffer queryXml) {
        JSONObject jsonObj = null;
        String serviceUrl = "";
        String nameSpace = "";
        String soapAction = "";
        try {
            jsonObj = XmlUtils.analysis(url, username, password, method);
            serviceUrl = jsonObj.getString("serviceUrl");
            nameSpace = jsonObj.getString("targetNamespace");
            soapAction = jsonObj.getString("soapAction");
        } catch (Exception var11) {
            var11.printStackTrace();
        }
        return invokeCrmClient(serviceUrl, soapAction, nameSpace, method, username, password, queryXml);
    }

    /**
     * 调用crm系统websservice接口
     *
     * @param url        访问地址
     * @param soapAction
     * @param nameSpace  命名空间
     * @param method     函数名称
     * @param username   用户名
     * @param password   密码
     * @param queryXml   传入参数
     * @return
     */
    public static String invokeCrmClient(String url, String soapAction, String nameSpace, String method, String username, String password, StringBuffer queryXml) {
        RestTemplate restTemplate = staticRestTemplate1;
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"  xmlns:urn=\"" + nameSpace + "\">");
        sb.append("<soapenv:Header>");
        sb.append("<SecurityHeader>");
        sb.append("<username>");
        sb.append("" + username);
        sb.append("</username>");
        sb.append("<password>");
        sb.append("" + password);
        sb.append("</password>");
        sb.append("</SecurityHeader>");
        sb.append("</soapenv:Header>");
        sb.append("<soapenv:Body>");
        sb.append("<urn:" + method + ">");
        sb.append(queryXml);
        sb.append("</urn:" + method + ">");
        sb.append("</soapenv:Body>");
        sb.append("</soapenv:Envelope>");

        String soap = sb.toString();
        HttpHeaders headers = new HttpHeaders();
        byte[] buf = soap.getBytes();
        String basicStr = "";
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
            basicStr = "Basic " + Base64.encodeBase64String((username + ":" + password).getBytes());
        }

        headers.add("Content-Length", String.valueOf(buf.length));
        headers.add("Content-Type", "text/xml; charset=utf-8");
        headers.add("SOAPAction", soapAction);
        headers.add("Authorization", basicStr);
        HttpEntity formEntity = new HttpEntity(soap, headers);
        String valStr = (String) restTemplate.postForObject(url, formEntity, String.class, new Object[0]);
        return valStr;
    }

    /**
     * 组装CRM接口传入参数
     * 格式类似于：
     * <DELIVERY_TYPE>百度</DELIVERY_TYPE>
     * <ZZCOUNTYS>滨湖区</ZZCOUNTYS>
     * <ZZCONSIGNEE_NO>10001</ZZCONSIGNEE_NO>
     * <ZZREGION_ZDLV>江苏</ZZREGION_ZDLV>
     *
     * @param params
     * @return
     */
    public static String convertMapToXml(Map<String, Object> params) {
        StringBuffer sb = new StringBuffer();
        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            String valStr = "";
            Object obj = params.get(key);
            if (obj != null) {
                valStr = obj.toString();
            }
            sb.append("<" + key + ">" + valStr + "</" + key + ">");
        }
        return sb.toString();
    }

    /**
     * 组装CRM接口传入参数
     * 格式类似于：
     * <HEADER>
     * <item>
     * <DELIVERY_TYPE>百度</DELIVERY_TYPE>
     * <ZZCOUNTYS>滨湖区</ZZCOUNTYS>
     * <ZZCONSIGNEE_NO>10001</ZZCONSIGNEE_NO>
     * <ZZREGION_ZDLV>江苏</ZZREGION_ZDLV>
     * <ZZDELIVER_TYPE>ZT01</ZZDELIVER_TYPE>
     * <ZZPAYS>ZPAY03</ZZPAYS>
     * </item>
     * </HEADER>
     *
     * @param params
     * @param header
     * @param item
     * @return
     */
    public static String convertListToXml(List<Map<String, Object>> params, String header, String item) {
        StringBuffer sb = new StringBuffer();
        sb.append("<" + header + ">");
        for (Map<String, Object> map : params) {
            Set<String> keySet = map.keySet();
            sb.append("<" + item + ">");
            for (String key : keySet) {
                String valStr = "";
                Object obj = map.get(key);
                if (obj != null) {
                    valStr = obj.toString();
                }
                sb.append("<" + key + ">" + valStr + "</" + key + ">");
            }
            sb.append("</" + item + ">");
        }
        sb.append("</" + header + ">");
        return sb.toString();
    }
}
