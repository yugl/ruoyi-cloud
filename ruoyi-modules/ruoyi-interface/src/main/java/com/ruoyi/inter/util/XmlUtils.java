package com.ruoyi.inter.util;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Iterator;

public class XmlUtils {

    public XmlUtils() {
    }

    public static JSONObject analysis(String wsdl, String username, String password, String method) throws Exception {
        JSONObject jsonObject = new JSONObject();
        String targetNamespace = "";
        String soapAction = "";
        String serviceUrl = "";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        String basicStr = "";
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
            basicStr = "Basic " + Base64.encodeBase64String((username + ":" + password).getBytes());
        }

        headers.add("Authorization", basicStr);
        HttpEntity formEntity = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(wsdl, HttpMethod.GET, formEntity, String.class, new Object[0]);
        int statusCodeValue = response.getStatusCodeValue();
        if (statusCodeValue == 401) {
            throw new Exception("接口地址认证失败");
        } else if (statusCodeValue != 200) {
            throw new Exception("接口wsdl地址请求失败");
        } else {
            String str = (String) response.getBody();
            byte[] bytes = str.getBytes();
            InputStream is = new ByteArrayInputStream(bytes);
            SAXReader reader = new SAXReader();
            Document document = reader.read(is);
            Element rootElement = document.getRootElement();
            targetNamespace = rootElement.attributeValue("targetNamespace");
            Iterator<Element> bindingIt = rootElement.elementIterator("binding");
            boolean methodFlag = false;

            Element serviceElement;
            Iterator portIt;
            Element element;
            while (bindingIt.hasNext()) {
                serviceElement = (Element) bindingIt.next();
                portIt = serviceElement.elementIterator("operation");

                while (portIt.hasNext()) {
                    element = (Element) portIt.next();
                    String name1 = element.attributeValue("name");
                    if (StringUtil.equals(name1, method)) {
                        methodFlag = true;
                        Element element2 = element.element("operation");
                        soapAction = element2.attributeValue("soapAction");
                    }
                }
            }

            if (!methodFlag) {
                throw new Exception("找不到方法" + method);
            } else {
                serviceElement = rootElement.element("service");

                Element element2;
                for (portIt = serviceElement.elementIterator("port"); portIt.hasNext(); serviceUrl = element2.attributeValue("location")) {
                    element = (Element) portIt.next();
                    element2 = element.element("address");
                }

                jsonObject.put("targetNamespace", targetNamespace);
                jsonObject.put("soapAction", soapAction);
                jsonObject.put("serviceUrl", serviceUrl);
                return jsonObject;
            }
        }
    }
}
