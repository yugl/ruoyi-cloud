package com.ruoyi.inter.service;

import com.ruoyi.inter.domain.InterfaceConf;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public interface InvokeInterfaceService {

    /**
     * 只允许rest form表单调用外部接口
     *
     * @param action String
     * @param header json 格式的参数
     * @param map    form表单提交
     * @return String 返回结果
     */
    String invokeRest(InterfaceConf interfaceConf, String header, MultiValueMap<String, Object> map);

    /**
     * rest调用外部接口
     *
     * @param confInvoke IfConfInvoke 调用接口配置信息
     * @param header     jsonString 格式的参数 调用外部 restful接口使用
     * @param body       jsonString 格式的参数 调用外部 restful接口使用
     * @return String 返回结果
     */
    String invokeRest(InterfaceConf interfaceConf, String header, String body);

    /**
     * 调用外部 webservice接口
     *
     * @param code
     * @param queryXml xml 格式的参数
     * @return
     */
    String invokeWs(InterfaceConf interfaceConf, String action, String queryXml);
}
