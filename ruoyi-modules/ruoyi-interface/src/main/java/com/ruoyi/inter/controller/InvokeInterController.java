package com.ruoyi.inter.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.service.IInterfaceConfService;
import com.ruoyi.inter.service.InvokeInterfaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Slf4j
@RestController
@RequestMapping("/api/invoke")
public class InvokeInterController {

    @Autowired
    private InvokeInterfaceService invokeInterfaceService;
    @Autowired
    private IInterfaceConfService interfaceConfService;

    /**
     * 只允许rest form表单调用外部接口
     *
     * @param action String
     * @param header json 格式的参数
     * @param map    form表单提交
     * @return String 返回结果
     */
    @PostMapping("/invokeRestForm")
    public String callRestFormRoute(@RequestParam("code") String code,
                                    @RequestParam(value = "header", required = false) String header,
                                    @RequestParam MultiValueMap<String, Object> map) {
        InterfaceConf interfaceConf = interfaceConfService.getBaseMapper().selectOne(new QueryWrapper<InterfaceConf>().eq("code", code).eq("status", "0"));
        JSONObject jsonObject = new JSONObject();
        if (interfaceConf == null) {
            jsonObject.put("STATUS", "E");
            jsonObject.put("MSG", String.format("当前code:[%s],未配置处理接口。", code));
            return jsonObject.toJSONString();
        }
        if (!StringUtils.isBlank(header)) {
            header = new String(header.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        }
        return invokeInterfaceService.invokeRest(interfaceConf, header, map);
    }


    /**
     * 只允许rest调用外部接口
     *
     * @param action String
     * @param header json 格式的参数
     * @param body   json 格式的参数
     * @return String 返回结果
     */
    @PostMapping("/invokeRest")
    public String callRestRoute(@RequestParam("code") String code,
                                @RequestParam(value = "header", required = false) String header,
                                @RequestBody String body) {
        InterfaceConf interfaceConf = interfaceConfService.getBaseMapper().selectOne(new QueryWrapper<InterfaceConf>().eq("code", code).eq("status", "0"));
        JSONObject jsonObject = new JSONObject();
        if (interfaceConf == null) {
            jsonObject.put("STATUS", "E");
            jsonObject.put("MSG", String.format("当前code:[%s],未配置处理接口。", code));
            return jsonObject.toJSONString();
        }
        if (!StringUtils.isBlank(header)) {
            header = new String(header.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        }
        if (!StringUtils.isBlank(body)) {
            body = new String(body.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        }
        return invokeInterfaceService.invokeRest(interfaceConf, header, body);
    }

    /**
     * 调用外部 webservice接口
     *
     * @param code
     * @param queryXml xml 格式的参数
     * @return
     */
    @PostMapping("/invokeWs")
    public String invokeWs(@RequestParam("code") String code,
                           @RequestParam("queryXml") String queryXml) {
        InterfaceConf interfaceConf = interfaceConfService.getBaseMapper().selectOne(new QueryWrapper<InterfaceConf>().eq("code", code).eq("status", "0"));
        JSONObject jsonObject = new JSONObject();
        if (interfaceConf == null) {
            jsonObject.put("STATUS", "E");
            jsonObject.put("MSG", String.format("当前code:[%s],未配置处理接口。", code));
            return jsonObject.toJSONString();
        }
        return invokeInterfaceService.invokeWs(interfaceConf, code, queryXml);
    }

    public static void main(String[] args) {
        String url = "http://47.95.121.34:8888/sms.aspx";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        //接口参数
        map.add("action", "send");
        map.add("userid", "127");
        map.add("account", "XHKJ");
        map.add("password", "123456");
        map.add("mobile", "18563719675");
        map.add("content", "【展安通】测试短信发斯蒂芬斯蒂芬送");
        //头部类型
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //构造实体对象
        HttpEntity<MultiValueMap<String, Object>> param = new HttpEntity<>(map, headers);
        //发起请求,服务地址，请求参数，返回消息体的数据类型
        ResponseEntity<String> response = restTemplate.postForEntity(url, param, String.class);
        //body
        String body = response.getBody();
        System.out.println(body);
    }

}
