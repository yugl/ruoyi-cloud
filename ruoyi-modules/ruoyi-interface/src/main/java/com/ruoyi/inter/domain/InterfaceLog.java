package com.ruoyi.inter.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口日志对象 interface_log
 *
 * @author ruoyi
 * @date 2022-04-04
 */
@Data
public class InterfaceLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 接口编号
     */
    @Excel(name = "接口编号")
    private String code;

    /**
     * 对接系统
     */
    @Excel(name = "对接系统")
    private String module;

    /**
     * 接口名称
     */
    @Excel(name = "接口名称")
    private String name;

    /**
     * 接口地址
     */
    @Excel(name = "接口地址")
    private String url;

    /**
     * 方法名称
     */
    @Excel(name = "方法名称")
    private String method;

    /**
     * 接口类型（ws/rest）
     */
    @Excel(name = "接口类型", readConverterExp = "ws/rest")
    private String type;

    /**
     * 请求方式
     */
    @Excel(name = "请求方式")
    private String requestMode;

    /**
     * 入参
     */
    private String inParams;

    /**
     * 结果
     */
    @Excel(name = "结果")
    private String result;

    /**
     * 调用时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "调用时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date invokeTime;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private String status;

    private String remark;


}
