package com.ruoyi.inter.util;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 身份认证返回状态码
 */
@Component
public class AuthCodeUtil {

    public static Map<String, String> codeMap1 = new HashMap<>();
    public static Map<String, String> codeMap2 = new HashMap<>();
    public static Map<String, String> codeMap3 = new HashMap<>();

    @PostConstruct
    public static void init() {
        codeMap1.put("0", "身份信息有效");
        codeMap1.put("5", "身份信息无效");
        codeMap1.put("7", "系统错误或服务异常");
        codeMap1.put("A", "穿网调用超时");
        codeMap1.put("E", "公安接口调用异常 ");
        codeMap1.put("T", "公安接口调用超时");
        codeMap1.put("X", "未执行");
        codeMap1.put("Z", "该站点配置的每日公安网访问次数耗尽");
        codeMap1.put("N", "最新证件有效期截至日期不在输入的截止日期区段内");

        codeMap2.put("0", "同一人(人像加密)");
        codeMap2.put("1", "请重新拍照");
        codeMap2.put("2", "光线过暗或角度问题，请重新拍照");
        codeMap2.put("A", "系统错误或服务异常");
        codeMap2.put("B", "系统错误或服务异常 ");
        codeMap2.put("C", "请检查业务流水号");
        codeMap2.put("D", "数据库中无该人像信息");
        codeMap2.put("E", "确认图片格式是否正确,支持JPG,BMP,PNG 三种照片格式");
        codeMap2.put("F", "请重新拍照");
        codeMap2.put("G", "上传照片质量不合格");
        codeMap2.put("J", "照片长度过小，请重拍照片");
        codeMap2.put("K", "公安制证照片质量不佳");
        codeMap2.put("T", "人像引擎超时");
        codeMap2.put("W", "认证超时");
        codeMap2.put("X", "未执行");

        codeMap3.put("0", "网证状态正常");
        codeMap3.put("1", "身份证已被注销或已挂失");
        codeMap3.put("2", "该身份证没有申请居民身份证网证");
        codeMap3.put("3", "确认是否为本人网证");
        codeMap3.put("4", "网证口令和申请时不一致");
        codeMap3.put("5", "身份证已做冻结操作，未解冻");
        codeMap3.put("6", "身份信息不匹配");
        codeMap3.put("8", "身份证已过期");
        codeMap3.put("9", "ID 验证数据为空");
        codeMap3.put("J", "网证信息有误");
        codeMap3.put("B", "网证下载设备与认证设备不匹配");
        codeMap3.put("C", "网证数据格式不正确");
        codeMap3.put("D", "确认身份证是否有效");
        codeMap3.put("E", "身份认证系统异常");
        codeMap3.put("X", "未执行");
        codeMap3.put("A", "网证口令错误次数达上线");
    }
}
