package com.ruoyi.inter.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.anxin.auth.AuthenticationUtil;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.inter.properties.SignAuthConfig;
import com.ruoyi.inter.service.IInterfaceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Map;

@RefreshScope
@Component
public class AuthUtil {

    @Autowired
    SignAuthConfig signAuthConfig;

    @Autowired
    IInterfaceLogService iInterfaceLogService;

    public AjaxResult auth(Map<String, String> map) throws Exception {
        map.put("signatureDir", signAuthConfig.getSignatureDir());
        map.put("request_url", signAuthConfig.getRequest_url());
        map.put("auth_url", signAuthConfig.getAuth_url());
        map.put("customerNumber", signAuthConfig.getCustomerNumber());
        map.put("appName", signAuthConfig.getAppName());
        map.put("authMode", "0x42");
        String json = AuthenticationUtil.auth(map);
        JSONObject jsonObject = JSONObject.parseObject(json);
        String authResult = jsonObject.getJSONObject("bizPackage").getString("authResult");
        if ("00".equals(StrUtil.sub(authResult, 0, 2))) {
            iInterfaceLogService.saveInterfaceLog("1003", JSON.toJSONString(map), json, "0", null);
            return AjaxResult.success();
        } else if (authResult.equals("XXXX")) {
            iInterfaceLogService.saveInterfaceLog("1003", JSON.toJSONString(map), json, "1", null);
            return AjaxResult.error(500, jsonObject.getJSONObject("bizPackage").getString("errorDesc"));
        } else {
            iInterfaceLogService.saveInterfaceLog("1003", JSON.toJSONString(map), json, "1", null);
            String first = StrUtil.sub(authResult, 0, 1);
            if (!"0".equals(first)) {
                return AjaxResult.error(500, AuthCodeUtil.codeMap1.get(first));
            }
            first = StrUtil.sub(authResult, 1, 2);
            return AjaxResult.error(500, AuthCodeUtil.codeMap2.get(first));
        }
    }
}
