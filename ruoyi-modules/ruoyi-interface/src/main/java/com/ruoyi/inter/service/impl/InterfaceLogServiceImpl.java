package com.ruoyi.inter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.domain.InterfaceLog;
import com.ruoyi.inter.dto.InterfaceLogDto;
import com.ruoyi.inter.mapper.InterfaceConfMapper;
import com.ruoyi.inter.mapper.InterfaceLogMapper;
import com.ruoyi.inter.service.IInterfaceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 接口日志Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-28
 */
@Service
public class InterfaceLogServiceImpl extends ServiceImpl<InterfaceLogMapper, InterfaceLog> implements IInterfaceLogService {
    @Autowired
    private InterfaceLogMapper interfaceLogMapper;
    @Autowired
    InterfaceConfMapper interfaceConfMapper;

    /**
     * 查询接口日志列表
     *
     * @param interfaceLog 接口日志
     * @return 接口日志
     */
    @Override
    public List<InterfaceLog> selectdList(InterfaceLogDto interfaceLog) {
        QueryWrapper<InterfaceLog> q = new QueryWrapper<>();
        if (interfaceLog.getCode() != null && !interfaceLog.getCode().trim().equals("")) {
            q.eq("code", interfaceLog.getCode());
        }
        if (interfaceLog.getModule() != null && !interfaceLog.getModule().trim().equals("")) {
            q.eq("module", interfaceLog.getModule());
        }
        if (interfaceLog.getInParams() != null && !interfaceLog.getInParams().trim().equals("")) {
            q.like("in_params", interfaceLog.getInParams());
        }
        if (interfaceLog.getResult() != null && !interfaceLog.getResult().trim().equals("")) {
            q.like("result", interfaceLog.getResult());
        }
        if (interfaceLog.getInvokeTime() != null) {
            q.eq("invoke_time", interfaceLog.getInvokeTime());
        }
        if (interfaceLog.getStatus() != null && !interfaceLog.getStatus().trim().equals("")) {
            q.eq("status", interfaceLog.getStatus());
        }
        q.orderByDesc("invoke_time");
        List<InterfaceLog> list = this.list(q);
        return list;
    }

    @Transactional
    @Override
    public void saveInterfaceLog(InterfaceConf interfaceConf, String inParams, String result, String status, String remark) {
        try {
            InterfaceLog sysInterfaceLog = new InterfaceLog();
            sysInterfaceLog.setType(interfaceConf.getType());
            sysInterfaceLog.setRequestMode(interfaceConf.getRequestMode());
            sysInterfaceLog.setCode(interfaceConf.getCode());
            sysInterfaceLog.setModule(interfaceConf.getModule());
            sysInterfaceLog.setInvokeTime(new Date());
            sysInterfaceLog.setName(interfaceConf.getName());
            sysInterfaceLog.setMethod(interfaceConf.getMethod());
            sysInterfaceLog.setUrl(interfaceConf.getUrl());
            if (!StringUtils.isEmpty(inParams) && inParams.length() > 30000) {
                inParams = inParams.substring(0, 30000);
            }
            sysInterfaceLog.setInParams(inParams);
            if (result.length() > 30000) {
                result = result.substring(0, 30000);
            }
            sysInterfaceLog.setResult(result);
            sysInterfaceLog.setStatus(status);
            sysInterfaceLog.setRemark(remark);
            this.getBaseMapper().insert(sysInterfaceLog);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Transactional
    @Override
    public void saveInterfaceLog(String code, String inParams, String result, String status, String remark) {
        try {
            InterfaceConf interfaceConf = interfaceConfMapper.selectOne(new QueryWrapper<InterfaceConf>()
                    .eq("code", code)
                    .eq("status", "0")
            );
            InterfaceLog sysInterfaceLog = new InterfaceLog();
            sysInterfaceLog.setType(interfaceConf.getType());
            sysInterfaceLog.setRequestMode(interfaceConf.getRequestMode());
            sysInterfaceLog.setCode(interfaceConf.getCode());
            sysInterfaceLog.setModule(interfaceConf.getModule());
            sysInterfaceLog.setInvokeTime(new Date());
            sysInterfaceLog.setName(interfaceConf.getName());
            sysInterfaceLog.setMethod(interfaceConf.getMethod());
            sysInterfaceLog.setUrl(interfaceConf.getUrl());
            if (!StringUtils.isEmpty(inParams) && inParams.length() > 30000) {
                inParams = inParams.substring(0, 30000);
            }
            sysInterfaceLog.setInParams(inParams);
            if (result.length() > 30000) {
                result = result.substring(0, 30000);
            }
            sysInterfaceLog.setResult(result);
            sysInterfaceLog.setStatus(status);
            sysInterfaceLog.setRemark(remark);
            this.getBaseMapper().insert(sysInterfaceLog);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
