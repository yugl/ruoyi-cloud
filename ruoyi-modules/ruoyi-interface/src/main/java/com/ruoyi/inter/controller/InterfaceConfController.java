package com.ruoyi.inter.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.dto.InterfaceConfDto;
import com.ruoyi.inter.service.IInterfaceConfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * 接口配置Controller
 *
 * @author 于观礼
 * @date 2022-03-25
 */
@Api(value = "接口配置控制器", tags = {"接口配置管理"})
@RestController
@RequestMapping("/interfaceConf")
public class InterfaceConfController extends BaseController {
    @Autowired
    private IInterfaceConfService interfaceConfService;

    /**
     * 查询接口配置列表
     */
    @ApiOperation("查询接口配置列表")
    @RequiresPermissions("interface:interfaceConf:list")
    @GetMapping("/list")
    public TableDataInfo list(InterfaceConfDto interfaceConf) {
        startPage();
        List<InterfaceConf> list = interfaceConfService.selectdList(interfaceConf);
        return getDataTable(list);
    }

    /**
     * 导出接口配置列表
     */
    @ApiOperation("导出接口配置列表")
    @RequiresPermissions("interface:interfaceConf:export")
    @Log(title = "接口配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InterfaceConfDto interfaceConf) throws IOException {

        startPage();
        List<InterfaceConf> list = interfaceConfService.selectdList(interfaceConf);
        ExcelUtil<InterfaceConf> util = new ExcelUtil<InterfaceConf>(InterfaceConf.class);
        util.exportExcel(response, list, "接口配置数据");
    }

    /**
     * 导入接口配置
     */
    @ApiOperation("导出接口配置列表")
    @RequiresPermissions("interface:interfaceConf:importData")
    @Log(title = "接口配置导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<InterfaceConf> util = new ExcelUtil<InterfaceConf>(InterfaceConf.class);
        List<InterfaceConf> list = util.importExcel(file.getInputStream());
        boolean message = false;
        if (updateSupport) {
            message = interfaceConfService.saveOrUpdateBatch(list);
        } else {
            message = interfaceConfService.saveBatch(list);
        }
        return AjaxResult.success(message);
    }

    /**
     * 获取接口配置详细信息
     */
    @ApiOperation("获取接口配置详细信息")
    @RequiresPermissions("interface:interfaceConf:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(interfaceConfService.getById(id));
    }

    /**
     * 新增接口配置
     */
    @ApiOperation("新增接口配置")
    @RequiresPermissions("interface:interfaceConf:add")
    @Log(title = "接口配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InterfaceConf interfaceConf) {
        return toAjax(interfaceConfService.save(interfaceConf));
    }

    /**
     * 修改接口配置
     */
    @ApiOperation("修改接口配置")
    @RequiresPermissions("interface:interfaceConf:edit")
    @Log(title = "接口配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InterfaceConf interfaceConf) {
        return toAjax(interfaceConfService.updateById(interfaceConf));
    }

    /**
     * 删除接口配置
     */
    @ApiOperation("删除接口配置")
    @RequiresPermissions("interface:interfaceConf:remove")
    @Log(title = "接口配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids) {
        return toAjax(interfaceConfService.removeByIds(ids));
    }
}
