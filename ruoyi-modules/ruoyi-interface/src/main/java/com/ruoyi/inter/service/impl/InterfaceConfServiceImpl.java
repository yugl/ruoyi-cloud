package com.ruoyi.inter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.inter.domain.InterfaceConf;
import com.ruoyi.inter.dto.InterfaceConfDto;
import com.ruoyi.inter.mapper.InterfaceConfMapper;
import com.ruoyi.inter.service.IInterfaceConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 接口配置Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-25
 */
@Service
public class InterfaceConfServiceImpl extends ServiceImpl<InterfaceConfMapper, InterfaceConf> implements IInterfaceConfService {
    @Autowired
    private InterfaceConfMapper interfaceConfMapper;

    /**
     * 查询接口配置列表
     *
     * @param interfaceConf 接口配置
     * @return 接口配置
     */
    @Override
    public List<InterfaceConf> selectdList(InterfaceConfDto interfaceConf) {
        QueryWrapper<InterfaceConf> q = new QueryWrapper<>();
        if (interfaceConf.getCode() != null && !interfaceConf.getCode().trim().equals("")) {
            q.eq("code", interfaceConf.getCode());
        }
        if (interfaceConf.getModule() != null && !interfaceConf.getModule().trim().equals("")) {
            q.eq("module", interfaceConf.getModule());
        }
        if (interfaceConf.getName() != null && !interfaceConf.getName().trim().equals("")) {
            q.like("name", interfaceConf.getName());
        }
        if (interfaceConf.getType() != null && !interfaceConf.getType().trim().equals("")) {
            q.eq("type", interfaceConf.getType());
        }
        List<InterfaceConf> list = this.list(q);
        return list;
    }


}
