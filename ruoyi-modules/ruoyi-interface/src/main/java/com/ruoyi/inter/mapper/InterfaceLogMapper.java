package com.ruoyi.inter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.inter.domain.InterfaceLog;

/**
 * 接口日志Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-28
 */
public interface InterfaceLogMapper extends BaseMapper<InterfaceLog> {


}
