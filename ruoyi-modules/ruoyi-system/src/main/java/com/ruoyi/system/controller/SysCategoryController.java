package com.ruoyi.system.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.service.ISysCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 行业分类Controller
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Api(value = "行业分类控制器", tags = {"行业分类管理"})
@RestController
@RequestMapping("/category")
public class SysCategoryController extends BaseController {
    @Autowired
    private ISysCategoryService sysCategoryService;

    /**
     * 查询行业级联下拉
     *
     * @return
     */
    @ApiOperation("行业分类级联下拉")
    @GetMapping("/findNameSelectList")
    public AjaxResult findNameSelectList() {
        return sysCategoryService.findNameSelectList();
    }

    /**
     * 查询行业分类列表
     */
    @ApiOperation("查询行业分类列表")
    //@RequiresPermissions("system:category:list")
    @GetMapping("/list")
    public AjaxResult list(SysCategory sysCategory) {
        List<SysCategory> list = sysCategoryService.selectSysCategoryList(sysCategory);
        return AjaxResult.success(list);
    }

    /**
     * 获取行业分类详细信息
     */
    @ApiOperation("获取行业分类详细信息")
    //@RequiresPermissions("system:category:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(sysCategoryService.selectSysCategoryById(id));
    }

    /**
     * 获取行业下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(SysCategory sysCategory) {
        Long userId = SecurityUtils.getUserId();
        List<SysCategory> list = sysCategoryService.selectSysCategoryList(sysCategory);
        return AjaxResult.success(sysCategoryService.buildMenuTreeSelect(list));
    }

    /**
     * 新增行业分类
     */
    @ApiOperation("新增行业分类")
    //@RequiresPermissions("system:category:add")
    @Log(title = "行业分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCategory sysCategory) {
        if (UserConstants.NOT_UNIQUE.equals(sysCategoryService.checkNameUnique(sysCategory))) {
            return AjaxResult.error("新增行业'" + sysCategory.getName() + "'失败，行业名称已存在");
        }
        sysCategory.setCreateBy(SecurityUtils.getUsername());
        return toAjax(sysCategoryService.insertSysCategory(sysCategory));
    }

    /**
     * 修改行业分类
     */
    @ApiOperation("修改行业分类")
    //@RequiresPermissions("system:category:edit")
    @Log(title = "行业分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCategory sysCategory) {
        if (UserConstants.NOT_UNIQUE.equals(sysCategoryService.checkNameUnique(sysCategory))) {
            return AjaxResult.error("修改行业'" + sysCategory.getName() + "'失败，行业名称已存在");
        } else if (sysCategory.getId().equals(sysCategory.getParentId())) {
            return AjaxResult.error("修改行业'" + sysCategory.getName() + "'失败，上级行业不能选择自己");
        }
        sysCategory.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(sysCategoryService.updateSysCategory(sysCategory));
    }

    /**
     * 删除行业分类
     */
    @ApiOperation("删除行业分类")
    //@RequiresPermissions("system:category:remove")
    @Log(title = "行业分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id) {
        if (sysCategoryService.hasChildById(id)) {
            return AjaxResult.error("存在子行业,不允许删除");
        }
        return toAjax(sysCategoryService.deleteSysCategoryById(id));
    }
}
