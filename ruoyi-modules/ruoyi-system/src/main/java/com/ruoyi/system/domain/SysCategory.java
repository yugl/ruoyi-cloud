package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 行业分类对象 sys_category
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Data
@NoArgsConstructor
public class SysCategory extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 行业id
     */
    private Long id;

    /**
     * 行业名称
     */
    @Excel(name = "行业名称")
    private String name;

    /**
     * 父行业id
     */
    @Excel(name = "父行业id")
    private Long parentId;

    /**
     * 显示顺序
     */
    @Excel(name = "显示顺序")
    private Long orderNum;

    /**
     * 菜单状态（0正常 1停用）
     */
    @Excel(name = "菜单状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 子菜单
     */
    private List<SysCategory> children = new ArrayList<SysCategory>();


}
