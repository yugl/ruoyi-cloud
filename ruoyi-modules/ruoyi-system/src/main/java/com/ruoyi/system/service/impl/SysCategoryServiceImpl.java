package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.vo.TreeSelect;
import com.ruoyi.system.mapper.SysCategoryMapper;
import com.ruoyi.system.service.ISysCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 行业分类Service业务层处理
 *
 * @author 于观礼
 * @date 2022-03-14
 */
@Service
public class SysCategoryServiceImpl implements ISysCategoryService {
    @Autowired
    private SysCategoryMapper sysCategoryMapper;

    /**
     * 查询行业分类
     *
     * @param id 行业分类主键
     * @return 行业分类
     */
    @Override
    public SysCategory selectSysCategoryById(Long id) {
        return sysCategoryMapper.selectSysCategoryById(id);
    }

    /**
     * 查询行业分类列表
     *
     * @param sysCategory 行业分类
     * @return 行业分类
     */
    @Override
    public List<SysCategory> selectSysCategoryList(SysCategory sysCategory) {
        List<SysCategory> list = sysCategoryMapper.selectSysCategoryList(sysCategory);
        return list;
    }

    /**
     * 新增行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    @Override
    public int insertSysCategory(SysCategory sysCategory) {
        sysCategory.setCreateTime(DateUtils.getNowDate());
        return sysCategoryMapper.insertSysCategory(sysCategory);
    }

    /**
     * 修改行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    @Override
    public int updateSysCategory(SysCategory sysCategory) {
        sysCategory.setUpdateTime(DateUtils.getNowDate());
        return sysCategoryMapper.updateSysCategory(sysCategory);
    }

    /**
     * 批量删除行业分类
     *
     * @param ids 需要删除的行业分类主键
     * @return 结果
     */
    @Override
    public int deleteSysCategoryByIds(Long[] ids) {
        return sysCategoryMapper.deleteSysCategoryByIds(ids);
    }

    /**
     * 删除行业分类信息
     *
     * @param id 行业分类主键
     * @return 结果
     */
    @Override
    public int deleteSysCategoryById(Long id) {
        return sysCategoryMapper.deleteSysCategoryById(id);
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param list 菜单列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildMenuTreeSelect(List<SysCategory> list) {
        List<SysCategory> sysCategoryList = buildMenuTree(list);
        return sysCategoryList.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    public List<SysCategory> buildMenuTree(List<SysCategory> list) {
        List<SysCategory> returnList = new ArrayList<SysCategory>();
        List<Long> tempList = new ArrayList<Long>();
        for (SysCategory sysCategory : list) {
            tempList.add(sysCategory.getId());
        }
        for (Iterator<SysCategory> iterator = list.iterator(); iterator.hasNext(); ) {
            SysCategory sysCategory = (SysCategory) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(sysCategory.getParentId())) {
                recursionFn(list, sysCategory);
                returnList.add(sysCategory);
            }
        }
        if (returnList.isEmpty()) {
            returnList = list;
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<SysCategory> list, SysCategory t) {
        // 得到子节点列表
        List<SysCategory> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysCategory tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysCategory> getChildList(List<SysCategory> list, SysCategory t) {
        List<SysCategory> tlist = new ArrayList<SysCategory>();
        Iterator<SysCategory> it = list.iterator();
        while (it.hasNext()) {
            SysCategory n = (SysCategory) it.next();
            if (n.getParentId().longValue() == t.getId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysCategory> list, SysCategory t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }


    /**
     * 校验菜单名称是否唯一
     *
     * @param sysCategory 菜单信息
     * @return 结果
     */
    @Override
    public String checkNameUnique(SysCategory sysCategory) {
        Long id = StringUtils.isNull(sysCategory.getId()) ? -1L : sysCategory.getId();
        SysCategory info = sysCategoryMapper.checkNameUnique(sysCategory.getName(), sysCategory.getParentId());
        if (StringUtils.isNotNull(info) && info.getId().longValue() != id.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public boolean hasChildById(Long menuId) {
        int result = sysCategoryMapper.hasChildById(menuId);
        return result > 0 ? true : false;
    }

    /**
     * 行业级联下拉
     *
     * @return
     */
    @Override
    public AjaxResult findNameSelectList() {
        List<SysCategory> list = sysCategoryMapper.selectSysCategoryList(new SysCategory());
        List<TreeSelect> treeSelects = buildMenuTreeSelect(list);
        List<Map<String, Object>> result = treeSelects.stream().map(obj -> {
            Map<String, Object> map = new HashMap<>();
            map.put("label", obj.getLabel());
            map.put("value", obj.getId());
            map.put("children", getChildren(obj.getChildren()));
            return map;
        }).collect(Collectors.toList());
        return AjaxResult.success(result);
    }

    private List<Map<String, Object>> getChildren(List<TreeSelect> list) {
        return list.stream().map(obj -> {
            Map<String, Object> map = new HashMap<>();
            map.put("label", obj.getLabel());
            map.put("value", obj.getId());
            List<TreeSelect> children = obj.getChildren();
            if (children != null && children.size() > 0) {
                map.put("children", getChildren(children));
            }
            return map;
        }).collect(Collectors.toList());
    }


}
