package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.InterfaceInvokeConfMapper;
import com.ruoyi.system.domain.InterfaceInvokeConf;
import com.ruoyi.system.service.IInterfaceInvokeConfService;

/**
 * 接口配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-03-12
 */
@Service
public class InterfaceInvokeConfServiceImpl implements IInterfaceInvokeConfService 
{
    @Autowired
    private InterfaceInvokeConfMapper interfaceInvokeConfMapper;

    /**
     * 查询接口配置
     * 
     * @param id 接口配置主键
     * @return 接口配置
     */
    @Override
    public InterfaceInvokeConf selectInterfaceInvokeConfById(Long id)
    {
        return interfaceInvokeConfMapper.selectInterfaceInvokeConfById(id);
    }

    /**
     * 查询接口配置列表
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 接口配置
     */
    @Override
    public List<InterfaceInvokeConf> selectInterfaceInvokeConfList(InterfaceInvokeConf interfaceInvokeConf)
    {
        return interfaceInvokeConfMapper.selectInterfaceInvokeConfList(interfaceInvokeConf);
    }

    /**
     * 新增接口配置
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 结果
     */
    @Override
    public int insertInterfaceInvokeConf(InterfaceInvokeConf interfaceInvokeConf)
    {
        return interfaceInvokeConfMapper.insertInterfaceInvokeConf(interfaceInvokeConf);
    }

    /**
     * 修改接口配置
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 结果
     */
    @Override
    public int updateInterfaceInvokeConf(InterfaceInvokeConf interfaceInvokeConf)
    {
        return interfaceInvokeConfMapper.updateInterfaceInvokeConf(interfaceInvokeConf);
    }

    /**
     * 批量删除接口配置
     * 
     * @param ids 需要删除的接口配置主键
     * @return 结果
     */
    @Override
    public int deleteInterfaceInvokeConfByIds(Long[] ids)
    {
        return interfaceInvokeConfMapper.deleteInterfaceInvokeConfByIds(ids);
    }

    /**
     * 删除接口配置信息
     * 
     * @param id 接口配置主键
     * @return 结果
     */
    @Override
    public int deleteInterfaceInvokeConfById(Long id)
    {
        return interfaceInvokeConfMapper.deleteInterfaceInvokeConfById(id);
    }
}
