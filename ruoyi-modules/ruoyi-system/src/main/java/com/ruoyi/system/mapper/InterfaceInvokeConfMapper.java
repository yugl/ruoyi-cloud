package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.InterfaceInvokeConf;

/**
 * 接口配置Mapper接口
 * 
 * @author ruoyi
 * @date 2022-03-12
 */
public interface InterfaceInvokeConfMapper 
{
    /**
     * 查询接口配置
     * 
     * @param id 接口配置主键
     * @return 接口配置
     */
    public InterfaceInvokeConf selectInterfaceInvokeConfById(Long id);

    /**
     * 查询接口配置列表
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 接口配置集合
     */
    public List<InterfaceInvokeConf> selectInterfaceInvokeConfList(InterfaceInvokeConf interfaceInvokeConf);

    /**
     * 新增接口配置
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 结果
     */
    public int insertInterfaceInvokeConf(InterfaceInvokeConf interfaceInvokeConf);

    /**
     * 修改接口配置
     * 
     * @param interfaceInvokeConf 接口配置
     * @return 结果
     */
    public int updateInterfaceInvokeConf(InterfaceInvokeConf interfaceInvokeConf);

    /**
     * 删除接口配置
     * 
     * @param id 接口配置主键
     * @return 结果
     */
    public int deleteInterfaceInvokeConfById(Long id);

    /**
     * 批量删除接口配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteInterfaceInvokeConfByIds(Long[] ids);
}
