package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 接口日志对象 interface_log
 * 
 * @author ruoyi
 * @date 2022-03-12
 */
public class InterfaceLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 接口编号 */
    @Excel(name = "接口编号")
    private String code;

    /** 系统模块 */
    @Excel(name = "系统模块")
    private String module;

    /** 接口名称 */
    @Excel(name = "接口名称")
    private String interfaceName;

    /** 接口地址 */
    @Excel(name = "接口地址")
    private String interfaceUrl;

    /** 方法名称 */
    @Excel(name = "方法名称")
    private String method;

    /** 接口方式（1 输入，2输出） */
    @Excel(name = "接口方式", readConverterExp = "1=,输=入，2输出")
    private String interfaceType;

    /** 触发方式（1实时、2定时） */
    @Excel(name = "触发方式", readConverterExp = "1=实时、2定时")
    private String triggerType;

    /** 入参 */
    @Excel(name = "入参")
    private String inParams;

    /** 结果 */
    @Excel(name = "结果")
    private String result;

    /** 调用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "调用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date invokeTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setModule(String module) 
    {
        this.module = module;
    }

    public String getModule() 
    {
        return module;
    }
    public void setInterfaceName(String interfaceName) 
    {
        this.interfaceName = interfaceName;
    }

    public String getInterfaceName() 
    {
        return interfaceName;
    }
    public void setInterfaceUrl(String interfaceUrl) 
    {
        this.interfaceUrl = interfaceUrl;
    }

    public String getInterfaceUrl() 
    {
        return interfaceUrl;
    }
    public void setMethod(String method) 
    {
        this.method = method;
    }

    public String getMethod() 
    {
        return method;
    }
    public void setInterfaceType(String interfaceType) 
    {
        this.interfaceType = interfaceType;
    }

    public String getInterfaceType() 
    {
        return interfaceType;
    }
    public void setTriggerType(String triggerType) 
    {
        this.triggerType = triggerType;
    }

    public String getTriggerType() 
    {
        return triggerType;
    }
    public void setInParams(String inParams) 
    {
        this.inParams = inParams;
    }

    public String getInParams() 
    {
        return inParams;
    }
    public void setResult(String result) 
    {
        this.result = result;
    }

    public String getResult() 
    {
        return result;
    }
    public void setInvokeTime(Date invokeTime) 
    {
        this.invokeTime = invokeTime;
    }

    public Date getInvokeTime() 
    {
        return invokeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("module", getModule())
            .append("interfaceName", getInterfaceName())
            .append("interfaceUrl", getInterfaceUrl())
            .append("method", getMethod())
            .append("interfaceType", getInterfaceType())
            .append("triggerType", getTriggerType())
            .append("inParams", getInParams())
            .append("result", getResult())
            .append("invokeTime", getInvokeTime())
            .append("remark", getRemark())
            .toString();
    }
}
