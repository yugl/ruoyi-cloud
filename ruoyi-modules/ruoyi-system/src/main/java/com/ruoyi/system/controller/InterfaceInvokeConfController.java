package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.InterfaceInvokeConf;
import com.ruoyi.system.service.IInterfaceInvokeConfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 接口配置Controller
 *
 * @author ruoyi
 * @date 2022-03-12
 */
@Api(value = "接口配置控制器", tags = {"接口配置管理"})
@RestController
@RequestMapping("/interfaceInvoke")
public class InterfaceInvokeConfController extends BaseController {
    @Autowired
    private IInterfaceInvokeConfService interfaceInvokeConfService;

    /**
     * 查询接口配置列表
     */
    @ApiOperation("查询接口配置列表")
    @RequiresPermissions("system:interfaceInvoke:list")
    @GetMapping("/list")
    public TableDataInfo list(InterfaceInvokeConf interfaceInvokeConf) {
        startPage();
        List<InterfaceInvokeConf> list = interfaceInvokeConfService.selectInterfaceInvokeConfList(interfaceInvokeConf);
        return getDataTable(list);
    }

    /**
     * 导出接口配置列表
     */
    @ApiOperation("导出接口配置列表")
    @RequiresPermissions("system:interfaceInvoke:export")
    @Log(title = "接口配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InterfaceInvokeConf interfaceInvokeConf) {
        List<InterfaceInvokeConf> list = interfaceInvokeConfService.selectInterfaceInvokeConfList(interfaceInvokeConf);
        ExcelUtil<InterfaceInvokeConf> util = new ExcelUtil<InterfaceInvokeConf>(InterfaceInvokeConf.class);
        util.exportExcel(response, list, "接口配置数据");
    }

    /**
     * 获取接口配置详细信息
     */
    @ApiOperation("获取接口配置详细信息")
    @RequiresPermissions("system:interfaceInvoke:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(interfaceInvokeConfService.selectInterfaceInvokeConfById(id));
    }

    /**
     * 新增接口配置
     */
    @ApiOperation("新增接口配置")
    @RequiresPermissions("system:interfaceInvoke:add")
    @Log(title = "接口配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InterfaceInvokeConf interfaceInvokeConf) {
        return toAjax(interfaceInvokeConfService.insertInterfaceInvokeConf(interfaceInvokeConf));
    }

    /**
     * 修改接口配置
     */
    @ApiOperation("修改接口配置")
    @RequiresPermissions("system:interfaceInvoke:edit")
    @Log(title = "接口配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InterfaceInvokeConf interfaceInvokeConf) {
        return toAjax(interfaceInvokeConfService.updateInterfaceInvokeConf(interfaceInvokeConf));
    }

    /**
     * 删除接口配置
     */
    @ApiOperation("删除接口配置")
    @RequiresPermissions("system:interfaceInvoke:remove")
    @Log(title = "接口配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(interfaceInvokeConfService.deleteInterfaceInvokeConfByIds(ids));
    }
}
