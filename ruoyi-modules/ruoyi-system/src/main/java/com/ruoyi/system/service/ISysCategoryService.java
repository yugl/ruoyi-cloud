package com.ruoyi.system.service;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.vo.TreeSelect;

import java.util.List;

/**
 * 行业分类Service接口
 *
 * @author 于观礼
 * @date 2022-03-14
 */
public interface ISysCategoryService {
    /**
     * 查询行业分类
     *
     * @param id 行业分类主键
     * @return 行业分类
     */
    public SysCategory selectSysCategoryById(Long id);

    /**
     * 查询行业分类列表
     *
     * @param sysCategory 行业分类
     * @return 行业分类集合
     */
    public List<SysCategory> selectSysCategoryList(SysCategory sysCategory);

    /**
     * 新增行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    public int insertSysCategory(SysCategory sysCategory);

    /**
     * 修改行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    public int updateSysCategory(SysCategory sysCategory);

    /**
     * 批量删除行业分类
     *
     * @param ids 需要删除的行业分类主键集合
     * @return 结果
     */
    public int deleteSysCategoryByIds(Long[] ids);

    /**
     * 删除行业分类信息
     *
     * @param id 行业分类主键
     * @return 结果
     */
    public int deleteSysCategoryById(Long id);

    /**
     * 获取菜单下拉树列表
     */
    List<TreeSelect> buildMenuTreeSelect(List<SysCategory> list);

    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public String checkNameUnique(SysCategory sysCategory);

    /**
     * 是否存在菜单子节点
     *
     * @param id 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean hasChildById(Long id);

    AjaxResult findNameSelectList();
}
