package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 行业分类Mapper接口
 *
 * @author 于观礼
 * @date 2022-03-14
 */
public interface SysCategoryMapper {
    /**
     * 查询行业分类
     *
     * @param id 行业分类主键
     * @return 行业分类
     */
    public SysCategory selectSysCategoryById(Long id);

    /**
     * 查询行业分类列表
     *
     * @param sysCategory 行业分类
     * @return 行业分类集合
     */
    public List<SysCategory> selectSysCategoryList(SysCategory sysCategory);

    /**
     * 新增行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    public int insertSysCategory(SysCategory sysCategory);

    /**
     * 修改行业分类
     *
     * @param sysCategory 行业分类
     * @return 结果
     */
    public int updateSysCategory(SysCategory sysCategory);

    /**
     * 删除行业分类
     *
     * @param id 行业分类主键
     * @return 结果
     */
    public int deleteSysCategoryById(Long id);

    /**
     * 批量删除行业分类
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysCategoryByIds(Long[] ids);

    /**
     * 校验菜单名称是否唯一
     *
     * @param name     菜单名称
     * @param parentId 父菜单ID
     * @return 结果
     */
    public SysCategory checkNameUnique(@Param("name") String name, @Param("parentId") Long parentId);

    /**
     * 是否存在菜单子节点
     *
     * @param id 菜单ID
     * @return 结果
     */
    public int hasChildById(Long id);
}
