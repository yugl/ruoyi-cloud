package com.ruoyi.system.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.InterfaceLog;
import com.ruoyi.system.service.IInterfaceLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 接口日志Controller
 *
 * @author ruoyi
 * @date 2022-03-12
 */
@Api(value = "接口日志控制器", tags = {"接口日志管理"})
@RestController
@RequestMapping("/interfaceLog")
public class InterfaceLogController extends BaseController {
    @Autowired
    private IInterfaceLogService interfaceLogService;

    /**
     * 查询接口日志列表
     */
    @ApiOperation("查询接口日志列表")
    @RequiresPermissions("system:interfaceLog:list")
    @GetMapping("/list")
    public TableDataInfo list(InterfaceLog interfaceLog) {
        startPage();
        List<InterfaceLog> list = interfaceLogService.selectInterfaceLogList(interfaceLog);
        return getDataTable(list);
    }

    /**
     * 导出接口日志列表
     */
    @ApiOperation("导出接口日志列表")
    @RequiresPermissions("system:interfaceLog:export")
    @Log(title = "接口日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InterfaceLog interfaceLog) {
        List<InterfaceLog> list = interfaceLogService.selectInterfaceLogList(interfaceLog);
        ExcelUtil<InterfaceLog> util = new ExcelUtil<InterfaceLog>(InterfaceLog.class);
        util.exportExcel(response, list, "接口日志数据");
    }

    /**
     * 获取接口日志详细信息
     */
    @ApiOperation("获取接口日志详细信息")
    @RequiresPermissions("system:interfaceLog:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam("主键")
                              @NotNull(message = "主键不能为空")
                              @PathVariable("id") Long id) {
        return AjaxResult.success(interfaceLogService.selectInterfaceLogById(id));
    }

    /**
     * 新增接口日志
     */
    @ApiOperation("新增接口日志")
    @RequiresPermissions("system:interfaceLog:add")
    @Log(title = "接口日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InterfaceLog interfaceLog) {
        return toAjax(interfaceLogService.insertInterfaceLog(interfaceLog));
    }

    /**
     * 修改接口日志
     */
    @ApiOperation("修改接口日志")
    @RequiresPermissions("system:interfaceLog:edit")
    @Log(title = "接口日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InterfaceLog interfaceLog) {
        return toAjax(interfaceLogService.updateInterfaceLog(interfaceLog));
    }

    /**
     * 删除接口日志
     */
    @ApiOperation("删除接口日志")
    @RequiresPermissions("system:interfaceLog:remove")
    @Log(title = "接口日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(interfaceLogService.deleteInterfaceLogByIds(ids));
    }
}
