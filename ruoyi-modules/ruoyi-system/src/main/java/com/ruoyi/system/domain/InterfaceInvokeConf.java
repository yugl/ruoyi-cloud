package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 接口配置对象 interface_invoke_conf
 * 
 * @author ruoyi
 * @date 2022-03-12
 */
public class InterfaceInvokeConf extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 接口编号 */
    @Excel(name = "接口编号")
    private String code;

    /** 系统模块 */
    @Excel(name = "系统模块")
    private String module;

    /** 接口名称 */
    @Excel(name = "接口名称")
    private String name;

    /** 触发方式（1实时、2定时） */
    @Excel(name = "触发方式", readConverterExp = "1=实时、2定时")
    private String triggerType;

    /** 接口方式（1 输入，2输出） */
    @Excel(name = "接口方式", readConverterExp = "1=,输=入，2输出")
    private String interfaceType;

    /** 接口类型（ws/rest） */
    @Excel(name = "接口类型", readConverterExp = "w=s/rest")
    private String type;

    /** 目标URL */
    private String url;

    /** 认证用户名 */
    @Excel(name = "认证用户名")
    private String userName;

    /** 认证密码 */
    @Excel(name = "认证密码")
    private String password;

    /** 方法名 */
    @Excel(name = "方法名")
    private String method;

    /** 状态（0有效 1无效） */
    private String status;

    /** 创建时间 */
    private Date createdTime;

    /** 修改时间 */
    private Date updatedTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setModule(String module) 
    {
        this.module = module;
    }

    public String getModule() 
    {
        return module;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTriggerType(String triggerType) 
    {
        this.triggerType = triggerType;
    }

    public String getTriggerType() 
    {
        return triggerType;
    }
    public void setInterfaceType(String interfaceType) 
    {
        this.interfaceType = interfaceType;
    }

    public String getInterfaceType() 
    {
        return interfaceType;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setMethod(String method) 
    {
        this.method = method;
    }

    public String getMethod() 
    {
        return method;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("module", getModule())
            .append("name", getName())
            .append("triggerType", getTriggerType())
            .append("interfaceType", getInterfaceType())
            .append("type", getType())
            .append("remark", getRemark())
            .append("url", getUrl())
            .append("userName", getUserName())
            .append("password", getPassword())
            .append("method", getMethod())
            .append("status", getStatus())
            .append("createdTime", getCreatedTime())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
